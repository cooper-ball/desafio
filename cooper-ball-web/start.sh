#!/bin/bash

## Restore original default config if no config has been provided
#if [[ ! "$(ls -A /etc/nginx/conf.d)" ]]; then
#    cp -a /etc/nginx/.conf.d.orig/. /etc/nginx/conf.d 2>/dev/null
#fi
#
## Replace variables $ENV{<environment varname>}
#function ReplaceEnvironmentVariable() {
#    grep -rl "\$ENV{\"$1\"}" $3|xargs -r \
#        sed -i "s|\\\$ENV{\"$1\"}|$2|g"
#}
#
#if [ -n "$DEBUG" ]; then
#    echo "Environment variables:"
#    env
#    echo "..."
#fi
#
## Replace all variables
#for _curVar in `env | awk -F = '{print $1}'`;do
#    # awk has split them by the equals sign
#    # Pass the name and value to our function
#    ReplaceEnvironmentVariable ${_curVar} ${!_curVar} /etc/nginx/conf.d/*
#done

set -e

BUILD_PATH=" /usr/share/nginx/html"
if [ -n "$1" ]; then
    BUILD_PATH=$1
fi
echo "Replacing environment variables"
replace() {
    find $BUILD_PATH/ -name '*.js' | xargs sed -i "s|{$1}|$2|g"
    find $BUILD_PATH/ -name '*.html' | xargs sed -i "s|{$1}|$2|g"
}
# replace all the environments
replace REACT_APP_TITLE ${REACT_APP_TITLE}
replace REACT_APP_URL_API ${REACT_APP_URL_API}

# Run nginx
#exec /usr/sbin/nginx
exec nginx -g 'daemon off;'