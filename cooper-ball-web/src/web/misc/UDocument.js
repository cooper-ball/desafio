import ArrayLst from '../../commom/utils/array/ArrayLst';

export default class UDocument {
	static querySelectorAll(s) {
		let list = document.querySelectorAll(s);
		let elems = new ArrayLst();
		list.forEach(o => elems.push(o));
		return elems;
	}
}
