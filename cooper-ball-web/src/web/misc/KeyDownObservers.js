import ArrayLst from '../../commom/utils/array/ArrayLst';
import ClassSimpleName from '../../commom/utils/classe/ClassSimpleName';
import EventKey from '../../app/campos/support/EventKey';
import StringCompare from '../../commom/utils/string/StringCompare';

document.addEventListener("keydown", event => KeyDownObservers.exec(event));
export default class KeyDownObservers {

	static list = new ArrayLst();

	static add(observer) {
		if (!KeyDownObservers.list.contains(observer)) {
			KeyDownObservers.list.add(observer);
		}
	}

	static removeByClass(o) {
		let className = ClassSimpleName.exec(o);
		KeyDownObservers.list.filter(item => StringCompare.eq(ClassSimpleName.exec(item), className)).forEach(item => KeyDownObservers.remove(item));
	}

	static remove(observer) {
		KeyDownObservers.list.removeObject(observer);
	}

	static exec(event) {
		let e = new EventKey(event);
		KeyDownObservers.list.forEach(o => o.onKeyDown(e));
	}

}
