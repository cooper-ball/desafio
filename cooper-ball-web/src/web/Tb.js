import React from 'react';
import CommonStyles from '../app/misc/styles/CommonStyles';
import Null from '../commom/utils/object/Null';
import StringEmpty from '../commom/utils/string/StringEmpty';
import SuperComponent from '../app/misc/components/SuperComponent';

export default class Tb extends SuperComponent {

	render0() {

		let cssName = "tb";
		if (this.props.hover) {
			cssName += " hover";
		}

		if (!StringEmpty.is(this.props.className)) {
			cssName += " " + this.props.className;
		}

		return (
			<table onClick={this.props.onPress} style={CommonStyles.W100P.join(this.props.style).get()} className={cssName} id={this.id}>
				{this.getHead()}
				<tbody>
					{this.putParent(this.props.children)}
				</tbody>
			</table>
		);
	}

	getHead() {

		if (Null.is(this.props.head)) {
			return null;
		} else {
			return (
				<thead>
					{this.props.head}
				</thead>
			);
		}

	}
}

Tb.defaultProps = {
	...SuperComponent.defaultProps,
	hover: false
}
