import React from 'react';
import Null from '../commom/utils/object/Null';
import SuperComponent from '../app/misc/components/SuperComponent';

export default class Div extends SuperComponent {
	clicado = false;
	yClicado = 0;

	render0() {
		return (
			<div style={this.newStyle().join(this.props.style).get()}

				onTouchStart={e => {

					this.clicado = true;
					this.idTouchStart = this.getIdEvent(e);
					let touch = e.touches.get(0);
					this.yClicado = touch.clientY;

					if (!Null.is(this.props.onTouchStart)) {
						this.props.onTouchStart(e);
					}

				}}
				onTouchEnd={e => {

					if (this.clicado) {
						let touch = e.changedTouches.get(0);
						let movimento = this.yClicado - touch.clientY;

						if (movimento < 0) {
							movimento = -movimento;
						}

						let comp = e.target;
						if (comp.clientHeight > movimento) {
							this.clicar();
						}

						this.clicado = false;
					}

					if (!Null.is(this.props.onTouchEnd)) {
						this.props.onTouchEnd(e);
					}

				}}

				onTouchMove={this.props.onTouchMove}

				onTouchCancel={e => {
					this.clicado = false;
				}}
				onClick={e => this.clicar()}
				className={this.props.className}
				id={this.stringId}>
				{this.putParent(this.props.children)}
			</div>
		);
	}

	clicar() {

		if (this.clicado) {
			if (!Null.is(this.props.onPress)) {
				this.props.onPress();
			}
			this.clicado = false;
		}

	}

	getIdEvent(e) {
		if (Null.is(e) || Null.is(e.target)) {
			return null;
		} else {
			let comp = e.target;
			return comp.id;
		}
	}
}

Div.defaultProps = SuperComponent.defaultProps;
