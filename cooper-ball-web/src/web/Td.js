import React from 'react';
import SuperComponent from '../app/misc/components/SuperComponent';

export default class Td extends SuperComponent {

	render0() {
		return (
			<td colSpan={this.props.colSpan} rowSpan={this.props.rowSpan} style={this.newStyle().join(this.props.style).get()} onClick={this.props.onClick}>
				{this.putParent(this.props.children)}
			</td>
		);
	}
}

Td.defaultProps = {
	...SuperComponent.defaultProps,
	colSpan: 1,
	rowSpan: 1
}
