import React from 'react';
import Null from '../commom/utils/object/Null';
import SuperComponent from '../app/misc/components/SuperComponent';

export default class Tr extends SuperComponent {

	render0() {
		let st = Null.is(this.props.style) ? null : this.props.style.get();
		return (
			<tr style={st}>
				{this.putParent(this.props.children)}
			</tr>
		);
	}

}

Tr.defaultProps = SuperComponent.defaultProps;
