import React from 'react';
import ObjectFit from '../app/misc/consts/enums/ObjectFit';
import SuperComponent from '../app/misc/components/SuperComponent';

export default class Img extends SuperComponent {

	render0() {
		let st = this.newStyle().objectFit(ObjectFit.contain).join(this.props.style).get();
		return (
			<img alt={"-"} src={this.props.src} style={st}>
				{this.putParent(this.props.children)}
			</img>
		);
	}
}

Img.defaultProps = SuperComponent.defaultProps;
