import React from 'react';
import './my-antd.css';
import './index.css';
import 'antd/dist/antd.css';
import * as ServiceWorker from './serviceWorker';
import App from './App';
import ReactContext from './react/manager/ReactContext';
import ReactDOM from 'react-dom';
import StartPrototypes from './app/misc/utils/StartPrototypes';
import {BrowserRouter} from 'react-router-dom';

StartPrototypes.exec();
ReactContext.start();

setTimeout(() => {
	ReactDOM.render(
		<BrowserRouter>
			<App/>
		</BrowserRouter>
		, document.getElementById("root")
	);
});

ServiceWorker.unregister();
