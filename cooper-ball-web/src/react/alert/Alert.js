import AlertDefault from './AlertDefault';

export default class Alert {

	instance = new AlertDefault();

	error(s) {
		this.instance.error(s);
	}

	info(s) {
		this.instance.info(s);
	}

	success(s) {
		this.instance.success(s);
	}

	warn(s) {
		this.instance.warn(s);
	}

}
