import React from 'react';
import SuperComponent from '../app/misc/components/SuperComponent';
import {Fragment} from 'react';

export default class Frag extends SuperComponent {

	render0() {
		return (
			<Fragment>
				{this.putParent(this.props.children)}
			</Fragment>
		);
	}

}

Frag.defaultProps = SuperComponent.defaultProps;
