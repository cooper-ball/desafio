import ArrayEmpty from '../commom/utils/array/ArrayEmpty';

export default class Foreach {

	static get(array,func) {
		if (ArrayEmpty.is(array)) {
			return null;
		}

		if (Array.isArray(array)) {
			throw new Error("Passando o jeito velho ainda");
		}

		return Foreach.cast(array.map(func).getArray());
	}

	static geti(array, func) {
		if (ArrayEmpty.is(array)) {
			return null;
		}
		return Foreach.cast(array.mapi(func).getArray());
	}

	static cast(nodes) {
		return nodes;
	}

}
