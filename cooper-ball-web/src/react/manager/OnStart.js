import Console from '../../app/misc/utils/Console';
import StringCompare from '../../commom/utils/string/StringCompare';

export default class OnStart {

	/*O controle de sessao eh injetado para garantir sua usabilidade*/

	/*O controle de sessao eh injetado para garantir sua usabilidade*/

	exec() {

		this.torcedorPermissoes.setIncluirTelefones(true);
		this.torcedorPermissoes.setExcluirTelefones(true);

		Console.observers.add(o => {
			if (StringCompare.eq(o.type, "error")) {
				this.alert.error(o.value);
			}
		});

	}

}
