import CampoConsulta from '../../fc/components/consulta/CampoConsulta';
import CampoConsultaWebRenders from '../../fc/components/consulta/CampoConsultaWebRenders';

export default class Configure {

	init() {
		CampoConsulta.renders = new CampoConsultaWebRenders();
	}

}
