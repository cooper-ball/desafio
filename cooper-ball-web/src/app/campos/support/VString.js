import Binding from './Binding';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import StringReplace from '../../../commom/utils/string/StringReplace';
import StringTrim from '../../../commom/utils/string/StringTrim';

export default class VString extends Binding {

	maxLength = 50;
	numeric = false;
	aplicarLtrim = true;

	getMaxLength() {
		return this.maxLength;
	}

	setMaxLength(value) {
		this.maxLength = value;
		return this;
	}

	beforeSet(s) {
		if (StringEmpty.is(s)) {
			s = "";
		}
		if (this.numeric) {
			s = StringExtraiNumeros.exec(s);
		} else {
			if (this.aplicarLtrim) {
				s = StringTrim.left(s);
			}
			s = StringReplace.exec(s, "  ", " ");
			s = StringReplace.exec(s, ",,", ",");
			s = StringReplace.exec(s, "..", ".");
		}
		s = this.formatParcial(s);
		let max = this.getMaxLength();
		let len = StringLength.get(s);
		if (len > max) {
			if (len > max+1) {
				console.warn("O campo " + this.getLabel() + " suporta no maximo " + this.maxLength + " caracteres. O texto passado extrapolou e foi truncado");
			}
			s = StringLength.max(s, max);
		}
		return s;
	}

	setAplicarLtrim(value) {
		this.aplicarLtrim = value;
		return this;
	}

	formatParcial(s) {
		return s;
	}

	isEmpty() {
		return StringEmpty.is(this.get());
	}

	equals(s) {
		return StringCompare.eq(this.get(), s);
	}

	castFromString(s) {
		return s;
	}

	withPlaceHolder(value) {
		this.setPlaceHolder(value);
		return this;
	}

	eq(o) {
		return StringCompare.eq(this.get(), o);
	}

	isNumeric() {
		return this.numeric;
	}

	setSomenteNumeros(value) {
		this.numeric = value;
		return this;
	}

	copy() {
		let o = new VString().setMaxLength(this.maxLength).setLabel(this.getLabel());
		o.setAplicarLtrim(this.aplicarLtrim);
		o.setSomenteNumeros(this.numeric);
		o.set(this.get());
		return o;
	}

	addLeft(s) {
		this.set(s + this.get());
		return this;
	}

	add(s) {
		this.set(this.get() + s);
		return this;
	}

}
