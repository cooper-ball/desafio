import UEmail from '../../misc/utils/UEmail';
import VString from './VString';

export default class VEmailConfirmacao extends VString {

	constructor(email) {
		super();
		this.email = email;
		this.setObserversDisabled(true);
		this.setNotNull(email.notNull());
		this.setObserversDisabled(false);
	}

	afterConstruct() {
		this.setMaxLength(60);
	}

	getInvalidMessagePrivate() {
		return this.eq(this.email.get()) ? null : "Não corresponde";
	}

	formatParcial(s) {
		return UEmail.formatParcial(s);
	}

}
