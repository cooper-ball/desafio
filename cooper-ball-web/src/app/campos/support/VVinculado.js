import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import VBoolean from './VBoolean';

export default class VVinculado extends VBoolean {

	setText(s) {
		if (StringCompare.eq(s, this.text)) {
			this.set(false);
		} else {
			this.text = s;
			if (!this.set(false)) {
				this.forceNotifyObservers();/*pois mudou o texto*/
			}
		}
	}

	clear() {
		if (StringEmpty.is(this.text)) {
			return false;
		}
		this.text = "";
		if (!Null.is(this.onClear)) {
			this.onClear();
		}
		this.clear2();
		this.forceNotifyObservers();
		return true;
	}

	clear2() {}

	confirm() {
		if (!Null.is(this.onConfirm)) {
			this.onConfirm();
		}
		this.set(false);
	}

	setCast(value) {
		if (Null.is(value)) {
			this.clear();
			return this.set(false);
		} else {
			return this.set(true);
		}
	}

	asString() {
		return this.text;
	}

	setOnConfirm(value) {
		this.onConfirm = value;
		return this;
	}

	setOnClear(value) {
		this.onClear = value;
		return this;
	}

	isEmpty() {
		return StringEmpty.is(this.asString());
	}

	setTrue(value) {
		this.text = value;
		this.set(true);
	}

}
