import Binding from './Binding';
import Null from '../../../commom/utils/object/Null';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import UBoolean from '../../misc/utils/UBoolean';
import VBooleanBotao from './VBooleanBotao';

export default class VBoolean extends Binding {

	labelSim = "Sim";
	labelNao = "Não";

	setLabels(labelSimParam, labelNaoParam) {
		this.labelSim = labelSimParam;
		this.labelNao = labelNaoParam;
		return this;
	}

	isFalse() {
		return UBoolean.isFalse(this.get());
	}

	isTrue() {
		return UBoolean.isTrue(this.get());
	}

	change() {
		this.set(!this.isTrue());
	}

	getBotaoSim() {
		if (Null.is(this.botaoSim)) {
			this.botaoSim = new VBooleanBotao(this, true, this.labelSim);
			this.botaoSim.focusController = this.focusController;
		}
		return this.botaoSim;
	}

	getBotaoNao() {
		if (Null.is(this.botaoNao)) {
			this.botaoNao = new VBooleanBotao(this, false, this.labelNao);
			this.botaoNao.focusController = this.focusController;
		}
		return this.botaoNao;
	}

	castFromString(s) {
		if (StringEmpty.is(s)) return null;
		if (UBoolean.isTrue(s)) return true;
		if (UBoolean.isFalse(s)) return false;
		return null;
	}

}
