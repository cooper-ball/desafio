import Binding from './Binding';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import StringParse from '../../../commom/utils/string/StringParse';
import UInteger from '../../misc/utils/UInteger';

export default class VInteger extends Binding {

	minimo = 0;
	maximo = 999999;
	formatar = true;

	getInvalidMessagePrivate() {
		if (UInteger.between(this.get(), this.min(), this.max())) {
			return this.getInvalidMessage2();
		} else {
			return "O valor deve estar entre " + this.min() + " e " + this.max();
		}
	}

	beforeSet(x) {
		let s = StringParse.get(x);
		if (StringEmpty.is(s)) {
			return null;
		}
		let negativo = s.startsWith("-");
		s = StringExtraiNumeros.exec(s);
		if (StringEmpty.is(s)) {
			return null;
		}
		let result = UInteger.toInt(StringLength.max(s, StringLength.get(StringParse.get(this.max()))));
		if (negativo) {
			result = -result;
		}

		if (result < this.min()) {
			return this.min();
		} else if (result > this.max()) {
			return this.max();
		} else {
			return result;
		}

	}

	getInvalidMessage2() {
		return null;
	}

	min() {
		return this.minimo;
	}

	max() {
		return this.maximo;
	}

	castFromString(s) {
		return UInteger.toInt(s);
	}

	setMinimo(value) {
		this.minimo = value;
		return this;
	}

	setMaximo(value) {
		this.maximo = value;
		return this;
	}

	asString() {
		if (this.isEmpty()) {
			return "";
		} else if (this.formatar) {
			return UInteger.format(this.get());
		} else {
			return ""+this.get();
		}
	}

	eq(o) {
		return IntegerCompare.eq(o, this.get());
	}

	intValue() {
		if (this.isEmpty()) {
			return 0;
		} else {
			return this.get();
		}
	}

	menorQue(b) {
		return this.intValue() < b.intValue();
	}

	maiorQue(b) {
		return this.intValue() > b.intValue();
	}

	toJSON() {
		if (this.isEmpty()) {
			return null;
		} else {
			return "" + this.intValue();
		}
	}

	inc() {
		this.set(this.intValue()+1);
	}

	dec() {
		this.set(this.intValue()-1);
	}

	setFormatar(value) {
		if (this.formatar !== value) {
			this.formatar = value;
			this.forceNotifyObservers();
		}
	}

}
