import ArrayEmpty from '../../../commom/utils/array/ArrayEmpty';
import Null from '../../../commom/utils/object/Null';
import SuperComponent from '../../misc/components/SuperComponent';

export default class BindingListener extends SuperComponent {

	render0() {
		return this.props.func();
	}

	didMount() {
		if (!ArrayEmpty.is(this.props.itens)) {
			this.props.itens.forEach(o => this.observar(o));
		}
		if (!Null.is(this.props.bind)) {
			this.observar(this.props.bind);
		}
	}

}

BindingListener.defaultProps = SuperComponent.defaultProps;
