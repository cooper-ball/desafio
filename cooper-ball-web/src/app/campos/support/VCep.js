import IntegerParse from '../../../commom/utils/integer/IntegerParse';
import Null from '../../../commom/utils/object/Null';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import UCep from '../../misc/utils/UCep';
import VString from './VString';

export default class VCep extends VString {

	encontradoNosCorreios = false;
	buscando = false;
	uf = new VString().setMaxLength(100).setLabel("UF").disableObservers().setNotNull(false).setDisabled(true).setPlaceHolder("Informe um CEP válido").enableObservers();
	bairro = new VString().setMaxLength(100).setLabel("Bairro").disableObservers().setNotNull(false).setDisabled(true).setPlaceHolder("Informe um CEP válido").enableObservers();
	localidade = new VString().setMaxLength(100).setLabel("Localidade").disableObservers().setNotNull(false).setDisabled(true).setPlaceHolder("Informe um CEP válido").enableObservers();
	bairroLocalidadeUf = new VString().setMaxLength(150).setLabel("Bairro - Localidade / UF").disableObservers().setDisabled(true).setPlaceHolder("Informe um CEP válido").enableObservers();
	logradouro = new VString().setMaxLength(100).setLabel("Logradouro").disableObservers().setNotNull(false).setDisabled(true).setPlaceHolder("Informe um CEP válido").enableObservers();
	static service;

	constructor() {
		super();
		this.setMaxLength(10);
	}

	getInvalidMessagePrivate() {
		if (this.buscando) {
			return null;
		}
		if (!UCep.isValid(this.get())) {
			return "CEP Inválido!";
		}
		if (!this.encontradoNosCorreios) {
			return "Não encontrado nos correios";
		}
		return null;
	}

	formatParcial(s) {
		return UCep.formatParcial(s);
	}

	afterSet(old, value) {

		this.encontradoNosCorreios = false;
		this.buscando = false;
		this.setShowInvalidMessage(true);
		let s = this.get();
		this.bairro.set(null);
		this.uf.set(null);
		this.logradouro.set(null);
		this.localidade.set(null);
		this.bairroLocalidadeUf.set(null);

		if (UCep.isValid(s)) {

			let params = {method: "GET"};
			let uri = "https://viacep.com.br/ws/"+StringExtraiNumeros.exec(s)+"/json/";

			fetch(uri, params).then(res => res.json()).then(res => {

				let o = res;

				if (Null.is(o) || o.erro) {
					this.encontradoNosCorreios = false;
					this.setShowInvalidMessage(true);
					this.bairro.clear();
					this.uf.clear();
					this.logradouro.clear();
					this.localidade.clear();
					this.bairroLocalidadeUf.clear();
				} else {
					this.encontradoNosCorreios = true;
					this.setShowInvalidMessage(false);
					this.bairro.set(o.bairro);
					this.uf.set(o.uf);
					this.logradouro.set(o.logradouro);
					this.localidade.set(o.localidade);
					this.bairroLocalidadeUf.set(o.bairro + " - " + o.localidade + " / " + o.uf);
				}

				this.buscando = false;
				return res;

			});

		}
	}

	isNumeric() {
		return true;
	}

	getId() {
		if (this.isEmpty() || !this.isValid()) {
			return null;
		} else {
			return IntegerParse.toInt(StringExtraiNumeros.exec(this.get()));
		}
	}

}
