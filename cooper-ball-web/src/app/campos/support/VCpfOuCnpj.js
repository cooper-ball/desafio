import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import UCnpj from '../../../commom/utils/cp/UCnpj';
import UCpf from '../../../commom/utils/cp/UCpf';
import VString from './VString';

export default class VCpfOuCnpj extends VString {

	afterConstruct() {
		this.setMaxLength(21);
		this.setLabel("CPF ou CNPJ");
		this.setPlaceHolder("Entre com o CPF ou CNPJ");
	}

	getInvalidMessagePrivate() {
		if (UCpf.isValid(this.get()) || UCnpj.isValid(this.get())) {
			return null;
		} else {
			return "CPF ou CNPJ Inválido";
		}
	}

	formatParcial(s) {
		s = StringExtraiNumeros.exec(s);
		if (StringLength.get(s) < 12) {
			return UCpf.formatParcial(s);
		} else {
			return UCnpj.formatParcial(s);
		}
	}

	isNumeric() {
		return true;
	}

}
