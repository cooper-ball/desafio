import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import UCnpj from '../../../commom/utils/cp/UCnpj';
import VString from './VString';

export default class VCnpj extends VString {

	afterConstruct() {
		this.setLabel("CNPJ");
		this.setMaxLength(18);
	}

	getInvalidMessagePrivate() {
		let s = this.get();
		if (UCnpj.isValid(s)) {
			return null;
		} else if (StringLength.is(s, 18)) {
			return "Inválido";
		} else {
			return StringLength.get(StringExtraiNumeros.exec(s)) + " de 14 números";

		}
	}

	formatParcial(s) {
		return UCnpj.formatParcial(s);
	}

	isNumeric() {
		return true;
	}

}
