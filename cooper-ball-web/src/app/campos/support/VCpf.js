import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import UCpf from '../../../commom/utils/cp/UCpf';
import VString from './VString';

export default class VCpf extends VString {

	afterConstruct() {
		this.setMaxLength(14);
		this.setLabel("CPF");
	}

	getInvalidMessagePrivate() {
		let s = this.get();
		if (UCpf.isValid(s)) {
			return null;
		} else if (StringLength.is(s, 14)) {
			return "Inválido";
		} else {
			return StringLength.get(StringExtraiNumeros.exec(s)) + " de 11 números";

		}
	}

	formatParcial(s) {
		return UCpf.formatParcial(s);
	}

	isNumeric() {
		return true;
	}

}
