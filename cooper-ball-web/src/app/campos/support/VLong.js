import SeparaMilhares from '../../../commom/utils/comum/SeparaMilhares';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import VString from './VString';

export default class VLong extends VString {

	max = "9223372036854775807";

	afterConstruct() {
		this.setSomenteNumeros(true);
		this.setMax(this.max);/*para validar*/
	}

	setMax(value) {

		if (StringEmpty.is(value)) {
			/*este é o valor maximo de um long no java*/
			this.max = "9223372036854775807";
			this.setMaxLength(19);
			return this;
		}

		let s = StringExtraiNumeros.exec(value);

		if (!StringCompare.eq(value, s)) {
			throw new Error("Valor inválido: " + value);
		}

		if (StringLength.get(value) > 19) {
			throw new Error("max > 19 chars");
		}

		this.max = value;
		this.setMaxLength(StringLength.get(value));

		return this;

	}

	isNumeric() {
		return true;
	}

	getInvalidMessagePrivate() {

		let ml = this.getMaxLength();

		let s = this.get();
		if (StringLength.get(s) < ml) {
			return null;
		} else if (StringLength.get(s) > ml) {
			return "Máximo de "+ml+" números";
		} else if (this.valorMaiorQue(this.max)){
			return "Valor Máximo: " + this.max;
		} else {
			return null;
		}

	}

	valorMaiorQue(b) {
		return StringCompare.compare(this.get(), b) === 1;
	}

	valorMenorQue(b) {
		return StringCompare.compare(this.get(), b) === -1;
	}

	menorQue(b) {
		return this.valorMenorQue(b.get());
	}

	maiorQue(b) {
		return this.valorMaiorQue(b.get());
	}

	withPlaceHolder(value) {
		this.setPlaceHolder(value);
		return this;
	}

	asString() {
		return SeparaMilhares.exec(this.get());
	}

}
