import UEmail from '../../misc/utils/UEmail';
import VEmailConfirmacao from './VEmailConfirmacao';
import VString from './VString';

export default class VEmail extends VString {

	confirmacao = new VEmailConfirmacao(this).setLabel("Confirmar e-mail");

	constructor() {
		super();
		this.addObserver(this.confirmacao);
	}

	afterConstruct() {
		this.setMaxLength(60);
	}

	getInvalidMessagePrivate() {
		if (UEmail.isValid(this.get())) {
			return null;
		} else {
			return "E-mail inválido";
		}
	}

	formatParcial(s) {
		return UEmail.formatParcial(s);
	}

	addObserver(o) {
		return super.addObserver(o) | this.confirmacao.addObserver(o);
	}

	addRenderObserver(o) {
		super.addRenderObserver(o);
		this.confirmacao.addRenderObserver(o);
	}

	addChangeObserver(o) {
		return super.addChangeObserver(o) | this.confirmacao.addChangeObserver(o);
	}

	addFunctionObserver(funcao) {
		super.addFunctionObserver(funcao);
		this.confirmacao.addFunctionObserver(funcao);
	}

}
