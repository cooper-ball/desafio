import ArrayEquals from '../../../commom/utils/array/ArrayEquals';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Binding from './Binding';
import Console from '../../misc/utils/Console';
import Equals from '../../../commom/utils/object/Equals';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import Null from '../../../commom/utils/object/Null';
import StringBox from '../../../commom/utils/string/StringBox';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import UCommons from '../../misc/utils/UCommons';
import UString from '../../misc/utils/UString';
import VString from './VString';

export default class VList extends Binding {

	limparInput = true;

	constructor() {
		super();
		this.input = new VString();
		this.input.setMaxLength(250);
		this.input.addObserver(this);
		this.input.controller = this;
	}

	afterSetLabel(old, value) {
		this.input.setLabel(value);
	}

	setItens(itensParams) {

		if (ArrayEquals.equalsNotification(itensParams, this.getItens())) {
			return this;
		}

		if (Null.is(itensParams)) {
			return this.setItens(new ArrayLst());
		}

		let naoNulos = itensParams.filter(o => !Null.is(o));

		if (ArrayEquals.equalsNotification(naoNulos, this.getItens())) {
			return this;
		}

		this.itens = naoNulos;

		if (!this.itens.isEmpty()) {
			let itensComIdEmBranco = this.itens.filter(o => StringEmpty.is(o.text) && o.id > 0);
			if (itensComIdEmBranco.size() > 0) {
				let box = new StringBox("");
				itensComIdEmBranco.forEach(o => box.add(", " + o.id));
				let s = box.get().substring(1);
				throw new Error("Não se pode adicionar itens com text em branco: " + this.getLabel() + " - ids:" + s);
			}
		}

		this.forceNotifyObservers();
		return this;

	}

	getItens() {
		return this.itens;
	}

	isEmpty() {
		return Null.is(this.get()) || Equals.is(this.get(), 0);
	}

	getSugestoes() {
		let s = this.input.get();
		let list = new ArrayLst();
		if (!Null.is(this.itens)) {
			if (StringEmpty.is(s) || StringCompare.eq(s, this.asString())) {
				this.itens.forEach(obj => list.push({value: obj}));
			} else {
				this.itens.forEach(obj => {
					let highlights = UString.highlights(obj.text, s);
					if (highlights.some(o => o.highlight)) {
						list.push({value: obj, highlights: highlights});
					}
				});
			}
		}
		return list;
	}

	setId(id) {
		return this.set(this.getItens().byId(id, o => o.getId()));
	}

	selectSugestion(o) {
		this.set(o.value);
	}

	beforeSet(value) {
		if (Null.is(value)) {
			return null;
		} else {
			value = this.beforeSet2(value);
			return this.getItens().byId(value.id, o => o.getId());
		}
	}

	beforeSet2(value) {
		return value;
	}

	afterSet(old, value) {
		if (this.isEmpty()) {
			if (this.limparInput) {
				this.input.set(null);
			}
		} else {
			if (!this.getItens().contains(this.get())) {
				Console.log("BindingList", this.getLabel());
				Console.log("BindingList", this.getItens());
				Console.log("BindingList", this.get());
				throw new Error("O Item setado nao consta na lista de itens do campo: " + this.getLabel());
			}
			this.input.set(this.asString());
		}
		this.afterSet2();
	}

	afterSet2() {}

	eq(o) {
		return Equals.is(this.get(), o);
	}

	equals(id) {
		if (this.isEmpty()) {
			return false;
		} else {
			return IntegerCompare.eq(this.getId(), id);
		}
	}

	notify(o) {
		if (!StringCompare.eq(this.input.get(), this.asString())) {
			this.limparInput = UCommons.neq(o, this.input);
			this.set(null);
			this.limparInput = true;
		}
	}

	setUnique(o) {
		this.setItens(ArrayLst.build(o));
		this.set(o);
	}

	castFromString(s) {
		/* TODO Auto-g=65enerated method stub*/
		return null;
	}

	getId() {
		if (this.isEmpty()) {
			return null;
		}
		let o = this.get();
		if (Null.is(o)) {
			return null;
		} else {
			return o.id;
		}
	}

	getText() {
		if (this.isEmpty()) {
			return null;
		}
		let o = this.get();
		if (Null.is(o)) {
			return null;
		} else {
			return o.text;
		}
	}

	selectFirst() {
		this.set(this.getItens().get(0));
	}

	notNullAndIsEmptyInvalidMessage() {
		if (this.input.isEmpty()) {
			return null;
		} else {
			return "Valor inválido!";
		}
	}

	asString() {
		if (this.isEmpty()) {
			return "";
		} else {
			let o = this.get();
			return o.text;
		}
	}

	getSafeId() {
		return this.isEmpty() ? 0 : this.getId();
	}

	maior(value) {
		return this.getSafeId() > value;
	}
	menor(value) {
		return this.getSafeId() < value;
	}
	maiorOuIgual(value) {
		return this.getSafeId() >= value;
	}
	menorOuIgual(value) {
		return this.getSafeId() <= value;
	}

	maiorId(value) {
		return this.maior(VList.safeId(value));
	}
	menorId(value) {
		return this.menor(VList.safeId(value));
	}
	maiorOuIgualId(value) {
		return this.maiorOuIgual(VList.safeId(value));
	}
	menorOuIgualId(value) {
		return this.menorOuIgual(VList.safeId(value));
	}

	static safeId(value) {
		return Null.is(value) || Null.is(value.id) ? 0 : value.id;
	}

}
