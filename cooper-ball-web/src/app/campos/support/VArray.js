import ArrayEmpty from '../../../commom/utils/array/ArrayEmpty';
import ArrayEquals from '../../../commom/utils/array/ArrayEquals';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Binding from './Binding';
import Null from '../../../commom/utils/object/Null';

export default class VArray extends Binding {

	afterConstruct() {
		this.set(new ArrayLst());
	}

	castFromString(s) {
		return null;
	}

	setItens(itensP) {
		if (Null.is(itensP)) {
			if (Null.is(this.itens) || !this.itens.isEmpty()) {
				this.itens =  new ArrayLst();
				this.forceNotifyObservers();
			}
		} else {
			if (!ArrayEquals.equalsNotification(this.itens, itensP)) {
				this.itens = itensP;
				this.forceNotifyObservers();
			}
		}
	}

	getItens() {
		return this.itens;
	}

	beforeSet(value) {
		if (Null.is(value)) {
			return new ArrayLst();
		} else {
			return super.beforeSet(value);
		}
	}

	getItensNaoSelecionados() {

		if (Null.is(this.itens)) {
			return new ArrayLst();
		}

		let list = this.get();

		if (Null.is(list)) {
			return this.itens.copy();
		} else {
			return this.itens.filter(o => !list.contains(o));
		}

	}

	setArray(array) {
		this.set(new ArrayLst(array));
	}

	isEmpty() {
		return ArrayEmpty.is(this.get());
	}

	getArray() {
		return this.get().getArray();
	}

}
