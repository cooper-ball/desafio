import ArrayLst from '../../../commom/utils/array/ArrayLst';
import BindingObserver from './BindingObserver';
import Box from '../../../commom/utils/comum/Box';
import Equals from '../../../commom/utils/object/Equals';
import IconeStatus from '../../misc/consts/enums/IconeStatus';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import IntegerIs from '../../../commom/utils/integer/IntegerIs';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringParse from '../../../commom/utils/string/StringParse';
import StringRight from '../../../commom/utils/string/StringRight';
import UJson from '../../../commom/utils/object/UJson';

export default class Binding extends BindingObserver {

	static notificacoesDesligadas = 0;

	static notificacoesDesligadasInc() {
		Binding.notificacoesDesligadas++;
	}
	static notificacoesDesligadasDec() {
		Binding.notificacoesDesligadas--;
	}

	static enfileirarNotificacoes = false;
	static notificacoesEnfileiradas = new ArrayLst();

	val = new Box();

	label = "";
	placeHolder = "";
	virgin = true;
	enabled = true;
	visible = true;

	observersDisabled = true;
	replaceRenderBody = false;
	renderObservers = new ArrayLst();
	observers = new ArrayLst();
	changeObservers = new ArrayLst();
	functionsObservers = new ArrayLst();
	functionsTObservers = new ArrayLst();
	atributes = new Map();

	deveNotificar = false;

	constructor() {
		super();
		this.val.set(this.getStartValue());
		this.afterConstruct();
		this.observersDisabled = false;
	}

	afterConstruct() {}

	setLabel(value) {
		if (!StringCompare.eq(this.label, value)) {
			let old = this.label;
			this.label = value;
			this.afterSetLabel(old, value);
			this.forceNotifyObservers();
		}
		return this.THIS();
	}

	afterSetLabel(old, value) {}

	disableObservers() {
		this.setObserversDisabled(true);
		return this.THIS();
	}

	enableObservers() {
		this.setObserversDisabled(false);
		return this.THIS();
	}

	THIS() {
		return this;
	}

	forceNotifyObservers() {
		this.deveNotificar = true;
		this.notifyObservers();
	}

	get() {
		return this.val.get();
	}
	getToService() {
		if (this.isValid()) {
			return this.getToServiceImpl();
		} else {
			return null;
		}
	}

	getToServiceImpl() {
		return this.get();
	}

	restart() {
		this.set(this.getStartValue());
		this.setVirgin(true);
		return true;
	}

	setCast(value) {
		return this.set(value);
	}

	set(value) {
		if (!this.ativo()) {
			return false;
		}
		value = this.beforeSet(value);
		if (this.eq(value)) {
			this.afterCancelSet();
			return false;
		} else {
			let changed = this.isChanged();
			this.virgin = false;
			let old = this.val.get();
			this.val.set(value);
			this.afterSet(old, value);
			this.forceNotifyObservers();
			if (!Equals.is(changed, this.isChanged())) {
				this.changeObservers.forEach(o => o.notify(this));
			}
			return true;
		}
	}

	static dispararNotificacoesEnfieiradas() {
		while (!Binding.notificacoesEnfileiradas.isEmpty()) {
			Binding.notificacoesEnfileiradas.remove(0).notifyObservers();
		}
		Binding.enfileirarNotificacoes = false;
	}

	notifyObservers() {

		if (Binding.notificacoesDesligadas > 0) {
			return;
		}
		if (this.observersDisabled) {
			return;
		}
		if (Binding.enfileirarNotificacoes) {
			Binding.notificacoesEnfileiradas.addIfNotContains(this);
			return;
		}
		if (!this.deveNotificar) {
			return;
		}

		this.deveNotificar = false;

		this.observers.getArray().concat([]).forEach(o => {
			o.notify0(this);
			o.notify(this);
		});

		this.renderObservers.concat(new ArrayLst()).forEach(o => {
			if (!Null.is(o)) {
				o.forceUpdate();
			}
		});

		this.functionsObservers.concat(new ArrayLst()).forEach(o => o());
		this.functionsTObservers.concat(new ArrayLst()).forEach(o => o(this));

	}

	deveNotificarCustom() {
		return false;
	}

	notify(o) {}

	notify0(o) {
		let s = this.getInvalidMessagePrivate0();
		if (!StringCompare.eq(s, this.ultimaInvalidMessage)) {
			this.ultimaInvalidMessage = s;
			this.notifyObservers();
		}
	}

	isValid() {
		return StringEmpty.is(this.getInvalidMessage());
	}

	setVisible(value) {
		if (value) {
			value = this.isVisiblePrivate();
		}
		if (this.visible !== value) {
			this.visible = value;
			this.forceNotifyObservers();
		}
	}

	setEnabled(value) {
		if (value) {
			value = this.isEnabledPrivate();
		}
		if (this.enabled !== value) {
			this.enabled = value;
			this.forceNotifyObservers();
		}
		return this.THIS();
	}

	setDisabled(value) {
		return this.setEnabled(!value);
	}

	isEnabled() {
		return this.enabled;
	}
	isDisabled() {
		return !this.enabled;
	}
	isVisible() {
		return this.visible;
	}

	isVisiblePrivate() {
		return true;
	}
	isEnabledPrivate() {
		return true;
	}

	eq(o) {

		let atual = this.get();

		if (Equals.is(atual, o)) {
			return true;
		}

		if (Null.is(atual) || Null.is(o)) {
			return false;
		}

		let idA = o.idComponent;
		if (!Null.is(idA)) {
			let idB = this.get().idComponent;
			return IntegerCompare.eq(idA, idB);
		}

		idA = o.id;
		if (!Null.is(idA)) {
			let idB = this.get().id;
			return IntegerCompare.eq(idA, idB);
		}

		let sa = StringParse.get(atual);
		let sb = StringParse.get(o);
		return StringCompare.eq(sa, sb);

	}

	ativo() {
		return true;
	}

	getStatus() {
		if (this.isEmpty()) {
			return IconeStatus.warning;
		} else if (this.isValid()) {
			return IconeStatus.ok;
		} else {
			return IconeStatus.error;
		}
	}
	isPassword() {
		return false;
	}

	mostrarPassword = false;

	showPassword() {
		return this.mostrarPassword;
	}
	showBotaoShowHidePassword() {
		return true;
	}

	changeShowPassword() {
		this.mostrarPassword = !this.mostrarPassword;
		this.forceNotifyObservers();
	}
	isNumeric() {
		return false;
	}

	setPlaceHolder(value) {
		this.placeHolder = value;
		return this.THIS();
	}

	getPlaceHolder() {
		return this.placeHolder;
	}

	setAsStringFunc(value) {
		this.asStringFunc = value;
		return this.THIS();
	}

	asString() {
		if (Null.is(this.asStringFunc)) {
			return StringParse.get(this.get());
		} else {
			return this.asStringFunc(this.get());
		}
	}

	afterSet(old, value) {}
	afterCancelSet() {}

	beforeSet(value) {
		return value;
	}

	getInvalidMessage() {
		this.ultimaInvalidMessage = this.getInvalidMessagePrivate0();
		return this.ultimaInvalidMessage;
	}
	getInvalidMessagePrivate0() {
		if (!Null.is(this.controller)) {
			return this.controller.getInvalidMessage();
		}
		if (this.isDisabled() && !this.erroMesmoSeDisabled()) {
			return null;
		} else if (!this.isVisible()) {
			return null;
		} else if (this.isDisabled()) {
			return null;
		} else if (this.isEmpty()) {

			if (this.notNull()) {
				return this.getMensagemObrigatorio();
			} else {

				let s = this.notNullAndIsEmptyInvalidMessage();

				if (!StringEmpty.is(s)) {
					return s;
				}

				s = this.getInvalidMessageFunctionMesmoSeVazioCall();
				if (!StringEmpty.is(s)) {
					return s;
				}

				return null;

			}
		}

		if (!Null.is(this.getInvalidMessageFunction)) {
			let s = this.getInvalidMessageFunction();
			if (!StringEmpty.is(s)) {
				return s;
			}
		}

		let s = this.getInvalidMessageFunctionMesmoSeVazioCall();
		if (!StringEmpty.is(s)) {
			return s;
		}

		return this.getInvalidMessagePrivate();

	}

	getInvalidMessageFunctionMesmoSeVazioCall() {

		if (!Null.is(this.getInvalidMessageFunctionMesmoSeVazio)) {
			let s = this.getInvalidMessageFunctionMesmoSeVazio();
			if (!StringEmpty.is(s)) {
				return s;
			}
		}

		return null;

	}

	notNullAndIsEmptyInvalidMessage() {
		return null;
	}

	getMensagemObrigatorio() {
		return "Obrigatório!";
	}

	getInvalidMessagePrivate() {
		return null;
	}

	getLabel() {
		return this.label;
	}

	getStartValue() {
		return this.startValue;
	}
	commit() {
		if (this.isChanged()) {
			this.setStartValue(this.get());
			this.changeObservers.forEach(o => o.notify(this));
		}
	}
	setStartValue(o) {
		o = this.beforeSet(o);
		this.startValue = o;
		this.set(o);
		this.virgin = true;
		return this.THIS();
	}

	obrigatorio = true;
	showInvalidMessage = true;

	setShowInvalidMessage(value) {
		if (value !== this.showInvalidMessage) {
			this.showInvalidMessage = value;
			this.forceNotifyObservers();
		}
	}

	isShowInvalidMessage() {
		return this.showInvalidMessage;
	}

	notNull() {
		return this.obrigatorio;
	}
	allowNull() {
		return !this.notNull();
	}

	setNotNull(value) {
		if (this.obrigatorio !== value) {
			this.obrigatorio = value;
			this.forceNotifyObservers();
		}
		return this.THIS();
	}

	isEmpty() {
		return Null.is(this.get());
	}
	isNotEmpty() {
		return !this.isEmpty();
	}

	erroMesmoSeDisabled() {
		return true;
	}

	addRenderObserver(o) {
		if (!this.renderObservers.contains(o)) {
			this.renderObservers.push(o);
		}
	}

	addObserver(o) {
		if (this.observers.contains(o)) {
			return false;
		} else if (Equals.is(o, this)) {
			return false;
		} else {
			this.observers.push(o);
			return true;
		}
	}
	addChangeObserver(o) {
		if (this.changeObservers.contains(o)) {
			return false;
		} else {
			this.changeObservers.push(o);
			return true;
		}
	}
	addFunctionObserver(funcao) {
		if (!this.functionsObservers.contains(funcao)) {
			this.functionsObservers.push(funcao);
		}
	}
	addFunctionTObserver(funcao) {
		if (!this.functionsTObservers.contains(funcao)) {
			this.functionsTObservers.push(funcao);
		}
	}
	isChanged() {
		return !this.eq(this.getStartValue());
	}

	isVirgin() {
		return this.virgin;
	}

	setVirgin(value) {
		if (this.virgin !== value) {
			this.virgin = value;
			this.forceNotifyObservers();
		}
	}

	castFromString(s) {
		throw new Error();
	}

	setString(s) {
		return this.set(this.castFromString(s));
	}

	sendKey(key) {
		let s = this.asString();
		if (StringCompare.eq(key, "<")) {
			if (!StringEmpty.is(s)) {
				this.setString(StringRight.ignore1(s));
			}
			return;
		} else if (this.isNumeric()) {
			if (IntegerIs.is(key)) {
				s = StringExtraiNumeros.exec(s) + key;
				this.setString(s);
			}
			return;
		} else {
			this.setString(s + key);
		}
	}
	setAtribute(key, value) {
		this.atributes.set(key, value);
	}
	getAtribute(key) {
		return this.atributes.get(key);
	}

	removeRenderObserver(o) {
		this.renderObservers.removeObject(o);
	}

	removeFunctionObserver(o) {
		this.functionsObservers.removeObject(o);
	}

	setHelp(value) {
		if (this.help !== value) {
			this.help = value;
			this.forceNotifyObservers();
		}
	}
	getHelp() {
		return this.help;
	}
	setObserversDisabled(value) {
		this.observersDisabled = value;
	}
	clear() {
		return this.set(null);
	}

	getReplaceRenderBody() {
		return this.replaceRenderBody;
	}
	setReplaceRenderBody(value) {
		this.replaceRenderBody = value;
	}
	getRenderBody() {
		return this.renderBody();
	}
	setRenderBody(value) {
		this.renderBody = value;
	}

	toJsonImpl() {
		return "{"+UJson.itemString("value", this.asStringJson())+"}";
	}

	asStringJson() {
		return this.asString();
	}

}
