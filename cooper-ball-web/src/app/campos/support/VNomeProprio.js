import StringContains from '../../../commom/utils/string/StringContains';
import UNomeProprio from '../../../commom/utils/string/UNomeProprio';
import VString from './VString';

export default class VNomeProprio extends VString {

	afterConstruct() {
		this.setMaxLength(60);
	}

	formatParcial(s) {
		return UNomeProprio.formatParcial(s, false);
	}

	getInvalidMessagePrivate() {

		let s = this.get().trim();

		if (!StringContains.is(s, " ")) {
			return "Incompleto";
		}

		return null;
	}

}
