import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import StringRepete from '../../../commom/utils/string/StringRepete';
import VString from './VString';

export default class VSenha extends VString {

	afterConstruct() {
		this.setPlaceHolder("Entre com uma Senha");
	}

	formatParcial(s) {

		let x = this.get();
		let result = "";

		while (!StringEmpty.is(s) && !StringEmpty.is(x)) {
			if (s.startsWith("*")) {
				result += x.substring(0, 1);
			} else {
				result += s.substring(0, 1);
			}
			s = s.substring(1);
			x = x.substring(1);
		}

		s = result + s;
		if (this.isNumeric()) {
			s = StringExtraiNumeros.exec(s);
		}
		s = StringLength.max(s, this.getMaxLength());
		return s;

	}

	asString() {
		if (this.showPassword()) {
			return this.get();
		} else {
			return StringRepete.exec("*", StringLength.get(this.get()));
		}
	}

	isPassword() {
		return true;
	}

}
