import Null from '../../../commom/utils/object/Null';
import Numeric15 from '../../misc/utils/numbers/Numeric15';
import NumericJs from '../../misc/utils/numbers/NumericJs';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiCaracteres from '../../../commom/utils/string/StringExtraiCaracteres';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import StringRepete from '../../../commom/utils/string/StringRepete';
import StringReplace from '../../../commom/utils/string/StringReplace';
import UConstantes from '../../misc/utils/UConstantes';
import UNumbers from '../../misc/utils/UNumbers';
import VString from './VString';

export default class VNumeric extends VString {

	decimais = 0;
	inteiros = 0;
	nullIfZeroWhenDisabled = false;

	afterConstruct() {
		this.inteiros = 9;
		this.setDecimais(2);
	}

	setInteiros(value) {
		this.inteiros = value;
		this.ajustaSize();
		return this;
	}

	setDecimais(value) {
		this.decimais = value;
		this.ajustaSize();
		return this;
	}

	ajustaSize() {
		let s = StringRepete.exec("9", this.inteiros) + "," + StringRepete.exec("9", this.decimais);
		s = this.formatParcial(s);
		this.setMaxLength(StringLength.get(s));
	}

	formatParcial(s) {
		return UNumbers.formatParcial(s, this.inteiros, this.decimais);
	}

	getDouble() {
		if (this.isEmpty()) return null;
		let s = this.get().replace(".", "").replace(",", ".");
		return parseFloat(s);
	}

	setNullIfZeroWhenDisabled(value) {
		this.nullIfZeroWhenDisabled = value;
		return this;
	}

	beforeSet(s) {
		s = super.beforeSet(s);
		if (StringEmpty.is(s)) {
			return null;
		}
		if (this.nullIfZeroWhenDisabled) {
			let x = StringReplace.exec(s, "0", "");
			x = StringReplace.exec(x, ".", "");
			if (StringEmpty.is(x)) {
				return null;
			}
		}
		return s;
	}

	toJsNumeric(casas) {
		return new NumericJs(this.getDouble(), casas);
	}

	toNumeric15() {
		return new Numeric15(this.getDouble());
	}

	setDouble(d) {
		if (Null.is(d)) {
			return this.set(null);
		} else {
			let s = ""+d;
			return this.set(s.replace(",", "."));
		}
	}

	setNumeric(n) {
		if (Null.is(n)) {
			return this.set(null);
		} else {
			return this.set(n.asString());
		}
	}

	isNumeric() {
		return true;
	}

	menorQue(b) {
		return this.getDouble() < b.getDouble();
	}

	maiorQue(b) {
		return this.getDouble() > b.getDouble();
	}

	getInvalidMessagePrivate() {
		let s = this.get();
		let x = StringExtraiCaracteres.exec(s, UConstantes.caracteresNumericos);
		if (StringLength.is(s, x.length)) {
			return this.getInvalidMessagePrivate2();
		} else {
			return "Valor inválido!";
		}
	}

	getInvalidMessagePrivate2() {
		return null;
	}

	toInt() {
		if (this.isEmpty()) {
			return 0;
		} else {
			return parseInt(StringExtraiNumeros.exec(this.get()));
		}
	}

}
