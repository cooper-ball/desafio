import StringCompare from '../../../commom/utils/string/StringCompare';

export default class EventKey {
	constructor(e)  {
		this.e = e;
	}
	enter() {
		return StringCompare.eq(this.e.key, "Enter");
	}
	shift() {
		return this.e.shiftKey;
	}
	ctrl() {
		return this.e.ctrlKey;
	}
	esc() {
		return StringCompare.eq(this.e.key, "Escape");
	}
	del() {
		return StringCompare.eq(this.e.key, "Delete");
	}
	backspace() {
		return StringCompare.eq(this.e.key, "Backspace");
	}
	arrowDown() {
		return StringCompare.eq(this.e.key, "ArrowDown");
	}
	arrowUp() {
		return StringCompare.eq(this.e.key, "ArrowUp");
	}
	arrowLeft() {
		return StringCompare.eq(this.e.key, "ArrowLeft");
	}
	arrowRight() {
		return StringCompare.eq(this.e.key, "ArrowRight");
	}
	getKey() {
		return this.e.key;
	}
	preventDefault() {
		this.e.preventDefault();
	}
}
