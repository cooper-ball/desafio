import VBoolean from '../../campos/support/VBoolean';

export default class PermissoesMenu extends VBoolean {

	importacaoArquivo = false;

	carragar() {
		this.fcAxios.post("permissoes-menu", null, res => {
			let json = res;
			this.importacaoArquivo = json.importacaoArquivo;
			this.set(true);
		});
	}

	podeVerImportacaoArquivo() {
		return this.importacaoArquivo;
	}
}
