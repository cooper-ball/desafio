import ArrayLst from '../../../commom/utils/array/ArrayLst';
import AuditoriaObject from '../../../fc/components/auditoria/AuditoriaObject';
import Binding from '../../campos/support/Binding';
import EntityCampos from '../../../fc/components/EntityCampos';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import Telefone from '../telefone/Telefone';

export default class TorcedorCampos extends EntityCampos {

	getEntidade() {
		return "Torcedor";
	}

	getEntidadePath() {
		return "Torcedor";
	}

	initImpl() {
		this.cpf = this.newCpf("Cpf", true, "Geral");
		this.nome = this.newNomeProprio("Nome", true, "Geral");
		this.email = this.newEmail("E-mail", true, "Geral");
		this.cep = this.newCep("Cep", true, "Geral");
		this.complemento = this.newString("Complemento", 200, false, "Geral");
		this.numero = this.newInteger("Número", 99999, true, "Geral");
		this.telefones = this.newSubList(
			"Telefones", "telefones"
			, (de, para) => this.telefoneUtils.merge(de, para)
			, obj => {
				let array = obj;
				this.original.telefones = new ArrayLst();
				array.forEach(json => this.original.telefones.add(this.telefoneUtils.fromJson(json)));
				this.setTelefones();
				if (!Null.is(this.original.original)) {
					this.original.original.telefones = this.telefoneUtils.clonarList(this.original.telefones);
				}
			}
			, false, "Geral", false
		);
		this.telefones.setOnConfirm(() => {
			this.telefones.add(this.telefoneCampos.getTo());
			this.refreshTelefonesHouveMudancas();
		});
		this.telefones.setOnClear(() => {
			this.telefones.remove(this.telefoneCampos.getTo());
			this.refreshTelefonesHouveMudancas();
		});
		this.construido = true;
		this.auditoria = new AuditoriaObject(this);
	}

	setCampos(o) {
		if (Null.is(o)) {
			throw new Error("TorcedorCampos:o is null");
		}
		Binding.notificacoesDesligadasInc();
		this.disabledObservers = true;
		this.original = o;
		this.to = this.torcedorUtils.clonar(o);
		this.id.clear();
		this.id.set(this.to.id);
		this.cpf.set(this.to.cpf);
		this.nome.set(this.to.nome);
		this.email.set(this.to.email);
		this.cep.set(this.to.cep);
		this.complemento.set(this.to.complemento);
		this.numero.set(this.to.numero);
		this.excluido.set(this.to.excluido);
		this.registroBloqueado.set(this.to.registroBloqueado);
		this.id.setStartValue(this.to.id);
		this.cpf.setStartValue(this.cpf.get());
		this.nome.setStartValue(this.nome.get());
		this.email.setStartValue(this.email.get());
		this.cep.setStartValue(this.cep.get());
		this.complemento.setStartValue(this.complemento.get());
		this.numero.setStartValue(this.numero.get());
		this.telefones.setStartValue(this.telefones.get());
		this.excluido.setStartValue(this.excluido.get());
		this.registroBloqueado.setStartValue(this.registroBloqueado.get());
		this.setTelefones();
		let readOnly = this.isReadOnly();
		this.cpf.setDisabled(readOnly);
		this.nome.setDisabled(readOnly);
		this.email.setDisabled(readOnly);
		this.cep.setDisabled(readOnly);
		this.complemento.setDisabled(readOnly);
		this.numero.setDisabled(readOnly);
		this.telefones.setDisabled(readOnly);
		this.reiniciar();
	}

	setTelefones() {
		if (Null.is(this.original.telefones)) {
			this.telefones.clearItens();
		} else {
			this.telefones.clearItens2();
			let readOnly = this.isReadOnly();
			this.original.telefones.forEach(item => {
				let o = this.telefoneUtils.clonar(item);
				if (readOnly) {
					o.registroBloqueado = true;
				}
				this.telefones.add(o);
			});
		}
	}

	getTo() {
		this.to.setId(this.id.get());
		this.to.cpf = this.cpf.get();
		this.to.nome = this.nome.get();
		this.to.email = this.email.get();
		this.to.cep = this.cep.get();
		this.to.complemento = this.complemento.get();
		this.to.numero = this.numero.get();
		this.to.excluido = this.excluido.get();
		this.to.registroBloqueado = this.registroBloqueado.get();
		this.to.telefones = this.telefones.getItens();
		return this.to;
	}

	setJson(obj) {
		let json = obj;
		let o = this.torcedorUtils.fromJson(json);
		this.setCampos(o);
		let itensGrid = this.torcedorConsulta.getDataSource();
		if (!Null.is(itensGrid)) {
			let itemGrid = itensGrid.byId(o.getId(), i => i.getId());
			if (!Null.is(itemGrid)) {
				this.torcedorUtils.merge(o, itemGrid);
			}
		}
		return o;
	}

	refreshTelefonesHouveMudancas() {
		this.to.telefonesHouveMudancas = !this.telefoneUtils.equalsList(this.telefones.getItens(), this.original.telefones);
		if (this.to.telefonesHouveMudancas) {
			this.telefones.getItens().forEach(o => {
				let ori = this.original.telefones.byId(o.getId(), i => i.getId());
				o.setHouveMudancas(!this.telefoneUtils.equals(o, ori));
			});
		}
	}

	getText(o) {
		if (Null.is(o)) {
			return null;
		}
		return o.nome;
	}

	houveMudancas() {
		if (Null.is(this.original)) {
			return false;
		}
		return !this.torcedorUtils.equals(this.original, this.getTo());
	}

	camposAlterados() {
		return this.torcedorUtils.camposAlterados(this.original, this.getTo());
	}

	cancelarAlteracoes() {
		this.setCampos(this.original);
	}

	getOriginal() {
		return this.original;
	}

	telefonesNovo() {
		let o = new Telefone();
		o.setId(--EntityCampos.novos);
		this.telefonesEdit(o);
	}

	telefonesEdit(o) {
		this.telefoneCampos.torcedor.setVisible(false);
		this.telefoneCampos.setCampos(o);
		this.telefones.setTrue("edit");
	}

	setAttr(key, value) {
		if (StringCompare.eq(key, "cpf")) {
			this.cpf.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Cpf")) {
			this.cpf.setString(value);
			return;
		}
		if (StringCompare.eq(key, "nome")) {
			this.nome.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Nome")) {
			this.nome.setString(value);
			return;
		}
		if (StringCompare.eq(key, "email")) {
			this.email.setString(value);
			return;
		}
		if (StringCompare.eq(key, "E-mail")) {
			this.email.setString(value);
			return;
		}
		if (StringCompare.eq(key, "cep")) {
			this.cep.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Cep")) {
			this.cep.setString(value);
			return;
		}
		if (StringCompare.eq(key, "complemento")) {
			this.complemento.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Complemento")) {
			this.complemento.setString(value);
			return;
		}
		if (StringCompare.eq(key, "numero")) {
			this.numero.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Número")) {
			this.numero.setString(value);
			return;
		}
		if (StringCompare.eq(key, "telefones")) {
			this.telefones.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Telefones")) {
			this.telefones.setString(value);
			return;
		}
		let message = "Campo inválido: " + key;
		throw new Error(message);
	}

	reiniciar2() {
		this.auditoria.clearItens();
	}
}
