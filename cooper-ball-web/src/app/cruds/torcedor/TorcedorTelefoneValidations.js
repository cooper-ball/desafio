import ArrayLst from '../../../commom/utils/array/ArrayLst';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import TelefoneTipoConstantes from '../telefoneTipo/TelefoneTipoConstantes';

export default class TorcedorTelefoneValidations {

	init() {

		this.telefoneCampos.tipo.addFunctionObserver(() => {
			if (this.telefoneCampos.tipo.eq(TelefoneTipoConstantes.CELULAR)) {
				this.telefoneCampos.numero.setCasas(9);
			} else {
				this.telefoneCampos.numero.setCasas(8);
			}
			this.telefoneCampos.numero.set(this.telefoneCampos.numero.get());
		});

		this.telefoneCampos.afterSaveObservers.add(() => {
			if (this.telefoneCampos.principal.isTrue()) {
				this.outrosTelefones().forEach(o => {
					o.principal = false;
					this.alert.warn("O telefone principal foi alterado!");
				});
			}
		});

		this.telefoneCampos.numero.getInvalidMessageFunction = () => {

			if (this.outrosTelefones().exists(o => StringCompare.eqq(o.numero, this.telefoneCampos.numero.get()))) {
				return "Número já cadastrado!";
			}

			return null;
		};

		this.torcedorCampos.telefones.getInvalidMessageFunctionMesmoSeVazio = () => {

			let itens = this.telefones();

			if (itens.isEmpty()) {
				return "Pelo menos um telefone deve ser cadastrado!";
			}

			if (!itens.exists(o => o.principal)) {
				return "Pelo menos um telefone deve ser marcado como principal!";
			}

			return null;

		};

		this.torcedorCampos.telefones.forceNotifyObservers();

	}

	outrosTelefones() {
		return this.telefones().filter(o => o.principal && IntegerCompare.ne(o.id, this.telefoneCampos.id.get()));
	}

	telefones() {
		let itens = this.torcedorCampos.telefones.getItens();
		if (Null.is(itens)) {
			return new ArrayLst();
		} else {
			return itens.filter(o => !o.excluido);
		}
	}

}
