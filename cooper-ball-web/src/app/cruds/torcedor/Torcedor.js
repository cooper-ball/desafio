import EntityFront from '../../../fc/components/EntityFront';
import Null from '../../../commom/utils/object/Null';

export default class Torcedor extends EntityFront {

	cpf = null;
	nome = null;
	email = null;
	cep = null;
	complemento = null;
	numero = null;
	telefones = null;
	telefonesHouveMudancas = false;
	excluido = false;
	registroBloqueado = false;

	getText() {
		return this.nome;
	}

	asString() {
		let s = "{";
		s += "\"id\":"+this.getId()+",";
		if (!Null.is(this.cpf)) {
			s += "\"cpf\":\""+this.cpf+"\",";
		}
		if (!Null.is(this.nome)) {
			s += "\"nome\":\""+this.nome+"\",";
		}
		if (!Null.is(this.email)) {
			s += "\"email\":\""+this.email+"\",";
		}
		if (!Null.is(this.cep)) {
			s += "\"cep\":\""+this.cep+"\",";
		}
		if (!Null.is(this.complemento)) {
			s += "\"complemento\":\""+this.complemento+"\",";
		}
		if (!Null.is(this.numero)) {
			s += "\"numero\":"+this.numero+",";
		}
		if (this.telefones !== null) {
			s += "\"telefones\":[";
			s += this.telefones.reduce((ss, o) => ss + o.asString() + ",", "");
			s += "],";
		}
		s += "\"excluido\":"+this.excluido+",";
		s += "}";
		return s;
	}
}
