import ArrayEmpty from '../../../commom/utils/array/ArrayEmpty';
import ArrayEquals from '../../../commom/utils/array/ArrayEquals';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoAlterado from '../../../fc/components/campoAlterado/CampoAlterado';
import EntityCampos from '../../../fc/components/EntityCampos';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringParse from '../../../commom/utils/string/StringParse';
import Torcedor from './Torcedor';
import UBoolean from '../../misc/utils/UBoolean';

export default class TorcedorUtils {

	equalsNumero(a, b) {
		return IntegerCompare.eq(a.numero, b.numero);
	}

	equalsCpf(a, b) {
		return StringCompare.eqq(a.cpf, b.cpf);
	}

	equalsNome(a, b) {
		return StringCompare.eqq(a.nome, b.nome);
	}

	equalsEmail(a, b) {
		return StringCompare.eqq(a.email, b.email);
	}

	equalsCep(a, b) {
		return StringCompare.eqq(a.cep, b.cep);
	}

	equalsComplemento(a, b) {
		return StringCompare.eqq(a.complemento, b.complemento);
	}

	equalsTelefones(a, b) {
		return this.telefoneUtils.equalsList(a.telefones, b.telefones);
	}

	equalsExcluido(a, b) {
		return UBoolean.eq(a.excluido, b.excluido);
	}

	equals(a, b) {
		if (Null.is(a)) {
			return Null.is(b);
		}
		if (Null.is(b)) {
			return false;
		}
		if (!this.equalsNumero(a, b)) {
			return false;
		}
		if (!this.equalsCpf(a, b)) {
			return false;
		}
		if (!this.equalsNome(a, b)) {
			return false;
		}
		if (!this.equalsEmail(a, b)) {
			return false;
		}
		if (!this.equalsCep(a, b)) {
			return false;
		}
		if (!this.equalsComplemento(a, b)) {
			return false;
		}
		if (!this.equalsTelefones(a, b)) {
			return false;
		}
		if (!this.equalsExcluido(a, b)) {
			return false;
		}
		return true;
	}

	camposAlterados(a, b) {
		let list = new ArrayLst();
		if (!this.equalsNumero(a, b)) {
			list.add(new CampoAlterado().setKey("numero").setCampo("Número").setDe(StringParse.get(a.numero)).setPara(StringParse.get(b.numero)));
		}
		if (!this.equalsCpf(a, b)) {
			list.add(new CampoAlterado().setKey("cpf").setCampo("Cpf").setDe(a.cpf).setPara(b.cpf));
		}
		if (!this.equalsNome(a, b)) {
			list.add(new CampoAlterado().setKey("nome").setCampo("Nome").setDe(a.nome).setPara(b.nome));
		}
		if (!this.equalsEmail(a, b)) {
			list.add(new CampoAlterado().setKey("email").setCampo("E-mail").setDe(a.email).setPara(b.email));
		}
		if (!this.equalsCep(a, b)) {
			list.add(new CampoAlterado().setKey("cep").setCampo("Cep").setDe(a.cep).setPara(b.cep));
		}
		if (!this.equalsComplemento(a, b)) {
			list.add(new CampoAlterado().setKey("complemento").setCampo("Complemento").setDe(a.complemento).setPara(b.complemento));
		}
		if (!this.equalsTelefones(a, b)) {
			list.add(new CampoAlterado().setCampo("Telefones"));
		}
		if (!this.equalsExcluido(a, b)) {
			list.add(new CampoAlterado().setCampo("Excluído"));
		}
		return list;
	}

	equalsList(a, b) {
		return ArrayEquals.isT(a, b, (x, y) => this.equals(x, y));
	}

	fromJson(json) {
		if (Null.is(json)) return null;
		let o = new Torcedor();
		o.setId(json.id);
		if (!Null.is(json.cpf)) {
			o.cpf = json.cpf;
		}
		if (!Null.is(json.nome)) {
			o.nome = json.nome;
		}
		if (!Null.is(json.email)) {
			o.email = json.email;
		}
		if (!Null.is(json.cep)) {
			o.cep = json.cep;
		}
		if (!Null.is(json.complemento)) {
			o.complemento = json.complemento;
		}
		if (!Null.is(json.numero)) {
			o.numero = json.numero;
		}
		if (!Null.is(json.telefones)) {
			let array = json.telefones;
			let list = new ArrayLst(array);
			o.telefones = this.telefoneUtils.fromJsonList(list);
		}
		o.excluido = json.excluido;
		o.registroBloqueado = json.registroBloqueado;
		return o;
	}

	fromJsonList(jsons) {
		if (Null.is(jsons)) return null;
		return jsons.map(o => this.fromJson(o));
	}

	merge(de, para) {
		para.setId(de.getId());
		para.cpf = de.cpf;
		para.nome = de.nome;
		para.email = de.email;
		para.cep = de.cep;
		para.complemento = de.complemento;
		para.numero = de.numero;
		para.telefones = de.telefones;
		para.excluido = de.excluido;
		para.registroBloqueado = de.registroBloqueado;
		para.telefones = this.telefoneUtils.clonarList(de.telefones);
	}

	clonar(obj) {
		if (Null.is(obj)) {
			return null;
		}
		let o = new Torcedor();
		o.original = obj;
		this.merge(obj, o);
		return o;
	}

	clonarList(array) {
		if (ArrayEmpty.is(array)) {
			return null;
		} else {
			return array.map(o => this.clonar(o));
		}
	}

	novo() {
		let o = new Torcedor();
		o.setId(--EntityCampos.novos);
		o.excluido = false;
		o.registroBloqueado = false;
		o.telefones = new ArrayLst();
		return o;
	}
}
