import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Coluna from '../../../fc/components/tabela/Coluna';
import StringCompare from '../../../commom/utils/string/StringCompare';
import TextAlign from '../../misc/consts/enums/TextAlign';
import UBoolean from '../../misc/utils/UBoolean';
import UIdText from '../../misc/utils/UIdText';

export default class TorcedorTelefonesCols {

	getGrupos() {
		return null;
	}

	init() {
		this.NUMERO = new Coluna(150, "numero", "Número", o => o.numero, TextAlign.center).setSort((a, b) => StringCompare.compare(a.numero, b.numero)).setGrupo(false);
		this.TIPO = new Coluna(300, "tipo", "Tipo", o => o.tipo, TextAlign.left).setSort((a, b) => UIdText.compareText(a.tipo, b.tipo)).setGrupo(false);
		this.PRINCIPAL = new Coluna(100, "principal", "Principal", o => o.principal, TextAlign.center).setSort((a, b) => UBoolean.compare(a.principal, b.principal)).setGrupo(false);
		this.list = ArrayLst.build(this.NUMERO, this.TIPO, this.PRINCIPAL);
		this.NUMERO.renderItem = this.telefoneCols.NUMERO.renderItem;
		this.TIPO.renderItem = this.telefoneCols.TIPO.renderItem;
		this.PRINCIPAL.renderItem = this.telefoneCols.PRINCIPAL.renderItem;
	}

	getList() {
		return this.list.filter(o => o.isVisible());
	}
}
