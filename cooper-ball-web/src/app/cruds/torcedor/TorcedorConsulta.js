import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Consulta from '../../../fc/components/consulta/Consulta';
import ConsultaOperadorConstantes from '../consultaOperador/ConsultaOperadorConstantes';
import Null from '../../../commom/utils/object/Null';
import VConsultaList from '../../../fc/components/VConsultaList';

export default class TorcedorConsulta extends Consulta {

	grupoGeral = this.newSeparacaoDeGrupo("grupoGeral", "Geral");
	cpf = this.newCpf("cpf", "Cpf", true);
	nome = this.newString("nome", "Nome", 50, true);
	email = this.newEmail("email", "E-mail", true);
	grupoenderecoResidencial = this.newSeparacaoDeGrupo("grupoenderecoResidencial", "Endereço Residencial");
	cep = this.newCep("cep", "Cep", true);
	complemento = this.newString("complemento", "Complemento", 200, false);
	numero = this.newInteger("numero", "Número", 99999, true);
	excluido = this.newBoolean("excluido", "Excluído", true);

	init2() {
		this.nomeEntidade = "Torcedor";
		this.dados = new VConsultaList(
			"dados",
			(a, b) => {},
			body => {
				let result = body;
				let array = result.dados;
				let list = new ArrayLst(array);
				this.dados.addItens(this.torcedorUtils.fromJsonList(list));
				this.refreshConsulta(result);
			},
			"Torcedor/consulta",
			() => this.getTo(),
			this.fcAxios
		);
		this.excluido.setDefaultValue(ConsultaOperadorConstantes.NAO);
		this.titleForm.set("Torcedor");
	}

	getTo() {
		return this;
	}

	consultarImpl() {
		this.dados.clearItens();
		this.dados.carregar();
	}

	getDataSource() {
		if (Null.is(this.dados)) {
			return new ArrayLst();
		} else {
			return this.dados.getItens();
		}
	}
}
