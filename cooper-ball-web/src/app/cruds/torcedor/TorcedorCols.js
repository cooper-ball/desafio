import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Coluna from '../../../fc/components/tabela/Coluna';
import StringCompare from '../../../commom/utils/string/StringCompare';
import TextAlign from '../../misc/consts/enums/TextAlign';
import UInteger from '../../misc/utils/UInteger';

export default class TorcedorCols {

	getGrupos() {
		let lst = new ArrayLst();
		this.addGrupoGeral(lst);
		this.addGrupoEnderecoResidencial(lst);
		return lst;
	}

	addGrupoGeral(lst) {
		let cols = 0;
		let width = 0;

		if (this.CPF.isVisible()) {
			width += 150;
			cols++;
		}

		if (this.NOME.isVisible()) {
			width += 300;
			cols++;
		}

		if (this.EMAIL.isVisible()) {
			width += 150;
			cols++;
		}
		if (cols > 0) {
			lst.add(new Coluna(width, null, "Geral", null, TextAlign.center).setCols(cols));
		}
	}

	addGrupoEnderecoResidencial(lst) {
		let cols = 0;
		let width = 0;

		if (this.CEP.isVisible()) {
			width += 90;
			cols++;
		}

		if (this.COMPLEMENTO.isVisible()) {
			width += 400;
			cols++;
		}

		if (this.NUMERO.isVisible()) {
			width += 90;
			cols++;
		}
		if (cols > 0) {
			lst.add(new Coluna(width, null, "Endereço Residencial", null, TextAlign.center).setCols(cols));
		}
	}

	init() {
		this.torcedorConsulta.setGetCols(this);
		this.CPF = new Coluna(150, "cpf", "Cpf", o => o.cpf, TextAlign.center).setSort((a, b) => StringCompare.compare(a.cpf, b.cpf)).setGrupo(true).setVisible(() => this.torcedorConsulta.cpf.visibleCol.isTrue());
		this.NOME = new Coluna(300, "nome", "Nome", o => o.nome, TextAlign.left).setSort((a, b) => StringCompare.compare(a.nome, b.nome)).setGrupo(true).setVisible(() => this.torcedorConsulta.nome.visibleCol.isTrue());
		this.EMAIL = new Coluna(150, "email", "E-mail", o => o.email, TextAlign.left).setSort((a, b) => StringCompare.compare(a.email, b.email)).setGrupo(true).setVisible(() => this.torcedorConsulta.email.visibleCol.isTrue());
		this.CEP = new Coluna(90, "cep", "Cep", o => o.cep, TextAlign.center).setSort((a, b) => StringCompare.compare(a.cep, b.cep)).setGrupo(true).setVisible(() => this.torcedorConsulta.cep.visibleCol.isTrue());
		this.COMPLEMENTO = new Coluna(400, "complemento", "Complemento", o => o.complemento, TextAlign.left).setSort((a, b) => StringCompare.compare(a.complemento, b.complemento)).setGrupo(true).setVisible(() => this.torcedorConsulta.complemento.visibleCol.isTrue());
		this.NUMERO = new Coluna(90, "numero", "Número", o => o.numero, TextAlign.center).setSort((a, b) => UInteger.compareToInt(a.numero, b.numero)).setGrupo(true).setVisible(() => this.torcedorConsulta.numero.visibleCol.isTrue());
		this.list = ArrayLst.build(this.CPF, this.NOME, this.EMAIL, this.CEP, this.COMPLEMENTO, this.NUMERO);
	}

	getList() {
		return this.list.filter(o => o.isVisible());
	}
}
