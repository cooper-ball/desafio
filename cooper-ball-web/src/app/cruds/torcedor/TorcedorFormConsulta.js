import React from 'react';
import FcBotao from '../../../fc/components/FcBotao';
import FormConsulta from '../../../fc/components/consulta/FormConsulta';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import Tabela from '../../../fc/components/tabela/Tabela';
import Td from '../../../web/Td';
import TorcedorEdit from './TorcedorEdit';

export default class TorcedorFormConsulta extends FormConsulta {

	getConsulta() {
		return this.torcedorConsulta;
	}

	novo() {
		this.setEdit(this.torcedorUtils.novo());
	}

	botaoNovo() {
		if (this.podeInserir()) {
			return (
				<FcBotao
				title={"Novo"}
				acao={() => this.novo()}
				style={FormConsulta.STYLE_BOTAO_MAIS_FILTROS}
				/>
			);
		} else {
			return null;
		}
	}

	botoesSuperiores() {
		return (
			<Td style={FormConsulta.TD_BUTTONS_RIGHT}>
				{this.botaoNovo()}
			</Td>
		);
	}

	edit(id) {
		this.torcedorCampos.edit(id, o => this.setEdit(o));
	}

	setEdit(o) {
		this.torcedorCampos.setCampos(o);
		this.setShowEdit(true);
	}

	renderEdit() {
		return <TorcedorEdit onDelete={idP => this.delete(idP)} onClose={() => this.setShowEdit(false)} isModal={true}/>;
	}

	delete(idP) {
		if (!this.podeExcluir()) {
			return;
		}
		this.torcedorCampos.excluir(idP, () => {
			let o = this.torcedorConsulta.dados.getItens().uniqueObrig(i => IntegerCompare.eq(idP, i.getId()));
			this.torcedorConsulta.dados.remove(o);
		});
	}

	onDeleteFunction() {
		if (!this.podeExcluir()) return null;
		return o => this.delete(o.getId());
	}

	getTable() {
		return (
			<Tabela
			bind={this.torcedorConsulta.dados}
			colunas={this.torcedorCols.getList()}
			colunasGrupo={this.torcedorCols.getGrupos()}
			onClick={o => this.edit(o.getId())}
			onDelete={this.onDeleteFunction()}
			/>
		);
	}

	getEntidade() {
		return "Torcedor";
	}

	getEntidadePath() {
		return "Torcedor";
	}
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowConfirmLogout = o => this.setState({showConfirmLogout:o});
}

TorcedorFormConsulta.defaultProps = FormConsulta.defaultProps;
