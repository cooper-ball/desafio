import Permissoes from '../../../fc/components/Permissoes';

export default class TorcedorPermissoes extends Permissoes {

	incluir = false;
	alterar = false;
	excluir = false;
	ver = false;
	incluirTelefones = false;
	alterarTelefones = false;
	excluirTelefones = false;
	verTelefones = false;

	podeIncluir() {
		return this.incluir;
	}

	podeAlterar() {
		return this.alterar;
	}

	podeExcluir() {
		return this.excluir;
	}

	podeVer() {
		return this.ver;
	}

	podeIncluirTelefones() {
		return this.incluirTelefones;
	}

	podeAlterarTelefones() {
		return this.alterarTelefones;
	}

	podeExcluirTelefones() {
		return this.excluirTelefones;
	}

	podeVerTelefones() {
		return this.verTelefones;
	}

	setIncluir(value) {
		this.incluir = value;
	}

	setAlterar(value) {
		this.alterar = value;
	}

	setExcluir(value) {
		this.excluir = value;
	}

	setVer(value) {
		this.ver = value;
	}

	setIncluirTelefones(value) {
		this.incluirTelefones = value;
	}

	setAlterarTelefones(value) {
		this.alterarTelefones = value;
	}

	setExcluirTelefones(value) {
		this.excluirTelefones = value;
	}

	setVerTelefones(value) {
		this.verTelefones = value;
	}
}
