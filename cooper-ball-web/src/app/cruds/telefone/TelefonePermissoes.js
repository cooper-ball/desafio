import Permissoes from '../../../fc/components/Permissoes';

export default class TelefonePermissoes extends Permissoes {

	incluir = false;
	alterar = false;
	excluir = false;
	ver = false;

	podeIncluir() {
		return this.incluir;
	}

	podeAlterar() {
		return this.alterar;
	}

	podeExcluir() {
		return this.excluir;
	}

	podeVer() {
		return this.ver;
	}

	setIncluir(value) {
		this.incluir = value;
	}

	setAlterar(value) {
		this.alterar = value;
	}

	setExcluir(value) {
		this.excluir = value;
	}

	setVer(value) {
		this.ver = value;
	}
}
