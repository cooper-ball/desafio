import React from 'react';
import BotaoAuditoria from '../../../fc/components/auditoria/BotaoAuditoria';
import FormEdit from '../../../fc/components/FormEdit';
import FormItemInput from '../../../antd/form/FormItemInput';
import FormItemSelect from '../../../antd/form/FormItemSelect';
import LayoutApp from '../../../fc/components/LayoutApp';
import ModalAuditoria from '../../../fc/components/auditoria/ModalAuditoria';
import RadioBoolean from '../../../antd/form/RadioBoolean';
import StringCompare from '../../../commom/utils/string/StringCompare';
import Td from '../../../web/Td';
import {Card} from 'antd';
import {Col} from 'antd';
import {Row} from 'antd';
import {Tabs} from 'antd';
const TabPane = Tabs.TabPane;

export default class TelefoneEdit extends FormEdit {

	constructor(props) {
		super(props);
		this.init("Telefone");
	}

	getTabs() {
		return (
			<Tabs
			tabPosition={"top"}
			activeKey={this.state.abaSelecionada}
			defaultActiveKey={"Geral"}
			onChange={s => this.setAbaSelecionada(s)}>
				<TabPane key={"Geral"} tab={"Geral"}>
					{this.abaGeral()}
				</TabPane>
			</Tabs>
		);
	}

	abaGeral() {
		if (!StringCompare.eq(this.state.abaSelecionada, "Geral")) {
			return null;
		}
		return (
			<Row gutter={24}>
				{this.grupo_geral_0()}
			</Row>
		);
	}

	inputTorcedor() {
		return <FormItemSelect bind={this.campos.torcedor} lg={6}/>;
	}

	inputNumero() {
		return <FormItemInput bind={this.campos.numero} lg={6}/>;
	}

	inputTipo() {
		return <FormItemSelect bind={this.campos.tipo} lg={6}/>;
	}

	inputPrincipal() {
		return <RadioBoolean bind={this.campos.principal} lg={6}/>;
	}

	grupo_geral_0() {
		if (!this.campos.torcedor.isVisible() && !this.campos.numero.isVisible() && !this.campos.tipo.isVisible() && !this.campos.principal.isVisible()) {
			return null;
		}
		return (
			<Col lg={24} md={24} sm={24}>
				<Card size={"small"} style={LayoutApp.EMPTY.get()}>
					<Row gutter={24}>
						{this.inputTorcedor()}
						{this.inputNumero()}
						{this.inputTipo()}
						{this.inputPrincipal()}
					</Row>
				</Card>
			</Col>
		);
	}

	getTitleImpl() {
		return "Telefone";
	}

	getModal() {
		if (this.campos.auditoria.modalAuditoria.isTrue()) {
			return <ModalAuditoria auditoria={this.campos.auditoria}/>;
		}
		throw new Error("???");
	}

	getCampos() {
		return this.campos;
	}

	getPermissoes() {
		return this.permissoes;
	}

	botoesSuperiores() {
		return (
			<Td style={FormEdit.TD_BUTTONS_RIGHT}>
				<BotaoAuditoria auditoria={this.campos.auditoria}/>
			</Td>
		);
	}

	didMount2() {
		super.didMount2();
		this.observar(this.campos.auditoria.modalAuditoria);
	}

	esteFormEstahExibindoAlgumModal() {
		if (this.campos.auditoria.modalAuditoria.isTrue()) {
			return true;
		}
		return super.esteFormEstahExibindoAlgumModal();
	}
	setWidthForm = o => this.setState({widthForm:o});
	setAbaSelecionada = o => this.setState({abaSelecionada:o});
}

TelefoneEdit.defaultProps = FormEdit.defaultProps;
