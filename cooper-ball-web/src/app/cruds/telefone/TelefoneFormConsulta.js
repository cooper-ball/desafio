import React from 'react';
import FcBotao from '../../../fc/components/FcBotao';
import FormConsulta from '../../../fc/components/consulta/FormConsulta';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import Tabela from '../../../fc/components/tabela/Tabela';
import Td from '../../../web/Td';
import TelefoneEdit from './TelefoneEdit';

export default class TelefoneFormConsulta extends FormConsulta {

	getConsulta() {
		return this.telefoneConsulta;
	}

	novo() {
		this.setEdit(this.telefoneUtils.novo());
	}

	botaoNovo() {
		if (this.podeInserir()) {
			return (
				<FcBotao
				title={"Novo"}
				acao={() => this.novo()}
				style={FormConsulta.STYLE_BOTAO_MAIS_FILTROS}
				/>
			);
		} else {
			return null;
		}
	}

	botoesSuperiores() {
		return (
			<Td style={FormConsulta.TD_BUTTONS_RIGHT}>
				{this.botaoNovo()}
			</Td>
		);
	}

	edit(id) {
		this.telefoneCampos.edit(id, o => this.setEdit(o));
	}

	setEdit(o) {
		this.telefoneCampos.setCampos(o);
		this.setShowEdit(true);
	}

	renderEdit() {
		return <TelefoneEdit onDelete={idP => this.delete(idP)} onClose={() => this.setShowEdit(false)} isModal={true}/>;
	}

	delete(idP) {
		if (!this.podeExcluir()) {
			return;
		}
		this.telefoneCampos.excluir(idP, () => {
			let o = this.telefoneConsulta.dados.getItens().uniqueObrig(i => IntegerCompare.eq(idP, i.getId()));
			this.telefoneConsulta.dados.remove(o);
		});
	}

	onDeleteFunction() {
		if (!this.podeExcluir()) return null;
		return o => this.delete(o.getId());
	}

	getTable() {
		return (
			<Tabela
			bind={this.telefoneConsulta.dados}
			colunas={this.telefoneCols.getList()}
			colunasGrupo={this.telefoneCols.getGrupos()}
			onClick={o => this.edit(o.getId())}
			onDelete={this.onDeleteFunction()}
			/>
		);
	}

	getEntidade() {
		return "Telefone";
	}

	getEntidadePath() {
		return "Telefone";
	}
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowConfirmLogout = o => this.setState({showConfirmLogout:o});
}

TelefoneFormConsulta.defaultProps = FormConsulta.defaultProps;
