import AuditoriaObject from '../../../fc/components/auditoria/AuditoriaObject';
import Binding from '../../campos/support/Binding';
import EntityCampos from '../../../fc/components/EntityCampos';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import TelefoneTipoConstantes from '../telefoneTipo/TelefoneTipoConstantes';

export default class TelefoneCampos extends EntityCampos {

	getEntidade() {
		return "Telefone";
	}

	getEntidadePath() {
		return "Telefone";
	}

	initImpl() {
		this.torcedor = this.newFk("Torcedor","Torcedor", true, "Geral");
		this.numero = this.newTelefone("Número", true, "Geral");
		this.tipo = this.newList("Tipo", TelefoneTipoConstantes.LIST, true, "Geral");
		this.principal = this.newBoolean("Principal", true, "Geral");
		this.construido = true;
		this.auditoria = new AuditoriaObject(this);
	}

	setCampos(o) {
		if (Null.is(o)) {
			throw new Error("TelefoneCampos:o is null");
		}
		Binding.notificacoesDesligadasInc();
		this.disabledObservers = true;
		this.original = o;
		this.to = this.telefoneUtils.clonar(o);
		this.id.clear();
		this.id.set(this.to.id);
		this.torcedor.setUnique(this.to.torcedor);
		this.numero.set(this.to.numero);
		this.tipo.set(this.to.tipo);
		this.principal.set(this.to.principal);
		this.excluido.set(this.to.excluido);
		this.registroBloqueado.set(this.to.registroBloqueado);
		this.id.setStartValue(this.to.id);
		this.torcedor.setStartValue(this.torcedor.get());
		this.numero.setStartValue(this.numero.get());
		this.tipo.setStartValue(this.tipo.get());
		this.principal.setStartValue(this.principal.get());
		this.excluido.setStartValue(this.excluido.get());
		this.registroBloqueado.setStartValue(this.registroBloqueado.get());
		let readOnly = this.isReadOnly();
		this.torcedor.setDisabled(readOnly);
		this.numero.setDisabled(readOnly);
		this.tipo.setDisabled(readOnly);
		this.principal.setDisabled(readOnly);
		this.reiniciar();
	}

	getTo() {
		this.to.setId(this.id.get());
		this.to.torcedor = this.torcedor.get();
		this.to.numero = this.numero.get();
		this.to.tipo = this.tipo.get();
		this.to.principal = this.principal.get();
		this.to.excluido = this.excluido.get();
		this.to.registroBloqueado = this.registroBloqueado.get();
		return this.to;
	}

	setJson(obj) {
		let json = obj;
		let o = this.telefoneUtils.fromJson(json);
		this.setCampos(o);
		let itensGrid = this.telefoneConsulta.getDataSource();
		if (!Null.is(itensGrid)) {
			let itemGrid = itensGrid.byId(o.getId(), i => i.getId());
			if (!Null.is(itemGrid)) {
				this.telefoneUtils.merge(o, itemGrid);
			}
		}
		return o;
	}

	getText(o) {
		if (Null.is(o)) {
			return null;
		}
		return o.numero;
	}

	houveMudancas() {
		if (Null.is(this.original)) {
			return false;
		}
		return !this.telefoneUtils.equals(this.original, this.getTo());
	}

	camposAlterados() {
		return this.telefoneUtils.camposAlterados(this.original, this.getTo());
	}

	cancelarAlteracoes() {
		this.setCampos(this.original);
	}

	getOriginal() {
		return this.original;
	}

	setAttr(key, value) {
		if (StringCompare.eq(key, "torcedor")) {
			this.torcedor.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Torcedor")) {
			this.torcedor.setString(value);
			return;
		}
		if (StringCompare.eq(key, "numero")) {
			this.numero.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Número")) {
			this.numero.setString(value);
			return;
		}
		if (StringCompare.eq(key, "tipo")) {
			this.tipo.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Tipo")) {
			this.tipo.setString(value);
			return;
		}
		if (StringCompare.eq(key, "principal")) {
			this.principal.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Principal")) {
			this.principal.setString(value);
			return;
		}
		let message = "Campo inválido: " + key;
		throw new Error(message);
	}

	reiniciar2() {
		this.auditoria.clearItens();
	}
}
