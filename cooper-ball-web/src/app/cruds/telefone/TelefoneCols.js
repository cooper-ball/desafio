import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Coluna from '../../../fc/components/tabela/Coluna';
import StringCompare from '../../../commom/utils/string/StringCompare';
import TextAlign from '../../misc/consts/enums/TextAlign';
import UBoolean from '../../misc/utils/UBoolean';
import UIdText from '../../misc/utils/UIdText';

export default class TelefoneCols {

	getGrupos() {
		return null;
	}

	init() {
		this.telefoneConsulta.setGetCols(this);
		this.TORCEDOR = new Coluna(300, "torcedor", "Torcedor", o => o.torcedor, TextAlign.left).setSort((a, b) => UIdText.compareText(a.torcedor, b.torcedor)).setGrupo(false).setVisible(() => this.telefoneConsulta.torcedor.visibleCol.isTrue());
		this.NUMERO = new Coluna(150, "numero", "Número", o => o.numero, TextAlign.center).setSort((a, b) => StringCompare.compare(a.numero, b.numero)).setGrupo(false).setVisible(() => this.telefoneConsulta.numero.visibleCol.isTrue());
		this.TIPO = new Coluna(300, "tipo", "Tipo", o => o.tipo, TextAlign.left).setSort((a, b) => UIdText.compareText(a.tipo, b.tipo)).setGrupo(false).setVisible(() => this.telefoneConsulta.tipo.visibleCol.isTrue());
		this.PRINCIPAL = new Coluna(100, "principal", "Principal", o => o.principal, TextAlign.center).setSort((a, b) => UBoolean.compare(a.principal, b.principal)).setGrupo(false).setVisible(() => this.telefoneConsulta.principal.visibleCol.isTrue());
		this.list = ArrayLst.build(this.TORCEDOR, this.NUMERO, this.TIPO, this.PRINCIPAL);
	}

	getList() {
		return this.list.filter(o => o.isVisible());
	}
}
