import ArrayEmpty from '../../../commom/utils/array/ArrayEmpty';
import ArrayEquals from '../../../commom/utils/array/ArrayEquals';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoAlterado from '../../../fc/components/campoAlterado/CampoAlterado';
import EntityCampos from '../../../fc/components/EntityCampos';
import IdText from '../../../commom/utils/object/IdText';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringParse from '../../../commom/utils/string/StringParse';
import Telefone from './Telefone';
import UBoolean from '../../misc/utils/UBoolean';
import UEntity from '../../../fc/components/UEntity';

export default class TelefoneUtils {

	equalsPrincipal(a, b) {
		return UBoolean.eq(a.principal, b.principal);
	}

	equalsNumero(a, b) {
		return StringCompare.eqq(a.numero, b.numero);
	}

	equalsTipo(a, b) {
		return UEntity.equalsId(a.tipo, b.tipo);
	}

	equalsTorcedor(a, b) {
		return UEntity.equalsId(a.torcedor, b.torcedor);
	}

	equalsExcluido(a, b) {
		return UBoolean.eq(a.excluido, b.excluido);
	}

	equals(a, b) {
		if (Null.is(a)) {
			return Null.is(b);
		}
		if (Null.is(b)) {
			return false;
		}
		if (!this.equalsPrincipal(a, b)) {
			return false;
		}
		if (!this.equalsNumero(a, b)) {
			return false;
		}
		if (!this.equalsTipo(a, b)) {
			return false;
		}
		if (!this.equalsTorcedor(a, b)) {
			return false;
		}
		if (!this.equalsExcluido(a, b)) {
			return false;
		}
		return true;
	}

	camposAlterados(a, b) {
		let list = new ArrayLst();
		if (!this.equalsPrincipal(a, b)) {
			list.add(new CampoAlterado().setKey("principal").setCampo("Principal").setDe(StringParse.get(a.principal)).setPara(StringParse.get(b.principal)));
		}
		if (!this.equalsNumero(a, b)) {
			list.add(new CampoAlterado().setKey("numero").setCampo("Número").setDe(a.numero).setPara(b.numero));
		}
		if (!this.equalsTipo(a, b)) {
			list.add(new CampoAlterado().setKey("tipo").setCampo("Tipo").setDe(StringParse.get(a.tipo)).setPara(StringParse.get(b.tipo)));
		}
		if (!this.equalsTorcedor(a, b)) {
			list.add(new CampoAlterado().setKey("torcedor").setCampo("Torcedor").setDe(StringParse.get(a.torcedor)).setPara(StringParse.get(b.torcedor)));
		}
		if (!this.equalsExcluido(a, b)) {
			list.add(new CampoAlterado().setCampo("Excluído"));
		}
		return list;
	}

	equalsList(a, b) {
		return ArrayEquals.isT(a, b, (x, y) => this.equals(x, y));
	}

	fromJson(json) {
		if (Null.is(json)) return null;
		let o = new Telefone();
		o.setId(json.id);
		if (!Null.is(json.torcedor)) {
			o.torcedor = new IdText(json.torcedor.id, json.torcedor.text);
		}
		if (!Null.is(json.numero)) {
			o.numero = json.numero;
		}
		if (!Null.is(json.tipo)) {
			o.tipo = new IdText(json.tipo.id, json.tipo.text);
		}
		if (!Null.is(json.principal)) {
			o.principal = json.principal;
		}
		o.excluido = json.excluido;
		o.registroBloqueado = json.registroBloqueado;
		return o;
	}

	fromJsonList(jsons) {
		if (Null.is(jsons)) return null;
		return jsons.map(o => this.fromJson(o));
	}

	merge(de, para) {
		para.setId(de.getId());
		para.torcedor = de.torcedor;
		para.numero = de.numero;
		para.tipo = de.tipo;
		para.principal = de.principal;
		para.excluido = de.excluido;
		para.registroBloqueado = de.registroBloqueado;
	}

	clonar(obj) {
		if (Null.is(obj)) {
			return null;
		}
		let o = new Telefone();
		o.original = obj;
		this.merge(obj, o);
		return o;
	}

	clonarList(array) {
		if (ArrayEmpty.is(array)) {
			return null;
		} else {
			return array.map(o => this.clonar(o));
		}
	}

	novo() {
		let o = new Telefone();
		o.setId(--EntityCampos.novos);
		o.excluido = false;
		o.registroBloqueado = false;
		return o;
	}
}
