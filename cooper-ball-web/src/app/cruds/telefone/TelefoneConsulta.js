import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Consulta from '../../../fc/components/consulta/Consulta';
import ConsultaOperadorConstantes from '../consultaOperador/ConsultaOperadorConstantes';
import Null from '../../../commom/utils/object/Null';
import TelefoneTipoConstantes from '../telefoneTipo/TelefoneTipoConstantes';
import VConsultaList from '../../../fc/components/VConsultaList';

export default class TelefoneConsulta extends Consulta {

	grupoGeral = this.newSeparacaoDeGrupo("grupoGeral", "Geral");
	torcedor = this.newFk("torcedor", "Torcedor", "Torcedor", true);
	numero = this.newString("numero", "Número", 50, true);
	tipo = this.newList("tipo", "Tipo", TelefoneTipoConstantes.LIST, true);
	principal = this.newBoolean("principal", "Principal", true);
	excluido = this.newBoolean("excluido", "Excluído", true);

	init2() {
		this.nomeEntidade = "Telefone";
		this.dados = new VConsultaList(
			"dados",
			(a, b) => {},
			body => {
				let result = body;
				let array = result.dados;
				let list = new ArrayLst(array);
				this.dados.addItens(this.telefoneUtils.fromJsonList(list));
				this.refreshConsulta(result);
			},
			"Telefone/consulta",
			() => this.getTo(),
			this.fcAxios
		);
		this.excluido.setDefaultValue(ConsultaOperadorConstantes.NAO);
		this.titleForm.set("Telefone");
	}

	getTo() {
		return this;
	}

	consultarImpl() {
		this.dados.clearItens();
		this.dados.carregar();
	}

	getDataSource() {
		if (Null.is(this.dados)) {
			return new ArrayLst();
		} else {
			return this.dados.getItens();
		}
	}
}
