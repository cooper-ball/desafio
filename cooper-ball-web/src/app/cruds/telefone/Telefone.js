import EntityFront from '../../../fc/components/EntityFront';
import Null from '../../../commom/utils/object/Null';

export default class Telefone extends EntityFront {

	torcedor = null;
	numero = null;
	tipo = null;
	principal = false;
	excluido = false;
	registroBloqueado = false;

	getText() {
		return this.numero;
	}

	asString() {
		let s = "{";
		s += "\"id\":"+this.getId()+",";
		if (!Null.is(this.torcedor)) {
			s += "\"torcedor\":"+this.torcedor.id+",";
		}
		if (!Null.is(this.numero)) {
			s += "\"numero\":\""+this.numero+"\",";
		}
		if (!Null.is(this.tipo)) {
			s += "\"tipo\":"+this.tipo.id+",";
		}
		if (!Null.is(this.principal)) {
			s += "\"principal\":\""+this.principal+"\",";
		}
		s += "\"excluido\":"+this.excluido+",";
		s += "}";
		return s;
	}
}
