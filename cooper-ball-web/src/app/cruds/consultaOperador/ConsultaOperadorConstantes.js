import ArrayLst from '../../../commom/utils/array/ArrayLst';
import IdText from '../../../commom/utils/object/IdText';

export default class ConsultaOperadorConstantes {}
ConsultaOperadorConstantes.IGUAL = new IdText(1, "Igual");
ConsultaOperadorConstantes.COMECA_COM = new IdText(2, "Começa com");
ConsultaOperadorConstantes.CONTEM = new IdText(3, "Contém");
ConsultaOperadorConstantes.TERMINA_COM = new IdText(4, "Termina com");
ConsultaOperadorConstantes.MAIOR_OU_IGUAL = new IdText(5, "Maior ou igual");
ConsultaOperadorConstantes.MENOR_OU_IGUAL = new IdText(6, "Menor ou igual");
ConsultaOperadorConstantes.ENTRE = new IdText(7, "Entre");
ConsultaOperadorConstantes.EM = new IdText(8, "Em");
ConsultaOperadorConstantes.HOJE = new IdText(10, "Hoje");
ConsultaOperadorConstantes.HOJE_MENOS = new IdText(11, "Hoje menos");
ConsultaOperadorConstantes.HOJE_MAIS = new IdText(12, "Hoje mais");
ConsultaOperadorConstantes.VAZIOS = new IdText(13, "Vazios");
ConsultaOperadorConstantes.MAIS_OPCOES = new IdText(15, "Mais opções");
ConsultaOperadorConstantes.DESMEMBRAR = new IdText(16, "Desmembrar");
ConsultaOperadorConstantes.SIM = new IdText(17, "Sim");
ConsultaOperadorConstantes.NAO = new IdText(18, "Não");
ConsultaOperadorConstantes.DIFERENTE = new IdText(1001, "Diferente");
ConsultaOperadorConstantes.NAO_COMECA_COM = new IdText(1002, "Não Começa com");
ConsultaOperadorConstantes.NAO_CONTEM = new IdText(1003, "Não Contém");
ConsultaOperadorConstantes.NAO_TERMINA_COM = new IdText(1004, "Não Termina com");
ConsultaOperadorConstantes.MENOR_QUE = new IdText(1005, "Menor que");
ConsultaOperadorConstantes.MAIOR_QUE = new IdText(1006, "Maior que");
ConsultaOperadorConstantes.NAO_ENTRE = new IdText(1007, "Não Entre");
ConsultaOperadorConstantes.NAO_EM = new IdText(1008, "Não Em");
ConsultaOperadorConstantes.NAO_VAZIOS = new IdText(1013, "Não Vazios");
ConsultaOperadorConstantes.TODOS = new IdText(9999, "Todos");
ConsultaOperadorConstantes.LIST = ArrayLst.build(
	ConsultaOperadorConstantes.IGUAL,
	ConsultaOperadorConstantes.COMECA_COM,
	ConsultaOperadorConstantes.CONTEM,
	ConsultaOperadorConstantes.TERMINA_COM,
	ConsultaOperadorConstantes.MAIOR_OU_IGUAL,
	ConsultaOperadorConstantes.MENOR_OU_IGUAL,
	ConsultaOperadorConstantes.ENTRE,
	ConsultaOperadorConstantes.EM,
	ConsultaOperadorConstantes.HOJE,
	ConsultaOperadorConstantes.HOJE_MENOS,
	ConsultaOperadorConstantes.HOJE_MAIS,
	ConsultaOperadorConstantes.VAZIOS,
	ConsultaOperadorConstantes.MAIS_OPCOES,
	ConsultaOperadorConstantes.DESMEMBRAR,
	ConsultaOperadorConstantes.SIM,
	ConsultaOperadorConstantes.NAO,
	ConsultaOperadorConstantes.DIFERENTE,
	ConsultaOperadorConstantes.NAO_COMECA_COM,
	ConsultaOperadorConstantes.NAO_CONTEM,
	ConsultaOperadorConstantes.NAO_TERMINA_COM,
	ConsultaOperadorConstantes.MENOR_QUE,
	ConsultaOperadorConstantes.MAIOR_QUE,
	ConsultaOperadorConstantes.NAO_ENTRE,
	ConsultaOperadorConstantes.NAO_EM,
	ConsultaOperadorConstantes.NAO_VAZIOS,
	ConsultaOperadorConstantes.TODOS
);
