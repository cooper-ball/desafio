import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Coluna from '../../../fc/components/tabela/Coluna';
import StringCompare from '../../../commom/utils/string/StringCompare';
import TextAlign from '../../misc/consts/enums/TextAlign';

export default class TelefoneTipoCols {

	getGrupos() {
		return null;
	}

	init() {
		this.telefoneTipoConsulta.setGetCols(this);
		this.NOME = new Coluna(100, "nome", "Nome", o => o.nome, TextAlign.left).setSort((a, b) => StringCompare.compare(a.nome, b.nome)).setGrupo(false).setVisible(() => this.telefoneTipoConsulta.nome.visibleCol.isTrue());
		this.list = ArrayLst.build(this.NOME);
	}

	getList() {
		return this.list.filter(o => o.isVisible());
	}
}
