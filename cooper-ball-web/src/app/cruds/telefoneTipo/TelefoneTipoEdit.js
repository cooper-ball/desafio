import React from 'react';
import FormEdit from '../../../fc/components/FormEdit';
import FormItemInput from '../../../antd/form/FormItemInput';
import LayoutApp from '../../../fc/components/LayoutApp';
import StringCompare from '../../../commom/utils/string/StringCompare';
import {Card} from 'antd';
import {Col} from 'antd';
import {Row} from 'antd';
import {Tabs} from 'antd';
const TabPane = Tabs.TabPane;

export default class TelefoneTipoEdit extends FormEdit {

	constructor(props) {
		super(props);
		this.init("TelefoneTipo");
	}

	getTabs() {
		return (
			<Tabs
			tabPosition={"top"}
			activeKey={this.state.abaSelecionada}
			defaultActiveKey={"Geral"}
			onChange={s => this.setAbaSelecionada(s)}>
				<TabPane key={"Geral"} tab={"Geral"}>
					{this.abaGeral()}
				</TabPane>
			</Tabs>
		);
	}

	abaGeral() {
		if (!StringCompare.eq(this.state.abaSelecionada, "Geral")) {
			return null;
		}
		return (
			<Row gutter={24}>
				{this.grupo_geral_0()}
			</Row>
		);
	}

	inputNome() {
		return <FormItemInput bind={this.campos.nome} lg={24}/>;
	}

	grupo_geral_0() {
		if (!this.campos.nome.isVisible()) {
			return null;
		}
		return (
			<Col lg={24} md={24} sm={24}>
				<Card size={"small"} style={LayoutApp.EMPTY.get()}>
					<Row gutter={24}>
						{this.inputNome()}
					</Row>
				</Card>
			</Col>
		);
	}

	getTitleImpl() {
		return "Telefone Tipo";
	}

	getCampos() {
		return this.campos;
	}

	getPermissoes() {
		return this.permissoes;
	}
	setWidthForm = o => this.setState({widthForm:o});
	setAbaSelecionada = o => this.setState({abaSelecionada:o});
}

TelefoneTipoEdit.defaultProps = FormEdit.defaultProps;
