import Binding from '../../campos/support/Binding';
import EntityCampos from '../../../fc/components/EntityCampos';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';

export default class TelefoneTipoCampos extends EntityCampos {

	getEntidade() {
		return "TelefoneTipo";
	}

	getEntidadePath() {
		return "TelefoneTipo";
	}

	initImpl() {
		this.nome = this.newString("Nome", 50, true, "Geral").setDisabled(true);
		this.construido = true;
	}

	setCampos(o) {
		if (Null.is(o)) {
			throw new Error("TelefoneTipoCampos:o is null");
		}
		Binding.notificacoesDesligadasInc();
		this.disabledObservers = true;
		this.original = o;
		this.to = this.telefoneTipoUtils.clonar(o);
		this.id.clear();
		this.id.set(this.to.id);
		this.nome.set(this.to.nome);
		this.excluido.set(this.to.excluido);
		this.registroBloqueado.set(this.to.registroBloqueado);
		this.id.setStartValue(this.to.id);
		this.nome.setStartValue(this.nome.get());
		this.excluido.setStartValue(this.excluido.get());
		this.registroBloqueado.setStartValue(this.registroBloqueado.get());
		this.reiniciar();
	}

	getTo() {
		this.to.setId(this.id.get());
		this.to.nome = this.nome.get();
		this.to.excluido = this.excluido.get();
		this.to.registroBloqueado = this.registroBloqueado.get();
		return this.to;
	}

	setJson(obj) {
		let json = obj;
		let o = this.telefoneTipoUtils.fromJson(json);
		this.setCampos(o);
		let itensGrid = this.telefoneTipoConsulta.getDataSource();
		if (!Null.is(itensGrid)) {
			let itemGrid = itensGrid.byId(o.getId(), i => i.getId());
			if (!Null.is(itemGrid)) {
				this.telefoneTipoUtils.merge(o, itemGrid);
			}
		}
		return o;
	}

	getText(o) {
		if (Null.is(o)) {
			return null;
		}
		return o.nome;
	}

	houveMudancas() {
		if (Null.is(this.original)) {
			return false;
		}
		return !this.telefoneTipoUtils.equals(this.original, this.getTo());
	}

	camposAlterados() {
		return this.telefoneTipoUtils.camposAlterados(this.original, this.getTo());
	}

	cancelarAlteracoes() {
		this.setCampos(this.original);
	}

	getOriginal() {
		return this.original;
	}

	setAttr(key, value) {
		if (StringCompare.eq(key, "nome")) {
			this.nome.setString(value);
			return;
		}
		if (StringCompare.eq(key, "Nome")) {
			this.nome.setString(value);
			return;
		}
		let message = "Campo inválido: " + key;
		throw new Error(message);
	}
}
