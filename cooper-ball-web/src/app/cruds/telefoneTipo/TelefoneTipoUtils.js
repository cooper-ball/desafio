import ArrayEmpty from '../../../commom/utils/array/ArrayEmpty';
import ArrayEquals from '../../../commom/utils/array/ArrayEquals';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoAlterado from '../../../fc/components/campoAlterado/CampoAlterado';
import EntityCampos from '../../../fc/components/EntityCampos';
import Null from '../../../commom/utils/object/Null';
import TelefoneTipo from './TelefoneTipo';
import UBoolean from '../../misc/utils/UBoolean';

export default class TelefoneTipoUtils {

	equalsExcluido(a, b) {
		return UBoolean.eq(a.excluido, b.excluido);
	}

	equals(a, b) {
		if (Null.is(a)) {
			return Null.is(b);
		}
		if (Null.is(b)) {
			return false;
		}
		if (!this.equalsExcluido(a, b)) {
			return false;
		}
		return true;
	}

	camposAlterados(a, b) {
		let list = new ArrayLst();
		if (!this.equalsExcluido(a, b)) {
			list.add(new CampoAlterado().setCampo("Excluído"));
		}
		return list;
	}

	equalsList(a, b) {
		return ArrayEquals.isT(a, b, (x, y) => this.equals(x, y));
	}

	fromJson(json) {
		if (Null.is(json)) return null;
		let o = new TelefoneTipo();
		o.setId(json.id);
		if (!Null.is(json.nome)) {
			o.nome = json.nome;
		}
		o.excluido = json.excluido;
		o.registroBloqueado = json.registroBloqueado;
		return o;
	}

	fromJsonList(jsons) {
		if (Null.is(jsons)) return null;
		return jsons.map(o => this.fromJson(o));
	}

	merge(de, para) {
		para.setId(de.getId());
		para.nome = de.nome;
		para.excluido = de.excluido;
		para.registroBloqueado = de.registroBloqueado;
	}

	clonar(obj) {
		if (Null.is(obj)) {
			return null;
		}
		let o = new TelefoneTipo();
		o.original = obj;
		this.merge(obj, o);
		return o;
	}

	clonarList(array) {
		if (ArrayEmpty.is(array)) {
			return null;
		} else {
			return array.map(o => this.clonar(o));
		}
	}

	novo() {
		let o = new TelefoneTipo();
		o.setId(--EntityCampos.novos);
		o.excluido = false;
		o.registroBloqueado = false;
		return o;
	}
}
