import EntityFront from '../../../fc/components/EntityFront';

export default class TelefoneTipo extends EntityFront {

	nome = null;
	excluido = false;
	registroBloqueado = false;

	getText() {
		return this.nome;
	}

	asString() {
		let s = "{";
		s += "\"id\":"+this.getId()+",";
		s += "\"excluido\":"+this.excluido+",";
		s += "}";
		return s;
	}
}
