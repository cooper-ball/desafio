import ArrayLst from '../../../commom/utils/array/ArrayLst';
import IdText from '../../../commom/utils/object/IdText';

export default class TelefoneTipoConstantes {}
TelefoneTipoConstantes.RESIDENCIAL = new IdText(1, "Residencial");
TelefoneTipoConstantes.COMERCIAL = new IdText(2, "Comercial");
TelefoneTipoConstantes.CELULAR = new IdText(3, "Celular");
TelefoneTipoConstantes.LIST = ArrayLst.build(
	TelefoneTipoConstantes.RESIDENCIAL,
	TelefoneTipoConstantes.COMERCIAL,
	TelefoneTipoConstantes.CELULAR
);
