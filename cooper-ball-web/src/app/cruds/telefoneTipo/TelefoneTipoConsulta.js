import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Consulta from '../../../fc/components/consulta/Consulta';
import ConsultaOperadorConstantes from '../consultaOperador/ConsultaOperadorConstantes';
import Null from '../../../commom/utils/object/Null';
import VConsultaList from '../../../fc/components/VConsultaList';

export default class TelefoneTipoConsulta extends Consulta {

	grupoGeral = this.newSeparacaoDeGrupo("grupoGeral", "Geral");
	nome = this.newString("nome", "Nome", 50, true);
	excluido = this.newBoolean("excluido", "Excluído", true);
	registroBloqueado = this.newBoolean("registroBloqueado", "Registro Bloqueado", true);

	init2() {
		this.nomeEntidade = "TelefoneTipo";
		this.dados = new VConsultaList(
			"dados",
			(a, b) => {},
			body => {
				let result = body;
				let array = result.dados;
				let list = new ArrayLst(array);
				this.dados.addItens(this.telefoneTipoUtils.fromJsonList(list));
				this.refreshConsulta(result);
			},
			"TelefoneTipo/consulta",
			() => this.getTo(),
			this.fcAxios
		);
		this.excluido.setDefaultValue(ConsultaOperadorConstantes.NAO);
		this.titleForm.set("Telefone Tipo");
	}

	getTo() {
		return this;
	}

	consultarImpl() {
		this.dados.clearItens();
		this.dados.carregar();
	}

	getDataSource() {
		if (Null.is(this.dados)) {
			return new ArrayLst();
		} else {
			return this.dados.getItens();
		}
	}
}
