import Equals from '../../../commom/utils/object/Equals';
import Null from '../../../commom/utils/object/Null';

export default class Uncycle {

	static selectedCombo;

	static setSelectedCombo(value) {
		if (Equals.is(Uncycle.selectedCombo, value)) {
			return;
		} else {
			if (!Null.is(Uncycle.selectedCombo)) {
				Uncycle.selectedCombo.forceUpdate();
			}
			Uncycle.selectedCombo = value;
			if (!Null.is(Uncycle.selectedCombo)) {
				Uncycle.selectedCombo.forceUpdate();
			}
		}
	}

	static getSelectedCombo() {
		return Uncycle.selectedCombo;
	}

}
