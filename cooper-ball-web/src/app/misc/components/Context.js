import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Null from '../../../commom/utils/object/Null';

export default class Context {

	static createCDI;
	static criando = false;

	static injetarFila = new ArrayLst();
	static injetarFathersFila = new ArrayLst();
	static injetarObserversFila = new ArrayLst();

	static injetar(o) {
		if (Context.criando) {
			Context.injetarFila.add(o);
			return;
		}
		if (!Null.is(Context.createCDI)) {
			let cdi = Context.getContext();
			cdi.injetar(o);
		}
	}

	static injetarObservers(o) {
		if (Context.criando || Null.is(Context.createCDI)) {
			Context.injetarObserversFila.add(o);
			return;
		}
		Context.getContext().injetarObservers(o);
	}

	static injetarFathers(o) {
		if (Context.criando || Null.is(Context.createCDI)) {
			Context.injetarFathersFila.add(o);
			return;
		}
		Context.getContext().injetarFathers(o);
	}

	static instance;

	static getContext() {

		if (Null.is(Context.instance)) {
			Context.criando = true;
			Context.instance = Context.createCDI();
			Context.init(Context.instance);
		}

		return Context.instance;

	}

	static init(cdi) {
		cdi.init();
		Context.criando = false;
		Context.injetarFila.forEach(o => cdi.injetar(o));
		Context.injetarFathersFila.forEach(o => cdi.injetarFathers(o));
		Context.injetarObserversFila.forEach(o => cdi.injetarObservers(o));
	}

	static get(classe) {
		return Context.getContext().get(classe);
	}

}
