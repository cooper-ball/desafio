import React from 'react';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import ClassSimpleName from '../../../commom/utils/classe/ClassSimpleName';
import Console from '../utils/Console';
import Context from './Context';
import Foreach from '../../../react/Foreach';
import Null from '../../../commom/utils/object/Null';
import Obrig from '../../../commom/utils/object/Obrig';
import StartPrototypes from '../utils/StartPrototypes';
import Style from '../utils/Style';

export default class SuperComponent extends React.PureComponent {
	injetarFathers = true;

	static idSuperComponentCount = 0;
	idSuperComponent = 0;

	static tempos = [];

	parent;

	constructor(props) {
		super(props);
		this.state = {};
		this.thisClassName = ClassSimpleName.exec(this);
		this.idSuperComponent = SuperComponent.idSuperComponentCount++;
		Context.injetar(this);
		this.stringId = this.thisClassName + this.idSuperComponent;
	}

	render() {

		if (this.injetarFathers) {
			Context.injetarFathers(this);
			this.injetarFathers = false;
		}

		return this.putParent(this.render0());
	}

	putParent(rs) {

		if (Null.is(rs)) {
			return rs;
		}

		let newProps = {};
		newProps.parent = this;

		return React.Children.map(rs, child => {
            if (React.isValidElement(child)) {
                return React.cloneElement(child, newProps);
            } else {
            	return child;
            }
        });

	}

	se(condicao, funcaoSeTrue) {
		if (condicao) {
			return funcaoSeTrue();
		} else {
			return null;
		}
	}

	getTempoAceitavelParaRenderizacao() {
		return 200;
	}

	componentWillUnmount() {
		if (!Null.is(this.itensQueObservo)) {
			this.itensQueObservo.forEach(o => o.removeRenderObserver(this));
		}
		this.componentWillUnmount2();
	}

	componentWillUnmount2() {}

	observar(o) {
		Obrig.checkWithMessage(o, this.thisClassName + ".observar: o is null");
		if (Null.is(this.itensQueObservo)) {
			this.itensQueObservo = new ArrayLst();
		}
		this.itensQueObservo.addIfNotContains(o);
		o.addRenderObserver(this);
	}

	log(o) {
		Console.log(this.thisClassName, o);
	}

	logError(o) {
		Console.error(this.thisClassName, o);
	}

	map(array,mapFunction) {
		return Foreach.get(array, mapFunction);
	}

	mapi(array, mapFunction) {
		return Foreach.geti(array, mapFunction);
	}

	getParent() {
		return this.props.parent;
	}

	getClassSimpleName() {
		return this.thisClassName;
	}

	newStyle() {
		return Style.create().setScala(this.getScala());
	}

	componentDidMount() {
		if (!Null.is(this.props.refreshOnChange)) {
			this.observar(this.props.refreshOnChange);
		}
		Context.injetarObservers(this);
		this.didMount();
	}

	didMount() {}

	getScala() {
		if (Null.is(this.props.scala)) {
			if (Null.is(this.props.parent)) {
				return 1.0;
			} else {
				return this.props.parent.getScala();
			}
		} else {
			return this.props.scala;
		}
	}

}
SuperComponent.start = StartPrototypes.exec();

SuperComponent.defaultProps = React.PureComponent.defaultProps;
