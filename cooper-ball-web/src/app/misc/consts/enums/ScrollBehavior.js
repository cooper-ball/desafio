export default class ScrollBehavior {
	static auto = 'auto';
	static smooth = 'smooth';
	static initial = 'initial';
	static inherit = 'inherit';
}
