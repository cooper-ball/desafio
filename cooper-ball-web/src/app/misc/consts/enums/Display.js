export default class Display {
	static none = 'none';
	static flex = 'flex';
	static block = 'block';
	static inlineFlex = 'inline-flex';
	static inlineBlock = 'inline-block';
	static initial = 'initial';
}
