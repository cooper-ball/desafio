import ArrayLst from '../../../commom/utils/array/ArrayLst';
import StringAfterFirst from '../../../commom/utils/string/StringAfterFirst';
import StringBeforeFirst from '../../../commom/utils/string/StringBeforeFirst';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringContains from '../../../commom/utils/string/StringContains';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiCaracteres from '../../../commom/utils/string/StringExtraiCaracteres';
import StringLength from '../../../commom/utils/string/StringLength';
import StringOcorrencias from '../../../commom/utils/string/StringOcorrencias';
import StringReplace from '../../../commom/utils/string/StringReplace';
import StringRight from '../../../commom/utils/string/StringRight';
import UConstantes from './UConstantes';

export default class UEmail {

	static CARACTERES_VALIDOS =
		UConstantes.numeros
		.concat(UConstantes.letrasMinusculas)
		.concat(ArrayLst.build(".","@","-","_"))
	;

	static EXTREMIDADES_VALIDAS =
		UConstantes.numeros
		.concat(UConstantes.letrasMinusculas)
		.concat(ArrayLst.build("_"))
	;

	static formatParcial(s) {

		if (StringEmpty.is(s)) {
			return "";
		}

		s = s.toLowerCase();
		s = StringExtraiCaracteres.exec(s, UEmail.CARACTERES_VALIDOS);

		s = StringReplace.exec(s, ".-", ".");
		s = StringReplace.exec(s, "-.", "-");
		s = StringReplace.exec(s, "..", ".");
		s = StringReplace.exec(s, "--", "-");

		while (!StringEmpty.is(s) && !UEmail.EXTREMIDADES_VALIDAS.contains(s.substring(0, 1)) ) {
			s = s.substring(1);
		}

		if (UEmail.isValid(s)) {
			return s;
		}

		if (StringEmpty.is(s)) {
			return "";
		}

		if (StringLength.get(s) > 60) {
			s = s.substring(0, 60);
		}

		if (!StringContains.is(s, "@")) {
			return s;
		}

		let before = StringBeforeFirst.get(s, "@");

		while (!StringEmpty.is(before) && !UEmail.EXTREMIDADES_VALIDAS.contains(StringRight.get(before, 1)) ) {
			before = StringRight.ignore1(before);
		}

		let after = StringAfterFirst.get(s, "@");
		after = StringReplace.exec(after, "@", "");

		while (!StringEmpty.is(after) && !UEmail.EXTREMIDADES_VALIDAS.contains(after.substring(0, 1)) ) {
			after = after.substring(1);
		}

		if (StringEmpty.is(before)) {
			return after;
		} else {
			return before + "@" + after;
		}

	}

	static isValid(s) {

		if (StringEmpty.is(s)) {
			return false;
		}

		let semCaracteresInvalidos = StringExtraiCaracteres.exec(s, UEmail.CARACTERES_VALIDOS);

		if (!StringCompare.eq(semCaracteresInvalidos, s)) {
			return false;
		}

		if (!StringContains.is(s, "@")) {
			return false;
		}

		if (StringContains.is(s, "..") || StringContains.is(s, "--")) {
			return false;
		}

		if (StringOcorrencias.get(s, "@") > 1) {
			return false;
		}

		if (StringLength.get(s) < 3) {
			return false;
		}

		let first = s.substring(0,1);

		if (!UEmail.EXTREMIDADES_VALIDAS.contains(first)) {
			return false;
		}

		let right = StringRight.get(s, 1);

		if (!UEmail.EXTREMIDADES_VALIDAS.contains(right)) {
			return false;
		}

		let before = StringBeforeFirst.get(s, "@");

		right = StringRight.get(before, 1);

		if (!UEmail.EXTREMIDADES_VALIDAS.contains(right)) {
			return false;
		}

		let after = StringAfterFirst.get(s, "@");

		first = after.substring(0,1);

		if (!UEmail.EXTREMIDADES_VALIDAS.contains(first)) {
			return false;
		}

		if (StringContains.is(after, "@")) {
			return false;
		}

		if (StringLength.get(s) > 60) {
			return false;
		}

		return true;

	}

}
