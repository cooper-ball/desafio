
export default class StartPrototypes {
	static exec () {}
}

const get = (Classe) => {
	return Classe.prototype;
}

get(Array).get = function(index) {
	return this[index];
}

get(Error).getMessage = function() {
	return this.message;
}

try {
	get(TouchList).get = function(index) {
		return this[index];
	}
} catch (error) {}
