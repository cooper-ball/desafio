import BorderStyle from '../consts/enums/BorderStyle';
import Color from '../consts/enums/Color';

export default class Border {
	color = Color.cinzaClaro2;
	style = BorderStyle.solid;
	width = 0.5;
	radius = null;
	radiusAlterado = false;
	withColor(o){this.color = o; return this;}
	withStyle(o){this.style = o; return this;}
	withWidth(o){this.width = o; return this;}

	withRadius(o){
		this.radius = o;
		this.radiusAlterado = true;
		return this;
	}

}
