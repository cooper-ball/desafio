import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Caracteres from '../consts/Caracteres';
import IntegerFormat from '../../../commom/utils/integer/IntegerFormat';
import Null from '../../../commom/utils/object/Null';
import StringAfterFirst from '../../../commom/utils/string/StringAfterFirst';
import StringAfterLast from '../../../commom/utils/string/StringAfterLast';
import StringBeforeFirst from '../../../commom/utils/string/StringBeforeFirst';
import StringBeforeLast from '../../../commom/utils/string/StringBeforeLast';
import StringBox from '../../../commom/utils/string/StringBox';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringContains from '../../../commom/utils/string/StringContains';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringEquivalente from '../../../commom/utils/string/StringEquivalente';
import StringLength from '../../../commom/utils/string/StringLength';
import StringLike from '../../../commom/utils/string/StringLike';
import StringOcorrencias from '../../../commom/utils/string/StringOcorrencias';
import StringPrimeiraMaiuscula from '../../../commom/utils/string/StringPrimeiraMaiuscula';
import StringRemoveAcentos from '../../../commom/utils/string/StringRemoveAcentos';
import StringRepete from '../../../commom/utils/string/StringRepete';
import StringReplace from '../../../commom/utils/string/StringReplace';
import StringRight from '../../../commom/utils/string/StringRight';
import StringTrim from '../../../commom/utils/string/StringTrim';

export default class UString {

	static notEmpty(s) {
		return !StringEmpty.is(s);
	}

	static replaceWhile(s, a, b) {
		return StringReplace.exec(s, a, b);
	}

	static replace(s, a, b) {
		return StringReplace.exec(s, a, b);
	}

	static contains(a, b) {
		return StringContains.is(a, b);
	}

	static preparaParaBusca(s) {

		if (StringEmpty.is(s)) {
			return "";
		}

		s = StringTrim.plus(s);

		if (StringEmpty.is(s)) {
			return "";
		}

		s = UString.removeAcentos(s);
		s = s.toLowerCase();

		return s;

	}

	static removeAcentos(s) {
		return StringRemoveAcentos.exec(s);
	}

	static beforeFirst(s, substring) {
		return StringBeforeFirst.get(s, substring);
	}

	static afterFirst(s, substring) {
		return StringAfterFirst.get(s, substring);
	}

	static highlights(original, substring) {

		let list = new ArrayLst();

		if (StringEmpty.is(original)) {
			return list;
		}

		if (!StringEmpty.is(substring)) {
			let s = UString.preparaParaBusca(original);
			let s2 = UString.preparaParaBusca(substring);
			while (!StringEmpty.is(s) && StringContains.is(s, s2)) {
				let before = StringBeforeFirst.get(s, s2);
				if (!StringEmpty.is(before)) {
					before = original.substring(0, before.length);
					list.push({text: before, highlight: false});
					original = original.substring(before.length);
				}
				list.push({text: original.substring(0, substring.length), highlight: true});
				s = StringAfterFirst.get(s, s2);
				original = original.substring(substring.length);
			}
		}

		list.push({text: original, highlight: false});

		return list;
	}

	static format00(numero, casas) {
		if (Null.is(casas)) {
			return IntegerFormat.xx(numero);
		} else {
			return IntegerFormat.zerosEsquerda(numero, casas);
		}
	}

	static afterLast(s, substring) {
		return StringAfterLast.get(s, substring);
	}

	static beforeLast(s, substring) {
		return StringBeforeLast.get(s, substring);
	}

	static afterFirstObrig(s, substring) {
		return StringAfterFirst.obrig(s, substring);
	}

	static length(s) {
		return StringLength.get(s);
	}

	static lengthIs(s, i) {
		return StringLength.is(s, i);
	}

	static right(s, count) {
		return StringRight.get(s, count);
	}

	static ocorrencias(s, substring) {
		return StringOcorrencias.get(s, substring);
	}

	static ignoreRight(s, count) {
		return StringRight.ignore(s, count);
	}

	static ltrim(s) {
		return StringTrim.left(s);
	}

	static repete(s, vezes) {
		return StringRepete.exec(s, vezes);
	}

	static maxLength(s, max) {
		return StringLength.max(s, max);
	}

	static quebraAoMeio(s) {

		s = StringTrim.plus(s);

		if (StringEmpty.is(s)) {
			return ArrayLst.build("", "");
		}

		if (!StringContains.is(s, " ")) {
			return ArrayLst.build(s, "");
		}

		let box = new StringBox(s);

		Caracteres.letrasMaiusculas.forEach(o => box.replace(o, o+"^^"));
		Caracteres.letrasMinusculas.forEach(o => box.replace(o, o+"^"));

		box.replace("i^", "i");
		box.replace("l^", "l");
		box.replace("f^", "f");
		box.replace("t^", "t");
		box.replace("I^^", "I");

		s = box.get();

		let len = StringLength.get(s) / 2;

		let left = s.substring(0, len);
		let right = s.substring(len);

		left = StringReplace.exec(left, "^", "");
		right = StringReplace.exec(right, "^", "");
		s = StringReplace.exec(s, "^", "");

		if (left.endsWith(" ") || right.startsWith(" ")) {
			return ArrayLst.build(left.trim(), right.trim());
		}

		if (!left.contains(" ") || !right.contains(" ")) {
			left = StringBeforeFirst.get(s, " ");
			right = StringAfterFirst.get(s, " ");
			return ArrayLst.build(left, right);
		}

		left = StringBeforeLast.get(left, " ");
		right = StringAfterFirst.get(s, left + " ");

		return ArrayLst.build(left, right);

	}

	static compare(a, b) {
		return StringCompare.compare(a, b);
	}

	static isString(o) {
		return StringCompare.eq(typeof(o), "string");
	}

	static like(a, b) {
		return StringLike.is(a, b);
	}

	static primeiraMaiuscula(s) {
		return StringPrimeiraMaiuscula.exec(s);
	}

	static equivalente(a, b) {
		return StringEquivalente.is(a, b);
	}

}
