import AlignItens from '../consts/enums/AlignItens';
import Display from '../consts/enums/Display';
import FlexDirection from '../consts/enums/FlexDirection';
import FontStyle from '../consts/enums/FontStyle';
import FontWeight from '../consts/enums/FontWeight';
import JustifyContent from '../consts/enums/JustifyContent';
import Null from '../../../commom/utils/object/Null';
import Overflow from '../consts/enums/Overflow';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringParse from '../../../commom/utils/string/StringParse';
import StringRight from '../../../commom/utils/string/StringRight';
import TextAlign from '../consts/enums/TextAlign';
import TextDecorationLine from '../consts/enums/TextDecorationLine';
import UDouble from './UDouble';

export default class Style {

	static modelo;

	static create() {

		let result;

		if (Null.is(Style.modelo)) {
			result = new Style();
			result.o = {};
		} else {
			result = Style.modelo.copy();
		}

		return result;
	}

	static getResult(st) {
		if (Null.is(st)) {
			return null;
		} else {
			return st.get();
		}
	}

	sombra(x, y, radius, cor) {
		this.boxShadow(x + "px " + y + "px " + radius + "px " + cor);
		return this;
	}

	transitionEase(duration) {
		return this.transition("."+duration+"s ease");
	}

	border(value) {
		this.borderTop(value);
		this.borderBottom(value);
		this.borderLeft(value);
		this.borderRight(value);
		return this;
	}

	borderBottom(value) {

		this.borderBottomColor(value.color);
		this.borderBottomStyle(value.style);
		this.borderBottomWidth(value.width);

		if (value.radiusAlterado) {
			this.borderBottomLeftRadius(value.radius);
			this.borderBottomRightRadius(value.radius);
		}

		return this;

	}

	borderTop(value) {

		this.borderTopColor(value.color);
		this.borderTopStyle(value.style);
		this.borderTopWidth(value.width);

		if (value.radiusAlterado) {
			this.borderTopLeftRadius(value.radius);
			this.borderTopRightRadius(value.radius);
		}

		return this;

	}

	borderLeft(value) {

		this.borderLeftColor(value.color);
		this.borderLeftStyle(value.style);
		this.borderLeftWidth(value.width);

		if (value.radiusAlterado) {
			this.borderTopLeftRadius(value.radius);
			this.borderBottomLeftRadius(value.radius);
		}

		return this;

	}

	borderRight(value) {
		this.borderRightColor(value.color);
		this.borderRightStyle(value.style);
		this.borderRightWidth(value.width);
		this.borderBottomRightRadius(value.radius);

		if (value.radiusAlterado) {
			this.borderTopRightRadius(value.radius);
			this.borderBottomRightRadius(value.radius);
		}

		return this;
	}

	borderRadius(value) {
		this.borderTopLeftRadius(value);
		this.borderBottomLeftRadius(value);
		this.borderTopRightRadius(value);
		this.borderBottomRightRadius(value);
		return this;
	}

	withPaddings(style) {
		if (!Null.is(style)) {
			this.o.paddingLeft = style.o.paddingLeft;
			this.o.paddingRight = style.o.paddingRight;
			this.o.paddingBottom = style.o.paddingBottom;
			this.o.paddingTop = style.o.paddingTop;
		}
		return this;
	}

	withPaddingLeft(style) {
		if (!Null.is(style)) {
			this.o.paddingLeft = style.o.paddingLeft;
		}
		return this;
	}

	withPaddingRight(style) {
		if (!Null.is(style)) {
			this.o.paddingRight = style.o.paddingRight;
		}
		return this;
	}

	withPaddingTop(style) {
		if (!Null.is(style)) {
			this.o.paddingTop = style.o.paddingTop;
		}
		return this;
	}

	withPaddingBottom(style) {
		if (!Null.is(style)) {
			this.o.paddingBottom = style.o.paddingBottom;
		}
		return this;
	}

	bg(color) {
		return this.backgroundColor(color);
	}

	wp(value) {
		return this.widthPercent(value);
	}

	scrollY() {
		return this.overflowY(Overflow.auto);
	}

	scrollX() {
		return this.overflowX(Overflow.auto);
	}

	textAlignCenter() {
		return this.textAlign(TextAlign.center);
	}

	alignItemsCenter() {
		return this.alignItems(AlignItens.center);
	}

	h100() {
		return this.heightPercent(100);
	}

	w100() {
		return this.widthPercent(100);
	}

	shadowOffset(width, height) {
		this.checkLock();
		this.o.shadowOffset = {width: width, height: height};
		return this;
	}

	textShadowOffset(width, height) {
		this.checkLock();
		this.o.textShadowOffset = {width: width, height: height};
		return this;
	}

	paddingLeftRight(value) {
		return this.paddingLeft(value).paddingRight(value);
	}

	paddingTopBottom(value) {
		return this.paddingTop(value).paddingBottom(value);
	}

	getHeight() {
		let height = this.get().height;
		if (Null.is(height)) {
			return 0;
		} else if (StringCompare.eq(typeof(height), "string")){
			let s = height;
			if (s.endsWith("%")) {
				return null;
			}
		}
		return parseInt(height);
	}

	getHeightPercent() {
		let s = this.get().height;
		if (StringEmpty.is(s) || !s.endsWith("%")) {
			return null;
		} else {
			return parseInt(StringRight.ignore1(s));
		}
	}

	getWidthPercent() {
		let s = this.get().width;
		if (StringEmpty.is(s) || !s.endsWith("%")) {
			return null;
		} else {
			return parseInt(StringRight.ignore1(s));
		}
	}

	getFontSize() {
		return this.get().fontSize;
	}

	locked = false;

	static scalaPadrao = 1;

	getScala() {
		if (Null.is(this.scala)) {
			return Style.scalaPadrao;
		} else {
			return this.scala;
		}
	}

	setScala(value) {
		this.scala = value;
		return this;
	}

	static ajusteFontSize = 0;
	ultimoAjusteFontSize = 0;

	naoEscalar = false;

	lock() {
		this.locked = true;
		return this;
	}

	debug() {
		return this;
	}

	noScale() {
		this.naoEscalar = true;
		return this;
	}

	bold(value) {
		return this.fontWeight(value ? FontWeight.bold : FontWeight.normal);
	}

	italic(value) {
		return this.fontStyle(value ? FontStyle.italic : FontStyle.italic);
	}

	underline(value) {
		return this.textDecorationLine(value ? TextDecorationLine.underline : TextDecorationLine.underline);
	}

	checkLock() {
		if (this.locked) {
			throw new Error("locked");
		}
	}

	trunc(value) {
		return UDouble.trunc(value*100) / 100.0;
	}

	calc(obj) {
		if (this.naoEscalar) {
			return obj;
		} else if (Null.is(obj)) {
			return obj;
		} else if (StringCompare.eq(typeof(obj), "number")) {
			let value = UDouble.toDouble(obj);
			if (value <= 0.5) {
				return value;
			} else {
				value = UDouble.toDouble(obj) * this.getScala();
				value = this.trunc(value);
				if (value < 0.5) {
					return 0.5;
				} else {
					return value;
				}
			}
		} else {
			return obj;
		}
	}

	toDouble(obj) {

		if (Null.is(obj)) {
			return null;
		} else if (!StringCompare.eq(typeof(obj), "number")) {
			return null;
		} else {
			return UDouble.toDouble(obj);
		}

	}

	menor(a, b) {

		a = this.toDouble(a);
		b = this.toDouble(b);

		if (Null.is(a)) {
			return b;
		} else if (Null.is(b)) {
			return a;
		} else if (a < b) {
			return a;
		} else {
			return b;
		}

	}

	afterGet(rs) {

		if (!Null.is(this.o.fontSize)) {
			rs.fontSize = this.trunc((this.o.fontSize + Style.ajusteFontSize) * this.getScala());
		}

		if (this.isQuadrado) {

			let value = this.menor(this.o.width, this.o.height);

			if (!Null.is(value)) {
				rs.width = this.calc(value);
				rs.height = rs.width;
			}

			value = this.menor(this.o.minWidth, this.o.minHeight);

			if (!Null.is(value)) {
				rs.minWidth = this.calc(value);
				rs.minHeight = rs.minWidth;
			}

			value = this.menor(this.o.maxWidth, this.o.maxHeight);

			if (!Null.is(value)) {
				rs.maxWidth = this.calc(value);
				rs.maxHeight = rs.maxWidth;
			}

		}

		if (!Null.is(rs.lineHeight)) {
			rs.lineHeight = rs.lineHeight + "px";
		}

	}

	displayFlexRow() {
		this.display(Display.flex);
		this.flexDirection(FlexDirection.row);
		this.justifyContent(JustifyContent.spaceBetween);
		return this;
	}

	displayFlexCol() {
		this.display(Display.flex);
		this.flexDirection(FlexDirection.column);
		this.justifyContent(JustifyContent.spaceBetween);
		return this;
	}

	isQuadrado = false;

	quadrado(value) {
		this.isQuadrado = true;
		if (!Null.is(value)) {
			this.width(value).height(value);
		}
		return this;
	}

	toJSON() {
		return StringParse.get(this.get());
	}

	o = {};

	get() {

		let rs = {};

		if (!Null.is(this.o.flex)) {
			rs.flex = this.o.flex;
		}
		if (!Null.is(this.o.width)) {
			rs.width = this.calc(this.o.width);
		}
		if (!Null.is(this.o.zIndex)) {
			rs.zIndex = this.o.zIndex;
		}
		if (!Null.is(this.o.minHeight)) {
			rs.minHeight = this.calc(this.o.minHeight);
		}
		if (!Null.is(this.o.maxHeight)) {
			rs.maxHeight = this.calc(this.o.maxHeight);
		}
		if (!Null.is(this.o.height)) {
			rs.height = this.calc(this.o.height);
		}
		if (!Null.is(this.o.bottom)) {
			rs.bottom = this.calc(this.o.bottom);
		}
		if (!Null.is(this.o.minWidth)) {
			rs.minWidth = this.o.minWidth;
		}
		if (!Null.is(this.o.maxWidth)) {
			rs.maxWidth = this.o.maxWidth;
		}
		if (!Null.is(this.o.opacity)) {
			rs.opacity = this.o.opacity;
		}
		if (!Null.is(this.o.lineHeight)) {
			rs.lineHeight = this.calc(this.o.lineHeight);
		}
		if (!Null.is(this.o.top)) {
			rs.top = this.o.top;
		}
		if (!Null.is(this.o.marginTop)) {
			rs.marginTop = this.calc(this.o.marginTop);
		}
		if (!Null.is(this.o.marginLeft)) {
			rs.marginLeft = this.calc(this.o.marginLeft);
		}
		if (!Null.is(this.o.marginRight)) {
			rs.marginRight = this.calc(this.o.marginRight);
		}
		if (!Null.is(this.o.marginBottom)) {
			rs.marginBottom = this.calc(this.o.marginBottom);
		}
		if (!Null.is(this.o.marginHorizontal)) {
			rs.marginHorizontal = this.o.marginHorizontal;
		}
		if (!Null.is(this.o.paddingTop)) {
			rs.paddingTop = this.calc(this.o.paddingTop);
		}
		if (!Null.is(this.o.paddingLeft)) {
			rs.paddingLeft = this.calc(this.o.paddingLeft);
		}
		if (!Null.is(this.o.paddingRight)) {
			rs.paddingRight = this.calc(this.o.paddingRight);
		}
		if (!Null.is(this.o.paddingBottom)) {
			rs.paddingBottom = this.calc(this.o.paddingBottom);
		}
		if (!Null.is(this.o.paddingVertical)) {
			rs.paddingVertical = this.calc(this.o.paddingVertical);
		}
		if (!Null.is(this.o.paddingHorizontal)) {
			rs.paddingHorizontal = this.o.paddingHorizontal;
		}
		if (!Null.is(this.o.borderLeftWidth)) {
			rs.borderLeftWidth = this.o.borderLeftWidth;
		}
		if (!Null.is(this.o.borderRightWidth)) {
			rs.borderRightWidth = this.o.borderRightWidth;
		}
		if (!Null.is(this.o.borderTopWidth)) {
			rs.borderTopWidth = this.o.borderTopWidth;
		}
		if (!Null.is(this.o.borderBottomWidth)) {
			rs.borderBottomWidth = this.o.borderBottomWidth;
		}
		if (!Null.is(this.o.color)) {
			rs.color = this.o.color;
		}
		if (!Null.is(this.o.textColor)) {
			rs.textColor = this.o.textColor;
		}
		if (!Null.is(this.o.shadowColor)) {
			rs.shadowColor = this.o.shadowColor;
		}
		if (!Null.is(this.o.textShadowColor)) {
			rs.textShadowColor = this.o.textShadowColor;
		}
		if (!Null.is(this.o.borderTopColor)) {
			rs.borderTopColor = this.o.borderTopColor;
		}
		if (!Null.is(this.o.borderBottomColor)) {
			rs.borderBottomColor = this.o.borderBottomColor;
		}
		if (!Null.is(this.o.borderLeftColor)) {
			rs.borderLeftColor = this.o.borderLeftColor;
		}
		if (!Null.is(this.o.borderRightColor)) {
			rs.borderRightColor = this.o.borderRightColor;
		}
		if (!Null.is(this.o.backgroundColor)) {
			rs.backgroundColor = this.o.backgroundColor;
		}
		if (!Null.is(this.o.backgroundImage)) {
			rs.backgroundImage = this.o.backgroundImage;
		}
		if (!Null.is(this.o.scrollBehavior)) {
			rs.scrollBehavior = this.o.scrollBehavior;
		}
		if (!Null.is(this.o.textTransform)) {
			rs.textTransform = this.o.textTransform;
		}
		if (!Null.is(this.o.position)) {
			rs.position = this.o.position;
		}
		if (!Null.is(this.o.textAlign)) {
			rs.textAlign = this.o.textAlign;
		}
		if (!Null.is(this.o.verticalAlign)) {
			rs.verticalAlign = this.o.verticalAlign;
		}
		if (!Null.is(this.o.alignItems)) {
			rs.alignItems = this.o.alignItems;
		}
		if (!Null.is(this.o.alignSelf)) {
			rs.alignSelf = this.o.alignSelf;
		}
		if (!Null.is(this.o.fontWeight)) {
			rs.fontWeight = this.o.fontWeight;
		}
		if (!Null.is(this.o.fontStyle)) {
			rs.fontStyle = this.o.fontStyle;
		}
		if (!Null.is(this.o.textDecorationLine)) {
			rs.textDecorationLine = this.o.textDecorationLine;
		}
		if (!Null.is(this.o.objectFit)) {
			rs.objectFit = this.o.objectFit;
		}
		if (!Null.is(this.o.borderBottomStyle)) {
			rs.borderBottomStyle = this.o.borderBottomStyle;
		}
		if (!Null.is(this.o.borderLeftStyle)) {
			rs.borderLeftStyle = this.o.borderLeftStyle;
		}
		if (!Null.is(this.o.borderTopStyle)) {
			rs.borderTopStyle = this.o.borderTopStyle;
		}
		if (!Null.is(this.o.borderRightStyle)) {
			rs.borderRightStyle = this.o.borderRightStyle;
		}
		if (!Null.is(this.o.flexDirection)) {
			rs.flexDirection = this.o.flexDirection;
		}
		if (!Null.is(this.o.justifyContent)) {
			rs.justifyContent = this.o.justifyContent;
		}
		if (!Null.is(this.o.visibility)) {
			rs.visibility = this.o.visibility;
		}
		if (!Null.is(this.o.float)) {
			rs.float = this.o.float;
		}
		if (!Null.is(this.o.overflow)) {
			rs.overflow = this.o.overflow;
		}
		if (!Null.is(this.o.overflowY)) {
			rs.overflowY = this.o.overflowY;
		}
		if (!Null.is(this.o.overflowX)) {
			rs.overflowX = this.o.overflowX;
		}
		if (!Null.is(this.o.display)) {
			rs.display = this.o.display;
		}
		if (!Null.is(this.o.flexGrow)) {
			rs.flexGrow = this.o.flexGrow;
		}
		if (!Null.is(this.o.left)) {
			rs.left = this.o.left;
		}
		if (!Null.is(this.o.right)) {
			rs.right = this.o.right;
		}
		if (!Null.is(this.o.boxShadow)) {
			rs.boxShadow = this.o.boxShadow;
		}
		if (!Null.is(this.o.cursor)) {
			rs.cursor = this.o.cursor;
		}
		if (!Null.is(this.o.listStyleType)) {
			rs.listStyleType = this.o.listStyleType;
		}
		if (!Null.is(this.o.flexWrap)) {
			rs.flexWrap = this.o.flexWrap;
		}
		if (!Null.is(this.o.resizeMode)) {
			rs.resizeMode = this.o.resizeMode;
		}
		if (!Null.is(this.o.fontFamily)) {
			rs.fontFamily = this.o.fontFamily;
		}
		if (!Null.is(this.o.userSelect)) {
			rs.userSelect = this.o.userSelect;
		}
		if (!Null.is(this.o.transform)) {
			rs.transform = this.o.transform;
		}
		if (!Null.is(this.o.transition)) {
			rs.transition = this.o.transition;
		}
		if (!Null.is(this.o.transformStyle)) {
			rs.transformStyle = this.o.transformStyle;
		}
		if (!Null.is(this.o.elevation)) {
			rs.elevation = this.o.elevation;
		}
		if (!Null.is(this.o.shadowRadius)) {
			rs.shadowRadius = this.o.shadowRadius;
		}
		if (!Null.is(this.o.textShadowRadius)) {
			rs.textShadowRadius = this.o.textShadowRadius;
		}
		if (!Null.is(this.o.shadowOpacity)) {
			rs.shadowOpacity = this.o.shadowOpacity;
		}
		if (!Null.is(this.o.textShadowOpacity)) {
			rs.textShadowOpacity = this.o.textShadowOpacity;
		}
		if (!Null.is(this.o.aspectRatio)) {
			rs.aspectRatio = this.o.aspectRatio;
		}
		if (!Null.is(this.o.borderTopLeftRadius)) {
			rs.borderTopLeftRadius = this.o.borderTopLeftRadius;
		}
		if (!Null.is(this.o.borderTopRightRadius)) {
			rs.borderTopRightRadius = this.o.borderTopRightRadius;
		}
		if (!Null.is(this.o.borderBottomLeftRadius)) {
			rs.borderBottomLeftRadius = this.o.borderBottomLeftRadius;
		}
		if (!Null.is(this.o.borderBottomRightRadius)) {
			rs.borderBottomRightRadius = this.o.borderBottomRightRadius;
		}

		this.afterGet(rs);

		return rs;

	}
	flex(value) {
		this.o.flex = value;
		return this;
	}

	flexPercent(value) {
		this.checkLock();
		this.o.flex = value + "%";
		return this;
	}

	width(value) {
		this.o.width = value;
		return this;
	}

	widthPercent(value) {
		this.checkLock();
		this.o.width = value + "%";
		return this;
	}

	zIndex(value) {
		this.o.zIndex = value;
		return this;
	}

	zIndexPercent(value) {
		this.checkLock();
		this.o.zIndex = value + "%";
		return this;
	}

	minHeight(value) {
		this.o.minHeight = value;
		return this;
	}

	minHeightPercent(value) {
		this.checkLock();
		this.o.minHeight = value + "%";
		return this;
	}

	maxHeight(value) {
		this.o.maxHeight = value;
		return this;
	}

	maxHeightPercent(value) {
		this.checkLock();
		this.o.maxHeight = value + "%";
		return this;
	}

	height(value) {
		this.o.height = value;
		return this;
	}

	heightPercent(value) {
		this.checkLock();
		this.o.height = value + "%";
		return this;
	}

	bottom(value) {
		this.o.bottom = value;
		return this;
	}

	bottomPercent(value) {
		this.checkLock();
		this.o.bottom = value + "%";
		return this;
	}

	minWidth(value) {
		this.o.minWidth = value;
		return this;
	}

	minWidthPercent(value) {
		this.checkLock();
		this.o.minWidth = value + "%";
		return this;
	}

	maxWidth(value) {
		this.o.maxWidth = value;
		return this;
	}

	maxWidthPercent(value) {
		this.checkLock();
		this.o.maxWidth = value + "%";
		return this;
	}

	opacity(value) {
		this.o.opacity = value;
		return this;
	}

	opacityPercent(value) {
		this.checkLock();
		this.o.opacity = value + "%";
		return this;
	}

	lineHeight(value) {
		this.o.lineHeight = value;
		return this;
	}

	lineHeightPercent(value) {
		this.checkLock();
		this.o.lineHeight = value + "%";
		return this;
	}

	top(value) {
		this.o.top = value;
		return this;
	}

	topPercent(value) {
		this.checkLock();
		this.o.top = value + "%";
		return this;
	}

	marginTop(value) {
		this.o.marginTop = value;
		return this;
	}

	marginTopPercent(value) {
		this.checkLock();
		this.o.marginTop = value + "%";
		return this;
	}

	marginLeft(value) {
		this.o.marginLeft = value;
		return this;
	}

	marginLeftPercent(value) {
		this.checkLock();
		this.o.marginLeft = value + "%";
		return this;
	}

	marginRight(value) {
		this.o.marginRight = value;
		return this;
	}

	marginRightPercent(value) {
		this.checkLock();
		this.o.marginRight = value + "%";
		return this;
	}

	marginBottom(value) {
		this.o.marginBottom = value;
		return this;
	}

	marginBottomPercent(value) {
		this.checkLock();
		this.o.marginBottom = value + "%";
		return this;
	}

	marginHorizontal(value) {
		this.o.marginHorizontal = value;
		return this;
	}

	marginHorizontalPercent(value) {
		this.checkLock();
		this.o.marginHorizontal = value + "%";
		return this;
	}

	paddingTop(value) {
		this.o.paddingTop = value;
		return this;
	}

	paddingTopPercent(value) {
		this.checkLock();
		this.o.paddingTop = value + "%";
		return this;
	}

	paddingLeft(value) {
		this.o.paddingLeft = value;
		return this;
	}

	paddingLeftPercent(value) {
		this.checkLock();
		this.o.paddingLeft = value + "%";
		return this;
	}

	paddingRight(value) {
		this.o.paddingRight = value;
		return this;
	}

	paddingRightPercent(value) {
		this.checkLock();
		this.o.paddingRight = value + "%";
		return this;
	}

	paddingBottom(value) {
		this.o.paddingBottom = value;
		return this;
	}

	paddingBottomPercent(value) {
		this.checkLock();
		this.o.paddingBottom = value + "%";
		return this;
	}

	paddingVertical(value) {
		this.o.paddingVertical = value;
		return this;
	}

	paddingVerticalPercent(value) {
		this.checkLock();
		this.o.paddingVertical = value + "%";
		return this;
	}

	paddingHorizontal(value) {
		this.o.paddingHorizontal = value;
		return this;
	}

	paddingHorizontalPercent(value) {
		this.checkLock();
		this.o.paddingHorizontal = value + "%";
		return this;
	}

	borderLeftWidth(value) {
		this.o.borderLeftWidth = value;
		return this;
	}

	borderLeftWidthPercent(value) {
		this.checkLock();
		this.o.borderLeftWidth = value + "%";
		return this;
	}

	borderRightWidth(value) {
		this.o.borderRightWidth = value;
		return this;
	}

	borderRightWidthPercent(value) {
		this.checkLock();
		this.o.borderRightWidth = value + "%";
		return this;
	}

	borderTopWidth(value) {
		this.o.borderTopWidth = value;
		return this;
	}

	borderTopWidthPercent(value) {
		this.checkLock();
		this.o.borderTopWidth = value + "%";
		return this;
	}

	borderBottomWidth(value) {
		this.o.borderBottomWidth = value;
		return this;
	}

	borderBottomWidthPercent(value) {
		this.checkLock();
		this.o.borderBottomWidth = value + "%";
		return this;
	}

	borderColor(value) {
		this.borderLeftColor(value);
		this.borderRightColor(value);
		this.borderTopColor(value);
		this.borderBottomColor(value);
		return this;
	}

	borderStyle(value) {
		this.borderLeftStyle(value);
		this.borderRightStyle(value);
		this.borderTopStyle(value);
		this.borderBottomStyle(value);
		return this;
	}

	margin(value) {
		this.marginLeft(value);
		this.marginRight(value);
		this.marginTop(value);
		this.marginBottom(value);
		return this;
	}

	marginPercent(value) {
		this.marginLeftPercent(value);
		this.marginRightPercent(value);
		this.marginTopPercent(value);
		this.marginBottomPercent(value);
		return this;
	}

	marginClear() {
		this.marginLeftClear();
		this.marginRightClear();
		this.marginTopClear();
		this.marginBottomClear();
		return this;
	}

	padding(value) {
		this.paddingLeft(value);
		this.paddingRight(value);
		this.paddingTop(value);
		this.paddingBottom(value);
		return this;
	}

	paddingPercent(value) {
		this.paddingLeftPercent(value);
		this.paddingRightPercent(value);
		this.paddingTopPercent(value);
		this.paddingBottomPercent(value);
		return this;
	}

	paddingClear() {
		this.paddingLeftClear();
		this.paddingRightClear();
		this.paddingTopClear();
		this.paddingBottomClear();
		return this;
	}

	borderWidth(value) {
		this.borderLeftWidth(value);
		this.borderRightWidth(value);
		this.borderTopWidth(value);
		this.borderBottomWidth(value);
		return this;
	}

	borderWidthPercent(value) {
		this.borderLeftWidthPercent(value);
		this.borderRightWidthPercent(value);
		this.borderTopWidthPercent(value);
		this.borderBottomWidthPercent(value);
		return this;
	}

	borderWidthClear() {
		this.borderLeftWidthClear();
		this.borderRightWidthClear();
		this.borderTopWidthClear();
		this.borderBottomWidthClear();
		return this;
	}

	color(value) {
		this.checkLock();
		this.o.color = value;
		return this;
	}

	textColor(value) {
		this.checkLock();
		this.o.textColor = value;
		return this;
	}

	shadowColor(value) {
		this.checkLock();
		this.o.shadowColor = value;
		return this;
	}

	textShadowColor(value) {
		this.checkLock();
		this.o.textShadowColor = value;
		return this;
	}

	borderTopColor(value) {
		this.checkLock();
		this.o.borderTopColor = value;
		return this;
	}

	borderBottomColor(value) {
		this.checkLock();
		this.o.borderBottomColor = value;
		return this;
	}

	borderLeftColor(value) {
		this.checkLock();
		this.o.borderLeftColor = value;
		return this;
	}

	borderRightColor(value) {
		this.checkLock();
		this.o.borderRightColor = value;
		return this;
	}

	backgroundColor(value) {
		this.checkLock();
		this.o.backgroundColor = value;
		return this;
	}

	backgroundImage(value) {
		this.checkLock();
		this.o.backgroundImage = value;
		return this;
	}

	scrollBehavior(value) {
		this.checkLock();
		this.o.scrollBehavior = value;
		return this;
	}

	textTransform(value) {
		this.checkLock();
		this.o.textTransform = value;
		return this;
	}

	position(value) {
		this.checkLock();
		this.o.position = value;
		return this;
	}

	textAlign(value) {
		this.checkLock();
		this.o.textAlign = value;
		return this;
	}

	verticalAlign(value) {
		this.checkLock();
		this.o.verticalAlign = value;
		return this;
	}

	alignItems(value) {
		this.checkLock();
		this.o.alignItems = value;
		return this;
	}

	alignSelf(value) {
		this.checkLock();
		this.o.alignSelf = value;
		return this;
	}

	fontWeight(value) {
		this.checkLock();
		this.o.fontWeight = value;
		return this;
	}

	fontStyle(value) {
		this.checkLock();
		this.o.fontStyle = value;
		return this;
	}

	textDecorationLine(value) {
		this.checkLock();
		this.o.textDecorationLine = value;
		return this;
	}

	objectFit(value) {
		this.checkLock();
		this.o.objectFit = value;
		return this;
	}

	borderBottomStyle(value) {
		this.checkLock();
		this.o.borderBottomStyle = value;
		return this;
	}

	borderLeftStyle(value) {
		this.checkLock();
		this.o.borderLeftStyle = value;
		return this;
	}

	borderTopStyle(value) {
		this.checkLock();
		this.o.borderTopStyle = value;
		return this;
	}

	borderRightStyle(value) {
		this.checkLock();
		this.o.borderRightStyle = value;
		return this;
	}

	flexDirection(value) {
		this.checkLock();
		this.o.flexDirection = value;
		return this;
	}

	justifyContent(value) {
		this.checkLock();
		this.o.justifyContent = value;
		return this;
	}

	visibility(value) {
		this.checkLock();
		this.o.visibility = value;
		return this;
	}

	float(value) {
		this.checkLock();
		this.o.float = value;
		return this;
	}

	overflow(value) {
		this.checkLock();
		this.o.overflow = value;
		return this;
	}

	overflowY(value) {
		this.checkLock();
		this.o.overflowY = value;
		return this;
	}

	overflowX(value) {
		this.checkLock();
		this.o.overflowX = value;
		return this;
	}

	display(value) {
		this.checkLock();
		this.o.display = value;
		return this;
	}

	flexGrow(value) {
		this.checkLock();
		this.o.flexGrow = value;
		return this;
	}

	left(value) {
		this.checkLock();
		this.o.left = value;
		return this;
	}

	right(value) {
		this.checkLock();
		this.o.right = value;
		return this;
	}

	boxShadow(value) {
		this.checkLock();
		this.o.boxShadow = value;
		return this;
	}

	cursor(value) {
		this.checkLock();
		this.o.cursor = value;
		return this;
	}

	listStyleType(value) {
		this.checkLock();
		this.o.listStyleType = value;
		return this;
	}

	flexWrap(value) {
		this.checkLock();
		this.o.flexWrap = value;
		return this;
	}

	resizeMode(value) {
		this.checkLock();
		this.o.resizeMode = value;
		return this;
	}

	fontFamily(value) {
		this.checkLock();
		this.o.fontFamily = value;
		return this;
	}

	userSelect(value) {
		this.checkLock();
		this.o.userSelect = value;
		return this;
	}

	transform(value) {
		this.checkLock();
		this.o.transform = value;
		return this;
	}

	transition(value) {
		this.checkLock();
		this.o.transition = value;
		return this;
	}

	transformStyle(value) {
		this.checkLock();
		this.o.transformStyle = value;
		return this;
	}

	elevation(value) {
		this.checkLock();
		this.o.elevation = value;
		return this;
	}

	shadowRadius(value) {
		this.checkLock();
		this.o.shadowRadius = value;
		return this;
	}

	textShadowRadius(value) {
		this.checkLock();
		this.o.textShadowRadius = value;
		return this;
	}

	shadowOpacity(value) {
		this.checkLock();
		this.o.shadowOpacity = value;
		return this;
	}

	textShadowOpacity(value) {
		this.checkLock();
		this.o.textShadowOpacity = value;
		return this;
	}

	aspectRatio(value) {
		this.checkLock();
		this.o.aspectRatio = value;
		return this;
	}

	borderTopLeftRadius(value) {
		this.checkLock();
		this.o.borderTopLeftRadius = value;
		return this;
	}

	borderTopRightRadius(value) {
		this.checkLock();
		this.o.borderTopRightRadius = value;
		return this;
	}

	borderBottomLeftRadius(value) {
		this.checkLock();
		this.o.borderBottomLeftRadius = value;
		return this;
	}

	borderBottomRightRadius(value) {
		this.checkLock();
		this.o.borderBottomRightRadius = value;
		return this;
	}

	fontSize(value) {
		this.checkLock();
		this.o.fontSize = value;
		return this;
	}

	copy() {
		let style = new Style();
		style.scala = this.scala;
		style.isQuadrado = this.isQuadrado;
		if (!Null.is(this.o.color)) {
			style.o.color = this.o.color;
		}
		if (!Null.is(this.o.textColor)) {
			style.o.textColor = this.o.textColor;
		}
		if (!Null.is(this.o.shadowColor)) {
			style.o.shadowColor = this.o.shadowColor;
		}
		if (!Null.is(this.o.textShadowColor)) {
			style.o.textShadowColor = this.o.textShadowColor;
		}
		if (!Null.is(this.o.borderTopColor)) {
			style.o.borderTopColor = this.o.borderTopColor;
		}
		if (!Null.is(this.o.borderBottomColor)) {
			style.o.borderBottomColor = this.o.borderBottomColor;
		}
		if (!Null.is(this.o.borderLeftColor)) {
			style.o.borderLeftColor = this.o.borderLeftColor;
		}
		if (!Null.is(this.o.borderRightColor)) {
			style.o.borderRightColor = this.o.borderRightColor;
		}
		if (!Null.is(this.o.backgroundColor)) {
			style.o.backgroundColor = this.o.backgroundColor;
		}
		if (!Null.is(this.o.backgroundImage)) {
			style.o.backgroundImage = this.o.backgroundImage;
		}
		if (!Null.is(this.o.scrollBehavior)) {
			style.o.scrollBehavior = this.o.scrollBehavior;
		}
		if (!Null.is(this.o.textTransform)) {
			style.o.textTransform = this.o.textTransform;
		}
		if (!Null.is(this.o.position)) {
			style.o.position = this.o.position;
		}
		if (!Null.is(this.o.textAlign)) {
			style.o.textAlign = this.o.textAlign;
		}
		if (!Null.is(this.o.verticalAlign)) {
			style.o.verticalAlign = this.o.verticalAlign;
		}
		if (!Null.is(this.o.alignItems)) {
			style.o.alignItems = this.o.alignItems;
		}
		if (!Null.is(this.o.alignSelf)) {
			style.o.alignSelf = this.o.alignSelf;
		}
		if (!Null.is(this.o.fontWeight)) {
			style.o.fontWeight = this.o.fontWeight;
		}
		if (!Null.is(this.o.fontStyle)) {
			style.o.fontStyle = this.o.fontStyle;
		}
		if (!Null.is(this.o.textDecorationLine)) {
			style.o.textDecorationLine = this.o.textDecorationLine;
		}
		if (!Null.is(this.o.objectFit)) {
			style.o.objectFit = this.o.objectFit;
		}
		if (!Null.is(this.o.borderBottomStyle)) {
			style.o.borderBottomStyle = this.o.borderBottomStyle;
		}
		if (!Null.is(this.o.borderLeftStyle)) {
			style.o.borderLeftStyle = this.o.borderLeftStyle;
		}
		if (!Null.is(this.o.borderTopStyle)) {
			style.o.borderTopStyle = this.o.borderTopStyle;
		}
		if (!Null.is(this.o.borderRightStyle)) {
			style.o.borderRightStyle = this.o.borderRightStyle;
		}
		if (!Null.is(this.o.flexDirection)) {
			style.o.flexDirection = this.o.flexDirection;
		}
		if (!Null.is(this.o.justifyContent)) {
			style.o.justifyContent = this.o.justifyContent;
		}
		if (!Null.is(this.o.visibility)) {
			style.o.visibility = this.o.visibility;
		}
		if (!Null.is(this.o.float)) {
			style.o.float = this.o.float;
		}
		if (!Null.is(this.o.overflow)) {
			style.o.overflow = this.o.overflow;
		}
		if (!Null.is(this.o.overflowY)) {
			style.o.overflowY = this.o.overflowY;
		}
		if (!Null.is(this.o.overflowX)) {
			style.o.overflowX = this.o.overflowX;
		}
		if (!Null.is(this.o.display)) {
			style.o.display = this.o.display;
		}
		if (!Null.is(this.o.flexGrow)) {
			style.o.flexGrow = this.o.flexGrow;
		}
		if (!Null.is(this.o.left)) {
			style.o.left = this.o.left;
		}
		if (!Null.is(this.o.right)) {
			style.o.right = this.o.right;
		}
		if (!Null.is(this.o.boxShadow)) {
			style.o.boxShadow = this.o.boxShadow;
		}
		if (!Null.is(this.o.cursor)) {
			style.o.cursor = this.o.cursor;
		}
		if (!Null.is(this.o.listStyleType)) {
			style.o.listStyleType = this.o.listStyleType;
		}
		if (!Null.is(this.o.flexWrap)) {
			style.o.flexWrap = this.o.flexWrap;
		}
		if (!Null.is(this.o.resizeMode)) {
			style.o.resizeMode = this.o.resizeMode;
		}
		if (!Null.is(this.o.fontFamily)) {
			style.o.fontFamily = this.o.fontFamily;
		}
		if (!Null.is(this.o.userSelect)) {
			style.o.userSelect = this.o.userSelect;
		}
		if (!Null.is(this.o.transform)) {
			style.o.transform = this.o.transform;
		}
		if (!Null.is(this.o.transition)) {
			style.o.transition = this.o.transition;
		}
		if (!Null.is(this.o.transformStyle)) {
			style.o.transformStyle = this.o.transformStyle;
		}
		if (!Null.is(this.o.flex)) {
			style.o.flex = this.o.flex;
		}
		if (!Null.is(this.o.width)) {
			style.o.width = this.o.width;
		}
		if (!Null.is(this.o.zIndex)) {
			style.o.zIndex = this.o.zIndex;
		}
		if (!Null.is(this.o.minHeight)) {
			style.o.minHeight = this.o.minHeight;
		}
		if (!Null.is(this.o.maxHeight)) {
			style.o.maxHeight = this.o.maxHeight;
		}
		if (!Null.is(this.o.height)) {
			style.o.height = this.o.height;
		}
		if (!Null.is(this.o.bottom)) {
			style.o.bottom = this.o.bottom;
		}
		if (!Null.is(this.o.minWidth)) {
			style.o.minWidth = this.o.minWidth;
		}
		if (!Null.is(this.o.maxWidth)) {
			style.o.maxWidth = this.o.maxWidth;
		}
		if (!Null.is(this.o.opacity)) {
			style.o.opacity = this.o.opacity;
		}
		if (!Null.is(this.o.lineHeight)) {
			style.o.lineHeight = this.o.lineHeight;
		}
		if (!Null.is(this.o.elevation)) {
			style.o.elevation = this.o.elevation;
		}
		if (!Null.is(this.o.shadowRadius)) {
			style.o.shadowRadius = this.o.shadowRadius;
		}
		if (!Null.is(this.o.textShadowRadius)) {
			style.o.textShadowRadius = this.o.textShadowRadius;
		}
		if (!Null.is(this.o.shadowOpacity)) {
			style.o.shadowOpacity = this.o.shadowOpacity;
		}
		if (!Null.is(this.o.textShadowOpacity)) {
			style.o.textShadowOpacity = this.o.textShadowOpacity;
		}
		if (!Null.is(this.o.aspectRatio)) {
			style.o.aspectRatio = this.o.aspectRatio;
		}
		if (!Null.is(this.o.fontSize)) {
			style.o.fontSize = this.o.fontSize;
		}
		if (!Null.is(this.o.top)) {
			style.o.top = this.o.top;
		}
		if (!Null.is(this.o.marginTop)) {
			style.o.marginTop = this.o.marginTop;
		}
		if (!Null.is(this.o.marginLeft)) {
			style.o.marginLeft = this.o.marginLeft;
		}
		if (!Null.is(this.o.marginRight)) {
			style.o.marginRight = this.o.marginRight;
		}
		if (!Null.is(this.o.marginBottom)) {
			style.o.marginBottom = this.o.marginBottom;
		}
		if (!Null.is(this.o.marginHorizontal)) {
			style.o.marginHorizontal = this.o.marginHorizontal;
		}
		if (!Null.is(this.o.paddingTop)) {
			style.o.paddingTop = this.o.paddingTop;
		}
		if (!Null.is(this.o.paddingLeft)) {
			style.o.paddingLeft = this.o.paddingLeft;
		}
		if (!Null.is(this.o.paddingRight)) {
			style.o.paddingRight = this.o.paddingRight;
		}
		if (!Null.is(this.o.paddingBottom)) {
			style.o.paddingBottom = this.o.paddingBottom;
		}
		if (!Null.is(this.o.paddingVertical)) {
			style.o.paddingVertical = this.o.paddingVertical;
		}
		if (!Null.is(this.o.paddingHorizontal)) {
			style.o.paddingHorizontal = this.o.paddingHorizontal;
		}
		if (!Null.is(this.o.borderLeftWidth)) {
			style.o.borderLeftWidth = this.o.borderLeftWidth;
		}
		if (!Null.is(this.o.borderRightWidth)) {
			style.o.borderRightWidth = this.o.borderRightWidth;
		}
		if (!Null.is(this.o.borderTopWidth)) {
			style.o.borderTopWidth = this.o.borderTopWidth;
		}
		if (!Null.is(this.o.borderBottomWidth)) {
			style.o.borderBottomWidth = this.o.borderBottomWidth;
		}
		if (!Null.is(this.o.borderTopLeftRadius)) {
			style.o.borderTopLeftRadius = this.o.borderTopLeftRadius;
		}
		if (!Null.is(this.o.borderTopRightRadius)) {
			style.o.borderTopRightRadius = this.o.borderTopRightRadius;
		}
		if (!Null.is(this.o.borderBottomLeftRadius)) {
			style.o.borderBottomLeftRadius = this.o.borderBottomLeftRadius;
		}
		if (!Null.is(this.o.borderBottomRightRadius)) {
			style.o.borderBottomRightRadius = this.o.borderBottomRightRadius;
		}
		return style;
	}

	join(style) {

		let c = this.copy();

		if (Null.is(style) || Null.is(style.o)) {
			return c;
		}

		if (!Null.is(style.scala)) {
			c.scala = style.scala;
		}
		if (style.isQuadrado) {
			c.isQuadrado = true;
		}

		if (!Null.is(style.o.color)) {
			c.o.color = style.o.color;
		}

		if (!Null.is(style.o.textColor)) {
			c.o.textColor = style.o.textColor;
		}

		if (!Null.is(style.o.shadowColor)) {
			c.o.shadowColor = style.o.shadowColor;
		}

		if (!Null.is(style.o.textShadowColor)) {
			c.o.textShadowColor = style.o.textShadowColor;
		}

		if (!Null.is(style.o.borderTopColor)) {
			c.o.borderTopColor = style.o.borderTopColor;
		}

		if (!Null.is(style.o.borderBottomColor)) {
			c.o.borderBottomColor = style.o.borderBottomColor;
		}

		if (!Null.is(style.o.borderLeftColor)) {
			c.o.borderLeftColor = style.o.borderLeftColor;
		}

		if (!Null.is(style.o.borderRightColor)) {
			c.o.borderRightColor = style.o.borderRightColor;
		}

		if (!Null.is(style.o.backgroundColor)) {
			c.o.backgroundColor = style.o.backgroundColor;
		}

		if (!Null.is(style.o.backgroundImage)) {
			c.o.backgroundImage = style.o.backgroundImage;
		}

		if (!Null.is(style.o.scrollBehavior)) {
			c.o.scrollBehavior = style.o.scrollBehavior;
		}

		if (!Null.is(style.o.textTransform)) {
			c.o.textTransform = style.o.textTransform;
		}

		if (!Null.is(style.o.position)) {
			c.o.position = style.o.position;
		}

		if (!Null.is(style.o.textAlign)) {
			c.o.textAlign = style.o.textAlign;
		}

		if (!Null.is(style.o.verticalAlign)) {
			c.o.verticalAlign = style.o.verticalAlign;
		}

		if (!Null.is(style.o.alignItems)) {
			c.o.alignItems = style.o.alignItems;
		}

		if (!Null.is(style.o.alignSelf)) {
			c.o.alignSelf = style.o.alignSelf;
		}

		if (!Null.is(style.o.fontWeight)) {
			c.o.fontWeight = style.o.fontWeight;
		}

		if (!Null.is(style.o.fontStyle)) {
			c.o.fontStyle = style.o.fontStyle;
		}

		if (!Null.is(style.o.textDecorationLine)) {
			c.o.textDecorationLine = style.o.textDecorationLine;
		}

		if (!Null.is(style.o.objectFit)) {
			c.o.objectFit = style.o.objectFit;
		}

		if (!Null.is(style.o.borderBottomStyle)) {
			c.o.borderBottomStyle = style.o.borderBottomStyle;
		}

		if (!Null.is(style.o.borderLeftStyle)) {
			c.o.borderLeftStyle = style.o.borderLeftStyle;
		}

		if (!Null.is(style.o.borderTopStyle)) {
			c.o.borderTopStyle = style.o.borderTopStyle;
		}

		if (!Null.is(style.o.borderRightStyle)) {
			c.o.borderRightStyle = style.o.borderRightStyle;
		}

		if (!Null.is(style.o.flexDirection)) {
			c.o.flexDirection = style.o.flexDirection;
		}

		if (!Null.is(style.o.justifyContent)) {
			c.o.justifyContent = style.o.justifyContent;
		}

		if (!Null.is(style.o.visibility)) {
			c.o.visibility = style.o.visibility;
		}

		if (!Null.is(style.o.float)) {
			c.o.float = style.o.float;
		}

		if (!Null.is(style.o.overflow)) {
			c.o.overflow = style.o.overflow;
		}

		if (!Null.is(style.o.overflowY)) {
			c.o.overflowY = style.o.overflowY;
		}

		if (!Null.is(style.o.overflowX)) {
			c.o.overflowX = style.o.overflowX;
		}

		if (!Null.is(style.o.display)) {
			c.o.display = style.o.display;
		}

		if (!Null.is(style.o.flexGrow)) {
			c.o.flexGrow = style.o.flexGrow;
		}

		if (!Null.is(style.o.left)) {
			c.o.left = style.o.left;
		}

		if (!Null.is(style.o.right)) {
			c.o.right = style.o.right;
		}

		if (!Null.is(style.o.boxShadow)) {
			c.o.boxShadow = style.o.boxShadow;
		}

		if (!Null.is(style.o.cursor)) {
			c.o.cursor = style.o.cursor;
		}

		if (!Null.is(style.o.listStyleType)) {
			c.o.listStyleType = style.o.listStyleType;
		}

		if (!Null.is(style.o.flexWrap)) {
			c.o.flexWrap = style.o.flexWrap;
		}

		if (!Null.is(style.o.resizeMode)) {
			c.o.resizeMode = style.o.resizeMode;
		}

		if (!Null.is(style.o.fontFamily)) {
			c.o.fontFamily = style.o.fontFamily;
		}

		if (!Null.is(style.o.userSelect)) {
			c.o.userSelect = style.o.userSelect;
		}

		if (!Null.is(style.o.transform)) {
			c.o.transform = style.o.transform;
		}

		if (!Null.is(style.o.transition)) {
			c.o.transition = style.o.transition;
		}

		if (!Null.is(style.o.transformStyle)) {
			c.o.transformStyle = style.o.transformStyle;
		}

		if (!Null.is(style.o.flex)) {
			c.o.flex = style.o.flex;
		}

		if (!Null.is(style.o.width)) {
			c.o.width = style.o.width;
		}

		if (!Null.is(style.o.zIndex)) {
			c.o.zIndex = style.o.zIndex;
		}

		if (!Null.is(style.o.minHeight)) {
			c.o.minHeight = style.o.minHeight;
		}

		if (!Null.is(style.o.maxHeight)) {
			c.o.maxHeight = style.o.maxHeight;
		}

		if (!Null.is(style.o.height)) {
			c.o.height = style.o.height;
		}

		if (!Null.is(style.o.bottom)) {
			c.o.bottom = style.o.bottom;
		}

		if (!Null.is(style.o.minWidth)) {
			c.o.minWidth = style.o.minWidth;
		}

		if (!Null.is(style.o.maxWidth)) {
			c.o.maxWidth = style.o.maxWidth;
		}

		if (!Null.is(style.o.opacity)) {
			c.o.opacity = style.o.opacity;
		}

		if (!Null.is(style.o.lineHeight)) {
			c.o.lineHeight = style.o.lineHeight;
		}

		if (!Null.is(style.o.elevation)) {
			c.o.elevation = style.o.elevation;
		}

		if (!Null.is(style.o.shadowRadius)) {
			c.o.shadowRadius = style.o.shadowRadius;
		}

		if (!Null.is(style.o.textShadowRadius)) {
			c.o.textShadowRadius = style.o.textShadowRadius;
		}

		if (!Null.is(style.o.shadowOpacity)) {
			c.o.shadowOpacity = style.o.shadowOpacity;
		}

		if (!Null.is(style.o.textShadowOpacity)) {
			c.o.textShadowOpacity = style.o.textShadowOpacity;
		}

		if (!Null.is(style.o.aspectRatio)) {
			c.o.aspectRatio = style.o.aspectRatio;
		}

		if (!Null.is(style.o.fontSize)) {
			c.o.fontSize = style.o.fontSize;
		}

		if (!Null.is(style.o.top)) {
			c.o.top = style.o.top;
		}

		if (!Null.is(style.o.marginTop)) {
			c.o.marginTop = style.o.marginTop;
		}

		if (!Null.is(style.o.marginLeft)) {
			c.o.marginLeft = style.o.marginLeft;
		}

		if (!Null.is(style.o.marginRight)) {
			c.o.marginRight = style.o.marginRight;
		}

		if (!Null.is(style.o.marginBottom)) {
			c.o.marginBottom = style.o.marginBottom;
		}

		if (!Null.is(style.o.marginHorizontal)) {
			c.o.marginHorizontal = style.o.marginHorizontal;
		}

		if (!Null.is(style.o.paddingTop)) {
			c.o.paddingTop = style.o.paddingTop;
		}

		if (!Null.is(style.o.paddingLeft)) {
			c.o.paddingLeft = style.o.paddingLeft;
		}

		if (!Null.is(style.o.paddingRight)) {
			c.o.paddingRight = style.o.paddingRight;
		}

		if (!Null.is(style.o.paddingBottom)) {
			c.o.paddingBottom = style.o.paddingBottom;
		}

		if (!Null.is(style.o.paddingVertical)) {
			c.o.paddingVertical = style.o.paddingVertical;
		}

		if (!Null.is(style.o.paddingHorizontal)) {
			c.o.paddingHorizontal = style.o.paddingHorizontal;
		}

		if (!Null.is(style.o.borderLeftWidth)) {
			c.o.borderLeftWidth = style.o.borderLeftWidth;
		}

		if (!Null.is(style.o.borderRightWidth)) {
			c.o.borderRightWidth = style.o.borderRightWidth;
		}

		if (!Null.is(style.o.borderTopWidth)) {
			c.o.borderTopWidth = style.o.borderTopWidth;
		}

		if (!Null.is(style.o.borderBottomWidth)) {
			c.o.borderBottomWidth = style.o.borderBottomWidth;
		}

		if (!Null.is(style.o.borderTopLeftRadius)) {
			c.o.borderTopLeftRadius = style.o.borderTopLeftRadius;
		}

		if (!Null.is(style.o.borderTopRightRadius)) {
			c.o.borderTopRightRadius = style.o.borderTopRightRadius;
		}

		if (!Null.is(style.o.borderBottomLeftRadius)) {
			c.o.borderBottomLeftRadius = style.o.borderBottomLeftRadius;
		}

		if (!Null.is(style.o.borderBottomRightRadius)) {
			c.o.borderBottomRightRadius = style.o.borderBottomRightRadius;
		}

		return c;

	}
	colorClear() {
		this.checkLock();
		this.o.color = undefined;
		return this;
	}

	textColorClear() {
		this.checkLock();
		this.o.textColor = undefined;
		return this;
	}

	shadowColorClear() {
		this.checkLock();
		this.o.shadowColor = undefined;
		return this;
	}

	textShadowColorClear() {
		this.checkLock();
		this.o.textShadowColor = undefined;
		return this;
	}

	borderTopColorClear() {
		this.checkLock();
		this.o.borderTopColor = undefined;
		return this;
	}

	borderBottomColorClear() {
		this.checkLock();
		this.o.borderBottomColor = undefined;
		return this;
	}

	borderLeftColorClear() {
		this.checkLock();
		this.o.borderLeftColor = undefined;
		return this;
	}

	borderRightColorClear() {
		this.checkLock();
		this.o.borderRightColor = undefined;
		return this;
	}

	backgroundColorClear() {
		this.checkLock();
		this.o.backgroundColor = undefined;
		return this;
	}

	backgroundImageClear() {
		this.checkLock();
		this.o.backgroundImage = undefined;
		return this;
	}

	scrollBehaviorClear() {
		this.checkLock();
		this.o.scrollBehavior = undefined;
		return this;
	}

	textTransformClear() {
		this.checkLock();
		this.o.textTransform = undefined;
		return this;
	}

	positionClear() {
		this.checkLock();
		this.o.position = undefined;
		return this;
	}

	textAlignClear() {
		this.checkLock();
		this.o.textAlign = undefined;
		return this;
	}

	verticalAlignClear() {
		this.checkLock();
		this.o.verticalAlign = undefined;
		return this;
	}

	alignItemsClear() {
		this.checkLock();
		this.o.alignItems = undefined;
		return this;
	}

	alignSelfClear() {
		this.checkLock();
		this.o.alignSelf = undefined;
		return this;
	}

	fontWeightClear() {
		this.checkLock();
		this.o.fontWeight = undefined;
		return this;
	}

	fontStyleClear() {
		this.checkLock();
		this.o.fontStyle = undefined;
		return this;
	}

	textDecorationLineClear() {
		this.checkLock();
		this.o.textDecorationLine = undefined;
		return this;
	}

	objectFitClear() {
		this.checkLock();
		this.o.objectFit = undefined;
		return this;
	}

	borderBottomStyleClear() {
		this.checkLock();
		this.o.borderBottomStyle = undefined;
		return this;
	}

	borderLeftStyleClear() {
		this.checkLock();
		this.o.borderLeftStyle = undefined;
		return this;
	}

	borderTopStyleClear() {
		this.checkLock();
		this.o.borderTopStyle = undefined;
		return this;
	}

	borderRightStyleClear() {
		this.checkLock();
		this.o.borderRightStyle = undefined;
		return this;
	}

	flexDirectionClear() {
		this.checkLock();
		this.o.flexDirection = undefined;
		return this;
	}

	justifyContentClear() {
		this.checkLock();
		this.o.justifyContent = undefined;
		return this;
	}

	visibilityClear() {
		this.checkLock();
		this.o.visibility = undefined;
		return this;
	}

	floatClear() {
		this.checkLock();
		this.o.float = undefined;
		return this;
	}

	overflowClear() {
		this.checkLock();
		this.o.overflow = undefined;
		return this;
	}

	overflowYClear() {
		this.checkLock();
		this.o.overflowY = undefined;
		return this;
	}

	overflowXClear() {
		this.checkLock();
		this.o.overflowX = undefined;
		return this;
	}

	displayClear() {
		this.checkLock();
		this.o.display = undefined;
		return this;
	}

	flexGrowClear() {
		this.checkLock();
		this.o.flexGrow = undefined;
		return this;
	}

	leftClear() {
		this.checkLock();
		this.o.left = undefined;
		return this;
	}

	rightClear() {
		this.checkLock();
		this.o.right = undefined;
		return this;
	}

	boxShadowClear() {
		this.checkLock();
		this.o.boxShadow = undefined;
		return this;
	}

	cursorClear() {
		this.checkLock();
		this.o.cursor = undefined;
		return this;
	}

	listStyleTypeClear() {
		this.checkLock();
		this.o.listStyleType = undefined;
		return this;
	}

	flexWrapClear() {
		this.checkLock();
		this.o.flexWrap = undefined;
		return this;
	}

	resizeModeClear() {
		this.checkLock();
		this.o.resizeMode = undefined;
		return this;
	}

	fontFamilyClear() {
		this.checkLock();
		this.o.fontFamily = undefined;
		return this;
	}

	userSelectClear() {
		this.checkLock();
		this.o.userSelect = undefined;
		return this;
	}

	transformClear() {
		this.checkLock();
		this.o.transform = undefined;
		return this;
	}

	transitionClear() {
		this.checkLock();
		this.o.transition = undefined;
		return this;
	}

	transformStyleClear() {
		this.checkLock();
		this.o.transformStyle = undefined;
		return this;
	}

	flexClear() {
		this.checkLock();
		this.o.flex = undefined;
		return this;
	}

	widthClear() {
		this.checkLock();
		this.o.width = undefined;
		return this;
	}

	zIndexClear() {
		this.checkLock();
		this.o.zIndex = undefined;
		return this;
	}

	minHeightClear() {
		this.checkLock();
		this.o.minHeight = undefined;
		return this;
	}

	maxHeightClear() {
		this.checkLock();
		this.o.maxHeight = undefined;
		return this;
	}

	heightClear() {
		this.checkLock();
		this.o.height = undefined;
		return this;
	}

	bottomClear() {
		this.checkLock();
		this.o.bottom = undefined;
		return this;
	}

	minWidthClear() {
		this.checkLock();
		this.o.minWidth = undefined;
		return this;
	}

	maxWidthClear() {
		this.checkLock();
		this.o.maxWidth = undefined;
		return this;
	}

	opacityClear() {
		this.checkLock();
		this.o.opacity = undefined;
		return this;
	}

	lineHeightClear() {
		this.checkLock();
		this.o.lineHeight = undefined;
		return this;
	}

	elevationClear() {
		this.checkLock();
		this.o.elevation = undefined;
		return this;
	}

	shadowRadiusClear() {
		this.checkLock();
		this.o.shadowRadius = undefined;
		return this;
	}

	textShadowRadiusClear() {
		this.checkLock();
		this.o.textShadowRadius = undefined;
		return this;
	}

	shadowOpacityClear() {
		this.checkLock();
		this.o.shadowOpacity = undefined;
		return this;
	}

	textShadowOpacityClear() {
		this.checkLock();
		this.o.textShadowOpacity = undefined;
		return this;
	}

	aspectRatioClear() {
		this.checkLock();
		this.o.aspectRatio = undefined;
		return this;
	}

	fontSizeClear() {
		this.checkLock();
		this.o.fontSize = undefined;
		return this;
	}

	topClear() {
		this.checkLock();
		this.o.top = undefined;
		return this;
	}

	marginTopClear() {
		this.checkLock();
		this.o.marginTop = undefined;
		return this;
	}

	marginLeftClear() {
		this.checkLock();
		this.o.marginLeft = undefined;
		return this;
	}

	marginRightClear() {
		this.checkLock();
		this.o.marginRight = undefined;
		return this;
	}

	marginBottomClear() {
		this.checkLock();
		this.o.marginBottom = undefined;
		return this;
	}

	marginHorizontalClear() {
		this.checkLock();
		this.o.marginHorizontal = undefined;
		return this;
	}

	paddingTopClear() {
		this.checkLock();
		this.o.paddingTop = undefined;
		return this;
	}

	paddingLeftClear() {
		this.checkLock();
		this.o.paddingLeft = undefined;
		return this;
	}

	paddingRightClear() {
		this.checkLock();
		this.o.paddingRight = undefined;
		return this;
	}

	paddingBottomClear() {
		this.checkLock();
		this.o.paddingBottom = undefined;
		return this;
	}

	paddingVerticalClear() {
		this.checkLock();
		this.o.paddingVertical = undefined;
		return this;
	}

	paddingHorizontalClear() {
		this.checkLock();
		this.o.paddingHorizontal = undefined;
		return this;
	}

	borderLeftWidthClear() {
		this.checkLock();
		this.o.borderLeftWidth = undefined;
		return this;
	}

	borderRightWidthClear() {
		this.checkLock();
		this.o.borderRightWidth = undefined;
		return this;
	}

	borderTopWidthClear() {
		this.checkLock();
		this.o.borderTopWidth = undefined;
		return this;
	}

	borderBottomWidthClear() {
		this.checkLock();
		this.o.borderBottomWidth = undefined;
		return this;
	}

	borderTopLeftRadiusClear() {
		this.checkLock();
		this.o.borderTopLeftRadius = undefined;
		return this;
	}

	borderTopRightRadiusClear() {
		this.checkLock();
		this.o.borderTopRightRadius = undefined;
		return this;
	}

	borderBottomLeftRadiusClear() {
		this.checkLock();
		this.o.borderBottomLeftRadius = undefined;
		return this;
	}

	borderBottomRightRadiusClear() {
		this.checkLock();
		this.o.borderBottomRightRadius = undefined;
		return this;
	}

}
