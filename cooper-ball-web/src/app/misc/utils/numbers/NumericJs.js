import Console from '../Console';
import Null from '../../../../commom/utils/object/Null';
import StringAfterFirst from '../../../../commom/utils/string/StringAfterFirst';
import StringBeforeFirst from '../../../../commom/utils/string/StringBeforeFirst';
import StringCompare from '../../../../commom/utils/string/StringCompare';
import StringContains from '../../../../commom/utils/string/StringContains';
import StringLength from '../../../../commom/utils/string/StringLength';
import StringParse from '../../../../commom/utils/string/StringParse';
import StringRight from '../../../../commom/utils/string/StringRight';
import UInteger from '../UInteger';
import UNumbers from '../UNumbers';

export default class NumericJs {

	casas = 0;

	asDouble = () => this.toDouble();

	constructor(o, casas) {
		this.casas = casas;
		this.value = this.convert(o);
	}

	convert(o) {
		if (Null.is(o)) {
			return 0.0;
		}
		let asDoubleFunc = o.asDouble;
		if (!Null.is(asDoubleFunc)) {
			return asDoubleFunc();
		}
		let x = UNumbers.round(parseFloat(o), this.casas);
		return x;
	}

	toDouble() {
		return this.value;
	}
	getCasas() {
		return this.casas;
	}

	dividido(o) {
		return new NumericJs(this.toDouble() / this.convert(o), this.casas);
	}
	vezes(o) {
		return new NumericJs(this.toDouble() * this.convert(o), this.casas);
	}
	mais(o) {
		return new NumericJs(this.toDouble() + this.convert(o), this.casas);
	}
	menos(o) {
		return new NumericJs(this.toDouble() - this.convert(o), this.casas);
	}
	pow(o) {
		return new NumericJs(Math.pow(this.toDouble(), this.convert(o)), this.casas);
	}
	isZero() {
		return this.toDouble() === 0.0;
	}
	menorOuIgual(o) {
		return this.toDouble() <= this.convert(o);
	}
	maiorOuIgual(o) {
		return this.toDouble() >= this.convert(o);
	}
	asString() {
		let s = StringParse.get(this.value);
		if (!StringContains.is(s, ".")) {
			s += ".0";
		}
		while (StringLength.get(StringAfterFirst.get(s, ".")) < this.casas) {
			s += "0";
		}
		while (StringLength.get(StringAfterFirst.get(s, ".")) > this.casas) {
			s = StringRight.ignore1(s);
		}
		return s;
	}
	asReal() {
		let s = this.asString();
		if (StringCompare.eq(s, "0.0")) {
			return "-";
		}
		let centavos = StringLength.max(StringAfterFirst.get(s, "."), 2);
		s = StringBeforeFirst.get(s, ".");
		s = UInteger.separarMilhares(s);
		return s + "," + centavos;
	}

	print() {
		Console.log("Numericprint", this.asString());
	}

}
