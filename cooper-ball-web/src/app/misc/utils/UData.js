import ArrayLst from '../../../commom/utils/array/ArrayLst';
import BaseData from '../../../commom/utils/date/BaseData';
import Console from './Console';
import IdText from '../../../commom/utils/object/IdText';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import IntegerFormat from '../../../commom/utils/integer/IntegerFormat';
import Null from '../../../commom/utils/object/Null';
import StringAfterLast from '../../../commom/utils/string/StringAfterLast';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringEquivalente from '../../../commom/utils/string/StringEquivalente';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import StringRight from '../../../commom/utils/string/StringRight';
import UCommons from './UCommons';
import UInteger from './UInteger';

export default class UData {

	static constroi(primeiro, ultimo) {
		let list = new ArrayLst();
		while (primeiro <= ultimo) {
			list.add(new IdText(primeiro, ""+primeiro));
			primeiro++;
		}
		return list;
	}

	static dias;
	static getDias() {
		if (Null.is(UData.dias)) {
			UData.dias = UData.constroi(1, 31);
		}
		return UData.dias;
	}

	static anos;
	static getAnos() {
		if (Null.is(UData.anos)) {
			UData.anos = UData.constroi(1970, UData.hoje().getAno());
		}
		return UData.anos;
	}

	static MESES = ArrayLst.build(
		new IdText(1, "Janeiro"), new IdText(2, "Fevereiro"), new IdText(3, "Março"),
		new IdText(4, "Abril"), new IdText(5, "Maio"), new IdText(6, "Junho"),
		new IdText(7, "Julho"), new IdText(8, "Agosto"), new IdText(9, "Setembro"),
		new IdText(10, "Outubro"), new IdText(11, "Novembro"), new IdText(12, "Dezembro")
	);

	static getIdMes(nome) {
		return UData.MESES.uniqueObrig(o => StringEquivalente.is(nome, o.text)).getId();
	}
	static compareNomeMes(a, b) {
		if (StringEmpty.is(a)) {
			if (StringEmpty.is(b)) {
				return 0;
			} else {
				return -1;
			}
		} else if (StringEmpty.is(b)) {
			return 1;
		} else {
			let va = UData.getIdMes(a);
			let vb = UData.getIdMes(b);
			return UInteger.compare(va, vb);
		}
	}

	static hoje() {
		return BaseData.hoje();
	}

	static toData(date) {
		return BaseData.toData(date);
	}

	static menorQueHoje(o) {
		return o.menorQueHoje();
	}

	static isAnoMesValido(s) {
		s = UData.formatParcialMesAno(s);
		if (!StringLength.is(s, 7)) {
			return false;
		} else {
			return UData.isValida("01/" + s);
		}
	}

	static isValida(s) {
		try {
			let o = UData.strToDate(s);
			return !Null.is(o);
		} catch (e) {
			return false;
		}
	}

	static strToDate(s) {

		if (!StringLength.is(s, 10)) {
			return null;
		}

		s = StringExtraiNumeros.exec(s);

		if (!StringLength.is(s, 8)) {
			return null;
		}

		return BaseData.unformat("[dd][mm][yyyy]", s);

	}

	static parcialValida(s) {

		s = StringExtraiNumeros.exec(s);

		if (StringEmpty.is(s)) {
			return true;
		}

		if (parseInt(s.substring(0, 1)) > 3) {
			return false;
		}

		if (StringLength.is(s, 1)) {
			return true;
		}

		let dia = parseInt(s.substring(0, 2));

		if (dia > 31 || IntegerCompare.eq(dia, 0)) {
			return false;
		}

		if (StringLength.is(s, 2)) {
			return true;
		}

		s = s.substring(2);

		if (parseInt(s.substring(0, 1)) > 1) {
			return false;
		}

		if (StringLength.is(s, 1)) {
			return true;
		}

		let mes = parseInt(s.substring(0, 2));

		if (mes > 12 || IntegerCompare.isZero(mes)) {
			return false;
		}

		if (IntegerCompare.eq(dia, 31) && !UData.MESES31.contains(mes)) {
			return false;
		}

		if (IntegerCompare.eq(mes, 2) && dia > 29) {
			return false;
		}

		if (StringLength.is(s, 2)) {
			return true;
		}

		s = s.substring(2);

		let c0 = parseInt(s.substring(0,1));

		if (c0 < 1 || c0 > 2) {
			return false;
		}

		s = s.substring(1);

		if (StringEmpty.is(s)) {
			return true;
		}

		let c1 = parseInt(s.substring(0,1));
		s = s.substring(1);

		if (UCommons.equals(c0, 1)) {
			if (UCommons.notEquals(c1, 9)) {
				return false;
			}
			if (StringEmpty.is(s)) {
				return true;
			}
			let c2 = parseInt(s.substring(0,1));
			if (c2 < 3) {
				return false;
			}

		} else {

			if (UCommons.notEquals(c1, 0)) {
				return false;
			}
			if (StringEmpty.is(s)) {
				return true;
			}

			if (StringLength.is(s, 1)) {
				let c2 = parseInt(s);
				if ((UData.hoje().getAno() - 2000)/10 < c2) {
					return false;
				}
			} else {
				s = "20" + s.substring(0, 2);
				if (UData.hoje().getAno() < parseInt(s)) {
					return false;
				}
			}
		}

		return true;

	}

	static formatParcialMesAno(s) {
		s = StringExtraiNumeros.exec(s);
		if (StringEmpty.is(s)) {
			return "";
		}
		return UData.formatParcial("01" + s).substring(3);
	}

	static formatParcial(s) {
		s = StringExtraiNumeros.exec(s);
		if (StringEmpty.is(s)) {
			return "";
		}
		while (s.startsWith("00")) {
			s = s.substring(1);
		}
		if (StringCompare.eq(s, "0")) {
			return "0";
		}
		if (parseInt(s.substring(0, 1)) > 3) {
			s = "0" + s;
		}
		if (StringLength.is(s, 1)) {
			return s;
		}
		let dia = parseInt(s.substring(0,2));

		if (dia > 31) {
			return "3";
		}

		let sdia = IntegerFormat.xx(dia);
		s = s.substring(2);

		if (StringEmpty.is(s)) {
			return sdia;
		}

		/* mes */

		while (s.startsWith("00")) {
			s = s.substring(1);
		}
		if (StringCompare.eq(s, "0")) {
			return sdia + "/0";
		}
		if (parseInt(s.substring(0, 1)) > 1) {
			s = "0" + s;
		}
		if (StringLength.is(s, 1)) {
			return sdia + "/" + s;
		}
		let mes = parseInt(s.substring(0,2));
		if (mes > 12) {
			s = "0" + s;
			mes = parseInt(s.substring(0,2));
		}

		let smes = IntegerFormat.xx(mes);

		if (dia > 29 && IntegerCompare.eq(mes, 2)) {
			return sdia + "/0";
		}
		if (IntegerCompare.eq(dia, 31)) {
			if (!UData.MESES31.contains(mes)) {
				return sdia + "/" + smes.substring(0, 1);
			}
		}

		s = s.substring(2);
		let result = sdia + "/" + smes;

		if (StringEmpty.is(s)) {
			return result;
		}

		/* ano */

		if (s.startsWith("2")) {
			result += "/2";
			s = s.substring(1);
			if (StringEmpty.is(s) || !s.startsWith("0")) {
				return result;
			} else {
				result = result + StringLength.max(s, 3);
			}
		} else if (StringCompare.eq(s, "1") || StringCompare.eq(s, "19")) {
			return result + "/" + s;
		} else if (!s.startsWith("19")) {
			result = result + "/19" + StringLength.max(s, 2);
		} else {
			result = result + "/" + StringLength.max(s, 4);
		}

		if (IntegerCompare.eq(mes, 2) && IntegerCompare.eq(dia, 29) && StringLength.is(result, 10)) {
			let ano = parseInt(StringAfterLast.get(result, "/"));
			if (!UCommons.equals(ano % 4, 0)) {
				result = StringRight.ignore1(result);
			}
		}

		return result;

	}

	static formatDataHora(data) {
		if (Null.is(data)) {
			return null;
		}
		let hora = data.getHours();
		let minuto = data.getMinutes();
		let segundo = data.getSeconds();
		return UData.formatData(data) + " " + IntegerFormat.xx(hora) + ":" + IntegerFormat.xx(minuto) + ":" + IntegerFormat.xx(segundo);
	}

	static formatData(data) {
		if (Null.is(data)) {
			return null;
		}
		Console.log("UData.formatData", data);

		let dia = data.getDate();
		let mes = data.getMonth() + 1;
		let ano = data.getYear() + 1900;
		return IntegerFormat.xx(dia) + "/" + IntegerFormat.xx(mes) + "/" + ano;
	}

	static test(s, esperado) {
		let x = UData.formatParcial(s);
		if (!esperado.equals(x)) {
			console.log(s + " - " + esperado + " - " + x);
		}
	}

	static convert(data) {
		return new Date(data.getAno(), data.getMes(), data.getDia(), data.getHora(), data.getMinuto(), data.getSegundo(), data.getMilesimo());
	}

	static build(date) {
		return new BaseData(date.getYear() + 1900, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), 0);
	}

}
UData.MESES31 = ArrayLst.build(1, 3, 5, 7, 8, 10, 12);
