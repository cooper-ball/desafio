import ArrayLst from '../../../commom/utils/array/ArrayLst';
import StringParse from '../../../commom/utils/string/StringParse';

export default class Console {

	static observers = new ArrayLst();

	static notify(type, key, value) {
		let c = {};
		c.type = type;
		c.key = key;
		c.value = StringParse.get(value);
		Console.observers.forEach(o => o(c));
	}

	static log(key, value) {
		console.log(key, value);
		Console.notify("log", key, value);
	}

	static error(key, value) {
		console.error(key, value);
		Console.notify("error", key, value);
	}

	static disableYellowBox() {
		console.disableYellowBox = true;
	}

	static erroDeDesenvolvimento(s) {
		console.error(s);
	}

}
