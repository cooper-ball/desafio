import Equals from '../../../commom/utils/object/Equals';
import IntegerCompare from '../../../commom/utils/integer/IntegerCompare';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';

export default class UIdText {
	static compareText(a, b) {
		if (Equals.is(a, b)) return 0;
		if (Null.is(a)) return -1;
		if (Null.is(b)) return 1;
		return StringCompare.compare(a.text, b.text);
	}
	static isId(o, id) {
		if (Null.is(o)) return Null.is(id);
		return IntegerCompare.eq(o.id, id);
	}
}
