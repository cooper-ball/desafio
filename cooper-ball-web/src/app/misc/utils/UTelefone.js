import Null from '../../../commom/utils/object/Null';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import StringRight from '../../../commom/utils/string/StringRight';

export default class UTelefone {

	static formatParcial(s, len) {

		if (StringEmpty.is(s)) {
			return "";
		}

		s = StringExtraiNumeros.exec(s);

		if (StringEmpty.is(s)) {
			return "";
		}

		if (StringLength.get(s) < 2) {
			return "(" + s;
		}

		let x = "(" + s.substring(0, 2);
		s = s.substring(2);

		if (StringEmpty.is(s)) {
			return x;
		}

		s = StringLength.max(s, len);

		x += ") ";

		s = x + UTelefone.formatNumero(s);

		return s;

	}

	static formatNumero(s) {
		if (StringEmpty.is(s)) {
			return "";
		}
		let x = "";
		if (StringLength.get(s) > 4) {
			while (StringLength.get(s) > 4) {
				x = "-" + StringRight.get(s, 4) + x;
				s = StringRight.ignore(s, 4);
			}
		}
		s = s + x;
		return s;
	}

	static isValid(s, len) {
		s = UTelefone.formatParcial(s, len);
		if (StringLength.get(s) < 14) {
			return false;
		}
		return true;
	}

	static main(args) {
		console.log(UTelefone.formatComDdi(55,61,"992559810"));
		console.log(UTelefone.formatComDdi(null,61,"992559810"));
		console.log(UTelefone.formatComDdi(null,null,"992559810858"));
		console.log(UTelefone.formatComDdi(null,null,"9810858"));
		console.log(UTelefone.formatComDdi(55,null,"992559810"));
	}

	static formatComDdi(ddi, ddd, numero) {
		let s = "";
		if (!Null.is(ddi)) {
			s = "+" + ddi + " ";
		}
		return s + UTelefone.formatComDdd(ddd, numero);
	}

	static formatComDdd(ddd, numero) {
		let s = "";
		if (!Null.is(ddd)) {
			s += "(" + ddd + ") ";
		}
		s += UTelefone.formatNumero(numero);
		return s.trim();
	}

}
