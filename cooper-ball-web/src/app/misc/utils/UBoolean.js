import Equals from '../../../commom/utils/object/Equals';
import Null from '../../../commom/utils/object/Null';
import StringCompare from '../../../commom/utils/string/StringCompare';
import StringParse from '../../../commom/utils/string/StringParse';

export default class UBoolean {

	/*nulls/falses/true*/
	static compare(a, b) {
		if (Equals.is(a, b)) return 0;
		if (Null.is(a)) return -1;
		if (Null.is(b)) return 1;
		if (UBoolean.isFalse(a)) return -1;
		if (UBoolean.isTrue(a) === UBoolean.isTrue(b)) return 0;
		return 1;
	}

	static eq(a, b) {
		return UBoolean.compare(a, b) === 0;
	}

	static isTrue(o) {
		if (Null.is(o)) {
			return false;
		} else if (StringCompare.eq(typeof(o), "boolean")) {
			let b = o;
			return b;
		} else if (StringCompare.eq(typeof(o), "string")) {
			let s = StringParse.get(o).toLowerCase();
			return StringCompare.eq(s, "true") || StringCompare.eq(s, "sim") || StringCompare.eq(s, "yes") || StringCompare.eq(s, "1") || StringCompare.eq(s, "y") || StringCompare.eq(s, "verdadeiro");
		} else {
			return false;
		}
	}
	static isFalse(o) {
		if (Null.is(o)) {
			return false;
		} else if (StringCompare.eq(typeof(o), "boolean")) {
			let b = o;
			return !b;
		} else if (StringCompare.eq(typeof(o), "string")) {
			let s = StringParse.get(o);
			return StringCompare.eq(s, "false");
		} else {
			return false;
		}
	}
}
