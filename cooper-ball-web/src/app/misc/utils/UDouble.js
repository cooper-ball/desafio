import IntegerParse from '../../../commom/utils/integer/IntegerParse';
import Null from '../../../commom/utils/object/Null';

export default class UDouble {

	static toDouble(o) {
		if (Null.is(o)) {
			return null;
		} else {
			return parseFloat(o);
		}
	}

	static isEmptyOrZero(o) {
		return Null.is(o) || UDouble.equals(o, 0.0);
	}

	static equals(a, b) {
		return UDouble.compare(a, b) === 0;
	}
	static compare(a, b) {
		if (Null.is(a)) {
			if (Null.is(b)) {
				return 0;
			} else {
				return -1;
			}
		} else if (Null.is(b)) {
			return 1;
		} else if (a === b || a - b === 0) {
			return 0;
		} else if (a > b) {
			return 1;
		} else if (b > a) {
			return -1;
		} else {
			return 0;
		}
	}

	static trunc(value) {
		let x = IntegerParse.toInt(value);
		if (x > value) {
			return x-1;
		} else {
			return x;
		}
	}

}
