import IntegerIs from '../../../commom/utils/integer/IntegerIs';
import StringAfterFirst from '../../../commom/utils/string/StringAfterFirst';
import StringBeforeFirst from '../../../commom/utils/string/StringBeforeFirst';
import StringContains from '../../../commom/utils/string/StringContains';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringExtraiCaracteres from '../../../commom/utils/string/StringExtraiCaracteres';
import StringExtraiNumeros from '../../../commom/utils/string/StringExtraiNumeros';
import StringLength from '../../../commom/utils/string/StringLength';
import StringRight from '../../../commom/utils/string/StringRight';
import StringSplit from '../../../commom/utils/string/StringSplit';
import UConstantes from './UConstantes';

export default class UNumbers {

	static separarMilhares(s) {
		s = StringExtraiNumeros.exec(s);
		if (StringLength.get(s) < 4) {
			return s;
		}
		let result = "";
		while (StringLength.get(s) > 3) {
			result = "." + StringRight.get(s, 3) + result;
			s = StringRight.ignore(s, 3);
		}
		return s + result;
	}

	static round(value, decimais) {

		let s = "" + value;
		if (!StringContains.is(s, ".")) {
			s += ".0";
		}

		let ints = StringBeforeFirst.get(s, ".");
		let decs = StringAfterFirst.get(s, ".");

		if (StringLength.get(decs) <= decimais) {
			return value;
		}

		let itens = StringSplit.exec(decs, "").map(o => parseInt(o));

		while (itens.size() > decimais) {
			let x = itens.removeLast();
			if (x >= 5) {
				let y = itens.removeLast();
				y++;
				itens.add(y);
			}
		}

		if (itens.exists(o => o > 9)) {
			let intValue = parseInt(ints);

			while (itens.exists(o => o > 9)) {
				if (itens.get(0) > 9) {
					let x = itens.remove(0);
					x -= 10;
					intValue++;
					itens.add(x, 0);
				}
				for (let i = 1; i < itens.size(); i++) {
					if (itens.get(i) > 9) {
						let x = itens.remove(i) - 10;
						let y = itens.remove(i-1) + 1;
						itens.add(y, i-1);
						itens.add(x, i);
					}
				}
			}

			ints = "" + intValue;

		}

		decs = itens.join("");

		s = ints + "." + decs;

		value = parseFloat(s);

		return value;
	}
	static formatDouble(value, decimais) {
		value = UNumbers.round(value, decimais);
		let s = "" + value;
		if (!StringContains.is(s, ".")) {
			s += ".0";
		}
		let ints = StringBeforeFirst.get(s, ".");
		return UNumbers.formatString(s, StringLength.get(ints), decimais);
	}
	static formatString(s, inteiros, decimais) {
		s = UNumbers.formatParcial(s, inteiros, decimais);
		if (StringEmpty.is(s)) return "";
		while (!StringLength.is(s, inteiros+decimais+1)) {
			s += "0";
		}
		return s;
	}

	static formatParcial(s, inteiros, decimais) {

		if (inteiros < 1) {
			throw new Error("Invalid inteiros < 1 : " + inteiros);
		}

		if (decimais < 0) {
			throw new Error("Invalid decimais < 0 : " + decimais);
		}

		s = StringExtraiCaracteres.exec(s, UNumbers.CHARS);

		if (StringEmpty.is(s)) {
			return "";
		}

		if (StringContains.is(s, ",")) {
			if (StringContains.is(s, ".")) {
				if (s.indexOf(",") < s.indexOf(".")) {
					s = s.replace(",", "");
				} else {
					s = s.replace(".", "");
					s = s.replace(",", ".");
				}
			} else {
				s = s.replace(",", ".");
			}
		}

		let left;
		let right;

		if (IntegerIs.is(s)) {

			if (StringLength.get(s) <= inteiros) {
				return s;
			}

			left = s.substring(0, inteiros);
			right = s.substring(inteiros);

		} else {

			left = StringBeforeFirst.get(s, ".");
			right = StringExtraiNumeros.exec(StringAfterFirst.get(s, "."));

			if (StringEmpty.is(left)) {
				left = "0";
			} else {
				while (StringLength.get(left) > inteiros) {
					right = StringRight.get(left, 1) + right;
					left = StringRight.ignore1(left);
				}
			}
		}

		right = StringLength.max(right, decimais);
		return left + "." + right;
	}

}
UNumbers.CHARS = UConstantes.numeros.concat2(",").concat2(".");
