import React from 'react';
import Color from '../app/misc/consts/enums/Color';
import Null from '../commom/utils/object/Null';
import Style from '../app/misc/utils/Style';
import SuperComponent from '../app/misc/components/SuperComponent';
import {Input} from 'antd';

export default class InputBind extends SuperComponent {

	didMount() {
		this.observar(this.props.bind);
	}

	render0() {
		return (
			<Input
				placeholder={this.props.bind.getPlaceHolder()}
				addonBefore={this.props.before}
				addonAfter={this.props.after}
				value={this.props.bind.asString()}
				onChange={e => this.props.bind.setCast(e.target.value)}
				size={this.props.small ? "small" : null}
				style={this.getStyle()}
				onKeyPress={e => {
					if (!Null.is(this.props.onKeyPress)) {
						this.props.onKeyPress(e);
					}
				}}
				onKeyDown={this.props.onKeyDown}
				disabled={this.props.bind.isDisabled()}
				allowClear={true}
				autoComplete={"off"}
				name={this.props.id}
				id={this.props.id}
				className={this.props.className}
			/>
		);
	}

	getStyle() {

		let st = Style.create().w100().join(this.props.style);

		if (!Null.is(this.props.bind)) {
			st.marginTop(5);
		}

		st = st.join(this.props.style);

		if (!this.props.bind.isValid() && !this.props.bind.isVirgin()) {
			return st.borderColor(Color.red).get();
		} else {
			return st.get();
		}
	}
}

InputBind.defaultProps = {
	...SuperComponent.defaultProps,
	small: false
}
