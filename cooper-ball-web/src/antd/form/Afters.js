import React from 'react';
import CommonStyles from '../../app/misc/styles/CommonStyles';
import Null from '../../commom/utils/object/Null';
import Span from '../../web/Span';
import SuperComponent from '../../app/misc/components/SuperComponent';

export default class Afters extends SuperComponent {

	static email;
	static alfa;
	static numeric;

	static getAlfa() {
		if (Null.is(Afters.alfa)) {
			Afters.alfa = Afters.get("Significa que o campo aceita letras e numeros", "A");
		}
		return Afters.alfa;
	}

	static getNumeric() {
		if (Null.is(Afters.numeric)) {
			Afters.numeric = Afters.get("Significa que o campo aceita somente numeros", "#");
		}
		return Afters.numeric;
	}

	static getEmail() {
		if (Null.is(Afters.email)) {
			Afters.email = Afters.get("Campo do tipo e-mail", "@");
		}
		return Afters.email;
	}

	static get(a, b) {
		return <Afters aa={a} bb={b}/>;
	}

	render0() {
		return <Span onPress={() => this.alert.info(this.props.aa)} style={Afters.STYLE_ICON} text={this.props.bb}/>;
	}

}
Afters.STYLE_ICON = CommonStyles.POINTER;

Afters.defaultProps = SuperComponent.defaultProps;
