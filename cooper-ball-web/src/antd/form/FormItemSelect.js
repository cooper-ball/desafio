import React from 'react';
import AFormItem from './AFormItem';
import SelectBind from '../SelectBind';

export default class FormItemSelect extends AFormItem {
	getBody(bindParam, idComponent, error) {
		let b = bindParam;
		return <SelectBind bind={b} id={"input-"+idComponent}/>;
	}
}

FormItemSelect.defaultProps = AFormItem.defaultProps;
