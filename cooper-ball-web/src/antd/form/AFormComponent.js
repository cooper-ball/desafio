import React from 'react';
import Color from '../../app/misc/consts/enums/Color';
import FormComponentError from './FormComponentError';
import Frag from '../../react/Frag';
import Label from '../../web/Label';
import StringEmpty from '../../commom/utils/string/StringEmpty';
import Style from '../../app/misc/utils/Style';
import SuperComponent from '../../app/misc/components/SuperComponent';
import {Col} from 'antd';
import {Form} from 'antd';
const FormItem = Form.Item;

export default class AFormComponent extends SuperComponent {

	render0() {

		let lgg = this.props.lg < 1 ? 3 : this.props.lg;
		let md = parseInt(lgg * 1.5);
		if (md > 24) {
			md = 24;
		}

		return (
			<Col lg={lgg} md={md} sm={24} id={"label-" + this.props.idComponent}>
				<FormItem label={this.getLabel()} className={"ant-form-item-with-help"}>
					{this.props.children}
					<FormComponentError message={this.props.error}/>
				</FormItem>
			</Col>
		);
	}

	getLabel() {
		if (StringEmpty.is(this.props.label)) {
			return null;
		} else {
			return (
				<Frag>
					{this.getAsterisco()}
					<Label htmlFor={"input-" + this.props.idComponent} text={this.props.label}/>
				</Frag>
			);
		}
	}

	getAsterisco() {
		if (this.props.asterisco) {
			return <Label style={AFormComponent.ASTERISCO_STYLE} htmlFor={"input-" + this.props.idComponent} text={"*"}/>;
		} else {
			return null;
		}
	}
}
AFormComponent.ASTERISCO_STYLE = Style.create().color(Color.red).fontSize(12).bold(true).marginRight(5);

AFormComponent.defaultProps = {
	...SuperComponent.defaultProps,
	lg: 0,
	idComponent: 0,
	asterisco: false
}
