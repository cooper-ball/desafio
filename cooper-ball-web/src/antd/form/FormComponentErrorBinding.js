import React from 'react';
import FormComponentError from './FormComponentError';
import SuperComponent from '../../app/misc/components/SuperComponent';

export default class FormComponentErrorBinding extends SuperComponent {

	render0() {
		return <FormComponentError message={this.props.bind.getInvalidMessage()}/>;
	}
}

FormComponentErrorBinding.defaultProps = SuperComponent.defaultProps;
