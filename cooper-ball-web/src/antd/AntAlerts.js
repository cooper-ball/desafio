import {message} from 'antd';

export default class AntAlerts {

	init() {
		this.alert.instance = this;
	}

	error(s) {
		message.error(s);
	}

	info(s) {
		message.info(s);
	}

	success(s) {
		message.success(s);
	}

	warn(s) {
		message.warn(s);
	}

	config(top, duration) {
		message.config({top: top, duration: duration});
	}

}
