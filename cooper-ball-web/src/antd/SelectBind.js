import React from 'react';
import Botao from './Botao';
import BotaoType from './BotaoType';
import CommonStyles from '../app/misc/styles/CommonStyles';
import InputBind from './InputBind';
import Null from '../commom/utils/object/Null';
import StringCompare from '../commom/utils/string/StringCompare';
import StringEmpty from '../commom/utils/string/StringEmpty';
import Style from '../app/misc/utils/Style';
import SuperComponent from '../app/misc/components/SuperComponent';
import TextAlign from '../app/misc/consts/enums/TextAlign';
import UCommons from '../app/misc/utils/UCommons';
import {AutoComplete} from 'antd';
import {CaretDownOutlined} from '@ant-design/icons';
const AutoCompleteOption = AutoComplete.Option;

export default class SelectBind extends SuperComponent {
	constructor(props){
		super(props);
		this.state.selectedIndex = 0;
	}

	render0() {

		if (!this.props.bind.isVisible()) return null;
		if (this.props.bind.isDisabled()) {
			return this.getDisabled();
		}

		let sugestoes = this.props.bind.getSugestoes();

		let sugestao = sugestoes.get(this.state.selectedIndex);

		return (
			<AutoComplete style={CommonStyles.W100P.get()} dropdownMatchSelectWidth={false} options={
				sugestoes.map(o => {

					let item = {};

					item.label = (
							<Botao
								title={o.value.text}
								type={UCommons.equals(sugestao, o) ? BotaoType.primary : BotaoType.link}
								block={true}
								onClick={() => this.props.bind.selectSugestion(o)}
								style={SelectBind.BOTAO_STYLE}
								key={o.value.id}
							/>
					);

					return item;

				}).getArray()}>
				<InputBind
				bind={this.props.bind.input}
				style={this.props.style}
				before={this.props.before}
				after={this.getAfter()}
				small={this.props.small}
				onKeyDown={e => {
					if (StringCompare.eq("ArrowDown", e.key)) {
						if (this.state.selectedIndex < sugestoes.size()-1) {
							this.setSelectedIndex(this.state.selectedIndex+1);
						}
					} else if (StringCompare.eq("ArrowUp", e.key)) {
						if (this.state.selectedIndex > 0) {
							this.setSelectedIndex(this.state.selectedIndex-1);
						}
					} else if (StringCompare.eq("Enter", e.key)) {
						this.props.bind.selectSugestion(sugestoes.get(this.state.selectedIndex));
					}
					this.dropDown();
				}}

				id={this.getId()}
				/>
			</AutoComplete>
		);
	}

	getDisabled() {
		return (
			<InputBind
			bind={this.props.bind.input}
			style={this.props.style}
			before={this.props.before}
			small={this.props.small}
			id={this.getId()}
			/>
		);
	}

	getAfter() {
		if (Null.is(this.props.after)) {
			return <CaretDownOutlined style={CommonStyles.POINTER.get()} onClick={() => this.dropDown()}/>;
		} else {
			return this.props.after;
		}
	}

	getId() {
		if (!StringEmpty.is(this.props.id)) {
			return this.props.id;
		} else {
			return this.stringId;
		}
	}

	didMount() {
		this.observar(this.props.bind);
		this.observar(this.props.bind.input);
	}

	dropDown() {
		let o = document.getElementById(this.getId());

		if (Null.is(o)) {
			this.log("Não encontrato um elemento com id " + this.getId());
		} else {
			o.click();
		}

	}
	setSelectedIndex = o => this.setState({selectedIndex:o});
}
SelectBind.BOTAO_STYLE = Style.create().textAlign(TextAlign.left);

SelectBind.defaultProps = {
	...SuperComponent.defaultProps,
	small: false,
	open: true
}
