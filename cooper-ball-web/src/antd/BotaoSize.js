export default class BotaoSize {
	constructor(s) {this.s = s;}
}
BotaoSize.def = new BotaoSize(null);
BotaoSize.large = new BotaoSize("large");
BotaoSize.small = new BotaoSize("small");
