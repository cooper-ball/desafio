import StringEmpty from '../string/StringEmpty';
import StringReplace from '../string/StringReplace';

export default class ObjJs {

	toJSON() {

		let s = this.toJsonImpl();

		if (StringEmpty.is(s)) {
			return null;
		}

		s = StringReplace.exec(s, " ,", ",");
		s = StringReplace.exec(s, ",,", ",");
		s = StringReplace.exec(s, " }", "}");
		s = StringReplace.exec(s, ",}", "}");
		s = StringReplace.exec(s, " ]", "]");
		s = StringReplace.exec(s, ",]", "]");

		return s;

	}

}
