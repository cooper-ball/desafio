import IntegerCompare from '../integer/IntegerCompare';
import Null from './Null';
import ObjJs from './ObjJs';
import StringEmpty from '../string/StringEmpty';

export default class IdText extends ObjJs {

	constructor(id, text) {
		super();
		this.id = id;
		this.text = text;
	}

	getId() {
		return this.id;
	}

	getText() {
		return this.text;
	}

	eqId(value) {
		return IntegerCompare.eq(this.id, value);
	}

	eq(o) {
		if (Null.is(o)) {
			return false;
		} else {
			return this.eqId(o.id);
		}
	}

	toJsonImpl() {

		let s = "";

		if (!Null.is(this.id)) {
			s += ", \"id\": " + this.id;
		}

		if (!Null.is(this.text)) {
			s += ", \"text\": " + this.text;
		}

		if (!Null.is(this.icon)) {
			s += ", \"icon\": " + this.icon;
		}

		if (StringEmpty.notIs(s)) {
			s = s.substring(2);
		}

		return "{"+s+"}";

	}

}
IdText.VAZIO = new IdText(0, "");
