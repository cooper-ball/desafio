import ArrayEmpty from './ArrayEmpty';
import IntegerCompare from '../integer/IntegerCompare';
import Null from '../object/Null';

export default class ArrayLst {

	constructor(array) {
		if (Null.is(array)) {
			this.valueArray = [];
		} else if (!Array.isArray(array)) {
			throw new Error("Não é um array");
		} else {
			this.valueArray = array;
		}
	}

	add(o, index) {
		if (Null.is(index)) {
			this.valueArray.push(o);
		} else {
			this.valueArray.splice(index, 0, o);
		}
		return this;
	}

	addAll(list) {
		if (!ArrayEmpty.is(list)) {
			list.forEach(o => this.push(o));
		}
	}

	filter(predicate) {
		return new ArrayLst(this.valueArray.filter(predicate));
	}

	reduce(func, startValue) {
		return this.valueArray.reduce(func, startValue);
	}

	exists(predicate) {
		return !this.filter(predicate).isEmpty();
	}

	some(predicate) {
		return this.exists(predicate);
	}

	isEmpty() {
		return IntegerCompare.isZero(this.valueArray.length);
	}

	clear() {
		while (!this.isEmpty()) {
			this.removeLast();
		}
		return this;
	}

	removeLast() {
		return this.valueArray.pop();
	}

	removeFirst() {
		return this.valueArray.shift();
	}

	removeIf(predicate) {
		let filter = this.filter(predicate);
		while (!filter.isEmpty()) {
			let o = filter.removeFirst();
			this.removeObject(o);
		}
	}

	removeObject(o) {
		let index = this.indexOf(o);
		if (index === -1) {
			return false;
		} else {
			this.remove(index);
			return true;
		}
	}

	remove(index) {
		let o = this.get(index);
		this.valueArray.splice(index, 1);
		return o;
	}

	get(index) {
		return this.valueArray.get(index);
	}

	indexOf(o) {
		return this.valueArray.indexOf(o);
	}

	concat(other) {
		let va = this.valueArray.length;
		let vb = other.valueArray.length;
		let a = new ArrayLst(this.valueArray.concat(other.valueArray));
		let vc = a.size();
		if (IntegerCompare.ne(va+vb, vc)) {
			throw new Error("O concat nao funcionou");
		}
		return a;
	}

	concat2(os) {
		return this.concat(ArrayLst.build(os));
	}

	forEach(action) {
		this.valueArray.forEach(action);
		return this;
	}

	forEachI(action) {
		this.valueArray.forEach(action);
		return this;
	}

	copy() {
		let list = new ArrayLst();
		this.forEach(o => list.add(o));
		return list;
	}

	contains(o) {
		return this.indexOf(o) > -1;
	}

	join(separator) {
		return this.valueArray.join(separator);
	}

	size() {
		return this.valueArray.length;
	}

	uniqueObrig(predicate) {
		let o = this.unique(predicate);
		if (Null.is(o)) {
			throw new Error("O filtro não retornou resultados");
		}
		return o;
	}

	unique(predicate) {
		let itens = this.filter(predicate);
		if (itens.isEmpty()) {
			return null;
		} else if (itens.size() > 1) {
			throw new Error("O filtro retornou + de 1 resultado");
		} else {
			return itens.get(0);
		}
	}

	sort(comparator) {
		this.valueArray.sort(comparator);
		return this;
	}

	pop() {
		return this.removeLast();
	}
	shift() {
		return this.removeFirst();
	}

	toString() {
		/* um array nao possui o metodo toJSON em js */
		if (this.isEmpty()) {
			return "";
		} else {
			return "["+this.valueArray.join(", ")+"]";
		}
	}

	byId(id,getId) {
		return this.unique(o => IntegerCompare.eq(getId(o), id));
	}

	existsId(id,getId) {
		return !Null.is(this.byId(id, getId));
	}

	push(o) {
		return this.add(o);
	}

	map(func) {
		return new ArrayLst(this.valueArray.map(func));
	}

	mapi(func) {
		return new ArrayLst(this.valueArray.map(func));
	}

	addIfNotContains(o) {
		if (Null.is(o) || this.contains(o)) {
			return false;
		} else {
			this.add(o);
			return true;
		}
	}

	getArray() {
		return this.copy().valueArray;
	}

	getSafe(index) {
		if (this.isEmpty()) {
			return null;
		} else if (this.size() < index +1) {
			return null;
		} else {
			return this.get(index);
		}
	}

	getLast() {
		return this.get(this.size()-1);
	}

	distinct(func) {
		let list = new ArrayLst();
		this.valueArray.forEach(o => list.addIfNotContains(func(o)));
		return list;
	}

	static build(...array) {
		let lst = new ArrayLst();
		let ar = array;
		ar.forEach(a => lst.add(a));
		return lst;
	}

}
