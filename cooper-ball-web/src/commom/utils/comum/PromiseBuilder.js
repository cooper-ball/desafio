export default class PromiseBuilder {

	static ft(func) {
		return new Promise((resolve,reject) => {
			try {
				resolve(func());
			} catch (e) {
				reject(e);
			}
		});
	}

}
