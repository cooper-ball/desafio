import Null from '../object/Null';

export default class StringContains {

	static is(a, b) {
		if (Null.is(a) || Null.is(b)) {
			return false;
		} else if (b.length === 0) {
			return true;
		} else if (a.length === 0) {
			return false;
		} else {
			return a.indexOf(b) > -1;
		}
	}

}
