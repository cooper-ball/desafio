import StringContains from './StringContains';
import StringEmpty from './StringEmpty';
import StringRemoveAcentos from './StringRemoveAcentos';

export default class StringLike {

	static is(a, b) {
		if (StringEmpty.is(b)) {
			return true;
		} else if (StringEmpty.is(a)) {
			return false;
		}
		a = StringRemoveAcentos.exec(a).toLowerCase();
		b = StringRemoveAcentos.exec(b).toLowerCase();
		return StringContains.is(a, b);
	}

}
