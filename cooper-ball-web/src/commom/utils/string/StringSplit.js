import ArrayLst from '../array/ArrayLst';
import Null from '../object/Null';

export default class StringSplit {

	static exec(s, separator) {

		if (Null.is(s)) {
			return new ArrayLst();
		}

		if (s.indexOf(separator) === -1) {
			return ArrayLst.build(s);
		}

		let sp = separator;

		let array = s.split(sp);

		return new ArrayLst(array);

	}

}
