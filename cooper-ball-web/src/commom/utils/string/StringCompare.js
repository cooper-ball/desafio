import Equals from '../object/Equals';
import Null from '../object/Null';
import StringEmpty from './StringEmpty';

export default class StringCompare {

	static eqq(a, b) {

		if (StringEmpty.is(a)) {
			return StringEmpty.is(b);
		} else if (StringEmpty.is(b)) {
			return false;
		} else {
			return StringCompare.eq(a, b);
		}

	}

	static eq(a, b) {

		if (Null.is(a)) {
			return Null.is(b);
		} else if (Null.is(b)) {
			return false;
		}

		/* garante que a comparacao seja por conteudo e não por referencia */
		a += "";
		b += "";

		if (Equals.is(a, b)) {
			return true;
		} else if (StringEmpty.is(a)) {
			return StringEmpty.is(b);
		} else if (StringEmpty.is(b)) {
			return false;
		} else {
			return false;
		}

	}

	static eqIgnoreCase(a, b) {
		if (Null.is(a)) {
			return Null.is(b);
		} else if (Null.is(b)) {
			return false;
		} else {
			return StringCompare.eq(a.toLowerCase(), b.toLowerCase());
		}
	}

	static compare(a, b) {
		if (StringCompare.eq(a, b)) {
			return 0;
		} else if (Null.is(a)) {
			return -1;
		} else if (Null.is(b)) {
			return 1;
		} else {
			return a.localeCompare(b);
		}
	}

}
