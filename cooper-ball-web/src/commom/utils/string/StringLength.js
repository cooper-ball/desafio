import IntegerCompare from '../integer/IntegerCompare';
import StringEmpty from './StringEmpty';

export default class StringLength {

	static get(s) {
		if (StringEmpty.is(s)) {
			return 0;
		} else {
			return s.length;
		}
	}

	static is(s, size) {
		return IntegerCompare.eq(StringLength.get(s), size);
	}

	static max(s, max) {
		if (StringLength.get(s) > max) {
			return s.substring(0, max);
		} else {
			return s;
		}
	}

}
