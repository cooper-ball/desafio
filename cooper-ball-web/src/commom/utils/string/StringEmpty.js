import IntegerCompare from '../integer/IntegerCompare';
import Null from '../object/Null';
import StringCompare from './StringCompare';
import StringTrim from './StringTrim';

export default class StringEmpty {

	static is(s) {
		return Null.is(s) || IntegerCompare.eq(s.trim().length, 0);
	}

	static isPlus(s) {

		if (StringEmpty.is(s)) {
			return true;
		}

		s = StringTrim.plus(s).toLowerCase();

		if (StringCompare.eq(s, "null")) {
			return true;
		} else if (StringCompare.eq(s, "undefined")) {
			return true;
		} else {
			return false;
		}

	}

	static notIs(s) {
		return !StringEmpty.is(s);
	}

}
