import Null from '../object/Null';
import StringContains from './StringContains';
import StringRight from './StringRight';
import StringSplit from './StringSplit';

export default class StringReplace {

	static execPrivate(s, a, b) {

		if (Null.is(s) || Null.is(a) || s.length === 0 || !StringContains.is(s, a)) {
			return s;
		}

		if (Null.is(b)) {
			b = "";
		}

		let split = StringSplit.exec(s, a);
		s = split.join(b);

		if (StringContains.is(s, a)) {
			return StringReplace.execPrivate(s, a, b);
		} else {
			return s;
		}

	}

	static exec(s, a, b) {

		let aux = StringReplace.OCORRENCIA_IMPROVAVEL;
		if (StringContains.is(aux, a)) {
			aux = StringReplace.execPrivate(aux, a, "");
		}

		s += aux;

		if (StringContains.is(b, a)) {
			s = StringReplace.execPrivate(s, a, aux);
			s = StringReplace.execPrivate(s, aux, b);
		} else {
			s = StringReplace.execPrivate(s, a, b);
		}

		s = StringRight.ignore(s, aux.length);

		return s;

	}

	static ifContains(s, a, b) {
		if (StringContains.is(s, a)) {
			return StringReplace.exec(s, a, b());
		} else {
			return s;
		}
	}

	static doWhile(s, a, b) {
		return StringReplace.exec(s, a, b);
	}

}
StringReplace.OCORRENCIA_IMPROVAVEL = "@[%85!2#$%]@))(6¬¢";
