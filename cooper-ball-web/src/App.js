import React from 'react';
import './App.css';
import BotaoLogout from './fc/state/login/BotaoLogout';
import Color from './app/misc/consts/enums/Color';
import Div from './web/Div';
import Frag from './react/Frag';
import LoadingView from './fc/loading/LoadingView';
import LoginView from './fc/state/login/LoginView';
import MessageView from './fc/messages/MessageView';
import Position from './app/misc/consts/enums/Position';
import Span from './web/Span';
import SuperComponent from './app/misc/components/SuperComponent';
import TorcedorFormConsulta from './app/cruds/torcedor/TorcedorFormConsulta';

export default class App extends SuperComponent {

	render0() {
		return (
			<Frag>
				<LoadingView/>
				<MessageView/>
				{this.body()}
			</Frag>
		);
	}

	body() {

		if (this.logado.isTrue()) {
			return (
				<Div style={this.newStyle().w100()}>
					<Div style={this.newStyle().w100().displayFlexRow().position(Position.fixed).padding(5).lineHeight(25).backgroundColor(Color.amareloCooper).top(0).zIndex(9999)}>
						<Span style={this.newStyle().bold(true)} text={"Sistema de Gestao de Torcedores"}/>
						<BotaoLogout/>
					</Div>
					<Div style={this.newStyle().marginTop(50)}>
						<TorcedorFormConsulta/>
					</Div>
				</Div>
			);
		} else {
			return <LoginView/>;
		}

	}

}

App.defaultProps = SuperComponent.defaultProps;
