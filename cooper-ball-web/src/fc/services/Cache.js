import BaseData from '../../commom/utils/date/BaseData';
import CacheItem from './CacheItem';
import JsMap from '../../commom/utils/comum/JsMap';
import Null from '../../commom/utils/object/Null';

export default class Cache {

	map = new JsMap();

	init() {
		this.logado.addFunctionObserver(() => {
			if (this.logado.isFalse()) {
				this.clear();
			}
		});
	}

	clear() {
		this.map.clear();
		this.cacheClear.forceNotifyObservers();
	}

	set(key, value) {
		let o = this.map.get(key);
		if (Null.is(o)) {
			o = new CacheItem();
			this.map.set(key, o);
		}
		o.res = value;
		o.data = BaseData.now();
	}

	get(key) {

		let o = this.map.get(key);
		if (Null.is(o)) {
			return null;
		} else if (o.data.jaPassouSegundos(5)) {
			return null;
		} else {
			return o.res;
		}

	}

}
