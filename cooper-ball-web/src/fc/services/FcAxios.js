import ArrayLst from '../../commom/utils/array/ArrayLst';
import axios from 'axios';
import Null from '../../commom/utils/object/Null';
import PromiseBuilder from '../../commom/utils/comum/PromiseBuilder';
import RequestOptions from './RequestOptions';
import Requisicao from './Requisicao';
import StringCompare from '../../commom/utils/string/StringCompare';
import StringEmpty from '../../commom/utils/string/StringEmpty';
import StringParse from '../../commom/utils/string/StringParse';
import StringRight from '../../commom/utils/string/StringRight';

export default class FcAxios {

	requisicoes = new ArrayLst();

	init() {

		this.uriBase = this.variaveisDeAmbiente.getUriBase();

		if (this.uriBase.endsWith("/")) {
			this.uriBase = StringRight.ignore1(this.uriBase);
		}

		axios.interceptors.request.use(config => {

			if (this.logado.isTrue() && !this.authorization.isEmpty()) {
				config.headers.authorization = this.authorization.get();
			} else {
				config.headers.authorization = null;
			}

			return config;

		});

	}

	getHeaders() {
		let o = {};
		o["content-type"] = "application/json;charset=UTF-8";
		o.accept = "*/*";
		return o;
	}

	getConfig() {
		let config = {};
		config.headers = this.getHeaders();
		config.withCredentials = true;
		return config;
	}

	exec(options, runner) {

		if (this.logado.isTrue()) {

			let o = this.cache.get(options.key);

			if (!Null.is(o)) {
				return PromiseBuilder.ft(() => {
					if (!Null.is(options.callback)) {
						options.callback(o);
					}
					return o;
				});
			}

		}

		let requisicaoEmAndamento = this.requisicoes.unique(o => StringCompare.eq(o.key, options.key));

		if (!Null.is(requisicaoEmAndamento)) {
			if (!Null.is(options.callback)) {
				requisicaoEmAndamento.callbacks.add(options.callback);
			}
			return requisicaoEmAndamento.promessa;
		}

		let req = new Requisicao();
		req.key = options.key;

		req.promessa = new Promise((resolve, reject) => {

			let config = this.getConfig();

			runner(this.uriBase + options.uri, config)
			.then(res => {
				if (Null.is(res.body)) {
					res.body = res.data;
				}
				this.cache.set(options.key, res);
				req.callbacks.forEach(o => o(res));
				resolve(res);
				return res;
			})
			.catch(e => {
				if (options.exibirMensagemCasoOcorraUmErro) {
					this.alert.error(FcAxios.getMessage(e));
				}
				reject(e);
			})
			.finally(() => {
				this.requisicoes.removeObject(req);
			});

		});

		if (!Null.is(options.callback)) {
			req.callbacks.add(options.callback);
		}

		this.requisicoes.add(req);

		return req.promessa;

	}

	static getMessage(e) {

		let res = e.response;

		if (Null.is(res)) {
			return e.message;
		}

		let dados = Null.is(res.data) ? res.body : res.data;

		let s;

		if (Null.is(dados)) {
			s = FcAxios.getMessagePrivate(e);
		} else {
			s = FcAxios.getMessagePrivate(dados);
		}

		if (StringEmpty.isPlus(s)) {
			return e.message;
		} else {
			return s;
		}

	}

	static getMessagePrivate(dados) {
		let s = dados.message;
		if (StringEmpty.notIs(s)) {
			return s;
		}
		s = dados.menssagem;
		if (StringEmpty.notIs(s)) {
			return s;
		}
		s = dados.msg;
		if (StringEmpty.notIs(s)) {
			return s;
		}
		s = dados.statusText;
		if (StringEmpty.notIs(s)) {
			return s;
		}
		return null;
	}

	getComOptions(options) {
		options.uri = this.tratarUrl(options.uri);
		options.key = "get+" + options.uri;
		return this.exec(options, (ur, config) => axios.get(ur, config));
	}

	get(uri, callback) {
		let options = new RequestOptions();
		options.uri = uri;
		options.callback = callback;
		return this.postComOptions(options);
	}

	postComOptions(options) {

		let paramsString = StringParse.get(options.params);

		/* nao remover, pois é o q faz funfar no js */
		let bodyRequest = JSON.parse(paramsString);

		if (StringCompare.eq(StringParse.get(bodyRequest), "[object Object]")) {
			console.log(options.params);
			throw new Error(paramsString);
		}

		options.uri = this.tratarUrl(options.uri);
		options.key = "post+" + options.uri + "+" + StringParse.get(options.params);
		return this.exec(options, (ur, config) => axios.post(ur, bodyRequest, config));

	}

	post(uri, params, callback) {
		let options = new RequestOptions();
		options.uri = uri;
		options.params = params;
		options.callback = callback;
		return this.postComOptions(options);
	}

	tratarUrl(uri) {

		if (!uri.startsWith("/")) {
			uri = "/" + uri;
		}

		return uri;

	}
}
