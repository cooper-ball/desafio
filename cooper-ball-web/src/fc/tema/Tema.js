import Binding from '../../app/campos/support/Binding';
import IntegerCompare from '../../commom/utils/integer/IntegerCompare';
import Null from '../../commom/utils/object/Null';
import TemaDefault from './TemaDefault';

export default class Tema extends Binding {

	afterConstruct() {
		if (this.isEmpty()) {
			this.set(new TemaDefault());
		}
	}

	static devMode = false;

	tituloAplicacao() {
		return this.get().tituloAplicacao();
	}

	banner() {
		return this.get().banner();
	}

	buttonPrimary() {
		return this.get().buttonPrimary();
	}

	id() {
		if (this.isEmpty()) {
			return 0;
		} else {
			return this.get().id();
		}
	}

	eq(o) {
		if (Null.is(o)) {
			return this.isEmpty();
		} else {
			return IntegerCompare.eq(o.id(), this.id());
		}
	}

}
