import React from 'react';
import AlignSelf from '../../app/misc/consts/enums/AlignSelf';
import Div from '../../web/Div';
import Img from '../../web/Img';
import Span from '../../web/Span';
import SuperComponent from '../../app/misc/components/SuperComponent';
import {Modal} from 'antd';

export default class MessageView extends SuperComponent {

	render0() {
		return (
			<Modal
				visible={this.mensagens.intValue() > 0}
				footer={null}
				onCancel={() => this.mensagens.remove()}>
				<Div style={this.newStyle().displayFlexCol().lineHeight(35).fontSize(20).textAlignCenter()}>
					<Img src={this.variaveisDeAmbiente.logo} style={this.newStyle().widthPercent(40).alignSelf(AlignSelf.center).marginBottom(10)}/>
					<Span text={this.mensagens.nextText()}/>
				</Div>
			</Modal>
		);
	}

}

MessageView.defaultProps = SuperComponent.defaultProps;
