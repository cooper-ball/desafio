import ArrayLst from '../../commom/utils/array/ArrayLst';
import VInteger from '../../app/campos/support/VInteger';

export default class Mensagens extends VInteger {

	constructor() {
		super();
		this.setMaximo(20);
	}

	list = new ArrayLst();

	add(s, tipo) {
		let o = {};
		o.s = s;
		o.tipo = tipo;
		this.list.add(o);
		this.inc();
	}

	success(s) {
		this.add(s, 1);
	}

	warning(s) {
		this.add(s, 2);
	}

	error(s) {
		this.add(s, 3);
	}

	nextText() {
		if (this.intValue() === 0) {
			return null;
		} else {
			return this.list.get(0).s;
		}
	}

	nextTipo() {
		if (this.intValue() === 0) {
			return 0;
		} else {
			return this.list.get(0).tipo;
		}
	}

	remove() {
		this.list.remove(0);
		this.dec();
	}

}
