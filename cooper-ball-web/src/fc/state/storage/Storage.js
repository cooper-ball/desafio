import ArrayLst from '../../../commom/utils/array/ArrayLst';
import IntegerParse from '../../../commom/utils/integer/IntegerParse';
import StorageBox from './StorageBox';
import StringCompare from '../../../commom/utils/string/StringCompare';
import UBoolean from '../../../app/misc/utils/UBoolean';

export default class Storage {

	keys = new ArrayLst();

	stringBox(key) {
		return this.createBox(key, s => s);
	}

	intBox(key) {
		return this.createBox(key, s => IntegerParse.toInt(s));
	}

	booleanBox(key) {
		return this.createBox(key, s => UBoolean.isTrue(s));
	}

	createBox(key,conversor) {

		if (this.keys.exists(s => StringCompare.eq(s, key))) {
			throw new Error("Key já adicionada: " + key);
		}

		this.keys.add(key);

		return new StorageBox(this.storage, key, conversor);

	}

}
