import Box from '../../../commom/utils/comum/Box';
import Null from '../../../commom/utils/object/Null';

export default class StorageBox extends Box {

	constructor(storage, key,conversor) {
		super();
		this.storage = storage;
		this.key = key;
		this.conversor = conversor;
	}

	set(valueP) {
		if (!Null.is(this.storage)) {
			this.storage.set(this.key, valueP);
		}
	}

	get() {
		let s = this.storage.get(this.key);
		if (Null.is(s)) {
			return null;
		} else {
			return this.conversor(s);
		}
	}

}
