import React from 'react';
import Div from '../../../web/Div';
import FcBotao from '../../components/FcBotao';
import FormItemInput from '../../../antd/form/FormItemInput';
import FormKeyDownObserver from '../../components/FormKeyDownObserver';
import Img from '../../../web/Img';
import LayoutApp from '../../components/LayoutApp';
import ResourcesFc from '../../../resources/ResourcesFc';
import Span from '../../../web/Span';
import Style from '../../../app/misc/utils/Style';
import {Card} from 'antd';
import {Row} from 'antd';

export default class LoginView extends FormKeyDownObserver {

	render00() {
		return (
			<Div style={LoginView.FORM_STYLE}>
				<Card size={"small"} style={LoginView.CARD_STYLE.get()} className={"card-table"}>
					<Row gutter={10}>
						<Div style={LoginView.ALIGN_ITEMS_CENTER}>
							<Img src={ResourcesFc.user} style={LoginView.IMAGE_STYLE}/>
						</Div>
					</Row>
					<Row gutter={10}>
						<Div style={LoginView.ALIGN_ITEMS_CENTER}>
							<Span style={LoginView.TEXT_STYLE} text={"Por favor, autentique-se!"}/>
						</Div>
					</Row>
					<Row gutter={10}>
						<FormItemInput bind={this.usuario} lg={24}/>
					</Row>
					<Row gutter={10}>
						<FormItemInput bind={this.senha} lg={24}/>
					</Row>
					<Row gutter={10}>
						<Div style={LoginView.ALIGN_ITEMS_CENTER}>
							<FcBotao title={"Efetuar Login"} acao={() => this.efetuarLogin.exec()}/>
						</Div>
					</Row>
				</Card>
			</Div>
		);
	}

	onKeyDown0(e) {
		if (e.enter()) {
			this.efetuarLogin.exec();
		}
	}

}
LoginView.TEXT_STYLE = Style.create();
LoginView.IMAGE_STYLE = Style.create().height(100);
LoginView.FORM_STYLE = Style.create().width(500).margin("auto").padding(30);
LoginView.ALIGN_ITEMS_CENTER = Style.create().textAlignCenter().alignItemsCenter().w100().paddingBottom(20);
LoginView.CARD_STYLE = LayoutApp.createStyle().marginTop(15).padding(20);

LoginView.defaultProps = FormKeyDownObserver.defaultProps;
