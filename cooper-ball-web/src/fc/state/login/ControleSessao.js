import BaseData from '../../../commom/utils/date/BaseData';
import Sistema from '../system/Sistema';
import VBaseData from '../../../app/campos/support/VBaseData';

export default class ControleSessao extends VBaseData {

	situacao = false;

	init() {

		this.logado.addFunctionObserver(() => {
			this.situacao = this.logado.isTrue();
			if (this.situacao) {
				this.setNow();
			} else {
				this.clear();
			}
		});

		this.val = this.storage.createBox("controleSessao", s => BaseData.unJson(s));

		this.monitorar();

	}

	monitorar() {

		if (Sistema.monitoramentoLigado) {

			if (this.isEmpty()) {
				if (this.situacao) {
					this.situacao = false;
					this.logado.forceNotifyObservers();
				}
			} else if (this.get().jaPassouSegundos(5)) {
				this.situacao = false;
				this.efetuarLogout.exec();
			} else {
				if (!this.situacao) {
					this.situacao = true;
					this.logado.forceNotifyObservers();
				}
				this.setNow();
			}

			setTimeout(() => this.monitorar(), 1000);

		}

	}

}
