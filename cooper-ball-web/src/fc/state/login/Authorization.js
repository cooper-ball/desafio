import VString from '../../../app/campos/support/VString';

export default class Authorization extends VString {

	init() {
		this.setMaxLength(8000);
		this.val = this.storage.stringBox("authorization");
	}

}
