import Sistema from '../system/Sistema';
import VBoolean from '../../../app/campos/support/VBoolean';

export default class Logado extends VBoolean {

	init() {
		this.val = this.storage.booleanBox("logado");
		this.authorization.addObserver(this);
		this.monitorar();
	}

	monitorar() {
		if (Sistema.monitoramentoLigado) {
			this.checar();
			setTimeout(() => this.monitorar(), 1000);
		}
	}

	checar() {
		this.set(!this.authorization.isEmpty());
	}

	notify(o) {
		this.checar();
	}

}
