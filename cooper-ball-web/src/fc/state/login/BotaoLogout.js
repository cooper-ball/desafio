import React from 'react';
import BotaoSize from '../../../antd/BotaoSize';
import FcBotao from '../../components/FcBotao';
import SuperComponent from '../../../app/misc/components/SuperComponent';

export default class BotaoLogout extends SuperComponent {

	render0() {

		return (
				<FcBotao title={"Sair"} acao={() => this.efetuarLogout.exec()} size={BotaoSize.small}/>
		);

	}

}

BotaoLogout.defaultProps = SuperComponent.defaultProps;
