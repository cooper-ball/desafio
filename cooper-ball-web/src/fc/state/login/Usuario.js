import VString from '../../../app/campos/support/VString';

export default class Usuario extends VString {

	constructor() {
		super();
		this.setPlaceHolder("usuario");
	}

	init() {
		this.val = this.storage.stringBox("usuario");
	}

}
