import LoginRequestTo from './LoginRequestTo';

export default class EfetuarLogin {

	exec() {

		let to = new LoginRequestTo();
		to.username = this.usuario.asString();
		to.password = this.senha.get();

		this.axios.post("login/signin", to, res => {
			this.callBack(res);
		}).catch(error => {
			setTimeout(() => this.loadings.dec());
			let s = error.getMessage();
			this.mensagens.error(s);
		}).finally(() => {
			this.loadings.dec();
		});

		this.loadings.inc();

	}

	callBack(res) {
		this.authorization.set(res.headers.authorization);
		if (this.authorization.isEmpty()) {
			this.mensagens.error(res.message);
		}
	}

}
