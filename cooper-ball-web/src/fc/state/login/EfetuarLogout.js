import RequestOptions from '../../services/RequestOptions';

export default class EfetuarLogout {

	exec() {
		if (this.authorization.isEmpty()) {
			this.authorization.forceNotifyObservers();
		} else {
			let options = new RequestOptions();
			options.uri = "login/signout";
			options.exibirMensagemCasoOcorraUmErro = false;
			this.fcAxios.getComOptions(options);
			this.authorization.clear();
		}
	}

}
