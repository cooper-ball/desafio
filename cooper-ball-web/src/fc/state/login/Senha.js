import VSenha from '../../../app/campos/support/VSenha';

export default class Senha extends VSenha {

	afterConstruct() {
		this.setPlaceHolder("Senha");
		this.setNotNull(true);
	}

	init() {
		this.val = this.storage.stringBox("senha");
	}

}
