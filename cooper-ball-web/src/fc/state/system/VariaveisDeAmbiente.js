import StringEmpty from '../../../commom/utils/string/StringEmpty';

export default class VariaveisDeAmbiente {

	getUriBase() {

		if (StringEmpty.is(process.env.REACT_APP_URL)) {
			return "http://localhost:8080/";
		} else {
			return process.env.REACT_APP_URL;
		}

	}

}
