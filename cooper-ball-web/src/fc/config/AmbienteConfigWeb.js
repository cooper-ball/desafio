import React from 'react';
import Ambiente from './Ambiente';
import CommonStyles from '../../app/misc/styles/CommonStyles';
import FcBotao from '../components/FcBotao';
import {message} from 'antd';

export default class AmbienteConfigWeb {

	init() {
		Ambiente.set(this);
		message.config({top: 100, duration: 5});
		this.antAlerts.config(100, 5);
	}

	getLocalStorage() {
		return this.lStorage;
	}

	getSessionStorage() {
		return this.sStorage;
	}

	getButton(title, onClick) {
		return <FcBotao style={CommonStyles.W100P} title={title} acao={onClick}/>;
	}

}
