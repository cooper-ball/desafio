import ArrayLst from '../../commom/utils/array/ArrayLst';
import Binding from '../../app/campos/support/Binding';
import GrupoDeCampos from './GrupoDeCampos';
import Id from './Id';
import Null from '../../commom/utils/object/Null';
import SearchService from './SearchService';
import UInteger from '../../app/misc/utils/UInteger';
import VBoolean from '../../app/campos/support/VBoolean';
import VCep from '../../app/campos/support/VCep';
import VCnpj from '../../app/campos/support/VCnpj';
import VCpf from '../../app/campos/support/VCpf';
import VCpfOuCnpj from '../../app/campos/support/VCpfOuCnpj';
import VData from '../../app/campos/support/VData';
import VEmail from '../../app/campos/support/VEmail';
import VFk from '../../app/campos/support/VFk';
import VId from '../../app/campos/support/VId';
import VInteger from '../../app/campos/support/VInteger';
import VList from '../../app/campos/support/VList';
import VLong from '../../app/campos/support/VLong';
import VNomeProprio from '../../app/campos/support/VNomeProprio';
import VNumeric from '../../app/campos/support/VNumeric';
import VSenha from '../../app/campos/support/VSenha';
import VString from '../../app/campos/support/VString';
import VSubList from './VSubList';
import VTelefone from '../../app/campos/support/VTelefone';
import VVinculado from '../../app/campos/support/VVinculado';

export default class EntityCampos extends VBoolean {

	modalComentarios = new VBoolean();
	camposInvalidos = new GrupoDeCampos(o => !o.isValid());

	disabledObservers = false;
	construido = false;

	static novos = 0;
	afterSaveObservers = new ArrayLst();

	init() {
		this.listBindings = new ArrayLst();
		this.controleVinculado = this.newBoolean("controleVinculado", true, "Sistema").setStartValue(false);
		this.id = new VId().setLabel("id");
		this.add(this.id, false, "Geral").setMinimo(-999999999);
		this.houveAlteracoes = new VBoolean().setLabel("houveAlteracoes").setStartValue(false);
		this.excluido = this.newBoolean("Excluído", true, "Sistema").setDisabled(true);
		this.registroBloqueado = this.newBoolean("Registro Bloqueado", true, "Sistema").setDisabled(true);
		this.initImpl();
	}

	isRascunho() {
		return this.id.isEmpty() || this.id.get() < 1;
	}

	post(uriP, params) {
		return this.fcAxios.post(this.getEntidade() + "/" + uriP, params, null);
	}

	excluir(idP, onSuccess) {
		return this.post("delete", new Id(idP)).then(res => {
			if (!Null.is(onSuccess)) {
				this.alert.info("Registro excluído com sucesso!");
				onSuccess();
			}
			return res;
		});
	}

	pronto() {
		return !this.disabledObservers && this.construido;
	}

	isReadOnly() {
		return this.excluido.isTrue() || this.registroBloqueado.isTrue();
	}

	calcChange(bind) {
		if (this.pronto()) {
			this.houveAlteracoes.set(this.houveMudancas());
		}
	}

	reiniciar() {
		this.houveAlteracoes.set(false);
		this.listBindings.forEach(o => o.setVirgin(true));
		this.reiniciar2();
		Binding.notificacoesDesligadasDec();
		this.disabledObservers = false;
		this.forceNotifyObservers();
		setTimeout(() => this.listBindings.forEach(o => o.notifyObservers()), 50);
	}

	reiniciar2() {}

	notify(o) {
		this.notifyObservers();
	}

	touch() {
		this.listBindings.forEach(campo => campo.setVirgin(false));
	}

	haImpedimentos() {
		return this.camposInvalidos.isEmpty.isFalse();
	}

	getErros() {
		return this.camposInvalidos.filter(campo => !campo.isVirgin());
	}

	getImpedimentos() {
		return this.listBindings.filter(campo => !campo.isValid()).reduce((s, campo) => s + ";" + campo.getLabel() + ":" + campo.getInvalidMessage(), "");
	}

	add(campoP, notNull, aba) {
		this.listBindings.add(campoP);
		campoP.setNotNull(notNull);
		campoP.setAtribute("aba", aba);
		campoP.addObserver(this);
		campoP.addFunctionTObserver(bind => this.calcChange(bind));
		this.camposInvalidos.add(campoP);
		return campoP;
	}

	newCpf(nomeP, notNull, aba) {
		return this.add(new VCpf().setLabel(nomeP), notNull, aba);
	}

	newCnpj(nomeP, notNull, aba) {
		return this.add(new VCnpj().setLabel(nomeP), notNull, aba);
	}

	newCpfOuCnpj(nomeP, notNull, aba) {
		return this.add(new VCpfOuCnpj().setLabel(nomeP), notNull, aba);
	}

	newData(nomeP, notNull, aba) {
		return this.add(new VData().setLabel(nomeP), notNull, aba);
	}

	newEmail(nomeP, notNull, aba) {
		return this.add(new VEmail().setLabel(nomeP), notNull, aba);
	}

	newImagem(nomeP, notNull, aba) {
		return this.add(new VString().setMaxLength(100).setLabel(nomeP), notNull, aba);
	}

	newBoolean(nomeP, notNull, aba) {
		return this.add(new VBoolean().setLabel(nomeP), notNull, aba);
	}

	newBotaoFront(nomeP, onClick) {
		let o = new VBoolean().setLabel(nomeP);
		o.setNotNull(false);
		o.setStartValue(false);
		o.addFunctionObserver(onClick);
		return o;
	}

	newBotao(nomeP, nomeMetodo, callBack) {
		let o = new VBoolean().setLabel(nomeP);
		o.setNotNull(false);
		o.setStartValue(false);
		o.addFunctionObserver(() => {
			this.post(nomeMetodo, this.id.get()).then(res => {
				this.setJson(res.body);
				callBack();
				return res;
			});
		});
		return o;
	}

	newCep(nomeP, notNull, aba) {
		return this.add(new VCep().setLabel(nomeP), notNull, aba);
	}

	newTelefone(nomeP, notNull, aba) {
		return this.add(new VTelefone().setLabel(nomeP), notNull, aba);
	}

	newNomeProprio(nomeP, notNull, aba) {
		return this.add(new VNomeProprio().setLabel(nomeP), notNull, aba);
	}

	newString(nomeP, size, notNull, aba) {
		return this.add(new VString().setMaxLength(size).setLabel(nomeP), notNull, aba);
	}

	newLong(nomeP, max, notNull, aba) {
		return this.add(new VLong().setMax(max).setLabel(nomeP), notNull, aba);
	}

	newSenha(nomeP, size, notNull, aba) {
		return this.add(new VSenha().setMaxLength(size).setLabel(nomeP), notNull, aba);
	}

	newVinculado(nomeP, notNull, aba) {
		let o = this.add(new VVinculado().setLabel(nomeP), notNull, aba);
		o.setStartValue(false);
		o.addFunctionObserver(() => this.controleVinculado.set(o.get()));
		return o;
	}

	newNumeric(nomeP, inteiros, decimais, nullIfZeroWhenDisabled, notNull, aba) {
		return this.add(new VNumeric().setInteiros(inteiros).setDecimais(decimais).setNullIfZeroWhenDisabled(nullIfZeroWhenDisabled).setLabel(nomeP), notNull, aba);
	}

	newInteger(nomeP, maximo, notNull, aba) {
		return this.add(new VInteger().setMaximo(maximo).setLabel(nomeP), notNull, aba);
	}

	newList(nomeP, itensP, notNull, aba) {
		return this.add(new VList().setItens(itensP).setLabel(nomeP), notNull, aba);
	}

	createSubList(titleP, nomeCampoP,mergeFunction, carragarCallBack) {
		let o = new VSubList(titleP, mergeFunction, carragarCallBack, this.id, this.getEntidadePath() + "/get-" + nomeCampoP, this.fcAxios);
		o.setStartValue(false);
		o.addFunctionObserver(() => this.controleVinculado.set(o.get()));
		return o;
	}

	newSubList(titleP, nomeCampoP,mergeFunction, carragarCallBack, notNull, aba, disabled) {
		let o = this.createSubList(titleP, nomeCampoP, mergeFunction, carragarCallBack);
		this.add(o, notNull, aba);
		if (disabled) {
			o.setDisabled(true);
		}
		return o;
	}

	newFk(nomeP, entidadeP, notNull, aba) {
		return this.add(new VFk(nomeP, new SearchService(entidadeP + "/consulta-select", null)), notNull, aba);
	}

	save(vinculo, onSuccess) {
		if (this.haImpedimentos()) {
			this.touch();
			return false;
		} else {
			if (Null.is(vinculo)) {
				this.post("save", this.getTo()).then(res => {
					this.setJson(res.body);
					this.afterSave(onSuccess);
					return res;
				});
			} else {
				vinculo.confirm();
				this.afterSave(onSuccess);
			}
			return true;
		}
	}

	afterSave(onSuccess) {
		if (!Null.is(onSuccess)) {
			onSuccess();
		}
		this.afterSaveObservers.forEach(func => func());
	}

	buscarCampoLookup(entidadePath, campoP, idP, onSuccess) {
		if (UInteger.isEmptyOrZero(idP)) {
			onSuccess(null);
		} else {
			this.post("lookup-"+campoP, idP).then(res => {
				onSuccess(res.body);
				return res;
			});
		}
	}

	edit(idP, onSuccess) {
		this.post("edit", new Id(idP)).then(res => {
			let o = this.setJson(res.body);
			if (!Null.is(onSuccess)) {
				onSuccess(o);
			}
			return res;
		});
	}

	observacoesEdit(o) {
		throw new Error("???");
	}

	static idNovo = 0;

	setNovo() {
		let value = --EntityCampos.idNovo;
		this.getOriginal().setId(value);
		this.id.set(value);
	}

	round(bind, decimais, nullIfZero) {
		let o = bind.toJsNumeric(decimais);
		if (nullIfZero && o.isZero()) {
			return null;
		} else {
			return o;
		}
	}
	callComando(path) {
		this.post(path, this.id.get()).then(res => {
			this.setJson(res.body);
			return res;
		});
	}

	getObservacoes() {
		return null;
	}

}
