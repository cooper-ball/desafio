import React from 'react';
import BorderStyle from '../../app/misc/consts/enums/BorderStyle';
import Color from '../../app/misc/consts/enums/Color';
import Div from '../../web/Div';
import Floats from '../../app/misc/consts/enums/Floats';
import LayoutApp from './LayoutApp';
import Null from '../../commom/utils/object/Null';
import Position from '../../app/misc/consts/enums/Position';
import SuperComponent from '../../app/misc/components/SuperComponent';
import UDocument from '../../web/misc/UDocument';

export default class FooterToolbar extends SuperComponent {
	constructor(props){
		super(props);
		this.state.width = null;
	}

	didMount() {
		this.resizeListener = () => this.resizeFooterToolbar();
		window.addEventListener("resize", this.resizeListener);
		this.resizeFooterToolbar();
	}

	componentWillUnmount2() {
		window.removeEventListener("resize", this.resizeListener);
	}

	resizeFooterToolbar() {
		let sider = UDocument.querySelectorAll(".ant-layout-sider").get(0);
		if (!Null.is(sider)) {
			let widthCalc = "calc(100% - " + sider.style.width + ")";
			this.setWidth(widthCalc);
		}
	}

	render0() {
		return (
			<Div style={FooterToolbar.STYLE_TOOLBAR.copy().width(this.state.width)}>
				<Div style={FooterToolbar.FLOAT_LEFT}>{this.props.extra}</Div>
				<Div style={FooterToolbar.FLOAT_RIGHT}>{this.props.children}</Div>
			</Div>
		);
	}
	setWidth = o => this.setState({width:o});
}
FooterToolbar.STYLE_TOOLBAR
= LayoutApp.createStyle()
.position(Position.fixed)
.right(0).bottom(0)
.zIndex(99).w100()
.height(56)
.paddingTop(0)
.paddingRight(24)
.lineHeight(56)
.borderTopWidth(1)
.borderTopStyle(BorderStyle.solid)
.borderTopColor(Color.cinzaClaro)
	;
FooterToolbar.FLOAT_LEFT = LayoutApp.createStyle().float(Floats.left);
FooterToolbar.FLOAT_RIGHT = LayoutApp.createStyle().float(Floats.right);

FooterToolbar.defaultProps = SuperComponent.defaultProps;
