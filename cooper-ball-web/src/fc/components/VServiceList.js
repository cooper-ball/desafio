import ArrayLst from '../../commom/utils/array/ArrayLst';
import Null from '../../commom/utils/object/Null';
import StringEmpty from '../../commom/utils/string/StringEmpty';
import VVinculado from '../../app/campos/support/VVinculado';

export default class VServiceList extends VVinculado {

	static count = 0;
	carregando = false;

	constructor(titleP,mergeFunctionP, carragarCallBackP, uriP) {
		super();
		this.setLabel(titleP);
		this.carragarCallBack = carragarCallBackP;
		this.mergeFunction = mergeFunctionP;
		this.uri = uriP;
	}
	addItens(list) {
		this.itens = list.copy();
		if (!this.carregando) {
			this.notifyObservers();
		}
	}
	add(o) {
		if (!this.carregado()) {
			this.itens = new ArrayLst();
		}
		if (Null.is(o.getId())) {
			VServiceList.count--;
			o.setId(VServiceList.count);
			this.itens.add(o);
		} else {
			let obj = this.itens.byId(o.getId(), i => i.getId());
			if (Null.is(obj)) {
				this.itens.add(o);
			} else {
				this.mergeFunction(o, obj);
			}
		}
		this.notifyObservers();
	}
	remove(o) {
		o = this.itens.byId(o.getId(), i => i.getId());
		this.itens.removeObject(o);
		this.notifyObservers();
	}
	clearItens() {
		this.itens = null;
		this.mensagemErro = null;
		this.notifyObservers();
	}
	clearItens2() {
		this.itens = new ArrayLst();
	}
	start() {
		this.clearItens2();
		this.notifyObservers();
	}
	getItens() {
		return this.itens;
	}
	carregado() {
		return !Null.is(this.itens);
	}
	carregar() {

		if (this.carregado() || this.carregando || !StringEmpty.is(this.mensagemErro)) {
			return;
		}

		if (this.preparadoParaBusca()) {
			this.carregando = true;
			this.getAxios().post(this.uri, this.getParametros(), res => {
				this.carragarCallBack(res.body);
				this.carregando = false;
				this.forceNotifyObservers();
			}).catch(e => {
				this.mensagemErro = e.message;
				this.carregando = false;
				this.forceNotifyObservers();
			});
		}

	}

	getMensagemErro() {
		return this.mensagemErro;
	}
}
