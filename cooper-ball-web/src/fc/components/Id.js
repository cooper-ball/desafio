import ObjJs from '../../commom/utils/object/ObjJs';
import UJson from '../../commom/utils/object/UJson';

export default class Id extends ObjJs {

	value = 0;
	nomeCampo = "id";

	constructor(value) {
		super();
		this.value = value;
	}

	nome(s) {
		this.nomeCampo = s;
		return this;
	}

	toJsonImpl() {
		return "{"+UJson.itemInteger(this.nomeCampo, this.value)+"}";
	}

}
