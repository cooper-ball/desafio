import CdiObj from '../../app/misc/components/CdiObj';

export default class SearchService extends CdiObj {

	constructor(url, getOutrosParams) {
		super();
		this.url = url;
		this.getOutrosParams = getOutrosParams;
	}

	exec(filtro, callback) {
		let params = {};
		params.text = filtro;
		if (this.getOutrosParams !== null) {
			params.outrosParams = this.getOutrosParams();
		}

		this.fcAxios.post(this.url, params, res => {
			let result = res.body;
			res.body = result.dados;
			callback(res);
		});
	}

}
