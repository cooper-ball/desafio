import VBoolean from '../../app/campos/support/VBoolean';

export default class Permissoes extends VBoolean {

	estahCarregado() {
		if (this.isFalse()) {
			this.carregar();
			return false;
		} else {
			return true;
		}
	}

	carregar() {}

}
