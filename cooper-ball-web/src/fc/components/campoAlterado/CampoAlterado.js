import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringLength from '../../../commom/utils/string/StringLength';

export default class CampoAlterado {

	getId() {
		return this.id;
	}

	setId(value) {
		this.id = value;
	}

	setKey(value) {
		this.key = value;
		return this;
	}

	setCampo(s) {
		this.campo = s;
		return this;
	}

	quebrar(s) {
		if (StringEmpty.is(s)) {
			return null;
		}

		if (StringLength.get(s) > 50) {
			let x = "";
			while (StringLength.get(s) > 50) {
				x += s.substring(0, 50) + "\n";
				s = s.substring(50);
			}
			s = x + s;
		}

		if (StringLength.get(s) > 500) {
			s = s.substring(0, 500) + " ...";
		}

		return s;
	}

	setDe(s) {
		this.deCompleto = s;
		this.de = this.quebrar(s);
		return this;
	}

	setPara(s) {
		this.para = this.quebrar(s);
		return this;
	}

	toJSON() {
		return this.campo + " > " + this.de + " > " + this.para;
	}

}
