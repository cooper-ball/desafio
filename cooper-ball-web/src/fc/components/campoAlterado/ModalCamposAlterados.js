import React from 'react';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import BotaoDesfazerCampoAlterado from './BotaoDesfazerCampoAlterado';
import Coluna from '../tabela/Coluna';
import Div from '../../../web/Div';
import FcBotao from '../FcBotao';
import FormEditButtons from '../FormEditButtons';
import FormGenerico from '../FormGenerico';
import LayoutApp from '../LayoutApp';
import StringCompare from '../../../commom/utils/string/StringCompare';
import Tabela from '../tabela/Tabela';
import TextAlign from '../../../app/misc/consts/enums/TextAlign';

export default class ModalCamposAlterados extends FormGenerico {

	ehModal() {
		return true;
	}

	getTitle() {
		return "Campos Alterados";
	}

	getWidthModal() {
		return 65;
	}

	getBody() {
		return <Tabela bind={this.camposAlterados} colunas={this.COLUNAS}/>;
	}

	getFooter() {
		let style = LayoutApp.createStyle().w100().padding(10);
		return (
			<Div style={style}>
				<FcBotao style={FormEditButtons.STYLE_BUTTON} acao={this.props.onClose} title={"Fechar (Esc)"}/>
			</Div>
		);
	}

	close() {
		this.props.onClose();
	}

	didMount2() {

		this.camposAlterados.addItens(this.props.valores);
		setTimeout(() => this.camposAlterados.carregar(), 250);

		this.COLUNAS = ArrayLst.build(ModalCamposAlterados.COL_NOME, ModalCamposAlterados.COL_DE, ModalCamposAlterados.COL_PARA);

		if (this.props.podeDesfazer) {

			this.COL_DESFAZER =
					new Coluna(45, null, "", o => true, TextAlign.center)
					.setSort((a, b) => 0)
					.setGrupo(false)
					.setRenderItem(o => <BotaoDesfazerCampoAlterado onClick={() => this.props.campos.setAttr(o.key, o.de)}/>);

			this.COLUNAS.add(this.COL_DESFAZER);
		}

	}
	setWidthForm = o => this.setState({widthForm:o});
}
ModalCamposAlterados.COL_NOME = new Coluna(35, null, "Campo", o => o.campo, TextAlign.left).setSort((a, b) => StringCompare.compare(a.campo, a.campo)).setGrupo(false);
ModalCamposAlterados.COL_DE = new Coluna(90, null, "De", o => o.de, TextAlign.left).setSort((a, b) => StringCompare.compare(a.de, b.de)).setGrupo(false);
ModalCamposAlterados.COL_PARA = new Coluna(90, null, "Para", o => o.para, TextAlign.left).setSort((a, b) => StringCompare.compare(a.para, b.para)).setGrupo(false);

ModalCamposAlterados.defaultProps = {
	...FormGenerico.defaultProps,
	podeDesfazer: false
}
