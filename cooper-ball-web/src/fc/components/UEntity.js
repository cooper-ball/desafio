import Null from '../../commom/utils/object/Null';
import UInteger from '../../app/misc/utils/UInteger';

export default class UEntity {

	static compareId(a, b) {
		if (Null.is(a)) {
			if (Null.is(b)) {
				return 0;
			} else {
				return -1;
			}
		} else if (Null.is(b)) {
			return 1;
		} else {
			return UInteger.compare(a.id, b.id);
		}
	}

	static equalsId(a, b) {
		return UEntity.compareId(a, b) === 0;
	}

}
