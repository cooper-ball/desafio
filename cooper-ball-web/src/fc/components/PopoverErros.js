import React from 'react';
import BorderStyle from '../../app/misc/consts/enums/BorderStyle';
import BotaoSize from '../../antd/BotaoSize';
import BotaoType from '../../antd/BotaoType';
import Color from '../../app/misc/consts/enums/Color';
import FcBotao from './FcBotao';
import IntegerCompare from '../../commom/utils/integer/IntegerCompare';
import LayoutApp from './LayoutApp';
import Null from '../../commom/utils/object/Null';
import StringCompare from '../../commom/utils/string/StringCompare';
import SuperComponent from '../../app/misc/components/SuperComponent';
import TextAlign from '../../app/misc/consts/enums/TextAlign';
import {ExclamationCircleTwoTone} from '@ant-design/icons';
import {Popover} from 'antd';

export default class PopoverErros extends SuperComponent {

	render0() {
		let erros = this.props.campos.getErros();
		if (erros.isEmpty()) {
			return null;
		} else {
			return (
				<Popover
					title={"Campos com erro"}
					placement={"top"}
					trigger={"click"}
					getPopupContainer={o => Null.is(o) || Null.is(o.parentNode) ? o : o.parentNode}
					content={
						this.map(erros, campo =>
							<li style={PopoverErros.STYLE_LI.get()} key={campo.getLabel()}>
								<FcBotao
									title={campo.getAtribute("aba") + " - " + campo.getLabel()}
									type={BotaoType.link}
									block={true}
									style={PopoverErros.STYLE_BOTAO_INFO_1}
									acao={() => this.focusCampo(campo)}
								/>

								<FcBotao
									title={campo.getInvalidMessage()}
									type={BotaoType.link}
									size={BotaoSize.small}
									block={true}
									style={PopoverErros.STYLE_BOTAO_INFO_2}
									acao={() => this.focusCampo(campo)}
								/>
							</li>
						)
					}>
					<FcBotao
						icon={<ExclamationCircleTwoTone/>}
						type={BotaoType.link}
						style={PopoverErros.ERROR_STYLE}
						title={IntegerCompare.eq(erros.size(), 1) ? "1 impedimento" : erros.size() + " impedimentos"}
					/>
				</Popover>
			);
		}
	}

	didMount() {
		this.observar(this.props.campos);
	}

	focusCampo(campo) {
		let aba = campo.getAtribute("aba");
		if (StringCompare.eq(aba, this.props.abaSelecionada)) {
			let labelNode = document.getElementById("label-"+campo.getIdComponent());
			if (!Null.is(labelNode)) {
				labelNode.scrollIntoView(true);
				let inputId = "input-"+campo.getIdComponent();
				let element = document.getElementById(inputId);
				if (Null.is(element)) {
					throw new Error("Nao encontrado: " + inputId);
				}
				element.focus();
			}
		} else {
			this.props.setAbaSelecionada(aba);
			setTimeout(() => this.focusCampo(campo), 250);
		}
	}
}
PopoverErros.STYLE_BOTAO_INFO_1 = LayoutApp.createStyle().textAlign(TextAlign.left).color(Color.black);
PopoverErros.STYLE_BOTAO_INFO_2 = LayoutApp.createStyle().textAlign(TextAlign.left).color(Color.red).fontSize(10);
PopoverErros.STYLE_LI = LayoutApp.createStyle().borderBottomColor(Color.cinzaClaro).borderBottomStyle(BorderStyle.solid).borderBottomWidth(1);
PopoverErros.ERROR_STYLE = LayoutApp.createStyle().color(Color.red);

PopoverErros.defaultProps = SuperComponent.defaultProps;
