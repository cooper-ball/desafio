import React from 'react';
import ClassSimpleName from '../../commom/utils/classe/ClassSimpleName';
import Console from '../../app/misc/utils/Console';
import Div from '../../web/Div';
import Equals from '../../commom/utils/object/Equals';
import FooterToolbar from './FooterToolbar';
import FormKeyDownObserver from './FormKeyDownObserver';
import LayoutApp from './LayoutApp';
import Null from '../../commom/utils/object/Null';
import Span from '../../web/Span';
import UDocument from '../../web/misc/UDocument';
import VString from '../../app/campos/support/VString';
import {Card} from 'antd';
import {Form} from 'antd';
import {Modal} from 'antd';

export default class FormGenerico extends FormKeyDownObserver {
	constructor(props){
		super(props);
		this.state.widthForm = "100";
	}

	montado = false;

	static getHeader;

	render00() {

		if (!this.montado) {
			return null;
		}

		if (!this.isVisible()) {
			return null;
		}

		let outroRender = this.getRender();
		if (!Null.is(outroRender)) {
			return outroRender;
		}

		if (this.ehModal()) {
			return (
				<Modal
					title={this.getTop()}
					width={this.getWidthModal()+"%"}
					visible={true}
					style={FormGenerico.STYLE_MODAL.get()}
					bodyStyle={FormGenerico.STYLE_MODAL_BODY.get()}
					footer={this.getFooter()}
					closable={false}
					keyboard={false}
					maskClosable={false}
					destroyOnClose={true}
					onCancel={() => this.onExit()}
				>{this.getBody()}</Modal>
			);
		} else {
			return (
				<Div>
					{this.miolo()}
					<FooterToolbar>
						{this.getFooter()}
					</FooterToolbar>
				</Div>
			);
		}
	}

	isVisible() {
		return true;
	}

	getTop() {
		if (Null.is(FormGenerico.getHeader)) {
			return <Span text={this.titleForm.asString()}/>;
		} else {
			return FormGenerico.getHeader(this.titleForm);
		}
	}

	ehModal() {
		return this.props.isModal;
	}

	getWidthModal() {
		return this.props.widthModal;
	}

	static removeFooter() {
		let list = document.getElementsByTagName("footer");
		if (Equals.is(list.length, 0)) {
			setTimeout(() => FormGenerico.removeFooter());
		} else {
			list[0].remove();
		}
	}

	miolo() {
		return (
			<Card title={this.getTop()} bordered={false} style={FormGenerico.STYLE_CARD.get()} className={"FormGenerico-miolo-Card"}>
				<Form hideRequiredMark={true}>
					{this.getBody()}
				</Form>
			</Card>
		);
	}
	getRender() {return null;}

	didMount1() {
		this.resizeListener = () => this.resizeFooterToolbar();
		window.addEventListener("resize", this.resizeListener, {passive: true});
		this.resizeFooterToolbar();
		this.didMount2();
		FormGenerico.removeFooter();
		this.montado = true;
		this.titleForm = this.createTitle();
		this.observar(this.titleForm);
		setTimeout(() => this.forceUpdate());
	}

	createTitle() {
		let o = new VString().setMaxLength(1000);
		o.set(this.getTitle());
		return o;
	}

	didMount2() {}

	componentWillUnmount2a() {
		window.removeEventListener("resize", this.resizeListener);
		this.componentWillUnmount3();
	}

	componentWillUnmount3() {}

	resizeFooterToolbar() {
		let sider = UDocument.querySelectorAll(".ant-layout-sider").get(0);
		if (!Null.is(sider)) {
			let widthCalc = "calc(100% - " + sider.style.width + ")";
			this.setWidthForm(widthCalc);
		}
	}

	close() {
		Console.log("FormGenerico.close chamado", ClassSimpleName.exec(this));
	}

	confirmar() {
		this.confirmarImpl();
	}

	confirmarImpl() {
		this.close();
	}

	cancelar() {
		this.close();
	}

	onExit() {
		this.esc();
	}

	esc() {
		this.cancelar();
	}

	ctrlEnter() {
		this.confirmar();
	}

	enter() {}

	ctrlDel() {}

	onKeyDown0(e) {
		if (e.ctrl()) {
			if (e.shift()) {} else if (e.enter()) {
				this.ctrlEnter();
			} else if (e.del()) {
				this.ctrlDel();
			}
		} else if (e.shift()) {} else if (e.esc()) {
			this.esc();
		} else if (e.enter()) {
			this.enter();
		}
	}
	setWidthForm = o => this.setState({widthForm:o});
}
FormGenerico.STYLE_MODAL = LayoutApp.createStyle().top(40).backgroundColorClear();
FormGenerico.STYLE_MODAL_BODY = LayoutApp.createStyle();
FormGenerico.STYLE_CARD = LayoutApp.createStyle().marginBottom(24);

FormGenerico.defaultProps = {
	...FormKeyDownObserver.defaultProps,
	isModal: false,
	widthModal: 96
}
