import ArrayLst from '../../commom/utils/array/ArrayLst';
import KeyDownObservers from '../../web/misc/KeyDownObservers';
import Null from '../../commom/utils/object/Null';
import SuperComponent from '../../app/misc/components/SuperComponent';
import UCommons from '../../app/misc/utils/UCommons';

export default class FormKeyDownObserver extends SuperComponent {

	render0() {
		let o = this.render00();
		if (Null.is(o)) {
			this.removeKeyDownObserver();
		} else {
			this.addKeyDownObserver();
		}
		return o;
	}

	didMount() {
		KeyDownObservers.add(this);
		this.didMount1();
		this.addKeyDownObserver();
	}

	onKeyDown(e) {
		if (UCommons.neq(FormKeyDownObserver.getFormularioAberto(), this)) {
			return;
		} else {
			this.onKeyDown0(e);
		}
	}

	componentWillUnmount2() {
		KeyDownObservers.remove(this);
		this.componentWillUnmount2a();
		this.removeKeyDownObserver();
	}

	removeKeyDownObserver() {
		FormKeyDownObserver.formulariosAbertos.removeObject(this);
	}

	addKeyDownObserver() {
		if (!FormKeyDownObserver.formulariosAbertos.contains(this)) {
			FormKeyDownObserver.formulariosAbertos.add(this);
		}
	}

	componentWillUnmount2a() {}

	static getFormularioAberto() {
		return FormKeyDownObserver.formulariosAbertos.getLast();
	}
	didMount1() {}

}
FormKeyDownObserver.formulariosAbertos = new ArrayLst();

FormKeyDownObserver.defaultProps = SuperComponent.defaultProps;
