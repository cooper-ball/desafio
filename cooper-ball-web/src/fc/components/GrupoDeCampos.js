import ArrayLst from '../../commom/utils/array/ArrayLst';
import VBoolean from '../../app/campos/support/VBoolean';
import VInteger from '../../app/campos/support/VInteger';

export default class GrupoDeCampos {

	constructor(condicao) {
		this.condicao = condicao;
	}

	isEmpty = new VBoolean();
	length = new VInteger().setMaximo(999);
	todos = new ArrayLst();
	filtrados = new ArrayLst();

	getItens() {
		return this.filtrados.copy();
	}

	filter(predicate) {
		return this.filtrados.filter(predicate);
	}

	add(o) {

		if (!this.todos.contains(o)) {
			this.todos.add(o);
			this.computa(o);
		}

		o.addFunctionTObserver(i => this.computa(i));

	}

	computa(o) {

		if (!this.todos.contains(o)) {
			return;
		}

		let result = this.condicao(o);
		let contains = this.filtrados.contains(o);

		if (result) {
			if (!contains) {
				this.filtrados.add(o);
				this.length.inc();
				this.ajustaBooleans();
			}
		} else {
			if (contains) {
				this.filtrados.removeObject(o);
				this.length.dec();
				this.ajustaBooleans();
			}
		}

	}

	ajustaBooleans() {
		this.isEmpty.set(this.length.eq(0));
	}

}
