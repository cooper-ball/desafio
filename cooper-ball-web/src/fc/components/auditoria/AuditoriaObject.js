import ArrayLst from '../../../commom/utils/array/ArrayLst';
import AuditoriaItem from './AuditoriaItem';
import CampoAlterado from '../campoAlterado/CampoAlterado';
import Id from '../Id';
import Null from '../../../commom/utils/object/Null';
import VBoolean from '../../../app/campos/support/VBoolean';

export default class AuditoriaObject {

	modalAuditoria = new VBoolean();

	constructor(campos) {
		this.campos = campos;

		this.list = campos.createSubList("Auditoria", "auditoria"
			, null
			, obj => {
				let array = obj;
				let itens = new ArrayLst(array);
				this.list.addItens(itens.map(json => {
					let o = new AuditoriaItem();
					o.id = json.id;
					o.idTipo = json.idTipo;
					o.tipo = json.tipo;
					o.usuario = json.usuario;
					o.data = json.data;
					o.tempo = json.tempo;
					return o;
				}));
			}
		);
	}

	detalharAuditoria(o) {
		this.auditoriaItem = o;
		if (Null.is(o.alteracoes)) {
			this.campos.fcAxios.post("AuditoriaCampo/consulta", new Id(o.id), res => {
				let result = res.body;
				let array = result.dados;
				let itens = new ArrayLst(array);
				this.auditoriaItem.alteracoes = itens.map(obj => {
					let item = new CampoAlterado();
					item.setId(obj.id);
					item.setCampo(obj.campo);
					item.setDe(obj.de);
					item.setPara(obj.para);
					return item;
				});
				this.list.set(true);
			});
		} else {
			this.list.set(true);
		}
	}

	clearItens() {
		this.list.clearItens();
	}

}
