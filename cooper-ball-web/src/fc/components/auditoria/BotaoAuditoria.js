import React from 'react';
import Estilos from '../../outros/Estilos';
import Span from '../../../web/Span';
import SuperComponent from '../../../app/misc/components/SuperComponent';
import {AuditOutlined} from '@ant-design/icons';
import {Popover} from 'antd';

export default class BotaoAuditoria extends SuperComponent {

	render0() {

		if (this.props.auditoria.campos.isRascunho()) {
			return null;
		} else {
			return (
				<Popover
				mouseEnterDelay={0.5}
				placement={"bottom"}
				content={
					<Span text={"Auditoria"}/>
				}>
					<AuditOutlined style={Estilos.ICON_RIGHT.get()} onClick={() => this.props.auditoria.modalAuditoria.set(true)}/>
				</Popover>
			);
		}

	}
}

BotaoAuditoria.defaultProps = SuperComponent.defaultProps;
