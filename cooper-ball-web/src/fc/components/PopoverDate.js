import React from 'react';
import Calendario from './Calendario';
import moment from 'moment';
import Null from '../../commom/utils/object/Null';
import SuperComponent from '../../app/misc/components/SuperComponent';
import {CalendarTwoTone} from '@ant-design/icons';
import {Popover} from 'antd';

export default class PopoverDate extends SuperComponent {

	render0() {

		if (this.props.bind.isDisabled()) {
			return PopoverDate.ICON_CALENDAR;
		} else {

			let defaultValue = this.props.bind.getMoment();

			if (Null.is(defaultValue)) {
				defaultValue = moment();
			}

			return (
				<Popover
				placement={"bottomRight"}
				trigger={"click"}
				content={
					<Calendario bind={this.props.bind}/>

				}>
					{PopoverDate.ICON_CALENDAR}
				</Popover>
			);
		}

	}

	didMount() {
		this.observar(this.props.bind);
	}
}
PopoverDate.ICON_CALENDAR = <CalendarTwoTone/>;

PopoverDate.defaultProps = SuperComponent.defaultProps;
