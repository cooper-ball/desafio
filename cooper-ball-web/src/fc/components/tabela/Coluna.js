import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Null from '../../../commom/utils/object/Null';
import Obrig from '../../../commom/utils/object/Obrig';

export default class Coluna {

	static idCount = 0;

	id = "Coluna" + (Coluna.idCount++);
	grupo = false;
	sorted = false;
	width = 0;
	cols = 0;
	itensFiltrados = new ArrayLst();
	noClick = false;

	isVisible() {
		return Null.is(this.visible) || this.visible();
	}

	onEdit(o) {}
	constructor(widthP, campoP, titleP,getP, textAlign) {
		this.campo = campoP;
		this.width = widthP;
		this.title = Obrig.check(titleP);
		this.get = getP;
		this.textAlign = textAlign;
	}
	setCols(value) {
		this.cols = value;
		return this;
	}
	setVisible(func) {
		this.visible = func;
		return this;
	}
	setSort(value) {
		this.sort = value;
		return this;
	}
	setGrupo(value) {
		this.grupo = value;
		return this;
	}
	setRenderItem(value) {
		this.renderItem = value;
		return this;
	}
	hasRenderItem() {
		return !Null.is(this.renderItem);
	}
	callRenderItem(o) {
		return this.renderItem(o);
	}

	toJSON() {
		return this.id;
	}

}
