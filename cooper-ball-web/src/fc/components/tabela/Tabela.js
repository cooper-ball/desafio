import React from 'react';
import ArrayEmpty from '../../../commom/utils/array/ArrayEmpty';
import Border from '../../../app/misc/utils/Border';
import BorderStyle from '../../../app/misc/consts/enums/BorderStyle';
import Box from '../../../commom/utils/comum/Box';
import Color from '../../../app/misc/consts/enums/Color';
import CommonStyles from '../../../app/misc/styles/CommonStyles';
import Equals from '../../../commom/utils/object/Equals';
import Frag from '../../../react/Frag';
import GetText from '../../../app/misc/utils/GetText';
import LayoutApp from '../LayoutApp';
import Loading from '../../../antd/Loading';
import Null from '../../../commom/utils/object/Null';
import Span from '../../../web/Span';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import StringParse from '../../../commom/utils/string/StringParse';
import SuperComponent from '../../../app/misc/components/SuperComponent';
import Tb from '../../../web/Tb';
import Td from '../../../web/Td';
import Tr from '../../../web/Tr';
import UBoolean from '../../../app/misc/utils/UBoolean';
import {DeleteTwoTone} from '@ant-design/icons';
import {Popconfirm} from 'antd';
import {SearchOutlined} from '@ant-design/icons';

export default class Tabela extends SuperComponent {

	showEdit() {
		return !Null.is(this.props.onClick);
	}
	showDelete() {
		return !Null.is(this.props.onDelete);
	}

	render0() {
		if (this.props.bind.carregado()) {
			return (
				<Tb style={Tabela.STYLE_SUBTABLE} head={this.getHead()}>
					{this.getBody()}
				</Tb>
			);
		} else if (!StringEmpty.is(this.props.bind.getMensagemErro())) {
			let s = "Ocorreu um erro no servico: " + this.props.bind.getMensagemErro();
			return <Span style={Tabela.STYLE_ERROR} text={s}/>;
		} else {
			this.carregar();
			return <Loading/>;
		}
	}
	carregar() {
		setTimeout(() => this.props.bind.carregar(), 250);
	}

	getBody() {

		let itensFiltrados = this.getItensFiltrados();

		if (itensFiltrados.isEmpty()) {
			return null;
		}

		return (
			this.map(itensFiltrados, o =>
				<Tr style={Tabela.STYLE_ROW} key={StringParse.get(o)}>
					{this.tdEdit(o)}
					{this.tdDelete(o)}
					{this.map(this.props.colunas, col =>
						<Td
							style={this.getStyle(col)}
							key={col.id}
						>{this.getRender(col, o)}</Td>
					)}

				</Tr>
			)
		);
	}

	getRender(col, o) {
		if (col.hasRenderItem()) {
			return col.callRenderItem(o);
		} else {
			let obj = col.get(o);
			if (Null.is(obj)) {
				return null;
			} else {
				let value = GetText.get(obj);
				if (StringEmpty.is(value)) {
					return null;
				} else {
					return <Span text={value}/>;
				}
			}
		}
	}
	tdEdit(o) {
		if (this.showEdit()) {
			let icon;
			if (Null.is(this.props.funcGetEditIcon)) {
				icon = Tabela.ICONE_EDIT;
			} else {
				icon = this.props.funcGetEditIcon(o);
			}

			return (
				<Td style={Tabela.STYLE_COL_FIXED} onClick={() => this.props.onClick(o)}>{icon}</Td>
			);

		} else {
			return null;
		}
	}
	tdDelete(o) {
		if (this.showDelete()) {
			return (
				<Td style={Tabela.STYLE_COL_FIXED}>
					<Popconfirm
						onConfirm={() => this.props.onDelete(o)}
						title={"Confirma exclusão?"}
						okText={"Sim"}
						cancelText={"Não"}
					>{Tabela.ICONE_DELETE}</Popconfirm>
				</Td>
			);
		} else {
			return null;
		}
	}
	getItensFiltrados() {
		return this.props.bind.getItens().filter(o => {
			if (UBoolean.isTrue(o.excluido)) {
				return false;
			}
			let box = new Box(false);
			this.props.colunas.forEach(coluna => {
				if (box.get()) {
					return;
				}
				let value = coluna.get(o);
				if (coluna.itensFiltrados.contains(value)) {
					box.set(true);
				}
			});
			return !box.get();
		});
	}
	getHead() {
		return (
			<Frag>
				<Tr style={Tabela.STYLE_SUBTABLE_TITLE_ROW}>
					{this.showEdit() &&
						<Td rowSpan={2} style={Tabela.STYLE_COL_FIXED}/>
					}
					{this.showDelete() &&
						<Td rowSpan={2} style={Tabela.STYLE_COL_FIXED}/>
					}
					{this.map(this.colunasSemGrupo(), o => this.getTitulo(o, 2))}
					{this.map(this.props.colunasGrupo, o =>
						<Td colSpan={o.cols} style={this.getTitleStyle(o).textAlignCenter()} key={o.id}>
							<Span text={o.title}/>
						</Td>
					)}
				</Tr>
				<Tr style={Tabela.STYLE_SUBTABLE_TITLE_ROW}>
					{this.map(this.colunasComGrupo(), o => this.getTitulo(o, 2))}
				</Tr>
			</Frag>
		);
	}

	getTitulo(o, rowSpan) {
		return (
				<Td rowSpan={rowSpan} style={this.getTitleStyle(o)} key={o.id}>
					{this.getTitle(o)}
				</Td>
		);
	}

	getTitle(o) {
		return <Span text={o.title}/>;
	}

	getTitleStyle(o) {

		let result = Tabela.STYLE_TITLE_COL.copy();

		if (this.colunasSemGrupo().isEmpty()) {
			if (Equals.is(this.props.colunasGrupo.get(0), o) || Equals.is(this.colunasComGrupo().get(0), o)) {
				result.borderLeftStyle(BorderStyle.none);
			}
		} else {
			if (Equals.is(this.colunasSemGrupo().get(0), o)) {
				result.borderLeftStyle(BorderStyle.none);
			}
		}

		if (ArrayEmpty.is(this.props.colunasGrupo)) {
			if (Equals.is(this.colunasSemGrupo().getLast(), o)) {
				result.borderRightStyle(BorderStyle.none);
			}
		} else {
			if (Equals.is(this.props.colunasGrupo.getLast(), o) || Equals.is(this.colunasComGrupo().getLast(), o)) {
				result.borderRightStyle(BorderStyle.none);
			}
		}

		result.textAlign(o.textAlign);
		result.width(o.width);
		return result;
	}

	getStyle(o) {

		let result = Tabela.STYLE_SUBTABLE_COL.copy();
		result.textAlign(o.textAlign);

		return result;
	}

	getTempoAceitavelParaRenderizacao() {
		return 500;
	}

	didMount() {
		this.observar(this.props.bind);
	}
	colunasSemGrupo() {
		return this.props.colunas.filter(o => !o.grupo);
	}
	colunasComGrupo() {
		return this.props.colunas.filter(o => o.grupo);
	}

}
Tabela.border = new Border().withColor(Color.cinzaClaro2);
Tabela.ICON_WIDTH = 25;
Tabela.STYLE_ERROR = LayoutApp.createStyle().margin(10).bold(true).color(Color.red);
Tabela.STYLE_COL_FIXED = LayoutApp.createStyle().borderBottom(Tabela.border).paddingTop(0).paddingBottom(0).width(Tabela.ICON_WIDTH).maxWidth(Tabela.ICON_WIDTH).minWidth(Tabela.ICON_WIDTH);
Tabela.STYLE_ICON_EDIT = CommonStyles.POINTER.copy().margin(5).color(Color.blue);
Tabela.STYLE_ICON_DELETE = CommonStyles.POINTER.copy().margin(5).color(Color.red);
Tabela.STYLE_ROW = LayoutApp.createStyle().border(Tabela.border).borderLeftStyle(BorderStyle.none).borderRightStyle(BorderStyle.none);
Tabela.STYLE_SUBTABLE_TITLE_ROW = Tabela.STYLE_ROW.copy().bold(true);
Tabela.STYLE_SUBTABLE_COL = LayoutApp.createStyle().padding(5).paddingLeft(10).paddingRight(10);
Tabela.STYLE_TITLE_COL = LayoutApp.createStyle().padding(5).paddingLeft(10).paddingRight(10).border(Tabela.border);
Tabela.STYLE_SUBTABLE = LayoutApp.createStyle().w100().textAlignCenter();
Tabela.ICONE_DELETE = <DeleteTwoTone twoToneColor={Color.red} style={Tabela.STYLE_ICON_DELETE.get()}/>;
Tabela.ICONE_EDIT = <SearchOutlined style={Tabela.STYLE_ICON_EDIT.get()}/>;
Tabela.ICONE_SEARCH = <SearchOutlined style={Tabela.STYLE_ICON_EDIT.get()}/>;

Tabela.defaultProps = SuperComponent.defaultProps;
