import React from 'react';
import ArrayLst from '../../commom/utils/array/ArrayLst';
import BaseData from '../../commom/utils/date/BaseData';
import Border from '../../app/misc/utils/Border';
import BotaoSize from '../../antd/BotaoSize';
import BotaoType from '../../antd/BotaoType';
import FcBotao from './FcBotao';
import IntegerCompare from '../../commom/utils/integer/IntegerCompare';
import JustifyContent from '../../app/misc/consts/enums/JustifyContent';
import Null from '../../commom/utils/object/Null';
import Span from '../../web/Span';
import Style from '../../app/misc/utils/Style';
import SuperComponent from '../../app/misc/components/SuperComponent';
import Tb from '../../web/Tb';
import Td from '../../web/Td';
import TextAlign from '../../app/misc/consts/enums/TextAlign';
import Tr from '../../web/Tr';
import UData from '../../app/misc/utils/UData';
import VData from '../../app/campos/support/VData';

export default class Calendario extends SuperComponent {
	static MAP_ITENS = new Map();

	constructor(props) {
		super(props);
		let data = UData.hoje();
		data.setDia(1);
		this.mesAno = new VData();
		this.mesAno.set2(data);
	}

	static getItens(ano, mes) {

		let k = ano * 100 + mes;
		let list = Calendario.MAP_ITENS.get(k);

		if (Null.is(list)) {

			let o = new BaseData(ano, mes, 1);

			let diaSemana = o.toDate().getDay();

			let todos = new ArrayLst();

			while (diaSemana > 0) {
				todos.add(null);
				diaSemana--;
			}

			while (IntegerCompare.eq(o.getMes(), mes)) {
				todos.add(o.getDia());
				o.addDia();
			}

			list = new ArrayLst();

			while (!todos.isEmpty()) {
				let semana = new ArrayLst();
				list.add(semana);
				for (let i = 0; i < 7; i++) {
					if (todos.isEmpty()) {
						semana.add(null);
					} else {
						o = new BaseData(ano, mes, todos.remove(0));
						semana.add(o);
					}
				}
			}

			Calendario.MAP_ITENS.set(k, list);

		}

		return list;

	}

	avancarMes() {
		this.log("avancarMes()");
		let o = this.mesAno.toBaseData();
		o.addMes();
		this.mesAno.set2(o);
	}

	voltarMes() {
		this.log("voltarMes()");
		let o = this.mesAno.toBaseData();
		o.removeMes();
		this.mesAno.set2(o);
	}

	render0() {

		let data = this.mesAno.toBaseData();
		let itens = Calendario.getItens(data.getAno(), data.getMes());

		return (
			<Tb>
				<Tr>
					<Td colSpan={2}>
						<FcBotao
							size={BotaoSize.small}
							style={Calendario.STYLE_BOTAO}
							title={"<<"}
							acao={() => this.voltarMes()}
						/>
					</Td>
					<Td colSpan={3}>
						<FcBotao
							size={BotaoSize.small}
							style={Calendario.STYLE_BOTAO}
							type={BotaoType.link}
							title={data.format("[mmmm]/[yyyy]")}
						/>
					</Td>
					<Td colSpan={2}>
						<FcBotao
							size={BotaoSize.small}
							style={Calendario.STYLE_BOTAO}
							title={">>"} acao={() => this.avancarMes()}
						/>
					</Td>
				</Tr>
				<Tr key={"titulos"}>
					{this.map(Calendario.ds, d => this.col(d, <Span text={d}/>))}
				</Tr>

				{this.mapi(itens, (semana, semanaIndex) =>
					<Tr key={"semana"+semanaIndex}>
						{this.mapi(semana, (d, diaIndex) => this.dia(semanaIndex+"-"+diaIndex, d)
					)}
				</Tr>)}
			</Tb>
		);
	}

	dia(k, o) {

		if (Null.is(o) || Null.is(o.getDia())) {
			return this.col(k, null);
		}

		let s = ""+o.getDia();

		let type;

		if (this.props.bind.eq2(o)) {
			type = BotaoType.primary;
		} else if (o.isHoje()) {
			type = BotaoType.dashed;
		} else {
			type = BotaoType.link;
		}

		return this.col(k,
			<FcBotao
				style={this.newStyle().w100().h100()}
				type={type}
				title={s}
				acao={() => this.set(o)}
			/>
		);

	}

	set(o) {
		this.props.bind.set2(o);
	}

	col(k, conteudo) {
		return <Td style={Calendario.TD_STYLE} key={k}>{conteudo}</Td>;
	}

	didMount() {
		this.observar(this.props.bind);
		this.observar(this.mesAno);
	}
}
Calendario.TD_STYLE =
	Style.create()
	.textAlign(TextAlign.center)
	.width(40)
	.justifyContent(JustifyContent.center)
	.border(new Border());
Calendario.ds = ArrayLst.build("dom","seg","ter","qua","qui","sex","sáb");
Calendario.STYLE_BOTAO = Style.create().w100().marginBottom(10);

Calendario.defaultProps = SuperComponent.defaultProps;
