import Box from '../../commom/utils/comum/Box';
import Id from './Id';
import VServiceList from './VServiceList';

export default class VSubList extends VServiceList {

	axiosBox = new Box();

	constructor(titleP,mergeFunctionP, carragarCallBackP, idP, uriP, a) {
		super(titleP, mergeFunctionP, carragarCallBackP, uriP);
		this.id = idP;
		this.axiosBox.set(a);
	}

	getParametros() {
		return new Id(this.id.get());
	}

	preparadoParaBusca() {
		if (this.id.isEmpty() || this.id.eq(-1)) {
			this.start();
			return false;
		} else {
			return true;
		}
	}

	setDisabled(value) {
		super.setDisabled(value);
		return this;
	}

	getAxios() {
		return this.axiosBox.get();
	}
}
