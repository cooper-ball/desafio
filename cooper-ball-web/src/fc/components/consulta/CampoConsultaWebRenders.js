import React from 'react';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import BindingListener from '../../../app/campos/support/BindingListener';
import BotaoType from '../../../antd/BotaoType';
import CampoConsultaDate from './CampoConsultaDate';
import CampoConsultaInteger from './CampoConsultaInteger';
import CampoConsultaVazio from './CampoConsultaVazio';
import Color from '../../../app/misc/consts/enums/Color';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import ConsultaStyles from './ConsultaStyles';
import Estilos from '../../outros/Estilos';
import FcBotao from '../FcBotao';
import Frag from '../../../react/Frag';
import InputBind from '../../../antd/InputBind';
import PopoverDate from '../PopoverDate';
import SelectBindSimple from '../../../antd/SelectBindSimple';
import SelectMultipleBind from '../../../antd/SelectMultipleBind';
import Span from '../../../web/Span';
import Style from '../../../app/misc/utils/Style';
import Td from '../../../web/Td';
import TextAlign from '../../../app/misc/consts/enums/TextAlign';
import Tr from '../../../web/Tr';
import {Divider} from 'antd';
import {ExclamationCircleOutlined} from '@ant-design/icons';
import {EyeInvisibleTwoTone} from '@ant-design/icons';
import {EyeOutlined} from '@ant-design/icons';
import {Popover} from 'antd';

export default class CampoConsultaWebRenders {

	render(campoConsulta) {

		return (
			<Tr style={CampoConsultaWebRenders.TRS} key={campoConsulta.nomeCampo}>
				<Td style={ConsultaStyles.COL_A}>
					{this.renderTitle(campoConsulta)}
				</Td>
				<Td style={ConsultaStyles.COL_B}>
					{this.selectOperador(campoConsulta)}
				</Td>
				<BindingListener
					itens={ArrayLst.build(campoConsulta.operador)}
					func={() => this.getConformeOperador(campoConsulta)}
				/>
				<Td style={ConsultaStyles.COL_F}>
					<BindingListener
						itens={ArrayLst.build(campoConsulta.invalidMessage)}
						func={() => this.getIconValid(campoConsulta)}
					/>
				</Td>
				<Td style={ConsultaStyles.COL_G}>
					<BindingListener bind={campoConsulta.visibleCol}
						func={() => this.iconVisibleCol(campoConsulta)}
					/>
				</Td>
			</Tr>
		);

	}

	renderTitle(campoConsulta) {
		let st = Estilos.createWidth(100).textAlign(TextAlign.left).fontSize(CampoConsultaWebRenders.fontSize);
		return <Span style={st} text={campoConsulta.titulo}/>;
	}

	entre(campoConsulta) {
		return (
			<Frag>
				<Td style={ConsultaStyles.COL_C}>{campoConsulta.inputa()}</Td>
				<Td style={ConsultaStyles.COL_D}><Span text={"e"}/></Td>
				<Td style={ConsultaStyles.COL_E}>{campoConsulta.inputb()}</Td>
			</Frag>
		);
	}

	render0(campoConsulta) {
		if (campoConsulta instanceof CampoConsultaDate) {
			return this.render0Date(campoConsulta);
		} else if (campoConsulta instanceof CampoConsultaInteger) {
			return this.render0Integer(campoConsulta);
		} else {
			return <Td colSpan={3} style={ConsultaStyles.COL_CDE}>{campoConsulta.inputa()}</Td>;
		}
	}

	input(o, campoConsulta) {
		return <InputBind bind={o} style={CampoConsultaWebRenders.MARGIN_TOP_0.join(CampoConsultaWebRenders.FONT_STYLE)}/>;
	}

	iconVisibleCol(campoConsulta) {
		if (campoConsulta.visibleCol.isTrue()) {
			return (
				<FcBotao
					title={"Aparecer no resultado"}
					acao={() => campoConsulta.changeVisibleCol()}
					style={Style.create().fontSize(10).w100()}
					icon={<EyeOutlined twoToneColor={Color.blue}/>}
					type={BotaoType.normal}
				/>
			);
		} else {
			return (
				<FcBotao
					title={"Ocultar no resultado"}
					acao={() => campoConsulta.changeVisibleCol()}
					style={Style.create().fontSize(10).color(Color.red).w100()}
					icon={<EyeInvisibleTwoTone twoToneColor={Color.red}/>}
					type={BotaoType.normal}
				/>
			);
		}
	}

	getIconValid(campoConsulta) {
		if (campoConsulta.invalidMessage.isEmpty()) {
			return null;
		} else {
			return (
				<Popover content={<Span text={campoConsulta.invalidMessage.get()}/>}>
					<ExclamationCircleOutlined style={Style.create().color(Color.red).get()}/>
				</Popover>
			);
		}
	}

	selectOperador(campoConsulta) {
		if (campoConsulta instanceof CampoConsultaVazio) {
			return null;
		} else {
			return <SelectBindSimple bind={campoConsulta.operador} style={CampoConsultaWebRenders.FONT_STYLE}/>;
		}
	}

	getConformeOperador(campoConsulta) {

		if (campoConsulta.semBinding()) {
			return <Td colSpan={3} style={ConsultaStyles.COL_CDE}/>;
		}

		if (campoConsulta.operador.eq(ConsultaOperadorConstantes.ENTRE)) {
			return this.entre(campoConsulta);
		}

		if (campoConsulta.operador.eq(ConsultaOperadorConstantes.NAO_ENTRE)) {
			return this.entre(campoConsulta);
		}

		return this.render0(campoConsulta);
	}

	inputDate(campoConsulta, bind) {
		return (
			<InputBind
				bind={bind}
				after={<PopoverDate bind={bind}/>}
				style={CampoConsultaWebRenders.MARGIN_TOP_0.join(CampoConsultaWebRenders.FONT_STYLE)}
			/>
		);
	}

	render0Date(campoConsultaDate) {
		return (
			<Frag>
				<Td style={ConsultaStyles.COL_C}>{campoConsultaDate.inputa()}</Td>
				<Td style={ConsultaStyles.COL_D}/>
				<Td style={ConsultaStyles.COL_E}/>
			</Frag>
		);
	}

	render0Integer(campoConsultaInteger) {
		return (
			<Frag>
				<Td style={ConsultaStyles.COL_C}>{campoConsultaInteger.inputa()}</Td>
				<Td style={ConsultaStyles.COL_D}/>
				<Td style={ConsultaStyles.COL_E}/>
			</Frag>
		);
	}

	render0Long(campoConsultaLong) {
		return (
			<Frag>
				<Td style={ConsultaStyles.COL_C}>{campoConsultaLong.inputa()}</Td>
				<Td style={ConsultaStyles.COL_D}/>
				<Td style={ConsultaStyles.COL_E}/>
			</Frag>
		);
	}

	inputFk(campoConsulta, o) {
		return (
			<SelectMultipleBind
				bind={o}
				onSearch={s => campoConsulta.search(s)}
			/>
		);
	}

	inputList(campoConsulta, a) {
		return <SelectMultipleBind bind={a}/>;
	}

	divider(separacaoDeGrupo) {
		return (
			<Tr key={separacaoDeGrupo.nomeCampo}>
				<Td colSpan={7}>
					<Divider orientation={"left"} plain={true}>
						<Span style={Style.create().bold(true)} text={separacaoDeGrupo.titulo}/>
					</Divider>
				</Td>
			</Tr>
		);
	}

}
CampoConsultaWebRenders.fontSize = 12;
CampoConsultaWebRenders.FONT_STYLE = Style.create().textAlign(TextAlign.left).fontSize(CampoConsultaWebRenders.fontSize);
CampoConsultaWebRenders.MARGIN_TOP_0 = Style.create().marginTop(0).height(32);
CampoConsultaWebRenders.TRS = Style.create().height(40);
