import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VEmail from '../../../app/campos/support/VEmail';
import VString from '../../../app/campos/support/VString';

export default class CampoConsultaEmail extends CampoConsulta {

	bString = new VString().setMaxLength(60);
	bEmail = new VEmail().setLabel("e-mail");

	constructor(nomeCampoP, titulo, notNull) {
		super(nomeCampoP, titulo, CampoConsultaEmail.OPERADORES_POSSIVEIS, notNull);
		this.bEmail.addFunctionObserver(() => this.validar());
		this.bEmail.addFunctionObserver(() => this.bString.set(this.bEmail.get()));
		this.bString.addFunctionObserver(() => this.bEmail.set(this.bString.get()));
	}

	bindInicial() {
		if (this.operador.eq(ConsultaOperadorConstantes.IGUAL)) {
			return this.bEmail;
		} else {
			return this.bString;
		}
	}

}
CampoConsultaEmail.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.COMECA_COM
	, ConsultaOperadorConstantes.CONTEM
	, ConsultaOperadorConstantes.TERMINA_COM
	, ConsultaOperadorConstantes.IGUAL
);
