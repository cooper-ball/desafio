import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';

export default class CampoConsultaVazio extends CampoConsulta {

	constructor(nomeCampoP, titulo) {
		super(nomeCampoP, titulo, CampoConsultaVazio.OPERADORES_POSSIVEIS, false);
	}

	bindInicial() {
		return null;
	}

}
CampoConsultaVazio.OPERADORES_POSSIVEIS = new ArrayLst();
