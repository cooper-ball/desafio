import Estilos from '../../outros/Estilos';
import TextAlign from '../../../app/misc/consts/enums/TextAlign';

export default class ConsultaStyles {}
ConsultaStyles.A = 20;
ConsultaStyles.B = 20;
ConsultaStyles.F = 2;
ConsultaStyles.D = 2;
ConsultaStyles.G = 5;
ConsultaStyles.CDE = 100 - ConsultaStyles.A - ConsultaStyles.G - ConsultaStyles.B - ConsultaStyles.F - ConsultaStyles.D;
ConsultaStyles.C = (ConsultaStyles.CDE) / 2;
ConsultaStyles.E = ConsultaStyles.C;
ConsultaStyles.COL_A = Estilos.createWidth(ConsultaStyles.A).textAlign(TextAlign.right).paddingRight(5);
ConsultaStyles.COL_B = Estilos.createWidth(ConsultaStyles.B);
ConsultaStyles.COL_C = Estilos.createWidth(ConsultaStyles.C);
ConsultaStyles.COL_D = Estilos.createWidth(ConsultaStyles.D).textAlign(TextAlign.center);
ConsultaStyles.COL_E = Estilos.createWidth(ConsultaStyles.E);
ConsultaStyles.COL_F = Estilos.createWidth(ConsultaStyles.F).textAlign(TextAlign.center);
ConsultaStyles.COL_G = Estilos.createWidth(ConsultaStyles.G).textAlign(TextAlign.center);
ConsultaStyles.COL_AB = Estilos.createWidth(ConsultaStyles.A+ConsultaStyles.B);
ConsultaStyles.COL_CDE = Estilos.createWidth(ConsultaStyles.CDE);
ConsultaStyles.COL_BCDEF = Estilos.createWidth(ConsultaStyles.B+ConsultaStyles.CDE+ConsultaStyles.F);
