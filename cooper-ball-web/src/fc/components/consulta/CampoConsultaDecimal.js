import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VNumeric from '../../../app/campos/support/VNumeric';

export default class CampoConsultaDecimal extends CampoConsulta {

	constructor(nomeCampoP, titulo, inteiros, decimais, notNull) {
		super(nomeCampoP, titulo, CampoConsultaDecimal.OPERADORES_POSSIVEIS, notNull);
		this.a = new VNumeric().setInteiros(inteiros).setDecimais(decimais);
		this.b = new VNumeric().setInteiros(inteiros).setDecimais(decimais);
	}

	bindInicial() {
		return this.a;
	}

	bindFinal() {
		return this.b;
	}

	valorInicialMaiorQueValorFinal() {
		return this.a.maiorQue(this.b);
	}

}
CampoConsultaDecimal.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.IGUAL
	, ConsultaOperadorConstantes.MAIOR_OU_IGUAL
	, ConsultaOperadorConstantes.MENOR_OU_IGUAL
	, ConsultaOperadorConstantes.ENTRE
);
