import ArrayLst from '../../../commom/utils/array/ArrayLst';
import ClassSimpleName from '../../../commom/utils/classe/ClassSimpleName';
import Console from '../../../app/misc/utils/Console';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import Null from '../../../commom/utils/object/Null';
import StringEmpty from '../../../commom/utils/string/StringEmpty';
import VBoolean from '../../../app/campos/support/VBoolean';
import VList from '../../../app/campos/support/VList';
import VString from '../../../app/campos/support/VString';

export default class CampoConsulta {

	static renders;

	visible = true;

	invalidMessage = new VString().setMaxLength(500);
	operador = new VList();
	visibleCol = new VBoolean().setStartValue(true);

	constructor(nomeCampoP, tituloP, operadores, notNull) {

		this.nomeCampo = nomeCampoP;
		this.titulo = tituloP;
		operadores = operadores.copy();

		if (!notNull) {
			operadores.addIfNotContains(ConsultaOperadorConstantes.VAZIOS);
		}

		if (this.utilizarOperadoresInversos()) {

			let idsInversos = operadores.map(o => o.id + 1000);
			operadores.addAll(ConsultaOperadorConstantes.LIST.filter(o => idsInversos.contains(o.id)));
		}

		this.operador.setItens(operadores);
		this.defaultValue = operadores.get(0);
		this.operador.set(this.defaultValue);

	}

	changeVisibleCol() {
		this.visibleCol.change();
	}

	utilizarOperadoresInversos() {
		return true;
	}

	init() {

		if (this.operador.getItens().size() === 1) {
			this.operador.setDisabled(true);
		} else {
			this.operador.addFunctionObserver(() => this.validar());
		}

		if (this.hasInicial()) {
			this.bindInicial().addFunctionObserver(() => this.validar());
			this.bindInicial().setNotNull(false);
			if (this.hasFinal()) {
				this.bindFinal().addFunctionObserver(() => this.validar());
				this.bindFinal().setNotNull(false);
			}
		}
	}

	validar() {
		this.invalidMessage.set(this.getInvalidMessage());
	}

	setVisible(value) {
		this.visible = value;
	}

	render0() {

		if (!this.visible) {
			return null;
		}

		return CampoConsulta.renders.render(this);

	}

	getInvalidMessage() {
		if (this.semBinding()) {
			return null;
		}
		if (this.getOperadoresSemBinding().contains(this.operador.get())) {
			return null;
		}
		let s = this.bindInicial().getInvalidMessage();
		if (!StringEmpty.is(s)) {
			return s;
		}
		if (this.operador.eq(ConsultaOperadorConstantes.ENTRE) || this.operador.eq(ConsultaOperadorConstantes.NAO_ENTRE)) {
			s = this.bindFinal().getInvalidMessage();
			if (!StringEmpty.is(s)) {
				return s;
			}
			if (!this.bindInicial().isEmpty() && this.bindFinal().isEmpty()) {
				return "Preencha o valor final";
			} else if (this.bindInicial().isEmpty() && !this.bindFinal().isEmpty()) {
				return "Preencha o valor inicial";
			}
			if (this.valorInicialMaiorQueValorFinal()) {
				return "Valor inicial maior que valor final";
			}
		}
		return null;
	}

	valorInicialMaiorQueValorFinal() {
		return false;
	}

	getOperadoresSemBinding() {
		return CampoConsulta.LISTA_VAZIA;
	}

	isTodos() {
		return this.operador.eq(ConsultaOperadorConstantes.TODOS);
	}

	semBinding() {
		if (this.isTodos() || this.operador.eq(ConsultaOperadorConstantes.VAZIOS) || this.operador.eq(ConsultaOperadorConstantes.DESMEMBRAR)) {
			return true;
		}
		if (this.getOperadoresSemBinding().contains(this.operador.get())) {
			return true;
		}
		return false;
	}

	bindFinal() {
		return null;
	}

	clear() {
		if (this.hasInicial()) {
			this.bindInicial().clear();
			if (this.hasFinal()) {
				this.bindFinal().clear();
			}
		}
		this.operador.set(this.defaultValue);
		this.visibleCol.set(true);
	}

	hasInicial() {
		return !Null.is(this.bindInicial());
	}

	hasFinal() {
		return !Null.is(this.bindFinal());
	}

	clearB() {}

	render1() {
		return CampoConsulta.renders.render0(this);
	}

	entre() {
		return CampoConsulta.renders.entre(this);
	}

	log(s) {
		Console.log(ClassSimpleName.exec(this), s);
	}

	inputa() {
		return this.input(this.bindInicial());
	}

	inputb() {
		return this.input(this.bindFinal());
	}

	input(o) {
		return CampoConsulta.renders.input(o, this);
	}

	isValid() {
		return this.invalidMessage.isEmpty();
	}

	toString() {

		if (this.isTodos()) {
			return null;
		}

		let s = "\""+this.nomeCampo+"\": {\"operador\": " + this.operador.getId();

		if (this.semBinding() || this.bindInicial() === null) {
			return s + "}";
		}

		if (this.bindInicial().isEmpty()) {
			return null;
		}

		let entre = this.operador.eq(ConsultaOperadorConstantes.ENTRE) || this.operador.eq(ConsultaOperadorConstantes.NAO_ENTRE);

		if (entre && this.bindFinal().isEmpty()) {
			return null;
		}

		s += ", \"a\": \"" + this.toStringBind(this.bindInicial()) + "\"";

		if (entre) {
			s += ", \"b\": \"" + this.toStringBind(this.bindFinal()) + "\"";
		}

		return s + "}";

	}

	toStringBind(bind) {
		return bind.asString();
	}

	setDefaultValue(value) {
		this.defaultValue = value;
		this.operador.set(value);
	}

}
CampoConsulta.LISTA_VAZIA = new ArrayLst();
