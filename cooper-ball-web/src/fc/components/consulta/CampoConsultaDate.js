import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VData from '../../../app/campos/support/VData';

export default class CampoConsultaDate extends CampoConsulta {

	a = new VData();
	b = new VData();

	constructor(nomeCampoP, titulo, notNull) {
		super(nomeCampoP, titulo, CampoConsultaDate.OPERADORES_POSSIVEIS, notNull);
	}

	bindInicial() {
		return this.a;
	}

	bindFinal() {
		return this.b;
	}

	clearB() {
		this.bindFinal().clear();
	}

	getOperadoresSemBinding() {
		return ArrayLst.build(ConsultaOperadorConstantes.HOJE);
	}

	render1() {
		return CampoConsultaDate.renders.render0Date(this);
	}

	input(o) {
		let bind = o;
		return CampoConsultaDate.renders.inputDate(this, bind);
	}

	valorInicialMaiorQueValorFinal() {
		return this.a.maiorQue(this.b);
	}

}
CampoConsultaDate.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.IGUAL
	, ConsultaOperadorConstantes.HOJE
	, ConsultaOperadorConstantes.MAIOR_OU_IGUAL
	, ConsultaOperadorConstantes.MENOR_OU_IGUAL
	, ConsultaOperadorConstantes.ENTRE
	, ConsultaOperadorConstantes.VAZIOS
);
