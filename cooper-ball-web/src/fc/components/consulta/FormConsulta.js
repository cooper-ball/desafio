import React from 'react';
import AlignItens from '../../../app/misc/consts/enums/AlignItens';
import ArrayLst from '../../../commom/utils/array/ArrayLst';
import Div from '../../../web/Div';
import FormConsultaFiltros from './FormConsultaFiltros';
import FormGenerico from '../FormGenerico';
import LayoutApp from '../LayoutApp';
import Paginacao from './Paginacao';
import Span from '../../../web/Span';
import Style from '../../../app/misc/utils/Style';
import Tb from '../../../web/Tb';
import Td from '../../../web/Td';
import TextAlign from '../../../app/misc/consts/enums/TextAlign';
import Tr from '../../../web/Tr';
import {Col} from 'antd';
import {Row} from 'antd';

export default class FormConsulta extends FormGenerico {
	constructor(props){
		super(props);
		this.state.showEdit = false;
		this.state.showConfirmLogout = false;
	}
	columns = new ArrayLst();
	widthTotal = 0;

	getTop() {
		return (
			<Tb>
				<Tr>
					<Td style={FormConsulta.TD_BUTTONS_LEFT}>
						{super.getTop()}
					</Td>
					{this.botoesSuperiores()}
				</Tr>
			</Tb>
		);
	}

	botoesSuperiores() {
		return null;
	}

	getTitle() {
		return "?";
	}

	getFooter() {
		return null;
	}

	addColumn(s, key, width, funcRender) {
		this.columns.add({title: s, dataIndex: key, key: key, width: width, render: funcRender});
		this.widthTotal += width;
	}

	didMount2() {
		let consulta = this.getConsulta();
		this.observar(consulta);
		this.observar(consulta.pagina);
		this.observar(consulta.paginas);
		this.componentDidMount3();
	}

	componentDidMount3() {}

	getIdEntidade() {
		throw new Error();
	}

	getBody() {

		if (!this.podeExibir()) {
			return this.acessoNegado();
		}

		return (
			<Div style={FormConsulta.STYLE_CONTEUDO}>
				{this.renderConteudo()}
				{this.getModal()}
			</Div>
		);
	}

	podeInserir() {
		return true;
	}

	podeExibir() {
		return true;
	}

	podeExcluir() {
		return true;
	}

	acessoNegado() {
		return <Span text={"Acesso Negado!"}/>;
	}

	renderConteudo() {

		if (this.getConsulta().pagina.intValue() < 1) {
			return this.renderFiltros();
		}

		return (
			<Div style={FormConsulta.STYLE_DIV_TABLE}>
				{this.getPaginacao()}
				{this.getTable()}
				{this.getPaginacao()}
			</Div>
		);
	}

	getPaginacao() {
		return <Paginacao consulta={this.getConsulta()}/>;
	}

	getModal() {
		if (this.state.showEdit) {
			return this.renderEdit();
		} else {
			return this.getModal2();
		}
	}

	getModal2() {
		return null;
	}

	renderFiltros() {
		return (
			<Div style={LayoutApp.createStyle().padding(20)}>
				<FormConsultaFiltros consulta={this.getConsulta()}/>
			</Div>
		);
	}

	d(s) {
		return (
			<Td className={"ant-table-selection-column"}>
				<Span text={s}/>
			</Td>
		);
	}

	r(o) {
		return <Tr className={"ant-table-row ant-table-row-level-0"}>{o}</Tr>;
	}

	newFiltro(title, bd) {
		return (
			<Row gutter={0}>
				<Col lg={8}>
					<Span text={title}/>
				</Col>
				<Col lg={16}>{bd}</Col>
			</Row>
		);
	}

	cancelar() {
		if (!this.state.showEdit) {
			super.cancelar();
		}
	}

	esc() {
		if (this.getConsulta().pagina.intValue() > 0) {
			this.getConsulta().pagina.clear();
		} else {
			this.setShowConfirmLogout(true);
		}
	}

	ctrlEnter() {
		if (this.getConsulta().pagina.intValue() < 1) {
			this.getConsulta().consultar();
		}
	}

	novo() {
		throw new Error();
	}

	getTempoAceitavelParaRenderizacao() {
		return 350;
	}

	createTitle() {
		return this.getConsulta().titleForm;
	}
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowConfirmLogout = o => this.setState({showConfirmLogout:o});

}
FormConsulta.TD_BUTTONS_LEFT = Style.create().alignItems(AlignItens.flexStart).textAlign(TextAlign.left).paddingTop(10).paddingBottom(10).paddingLeft(20).widthPercent(75);
FormConsulta.TD_BUTTONS_RIGHT = Style.create().alignItems(AlignItens.flexEnd).textAlign(TextAlign.right).paddingTop(10).paddingBottom(10).paddingRight(20).widthPercent(25);
FormConsulta.STYLE_DIV_TABLE = LayoutApp.createStyle().w100().marginTop(20);
FormConsulta.STYLE_BOTAO_MAIS_FILTROS = LayoutApp.createStyle().marginLeft(10);
FormConsulta.STYLE_CONTEUDO = LayoutApp.createStyle().textAlign(TextAlign.center);

FormConsulta.defaultProps = FormGenerico.defaultProps;
