import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import IdText from '../../../commom/utils/object/IdText';
import VArray from '../../../app/campos/support/VArray';

export default class CampoConsultaList extends CampoConsulta {

	a = new VArray();

	constructor(nomeCampoP, titulo, itens, notNull) {
		super(nomeCampoP, titulo, CampoConsultaList.OPERADORES_POSSIVEIS, notNull);
		if (!notNull) {
			itens = itens.copy();
			itens.add(CampoConsultaList.VAZIOS0);
		}
		this.a.setItens(itens);
	}

	bindInicial() {
		return this.a;
	}

	input(o) {
		return CampoConsultaList.renders.inputList(this, this.a);
	}

	toStringBind(bind) {
		return this.a.get().map(o => o.id).join(",");
	}

}
CampoConsultaList.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.EM
);
CampoConsultaList.VAZIOS0 = new IdText(0, "Vazios");
