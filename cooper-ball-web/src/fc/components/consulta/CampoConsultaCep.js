import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VCep from '../../../app/campos/support/VCep';

export default class CampoConsultaCep extends CampoConsulta {

	a = new VCep();

	constructor(nomeCampoP, titulo, notNull) {
		super(nomeCampoP, titulo, CampoConsultaCep.OPERADORES_POSSIVEIS, notNull);
	}

	bindInicial() {
		return this.a;
	}

}
CampoConsultaCep.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.IGUAL
);
