import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VLong from '../../../app/campos/support/VLong';

export default class CampoConsultaLong extends CampoConsulta {

	constructor(nomeCampoP, titulo, max, notNull) {
		super(nomeCampoP, titulo, CampoConsultaLong.OPERADORES_POSSIVEIS, notNull);
		this.a = new VLong().setMax(max);
		this.b = new VLong().setMax(max);
	}

	bindInicial() {
		return this.a;
	}

	bindFinal() {
		return this.b;
	}

	valorInicialMaiorQueValorFinal() {
		return this.a.maiorQue(this.b);
	}

	render1() {
		return CampoConsultaLong.renders.render0Long(this);
	}

}
CampoConsultaLong.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.IGUAL
	, ConsultaOperadorConstantes.MAIOR_OU_IGUAL
	, ConsultaOperadorConstantes.MENOR_OU_IGUAL
	, ConsultaOperadorConstantes.ENTRE
);
