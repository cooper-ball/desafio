import React from 'react';
import ArrayEmpty from '../../../commom/utils/array/ArrayEmpty';
import BotaoType from '../../../antd/BotaoType';
import Display from '../../../app/misc/consts/enums/Display';
import Div from '../../../web/Div';
import FcBotao from '../FcBotao';
import FlexDirection from '../../../app/misc/consts/enums/FlexDirection';
import Frag from '../../../react/Frag';
import JustifyContent from '../../../app/misc/consts/enums/JustifyContent';
import Span from '../../../web/Span';
import Style from '../../../app/misc/utils/Style';
import SuperComponent from '../../../app/misc/components/SuperComponent';
import {Button} from 'antd';
import {Col} from 'antd';
import {Popover} from 'antd';
import {Row} from 'antd';

export default class Paginacao extends SuperComponent {

	voltarParaFiltros() {
		this.clear();
	}

	voltarParaPrimeira() {
		this.firstPage();
	}

	irParaUltima() {
		this.lastPage();
	}

	proxima() {
		this.nextPage();
	}

	anterior() {
		this.priorPage();
	}

	clear() {
		this.props.consulta.pagina.clear();
	}

	firstPage() {
		this.props.consulta.firstPage();
	}

	lastPage() {
		this.props.consulta.lastPage();
	}

	nextPage() {
		this.props.consulta.nextPage();
	}

	priorPage() {
		this.props.consulta.priorPage();
	}

	render0() {
		return (
			<Row style={Style.create().display(Display.flex).get()}>
				<Col span={6} style={Paginacao.FLEX_START.get()}>
					{this.botaoVoltarParaFiltros()}
				</Col>
				<Col span={12}>
					{this.navegacao()}
				</Col>
				<Col span={6} style={Paginacao.FLEX_END.get()}>
					{this.acoes()}
				</Col>
			</Row>
		);
	}

	acoes() {
		let itens = this.props.consulta.outrosBotoes();

		if (ArrayEmpty.is(itens)) {
			return null;
		}

		return (
			<Popover
			mouseEnterDelay={0.5}
			placement={"bottomRight"}
			trigger={"click"}
			content={
				<Frag>{itens.getArray()}</Frag>
			}>
				<Button
				type={"normal"}
				>Ações</Button>
			</Popover>
		);

	}

	botaoVoltarParaFiltros() {
		return (
			<FcBotao
				type={BotaoType.normal}
				title={"< Voltar Para Filtros (Esc)"}
				acao={() => this.voltarParaFiltros()}
			/>
		);
	}

	navegacao() {

		let pagina = "Página " + this.props.consulta.pagina.get() + " de " + this.props.consulta.paginas.get() + " - " + this.props.consulta.registros.asString() + " registros";

		let primeira = this.props.consulta.pagina.eq(1);
		let ultima = this.props.consulta.pagina.eq(this.props.consulta.paginas.get());

		return (
			<Div style={Paginacao.ST_DIV}>
				<FcBotao type={BotaoType.normal} title={"Primeira"} disabled={primeira} acao={() => this.voltarParaPrimeira()}/>
				<FcBotao type={BotaoType.normal} title={"<"} disabled={primeira} style={Paginacao.ST_BUTTON} acao={() => this.anterior()}/>
				<Span text={pagina}/>
				<FcBotao type={BotaoType.normal} title={">"} disabled={ultima} style={Paginacao.ST_BUTTON} acao={() => this.proxima()}/>
				<FcBotao type={BotaoType.normal} title={"Última"} disabled={ultima} acao={() => this.irParaUltima()}/>
			</Div>
		);

	}

	didMount() {
		this.observar(this.props.consulta.pagina);
		this.observar(this.props.consulta.paginas);
		this.observar(this.props.consulta.registros);
	}
}
Paginacao.FLEX_START = Style.create().display(Display.flex).justifyContent(JustifyContent.flexStart);
Paginacao.FLEX_END = Style.create().display(Display.flex).justifyContent(JustifyContent.flexEnd);
Paginacao.ST_DIV = Style.create().marginTop(5).marginBottom(5).flexDirection(FlexDirection.row);
Paginacao.ST_BUTTON = Style.create().marginLeft(5).marginRight(5);

Paginacao.defaultProps = SuperComponent.defaultProps;
