import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';

export default class SeparacaoDeGrupo extends CampoConsulta {

	constructor(nomeCampoP, tituloP) {
		super(nomeCampoP, tituloP, new ArrayLst(), false);
	}

	render0() {
		return SeparacaoDeGrupo.renders.divider(this);
	}

	bindInicial() {
		return null;
	}

	toJSON() {
		return null;
	}

}
