import ArrayLst from '../../../commom/utils/array/ArrayLst';
import CampoConsulta from './CampoConsulta';
import ConsultaOperadorConstantes from '../../../app/cruds/consultaOperador/ConsultaOperadorConstantes';
import VNumeric from '../../../app/campos/support/VNumeric';

export default class CampoConsultaMoney extends CampoConsulta {

	constructor(nomeCampoP, titulo, inteiros, notNull) {
		super(nomeCampoP, titulo, CampoConsultaMoney.OPERADORES_POSSIVEIS, notNull);
		this.a = new VNumeric().setInteiros(inteiros);
		this.b = new VNumeric().setInteiros(inteiros);
	}

	bindInicial() {
		return this.a;
	}

	bindFinal() {
		return this.b;
	}

	valorInicialMaiorQueValorFinal() {
		return this.a.maiorQue(this.b);
	}

}
CampoConsultaMoney.OPERADORES_POSSIVEIS = ArrayLst.build(
	ConsultaOperadorConstantes.IGUAL
	, ConsultaOperadorConstantes.MAIOR_OU_IGUAL
	, ConsultaOperadorConstantes.MENOR_OU_IGUAL
	, ConsultaOperadorConstantes.ENTRE
);
