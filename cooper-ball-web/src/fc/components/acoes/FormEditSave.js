import CdiObj from '../../../app/misc/components/CdiObj';
import Null from '../../../commom/utils/object/Null';

export default class FormEditSave extends CdiObj {

	saveJaFoiClicado = false;

	constructor(form) {
		super();
		this.form = form;
	}

	run() {

		if (this.form.getCampos().houveAlteracoes.isFalse()) {
			return;
		}

		let salvou = this.form.getCampos().save(this.form.getVinculo(), () => {
			if (this.form.isVinculado()) {
				this.alert.info("As alterações terão efeito quando o registro for salvo!");
			} else {
				this.alert.success("Registro Salvo com Sucesso!");
				this.form.afterSave();
			}
		});

		if (salvou) {
			this.saveJaFoiClicado = false;

			if (!Null.is(this.onConfirm)) {
				this.onConfirm();
			} else if (this.form.isVinculado()) {
				this.form.close();
			}

		} else {
			this.saveJaFoiClicado = true;
			this.form.forceUpdate();
			this.alert.error("Não foi possível salvar o registro pois há impedimentos!");
		}

	}

}
