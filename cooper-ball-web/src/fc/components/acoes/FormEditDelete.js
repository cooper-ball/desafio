import Null from '../../../commom/utils/object/Null';

export default class FormEditDelete {

	constructor(form) {
		this.form = form;
	}

	run() {

		if (!this.form.podeExcluir()) return;
		if (this.form.esteFormEstahExibindoAlgumModal()) {
			return;
		}
		if (this.form.getCampos().houveAlteracoes.isTrue()) {
			return;
		}
		if (this.form.isVinculado()) {
			this.form.getVinculo().clear();
		}
		if (!Null.is(this.onDelete)) {
			this.onDelete(this.form.getCampos().id.get());
		}
		this.form.cancelar();

	}

}
