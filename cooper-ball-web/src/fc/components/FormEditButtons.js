import React from 'react';
import BotaoType from '../../antd/BotaoType';
import Div from '../../web/Div';
import FcBotao from './FcBotao';
import Frag from '../../react/Frag';
import LayoutApp from './LayoutApp';
import ModalCamposAlterados from './campoAlterado/ModalCamposAlterados';
import Null from '../../commom/utils/object/Null';
import PopoverErros from './PopoverErros';
import SuperComponent from '../../app/misc/components/SuperComponent';
import {Popconfirm} from 'antd';

export default class FormEditButtons extends SuperComponent {
	constructor(props){
		super(props);
		this.state.showModalAlteracoes = false;
	}

	acaoShowModalAlteracoes() {
		this.setShowModalAlteracoes(true);
	}

	acaoCloseModalAlteracoes() {
		this.setShowModalAlteracoes(false);
	}

	render0() {

		if (this.props.campos.isReadOnly()) {
			return (
				<Div style={this.getStyle()}>
					{this.botaoCancelar()}
				</Div>
			);
		}

		if (this.props.campos.houveAlteracoes.isTrue()) {
			return (
				<Frag>
					<Div style={this.getStyle()}>
						{this.getBotaoErro()}
						<FcBotao style={FormEditButtons.STYLE_BUTTON} type={BotaoType.normal} acao={() => this.acaoShowModalAlteracoes()} title={"Listar Alterações"}/>
						<FcBotao style={FormEditButtons.STYLE_BUTTON} type={BotaoType.normal} acao={this.props.onCancel} title={"Cancelar Alterações (Esc)"}/>
						<FcBotao style={FormEditButtons.STYLE_BUTTON} acao={this.props.onSave} title={this.getSaveTitle()}/>
					</Div>
					{this.getModalAlteracoes()}
				</Frag>
			);
		} else {
			return (
				<Div style={this.getStyle()}>
					{this.renderDelete()}
					{this.botaoCancelar()}
				</Div>
			);
		}
	}

	botaoCancelar() {
		return <FcBotao style={FormEditButtons.STYLE_BUTTON} type={BotaoType.normal} acao={this.props.onCancel} title={this.getCancelTitle()}/>;
	}

	renderDelete() {

		if (this.props.somenteUpdate) {
			return null;
		}

		if (this.props.campos.id.isEmpty()) {
			return null;
		}

		if (this.props.campos.id.get() < 1) {
			return null;
		}

		if (Null.is(this.props.onDelete)) {
			return null;
		}

		return (
			<Popconfirm
			onConfirm={this.props.onDelete}
			title={"Confirma exclusão?"}
			okText={"Sim"}
			cancelText={"Não"}
			><FcBotao style={FormEditButtons.STYLE_BUTTON} type={BotaoType.danger} title={this.getRemoveTitle()}/></Popconfirm>
		);
	}

	getModalAlteracoes() {
		if (this.state.showModalAlteracoes) {
			return <ModalCamposAlterados podeDesfazer={true} valores={this.props.campos.camposAlterados()} onClose={() => this.acaoCloseModalAlteracoes()} campos={this.props.campos}/>;
		} else {
			return null;
		}
	}

	getStyle() {
		return LayoutApp.createStyle().w100();
	}

	didMount() {
		this.observar(this.props.campos);
		this.observar(this.props.campos.houveAlteracoes);
	}

	getBotaoErro() {
		if (!this.props.saveJaFoiClicado) {
			return null;
		} else {
			return (
				<PopoverErros
					campos={this.props.campos}
					abaSelecionada={this.props.abaSelecionada}
					setAbaSelecionada={this.props.setAbaSelecionada}
				/>
			);
		}
	}

	getSaveTitle() {
		return this.props.vinculado ? "Confirmar (Ctrl + Enter)" : "Salvar (Ctrl + Enter)";
	}
	getCancelTitle() {
		return this.props.vinculado ? "Fechar (Esc)" : "Sair (Esc)";
	}
	getRemoveTitle() {
		return this.props.vinculado ? "Remover (Ctrl + Del)" : "Excluir (Ctrl + Del)";
	}
	setShowModalAlteracoes = o => this.setState({showModalAlteracoes:o});
}
FormEditButtons.STYLE_BUTTON = LayoutApp.createStyle().margin(5).marginRight(0);

FormEditButtons.defaultProps = {
	...SuperComponent.defaultProps,
	saveJaFoiClicado: false,
	vinculado: false,
	somenteUpdate: false
}
