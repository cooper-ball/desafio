import IntegerCompare from '../../commom/utils/integer/IntegerCompare';
import Null from '../../commom/utils/object/Null';
import StringReplace from '../../commom/utils/string/StringReplace';

export default class EntityFront {

	static referenceCount = 0;
	referenceId = ++EntityFront.referenceCount;
	observacoesHouveMudancas = false;
	houveMudancas = false;

	getId() {
		return this.id;
	}

	setId(value) {
		if (!Null.is(this.id) && IntegerCompare.ne(this.id, value)) {
			throw new Error("Alteracao de id: de " + this.id + " para " + value);
		} else if (Null.is(value)) {
			this.id = null;
		} else {
			this.id = value;
		}
	}
	getObservacoes() {
		return null;
	}
	setObservacoes(value) {}
	getReferenceId() {
		return this.referenceId;
	}
	getObservacoesHouveMudancas() {
		return this.observacoesHouveMudancas;
	}
	setObservacoesHouveMudancas(value) {
		this.observacoesHouveMudancas = value;
	}
	getHouveMudancas() {
		return this.houveMudancas;
	}
	setHouveMudancas(value) {
		this.houveMudancas = value;
	}

	toJSON() {
		let s = this.asString();
		s = StringReplace.exec(s, " ,", ",");
		s = StringReplace.exec(s, ",,", ",");
		s = StringReplace.exec(s, " }", "}");
		s = StringReplace.exec(s, ",}", "}");
		s = StringReplace.exec(s, " ]", "]");
		s = StringReplace.exec(s, ",]", "]");
		return s;
	}

}
