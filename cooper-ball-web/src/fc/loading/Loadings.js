import VInteger from '../../app/campos/support/VInteger';

export default class Loadings extends VInteger {

	constructor() {
		super();
		this.setMaximo(99);
		this.setMinimo(0);
	}

}
