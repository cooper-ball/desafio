import React from 'react';
import AlignSelf from '../../app/misc/consts/enums/AlignSelf';
import Color from '../../app/misc/consts/enums/Color';
import Div from '../../web/Div';
import Img from '../../web/Img';
import Span from '../../web/Span';
import SuperComponent from '../../app/misc/components/SuperComponent';
import {LoadingOutlined} from '@ant-design/icons';
import {Modal} from 'antd';

export default class LoadingView extends SuperComponent {

	render0() {
		return (
			<Modal
				visible={this.loadings.intValue() > 0}
				closable={false}
				footer={null}>
				<Div style={this.newStyle().displayFlexCol().lineHeight(35).fontSize(20).textAlignCenter()}>
					<Img src={this.variaveisDeAmbiente.logo} style={this.newStyle().widthPercent(40).alignSelf(AlignSelf.center)}/>
					<LoadingOutlined
					style={this.newStyle().color(Color.amareloCooper).fontSize(35).padding(20).get()}
					/>
					<Span text={"Por favor aguarde alguns instantes."}/>
					<Span text={"Estamos processando sua solicitação."}/>
				</Div>
			</Modal>
		);
	}

}

LoadingView.defaultProps = SuperComponent.defaultProps;
