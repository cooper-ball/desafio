package js.promise;

import gm.utils.lambda.FTT;
import gm.utils.lambda.FVoidT;
import gm.utils.lambda.FVoidVoid;

public interface IPromise<T> {
//	IPromise<T> then(FVoidVoid func);
	IPromise<T> then(FTT<T,T> func);
	IPromise<T> catch_(FVoidT<Response> func);
	IPromise<T> finally_(FVoidVoid func);
}
