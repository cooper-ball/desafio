package js.promise;

import js.annotations.Support;
import js.html.support.Headers;
import js.outros.Blob;
import src.commom.utils.comum.PromiseBuilder;

@Support
public class Response {
	public Object body;
	public Object data;
	public String url;
	public int status;
	public String message;
	public Object rawResponse;
	public Headers headers;
	public Promise<Blob> blob(){
		return null;
	}
	public Promise<Object> json() {
		return PromiseBuilder.ft(() -> body);
	}

	public Response body(Object o){body = o; return this;}
	public Response data(Object o){data = o; return this;}
	public Response url(String o){url = o; return this;}
}
