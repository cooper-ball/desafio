package js.promise;

import gm.utils.anotacoes.Ignorar;
import gm.utils.comum.Lst;
import gm.utils.lambda.FTT;
import gm.utils.lambda.FVoidT;
import gm.utils.lambda.FVoidTT;
import gm.utils.lambda.FVoidVoid;
import js.Js;
import js.array.Array;

@Ignorar
public class Promise<T> {

	public static enum PromiseStatus {
		pending, // pendente: Estado inicial, que não foi realizada nem rejeitada.
		fulfilled, // realizada: sucesso na operação.
		rejected// rejeitado: falha na operação.
	}

	PromiseStatus status = PromiseStatus.pending;
//	private FVoidT<T> then;

	private Lst<FVoidT<T>> thens = new Lst<>();
	private Lst<FVoidT<Throwable>> catchs = new Lst<>();

	private final Lst<FVoidVoid> finallys;
	private FVoidTT<FVoidT<T>, FVoidT<Throwable>> func;
	T value;
	Throwable motivoRejeicao;

	private Promise(FVoidTT<FVoidT<T>, FVoidT<Throwable>> func, Lst<FVoidVoid> finallys) {
		this.func = func;
		this.finallys = finallys;
		Js.setTimeout(() -> exec());
	}

	public Promise(FVoidTT<FVoidT<T>, FVoidT<Throwable>> func) {
		this(func, new Lst<>());
	}

	//utilizado no await do js
	public T get() {
		exec();
		return value;
	}

	Promise(Lst<FVoidVoid> finallys) {
		this.finallys = finallys;
	}

	private void exec(T o) {
		exec(() -> resolve(o));
	}

	private void exec() {
		exec(() -> func.call(o -> resolve(o), error -> reject(error)));
	}

	private void exec(FVoidVoid funcao) {

		try {
			funcao.call();
		} catch (Exception e) {
			reject(e);
		} finally {
			for (FVoidVoid m : finallys) {
				m.call();
			}
		}

	}

	public <X> Promise<X> then(FTT<X, T> func) {
		Promise<X> p = new Promise<X>(finallys);
		FVoidT<T> f = o -> {
			X x = func.call(o);
			p.exec(x);
		};
		thens.add(f);
		return p;
	}

	public Promise<T> catch_(FVoidT<Throwable> func) {
		catchs.add(func);
		return this;
	}

	public Promise<T> finally_(FVoidVoid func) {
		finallys.add(func);
		return this;
	}

	void resolve(T o) {
		for (FVoidT<T> f : thens) {
			f.call(o);
		}
		this.value = o;
		status = PromiseStatus.fulfilled;
	}

	void reject(Throwable e) {
		this.motivoRejeicao = e;
		status = PromiseStatus.rejected;
		if (catchs.isEmpty()) {
			System.err.println("A função foi rejeitada e não tem nehum catch configurado");
		} else {
			for (FVoidT<Throwable> funcao : catchs) {
				funcao.call(e);
			}
		}
	}

	public static PromiseAll all(Array<Promise<?>> promises) {
		return new PromiseAll(promises.list);
	}

}
