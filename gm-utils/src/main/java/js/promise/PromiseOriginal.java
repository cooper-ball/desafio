package js.promise;

import gm.utils.anotacoes.Ignorar;
import gm.utils.lambda.FT;
import gm.utils.lambda.FTT;
import gm.utils.lambda.FVoidT;
import gm.utils.lambda.FVoidTT;
import gm.utils.lambda.FVoidVoid;
import gm.utils.map.MapSO;
import gm.utils.rest.ResponseException;
import js.Js;

@Ignorar
public class PromiseOriginal<T> implements IPromise<T> {

	private FTT<T,T> onThen;
	private FVoidT<Response> onCatch;
	private FVoidVoid onFinally;

	public PromiseOriginal<T> buffer(boolean value) {
		return this;
	}

	public PromiseOriginal<T> parse(FTT<Response, String> func) {
		return this;
	}

	@Override
	public PromiseOriginal<T> then(FTT<T,T> func) {
		if (this.onThen == null) {
			this.onThen = func;
			return this;
		} else {
			throw new RuntimeException("Nao preparado para then em cascata");
		}
	}

	@Override
	public PromiseOriginal<T> catch_(FVoidT<Response> func) {
		if (this.onCatch == null) {
			this.onCatch = func;
			return this;
		} else {
			throw new RuntimeException("Nao preparado para catch em cascata");
		}
	}

	@Override
	public PromiseOriginal<T> finally_(FVoidVoid func) {
		if (this.onFinally == null) {
			this.onFinally = func;
			return this;
		} else {
			throw new RuntimeException("Nao preparado para finally em cascata");
		}
	}

	public PromiseOriginal(FT<T> func) {
		Js.setTimeout(() -> {
			try {
				T o = func.call();
				if (onThen != null) {
					onThen.call(o);
				}
			} catch (ResponseException e) {
				if (onCatch != null) {
					Response res = new Response();
					res.body = e;
					res.status = e.getStatusCode();
					res.data = e.getBody();
					onCatch.call(res);
				}
			} catch (Exception e) {
				if (onCatch != null) {
					Response res = new Response();
					res.body = e;
					res.status = 500;
					res.data = new MapSO().add("message", e.getMessage());
					onCatch.call(res);
				} else {
					throw new RuntimeException(e);
				}
			} finally {
				if (onFinally != null) {
					onFinally.call();
				}
			}
		}, 0);
	}

	public PromiseOriginal(FVoidTT<FVoidT<Object>, Object> func) {

	}

	public T get() {
		return null;
	}

	public static <TT> PromiseOriginal<TT> resolve(FVoidVoid onSuccess) {
		onSuccess.call();
		return null;
	}

}
