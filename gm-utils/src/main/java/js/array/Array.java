package js.array;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import gm.utils.anotacoes.Ignorar;
import gm.utils.comum.Lst;
import gm.utils.lambda.FTT;
import gm.utils.lambda.FTTT;
import gm.utils.lambda.FVoidTT;
import src.commom.utils.array.ArrayLst;
import src.commom.utils.integer.IntegerBox;
import src.commom.utils.string.StringBox;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringParse;

@Ignorar
public class Array<T> {

	public Class<T> classe;

	public Array() {}

	@SafeVarargs
	public Array(T... itens) {
		for (int i = 0; i < itens.length; i++) {
			this.push(itens[i]);
		}
	}

	public final Lst<T> list = new Lst<>();

	public void push(T o) {
		this.list.add(o);
	}
	public void forEach(Consumer<T> action) {
		this.list.forEach(action);
	}
	public void forEach(FVoidTT<T, Integer> action) {
		for (int i = 0; i < list.size(); i++) {
			action.call(list.get(i), i);
		}
	}
	public void sort(FTTT<Integer, T, T> action) {
		this.list.sort((a, b) -> action.call(a, b));
	}
	public Array<T> filter(Predicate<T> predicate) {
		Array<T> array = new Array<>();
		array.classe = this.classe;
		List<T> collect = this.list.stream().filter(predicate).collect(Collectors.toList());
		for (T o : collect) {
			array.push(o);
		}
		return array;
	}
	public <TT> Array<TT> map(FTT<TT,T> func) {
		Array<TT> array = new Array<>();
		this.list.forEach(o -> array.push(func.call(o)));
		return array;
	}
	public <TT> Array<TT> map(FTTT<TT,T, Integer> func) {
		Array<TT> array = new Array<>();
		IntegerBox box = new IntegerBox(0);
		this.list.forEach(o -> {
			array.push(func.call(o, box.get()));
			box.inc1();
		});
		return array;
	}
	public T get(int index) {
		if (this.list.isEmpty()) {
			return null;
		} else {
			return this.list.get(index);
		}
	}

	@Deprecated
	public T removeFirst() {
		return shift();
	}

	@Deprecated
	public T removeLast() {
		return pop();
	}

	//removeFirst
	public T shift() {
		if (list.isEmpty()) {
			return null;
		} else {
			return list.remove(0);
		}
	}

	public boolean some(Predicate<T> predicate) {
		return list.stream().anyMatch(predicate);
	}
	@SuppressWarnings("unchecked")
	public Array<T> concat(T... os) {
		Array<T> array = new Array<>();
		for (T o : list) {
			array.push(o);
		}
		for (T o : os) {
			array.push(o);
		}
		return array;
	}
	public Array<T> concat(Array<T> list) {
		Array<T> array = new Array<>();
		for (T t : this.list) {
			array.push(t);
		}
		for (T t : list.list) {
			array.push(t);
		}
		return array;
	}
	public int indexOf(T o) {
		return list.indexOf(o);
	}

	public T pop() {
		if (list.isEmpty()) {
			return null;
		} else {
			return list.removeLast();
		}
	}

	public int length() {
		return list.size();
	}
	public <RESULT> RESULT reduce(FTTT<RESULT, RESULT, T> func, RESULT startValue) {
		for (T t : list) {
			startValue = func.call(startValue, t);
		}
		return startValue;
	}
	public String join(String separador) {
		if (list.isEmpty()) {
			return "";
		}
		if (list.size() == 1) {
			return list.get(0).toString();
		}
		StringBox box = new StringBox("");
		forEach(o -> box.add(separador + StringParse.get(o)));
		String s = box.get();
		if (!StringEmpty.is(s) && separador.length() > 0) {
			s = s.substring(separador.length());
		}
		return s;
	}
	public static boolean isArray(Object o) {
		return o instanceof Array;
	}
	public void splice(int index, int count) {
		for (int i = 0; i < count; i++) {
			this.list.remove(index);
		}
	}
	public void splice(int index, int count, T element) {
		splice(index, count);
		list.add(index, element);
	}

	public static Array<String> from(Set<String> keys) {
		Array<String> list = new Array<>();
		keys.forEach(s -> list.push(s));
		return list;
	}

	@SuppressWarnings("unchecked")
	public void java_addCast(Object o) {
		T t = (T) o;
		push(t);
	}

	@Override
	public String toString() {
		return list.toString();
	}

	@SafeVarargs
	public static <X> Array<X> build(X... array) {
		return ArrayLst.build(array).getArray();
	}


}
