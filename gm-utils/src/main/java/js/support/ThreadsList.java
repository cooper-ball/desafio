package js.support;

import java.util.ArrayList;
import java.util.List;

import gm.utils.lambda.FVoidVoid;

public class ThreadsList {
	private static List<FVoidVoid> list = new ArrayList<>();
	public static void add(FVoidVoid func) {
		list.add(func);
	}
	public static void add(FVoidVoid func, int index) {
		if (list.size() >= index) {
			list.add(index, func);
		} else {
			add(func);
		}
	}
	public static void run() {
		while (!list.isEmpty()) {
			FVoidVoid func = list.remove(0);
			func.call();
		}
	}
}
