package js.support;

import gm.utils.anotacoes.Ignorar;
import js.annotations.Support;
import src.commom.utils.string.StringParse;

@Support @Ignorar
public class console {

	public static boolean disableYellowBox;

	public static void log(Object... os) {
		for (Object o : os) {
			System.out
			.println(StringParse.get(o));
		}
	}
	public static void error(Object... os) {
		for (Object o : os) {
			System.err.println(o);
		}
	}
	public static void warn(Object... os) {
		error(os);
	}
}
