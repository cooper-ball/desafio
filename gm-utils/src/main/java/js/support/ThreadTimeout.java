package js.support;

import gm.utils.lambda.FVoidVoid;

public class ThreadTimeout {

	private FVoidVoid func;
	private int milisegundos;
	private long start;

	public ThreadTimeout(FVoidVoid func, int milisegundos) {
		this.func = func;
		this.milisegundos = milisegundos;
		this.start = System.currentTimeMillis();
		ThreadsList.add(() -> call());
	}

	private void call() {
		if (System.currentTimeMillis() - start >= milisegundos) {
			func.call();
		} else {
			ThreadsList.add(() -> call(), 1);
		}
	}

}
