package js;

import java.util.Map;

import gm.utils.anotacoes.Ignorar;
import gm.utils.classes.UClass;
import gm.utils.comum.Aleatorio;
import gm.utils.comum.UType;
import gm.utils.lambda.FT;
import gm.utils.lambda.FVoidVoid;
import gm.utils.number.UDouble;
import gm.utils.number.UInteger;
import gm.utils.reflection.Atributo;
import gm.utils.reflection.ListAtributos;
import gm.utils.reflection.ListMetodos;
import gm.utils.reflection.Metodo;
import gm.utils.rest.URest;
import js.html.HtmlDocument;
import js.html.HtmlWindow;
import js.html.support.FetchParams;
import js.promise.Promise;
import js.promise.Response;
import js.support.ThreadsList;
import js.support.console;
import src.commom.utils.comum.PromiseBuilder;
import src.commom.utils.string.StringContains;

public class Js {

	public static final boolean inJava = true;

	public static final HtmlDocument document = new HtmlDocument();
	public static final HtmlWindow window = HtmlWindow.instance;

	public static String typeof(Object o) {

		if (o == null)
			return null;
		if (o instanceof Boolean)
			return "boolean";
		if (o instanceof String)
			return "string";
		if (o instanceof Integer)
			return "number";
		if (o instanceof Double)
			return "number";

		Class<?> classe = o.getClass();
		if (classe.equals(boolean.class))
			return "boolean";
		if (classe.equals(int.class))
			return "number";
		if (classe.equals(double.class))
			return "number";

		String s = classe.getSimpleName();
		if (StringContains.is(s, "$Lambda$"))
			return "function";
		return s;

	}

	@SuppressWarnings("unchecked")
	public static <T> T get(Object o, String key) {

		if (UType.isPrimitiva(o)) {
			return null;
		}

		Atributo a = ListAtributos.get(o).get(key);

		if (a != null) {
			return a.get(o);
		}

		Metodo metodo = ListMetodos.get(UClass.getClass(o)).get(key);
		if (metodo != null) {
			FT<?> func = () -> metodo.invoke(o);
			return (T) func;
		}

		return null;
	}

	@Ignorar
	public static void setInterval(FVoidVoid f, int milisegundos) {
		Js.setTimeout(() -> {
			f.call();
			Js.setTimeout(f, milisegundos);
		}, milisegundos);
	}

	@Ignorar
	public static void setTimeout(FVoidVoid f) {
		Js.setTimeout(f, 0);
	}

	public static FT<Boolean> podeContinuarTimeout;

	private static boolean podeContinuarTimeout() {
		return podeContinuarTimeout == null || podeContinuarTimeout.call();
	}

	@Ignorar
	public static void setTimeout(FVoidVoid f, int milisegundos) {

		FVoidVoid x = () -> {
			if (podeContinuarTimeout()) {
				f.call();
			} else {
				Js.setTimeout(f, 200);
			}
		};

		ThreadsList.add(x);
	}

	public static final Object undefined = null;

	public static class Math {

		public static int random() {
			return Aleatorio.getInteger();
		}

		public static int floor(int i) {
			return i;
		}

	};

	public static <T> T undefined() {
		return null;
	}

	public static Integer parseInt(Object o) {
		return UInteger.toInt(o);
	}

	public static class JsConstructor {
		public String name;
	}

	public static class Proto {
		public JsConstructor constructor;
	}

	public Proto __proto__ = new Proto();

	public static Double parseFloat(Object o) {
		return UDouble.toDouble(o);
	}

	public static Promise<Response> fetch(String s) {
		return null;
	}

	public static Promise<Response> fetch(String url, FetchParams params) {

		String method = params.method;

		if (!method.contentEquals("GET") && !method.contentEquals("POST")) {
			throw new RuntimeException("Preparado apenas para GET e POST");
		}

		Map<String, String> headers = params.headers == null ? null : params.headers.map;

		return PromiseBuilder.ft(() -> {
			URest result;
			if (method.contentEquals("GET")) {
				result = URest.get(url, headers, params.body);
			} else {
				result = URest.post(url, headers, params.body);
			}
			Response o = new Response();
			o.data = result.getData();
			o.body = o.data;
			o.url = url;
			o.status = result.getCode();
			return o;
		});

	}

	public static <T> T await(Promise<T> promise) {
		return promise.get();
	}

	public static FVoidVoid async(FVoidVoid func) {
		return null;
	}

	public static class Object£ {
		public static void assign(Object a, Object b) {
			throw new RuntimeException("implementar");
		}
		public static Object fromEntries(js.map.Map<?, ?> map) {
			return map;
		}
	}

	public static void alert(String message) {
		console.log(message);
	}

}
