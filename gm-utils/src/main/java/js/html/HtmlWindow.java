package js.html;

import gm.utils.lambda.FVoidVoid;

public class HtmlWindow {
	public static final HtmlWindow instance = new HtmlWindow();

	public int innerWidth;
	public int innerHeight;
	public int outerWidth;
	public int outerHeight;

	public ReactNativeWebView ReactNativeWebView;
	public static class ReactNativeWebView {
		public void postMessage(String s) {

		}
	}



	public HtmlScreen screen = HtmlScreen.instance;
	public HtmlHistory history = HtmlHistory.instance;
	public HtmlWindowLocation location = HtmlWindowLocation.instance;
//	public Keycloak Keycloak;
	public void addEventListener(String s, FVoidVoid func) {}
	public void addEventListener(String s, FVoidVoid func, HtmlWindowEventListenerParams params) {}
	public void removeEventListener(String s, FVoidVoid func) {}
	public void open(String url) {}
	public void postMessage(int scrollHeight) {};
}
