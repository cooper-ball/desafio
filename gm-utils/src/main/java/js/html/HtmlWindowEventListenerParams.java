package js.html;

import js.annotations.Support;

@Support
public class HtmlWindowEventListenerParams {
	public HtmlWindowEventListenerParams passive(boolean value) {return this;};
}
