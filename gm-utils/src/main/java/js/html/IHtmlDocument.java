package js.html;

public interface IHtmlDocument {

	HtmlElement getElementById(String id);

}
