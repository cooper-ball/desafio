package js.html.support;

import js.annotations.Support;

@Support
public class SelectInputProps {
	public String name;
	public String id;
	public SelectInputProps name(String o){this.name = o; return this;}
	public SelectInputProps id(String o){this.id = o; return this;}
}
