package js.html.support;

import js.annotations.Support;

@Support
public class Rect {
	public int top;
	public int right;
	public int bottom;
	public int left;
}
