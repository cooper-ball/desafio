package js.html.support;

import js.annotations.Support;

@Support
public class DragDropEvent {
	public DragDropEventDataTransfer dataTransfer;
	public void preventDefault() {}
}
