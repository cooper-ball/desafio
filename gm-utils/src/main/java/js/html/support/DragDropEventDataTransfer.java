package js.html.support;

import js.annotations.Support;

@Support
public class DragDropEventDataTransfer {
	public Object[] files;
	public Object[] items;
}
