package js.html.support;

import js.annotations.Support;

@Support
public class RenderSuggestionOptions {
	public String query;
	public boolean isHighlighted;
}
