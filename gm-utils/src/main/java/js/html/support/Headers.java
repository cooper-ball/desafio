package js.html.support;

import java.util.HashMap;
import java.util.Map;

import gm.utils.anotacoes.Ignorar;
import gm.utils.lambda.FVoidTT;

@Ignorar
public class Headers {

	public final Map<String, String> map = new HashMap<>();

	public void set(String key, String value) {
		this.map.put(key, value);
	}

	public String get$(String key) {
		return this.map.get(key);
	}

	public void delete(String key) {
		this.map.remove(key);
	}

	public boolean has(String key) {
		return this.map.containsKey(key);
	}

	public void forEach(FVoidTT<String, String> func) {
		this.map.forEach((k,v) -> func.call(v, k));
	}

}
