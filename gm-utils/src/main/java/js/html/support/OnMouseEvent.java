package js.html.support;

import js.annotations.Support;
import js.html.HtmlElement;

@Support
public class OnMouseEvent {
	public int clientX;
	public int clientY;
	public int pageX;
	public int pageY;
	public int movementX;
	public int movementY;
	public int screenX;
	public int screenY;
	public HtmlElement target;
	public HtmlElement currentTarget;
	public OnMouseEvent clientX(int o){this.clientX = o; return this;}
	public OnMouseEvent clientY(int o){this.clientY = o; return this;}
	public OnMouseEvent pageX(int o){this.pageX = o; return this;}
	public OnMouseEvent pageY(int o){this.pageY = o; return this;}
	public OnMouseEvent movementX(int o){this.movementX = o; return this;}
	public OnMouseEvent movementY(int o){this.movementY = o; return this;}
	public OnMouseEvent screenX(int o){this.screenX = o; return this;}
	public OnMouseEvent screenY(int o){this.screenY = o; return this;}
	public OnMouseEvent target(HtmlElement o){this.target = o; return this;}
	public OnMouseEvent currentTarget(HtmlElement o){this.currentTarget = o; return this;}
}
