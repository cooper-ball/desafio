package js.html.support;

import gm.utils.anotacoes.Ignorar;
import gm.utils.map.MapSO;
import gm.utils.map.MapSoFromJson;
import gm.utils.map.MapSoFromObject;
import js.annotations.Support;
import src.commom.utils.string.StringEmpty;

@Support @Ignorar
public class FetchParams {

	public String method;
	public Headers headers;
	public String body;
	public String mode;
	public String cache;
	public String credentials;

	public MapSO getBody() {
		if (this.body == null) return null;
		if (this.body instanceof String) {
			String s = this.body;
			if (StringEmpty.is(s)) return null;
			return MapSoFromJson.get(s);
		} else {
			return MapSoFromObject.get(this.body);
		}
	}

	public FetchParams method(String o){this.method = o; return this;}
	public FetchParams headers(Headers o){this.headers = o; return this;}
	public FetchParams body(String o){this.body = o; return this;}
	public FetchParams mode(String o){this.mode = o; return this;}
	public FetchParams cache(String o){this.cache = o; return this;}
	public FetchParams credentials(String o){this.credentials = o; return this;}

}
