package js.html.support;

import js.annotations.Support;

@Support
public class Touch {
	public int clientX;
	public int clientY;
	public int force;
	public int identifier;
	public int pageX;
	public int pageY;
	public int radiusX;
	public int radiusY;
	public int rotationAngle;
	public int screenX;
	public int screenY;
	public Touch clientX(int o){this.clientX = o; return this;}
	public Touch clientY(int o){this.clientY = o; return this;}
	public Touch force(int o){this.force = o; return this;}
	public Touch identifier(int o){this.identifier = o; return this;}
	public Touch pageX(int o){this.pageX = o; return this;}
	public Touch pageY(int o){this.pageY = o; return this;}
	public Touch radiusX(int o){this.radiusX = o; return this;}
	public Touch radiusY(int o){this.radiusY = o; return this;}
	public Touch rotationAngle(int o){this.rotationAngle = o; return this;}
	public Touch screenX(int o){this.screenX = o; return this;}
	public Touch screenY(int o){this.screenY = o; return this;}
}
