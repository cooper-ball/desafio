package js.html;

public class ListenerEvent {

	protected ListenerEvent() {}

	public HtmlElement target;
	public String code;
	public int keyCode;
	public String key;
	public boolean shiftKey;
	public boolean ctrlKey;
	public void preventDefault() {}
	public void stopPropagation() {}
	public ListenerEvent keyCode(int o){this.keyCode = o; return this;}
	public ListenerEvent key(String o){this.key = o; return this;}
	public ListenerEvent target(HtmlElement o){this.target = o; return this;}
	public ListenerEvent code(String o){this.code = o; return this;}

}
