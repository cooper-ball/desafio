package js.html;

import gm.utils.lambda.FVoidT;

public class HtmlDocument {

	public static IHtmlDocument implementacao;

	public HtmlInstance body = new HtmlInstance();
	public DocumentElement documentElement = new DocumentElement();

	public Object hardware;

	public HtmlElement[] getElementsByTagName(String tag) {
		System.err.println("HtmlDocument - Não é possível utilizar este metodo em testes. Os componentes react mudam suas tags para tgs imprevisiveis. Utilize getElementsByClassName ou leia diretamente dos nodes");
		return new HtmlElement[] {};
	}

	public HtmlElement getElementById(String id) {
		return implementacao.getElementById(id);
	}

	public HtmlElement createElement(String tag) {
		return null;
	}

	public void addEventListener(String event, FVoidT<ListenerEvent> func) {
		if ("keydown".equals(event)) {

		}
	}

	public NodeList querySelectorAll(String string) {
		//TODO fazer um select por compoenents
		NodeList list = new NodeList();
		return list;
	}

}
