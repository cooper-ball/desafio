package js.html;

import java.util.function.Consumer;

import src.commom.utils.array.ArrayLst;

public class NodeList {

	public ArrayLst<HtmlElement> itens = new ArrayLst<>();

	public void forEach(Consumer<HtmlElement> action) {
		itens.forEach(action);
	}

	public int length() {
		return itens.size();
	}

}
