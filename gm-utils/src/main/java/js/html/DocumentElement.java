package js.html;

public class DocumentElement {
	public int scrollHeight;
	public DocumentElementStyle style = new DocumentElementStyle();

	public static class DocumentElementStyle {
		public String webkitUserSelect;
		public String userSelect;
	}

}
