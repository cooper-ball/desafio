package js.html;

import js.html.support.Rect;

public abstract class HtmlElement  {

	public Object[] files;
	public Object src;
	public String type;
	public boolean async;
	public String innerHTML;
	public String rel;
	public String href;
	public int clientHeight;
	public int clientWidth;
	public HtmlElement parentNode;
	public int offsetWidth;
	public int offsetLeft;
	public double scrollLeft;
	public HtmlElementStyle style;

	public int scrollTop;
	public int scrollHeight;

	public static class HtmlElementStyle {
		public int width;
	}

	public abstract void focus();
	public abstract void scrollIntoView(boolean b);
	public abstract void click();

//	public final void focus() {
//		node.getComponent().focus();
//	}
//
//	public void scrollIntoView(boolean b) {
//		node.getComponent().focus();
//	}
//
//	public void click() {
//		node.getComponent().click();
//	}

	public void remove() {

	}
	public Rect getBoundingClientRect() {
		// TODO Auto-generated method stub
		return null;
	}

}
