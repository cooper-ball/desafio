package src.commom.utils.date;

import src.commom.utils.array.ArrayLst;
import src.commom.utils.integer.IntegerCompare;

public class UAnoMes {

	private static ArrayLst<Integer> MESES_31 = ArrayLst.build(1,3,5,7,8,10,12);

	public static int getUltimoDiaDoMes(int ano, int mes) {
		if (IntegerCompare.eq(mes, 2)) {
			if (IntegerCompare.isZero(ano % 4)) {
				return 29;
			} else {
				return 28;
			}
		} else if (MESES_31.exists(o -> IntegerCompare.eq(o, mes))) {
			return 31;
		} else {
			return 30;
		}
	}

}
