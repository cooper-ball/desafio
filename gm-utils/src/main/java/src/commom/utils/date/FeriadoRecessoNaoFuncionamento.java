package src.commom.utils.date;

import src.commom.utils.array.ArrayLst;
import src.commom.utils.comum.Test;
import src.commom.utils.string.StringCompare;

public class FeriadoRecessoNaoFuncionamento {

	private static final ArrayLst<String> defaults = ArrayLst.build("01/01", "21/04", "01/05", "07/09", "12/10", "02/11", "15/11", "25/12");
	private static final ArrayLst<String> knows = ArrayLst.build("10/04/2020","11/06/2020");

	private static ArrayLst<Test<BaseData>> tests = new ArrayLst<>();

	public static void addTest(Test<BaseData> test) {
		tests.add(test);
	}

	public static void add(BaseData data) {
		knows.add(data.format("[dd]/[mm]/[yyyy]"));
	}

	public static boolean test(BaseData data) {

		String ddmm = data.format("[dd]/[mm]");
		if (defaults.exists(s -> StringCompare.eq(ddmm, s))) {
			return true;
		}

		String ddmmyyyy = data.format("[dd]/[mm]/[yyyy]");
		if (knows.exists(s -> StringCompare.eq(ddmmyyyy, s))) {
			return true;
		}

		if (tests.exists(o -> o.test(data))) {
			return true;
		}

		return false;

	}

}
