package src.commom.utils.date;

public interface IDate {
	int getAno();
	int getMes();
	int getDia();
	int getHora();
	int getMinuto();
	int getSegundo();
	int getMilesimo();
	int getDiaDaSemana();
	long getTime();
}