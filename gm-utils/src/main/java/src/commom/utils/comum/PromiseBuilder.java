package src.commom.utils.comum;

import gm.utils.lambda.FT;
import js.promise.Promise;

public class PromiseBuilder {

	public static <T> Promise<T> ft(FT<T> func) {
		return new Promise<T>((resolve,reject) -> {
			try {
				resolve.call(func.call());
			} catch (Exception e) {
				reject.call(e);
			}
		});
	}

}