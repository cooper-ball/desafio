package src.commom.utils.comum;

import src.commom.utils.array.ArrayLst;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringExtraiNumeros;
import src.commom.utils.string.StringSplit;

public class SeparaMilhares {

	public static String exec(String s) {
		s = StringExtraiNumeros.exec(s);
		if (StringEmpty.is(s)) {
			return "";
		}
		ArrayLst<String> split = StringSplit.exec(s, "");
		s = "";
		while (!split.isEmpty()) {
			s = split.pop() + s;
			if (!split.isEmpty()) {
				s = split.pop() + s;
				if (!split.isEmpty()) {
					s = split.pop() + s;
					if (!split.isEmpty()) {
						s = "." + s;
					}
				}
			}
		}
		return s;
	}

}
