package src.commom.utils.comum;

import gm.utils.anotacoes.Ignorar;
import gm.utils.lambda.FTTTT;
import gm.utils.lambda.FVoidTT;
import js.map.Map;
import js.map.MapIterator;
import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.integer.IntegerParse;
import src.commom.utils.object.Equals;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringIs;

public class JsMap<K,V> {

	private Map<K,V> map;

	//@IgnoreStart
	@Ignorar
	public JsMap() {
		this(null);
	}
	//@IgnoreEnd

	public JsMap(Map<K,V> map) {
		if (Null.is(map)) {
			this.map = new Map<>();
		} else {
			this.map = map;
		}
	}

	public MapIterator<K> keys() {
		return map.keys();
	}

	public JsMap<K, V> set(K key, V value) {
		map.set(key, value);
		return this;
	}

	public V get(K key) {
		return map.get(key);
	}

	public void forEach(FVoidTT<K, V> func) {
		map.forEach((v, k) -> func.call(k, v));
	}

	public void clear() {
		map.clear();
	}

	public void delete(Object key) {
		map.delete(key);
	}

	public int size() {
		return map.size;
	}

	public boolean isEmpty() {
		return IntegerCompare.eq(size(), 0);
	}

	public <T> T reduce(FTTTT<T, K, V, T> func, T initial) {
		Box<T> box = new Box<>(initial);
		map.forEach((v, k) -> box.set(func.call(k, v, box.get())));
		return box.get();
	}

	public String asString(String separador) {
		if (isEmpty()) {
			return "";
		} else {
			return reduce((k, v, atual) -> atual + separador + k + "="+v, "").substring(separador.length());
		}
	}

	public boolean containsValue(V value) {
		if (isEmpty()) {
			return false;
		} else {
			Box<Boolean> box = new Box<>(false);
			if (StringIs.is(value)) {
				String ss = (String) value;
				map.forEach((v,k) -> {
					String s = (String) v;
					if (StringCompare.eq(s, ss)) {
						box.set(true);
					}
				});
			} else {
				map.forEach((v,k) -> {
					if (Equals.is(k, value)) {
						box.set(true);
					}
				});
			}
			return box.get();
		}
	}

	public Integer getInt(K key) {
		return IntegerParse.toInt(get(key));
	}

	public int getIntDef(K key, int def) {
		Integer o = getInt(key);
		if (Null.is(o)) {
			return def;
		} else {
			return o;
		}
	}

	@Override
	public String toString() {

		if (isEmpty()) {
			return "{}";
		} else {
			return "{"+reduce((k, v, atual) -> atual + ",\"" + k + "\": \""+v+"\"", "").substring(1)+"}";
		}

	}

	public Map<K, V> getMap() {
		return map;
	}

}
