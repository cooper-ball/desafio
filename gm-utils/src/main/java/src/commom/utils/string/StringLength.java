package src.commom.utils.string;

import src.commom.utils.integer.IntegerCompare;

public class StringLength {

	private StringLength() {}

	public static int get(String s) {
		if (StringEmpty.is(s)) {
			return 0;
		} else {
			return s.length();
		}
	}

	public static boolean is(String s, int size) {
		return IntegerCompare.eq(get(s), size);
	}

	public static String max(String s, int max) {
		if (StringLength.get(s) > max) {
			return s.substring(0, max);
		} else {
			return s;
		}
	}

}
