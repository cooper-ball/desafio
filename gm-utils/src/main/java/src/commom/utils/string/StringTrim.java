package src.commom.utils.string;

import src.commom.utils.object.Null;

public class StringTrim {

	private StringTrim() {}

	public static String trim(String s) {
		if (Null.is(s)) {
			return null;
		} else {
			return s.trim();
		}
	}

	public static String plus(String s) {

		if (Null.is(s)) {
			return null;
		}

		s = trim(s);

		s = StringReplace.exec(s, "\t", " ");
		s = StringReplace.exec(s, "\n", " ");
		s = StringReplace.exec(s, "  ", " ");
		return s;

	}

	public static String plusNull(String s) {
		s = plus(s);
		return StringEmpty.is(s) ? null : s;
	}

	public static String left(String s) {
		if (StringEmpty.is(s)) {
			return "";
		}
		while (s.startsWith(" ") || s.startsWith("\t") || s.startsWith("\n")) {
			s = s.substring(1);
		}
		return s;
	}

	public static String right(String s) {
		if (StringEmpty.is(s)) {
			return "";
		}
		while (s.endsWith(" ") || s.endsWith("\t") || s.endsWith("\n")) {
			s = StringRight.ignore1(s);
		}
		return s;
	}

}
