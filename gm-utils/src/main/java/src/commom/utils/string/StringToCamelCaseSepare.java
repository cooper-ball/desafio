package src.commom.utils.string;

import src.commom.utils.array.ArrayLst;

public class StringToCamelCaseSepare {

	private StringToCamelCaseSepare() {}

	private static ArrayLst<String> ALL = StringConstants.MAIUSCULAS.concat(StringConstants.MINUSCULAS).concat(StringConstants.NUMEROS);

	public static String exec(String s) {

		if (StringEmpty.is(s)) {
			return s;
		}

		s = s.replace("aa", "aA");

		s = s.replace(" S.A.", " SA ");
		s = s.replace(".", " ");

		StringBox box = new StringBox(s);

		StringConstants.MAIUSCULAS.forEach(a -> StringConstants.MAIUSCULAS.forEach(b -> box.replace(a + "." + b, a + b)));

		StringConstants.MAIUSCULAS.forEach(maiuscula -> {
			StringConstants.MINUSCULAS.forEach(minuscula -> {
				box.replace(minuscula + maiuscula, minuscula + " " + maiuscula);

				StringConstants.MINUSCULAS.forEach(minuscula2 -> {
					box.replace(minuscula + "E" + maiuscula + minuscula2, minuscula + "E " + maiuscula + minuscula2);
				});
			});
			StringConstants.NUMEROS.forEach(numero -> {
				box.replace(numero + maiuscula, numero + " " + maiuscula);
				box.replace(maiuscula + numero, maiuscula + " " + numero);
			});
		});

		StringConstants.MINUSCULAS.forEach(minuscula -> {
			StringConstants.NUMEROS.forEach(numero -> {
				box.replace(numero + minuscula, numero + " " + minuscula);
				box.replace(minuscula + numero, minuscula + " " + numero);
			});
		});

		s = "";

		while (!box.isEmpty()) {
			String x = box.removeLeft(1);
			if (ALL.contains(x)) {
				s += x;
			} else {
				s = s.trim() + " ";
			}
		}

		s = StringToNomeProprio.exec(s, true);
		s = StringColocarAcentos.exec(s);
		s = StringPrimeiraMaiuscula.exec(s);

		return s;

	}

}
