package src.commom.utils.string;

import gm.utils.string.StringNormalizer;
import src.commom.utils.array.ArrayLst;
import src.commom.utils.integer.IntegerIs;

public class StringCamelCase {

	private static final ArrayLst<String> VALIDOS = StringConstants.MINUSCULAS.concat(StringConstants.NUMEROS).concat2(" ");

	public static String exec(String s) {

		s = StringNormalizer.get(s);
		String xx = "";

		while (!s.isEmpty()) {
			String n = s.substring(0,1);
			s = s.substring(1);
			if (n.equals(n.toUpperCase())) {
				xx += " ";
			}
			xx += n;
		}

		s = xx;

		while (IntegerIs.is(s.substring(0, 1))) {
			s = s.substring(1);
			if (s.isEmpty()) {
				return null;
			}
		}

		s = s.toLowerCase();
		StringExtraiCaracteres.exec(s, VALIDOS);
		s = StringTrim.plus(s);

		if (s == null) {
			return null;
		}

		s = s.replace("_", " ");

		s = StringTrim.plus(s);

		StringBox box = new StringBox(s);

		StringConstants.MINUSCULAS.forEach(x -> {
			box.replace(" " + x, x.toUpperCase());
		});

		box.replace(" ", "");

		return box.get();

	}

}
