package src.commom.utils.string;

public class StringPrimeiraMaiuscula {

	private StringPrimeiraMaiuscula() {}

	public static String exec(String s) {
		s = StringTrim.plus(s);
		if (StringEmpty.is(s)) {
			return s;
		} else {
			return s.substring(0, 1).toUpperCase() + s.substring(1);
		}
	}

}
