package src.commom.utils.string;

import src.commom.utils.array.ArrayLst;

public class UNomeProprio {

	private static ArrayLst<String> VALIDOS_SEM_NUMEROS = StringConstants.MAIUSCULAS.concat(StringConstants.MINUSCULAS).add(StringConstants.aspa_simples).add(" ");
	private static ArrayLst<String> VALIDOS_COM_NUMEROS = VALIDOS_SEM_NUMEROS.concat(StringConstants.NUMEROS);

	public static void main(String[] args) {
		System.out.println(formatParcial("Asdfsa d", false));
	}

	public static String formatParcial(String s, boolean manterNumeros) {

		if (StringEmpty.is(s)) {
			return "";
		}

		s = StringReplace.exec(s, "\t", " ");

		boolean espaco = s.endsWith(" ");
		s = StringTrim.plus(s);

		while (!StringEmpty.is(s) && s.startsWith("'")) {
			s = s.substring(1);
			s = StringTrim.plus(s);
		}

		if (StringEmpty.is(s)) {
			return "";
		}

		s = s.toLowerCase();

		ArrayLst<String> validos = manterNumeros ? VALIDOS_COM_NUMEROS : VALIDOS_SEM_NUMEROS;

		s = StringExtraiCaracteres.exec(s, validos);

		if (StringEmpty.is(s)) {
			return "";
		}

		StringBox box = new StringBox(s);

		VALIDOS_SEM_NUMEROS.forEach(o -> box.replace(o+o+o, o+o));

		box.replace("''", "'");
		box.set(" " + box + " ");

		StringConstants.MINUSCULAS.forEach(o ->
			box.set(StringReplace.exec(box.get(), " " + o, " " + o.toUpperCase()))
		);

		StringConstants.PREPOSICOES_NOME_PROPRIO.forEach(x -> box.replace(" " + StringPrimeiraMaiuscula.exec(x) + " ", " " + x + " "));
		StringConstants.ARTIGOS.forEach(x -> box.replace(" " + StringPrimeiraMaiuscula.exec(x) + " ", " " + x + " "));
		box.replace(" E ", " e ");
		box.replace(" Ou ", " ou ");
		box.replace(" de a ", " de A ");
		box.replace(" de o ", " de O ");

		s = StringTrim.plus(box.get());

		if (StringLength.get(s) < 2) {
			return s;
		}

		s = s.substring(0,1).toUpperCase() + s.substring(1);

		if (espaco) {
			s += " ";
		}

		return StringLength.max(s, 60);

	}

	public static boolean isValid(String s) {
		s = StringTrim.plus(s);
		if (StringLength.get(s) < 7) {
			return false;
		}
		if (!StringContains.is(s, " ")) {
			return false;
		}
		return true;
	}

}
