package src.commom.utils.string;

public class StringObrig {

	private StringObrig() {}

	public static String get(String s) {
		return get2(s, "s is empty");
	}

	public static String get2(String s, String message) {
		if (StringEmpty.is(s)) {
			throw new RuntimeException(message);
		} else {
			return s;
		}
	}

}
