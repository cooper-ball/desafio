package src.commom.utils.string;

public class StringRight {

	private StringRight() {}

	public static String ignore1(String s) {
		return ignore(s, 1);
	}

	public static String ignore(String s, int count) {

		if (StringEmpty.is(s)) {
			return "";
		}

		int len = StringLength.get(s);

		if (len <= count) {
			return "";
		} else {
			return s.substring(0, len - count);
		}

	}

	public static String get(String s, int count) {
		int len = StringLength.get(s);
		if (len < count) {
			return s;
		}
		return s.substring(len - count);
	}

}
