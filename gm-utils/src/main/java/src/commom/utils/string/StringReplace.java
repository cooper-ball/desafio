package src.commom.utils.string;

import gm.utils.lambda.FT;
import src.commom.utils.array.ArrayLst;
import src.commom.utils.object.Null;

public class StringReplace {

	private static final String OCORRENCIA_IMPROVAVEL = "@[%85!2#$%]@))(6¬¢";

	private StringReplace() {}

	private static String execPrivate(String s, String a, String b) {

		if (Null.is(s) || Null.is(a) || s.length() == 0 || !StringContains.is(s, a)) {
			return s;
		}

		if (Null.is(b)) {
			b = "";
		}

		ArrayLst<String> split = StringSplit.exec(s, a);
		s = split.join(b);

		if (StringContains.is(s, a)) {
			return execPrivate(s, a, b);
		} else {
			return s;
		}

	}

	public static String exec(String s, String a, String b) {

		String aux = OCORRENCIA_IMPROVAVEL;
		if (StringContains.is(aux, a)) {
			aux = execPrivate(aux, a, "");
		}

		s += aux;

		if (StringContains.is(b, a)) {
			s = execPrivate(s, a, aux);
			s = execPrivate(s, aux, b);
		} else {
			s = execPrivate(s, a, b);
		}

		s = StringRight.ignore(s, aux.length());

		return s;

	}

	public static String ifContains(String s, String a, FT<String> b) {
		if (StringContains.is(s, a)) {
			return exec(s, a, b.call());
		} else {
			return s;
		}
	}

	@Deprecated
	public static String doWhile(String s, String a, String b) {
		return exec(s, a, b);
	}

}
