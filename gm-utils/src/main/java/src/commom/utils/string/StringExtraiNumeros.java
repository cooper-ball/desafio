package src.commom.utils.string;

import src.commom.utils.array.ArrayLst;

public class StringExtraiNumeros {

	private StringExtraiNumeros() {}

	public static final ArrayLst<String> NUMEROS = ArrayLst.build("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

	public static String exec(String s) {
		return StringExtraiCaracteres.exec(s, NUMEROS);
	}

	public static String nullIfEmpty(String s) {
		s = exec(s);
		if (StringEmpty.is(s)) {
			return null;
		} else {
			return s;
		}
	}

}
