package src.commom.utils.string;

import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.object.Null;

public class StringBeforeFirst {

	private StringBeforeFirst() {}

	public static String get(String s, String substring) {
		int i = s.indexOf(substring);
		if (IntegerCompare.eq(i, -1)) {
			return null;
		}
		return s.substring(0, i);
	}

	public static String safe(String s, String substring) {
		s = get(s, substring);
		if (Null.is(s)) {
			return "";
		} else {
			return s;
		}
	}

	public static String obrig(String s, String substring) {
		return StringObrig.get(get(s, substring));
	}

}
