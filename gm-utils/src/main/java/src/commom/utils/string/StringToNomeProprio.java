package src.commom.utils.string;

public class StringToNomeProprio {

	private StringToNomeProprio() {}

	public static String exec(String s, boolean manterNumeros) {

		s = UNomeProprio.formatParcial(s, manterNumeros);

		if (StringEmpty.is(s)) {
			return null;
		} else {
			return s.trim();
		}

	}

}
