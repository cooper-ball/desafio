package src.commom.utils.string;

import java.util.regex.Pattern;

import js.Js;
import js.UNative;
import js.annotations.Method;
import js.array.Array;
import src.commom.utils.array.ArrayLst;
import src.commom.utils.object.Null;

@Method
public class StringSplit {

	private StringSplit() {}

	public static ArrayLst<String> exec(String s, String separator) {

		if (Null.is(s)) {
			return new ArrayLst<>();
		}

		if (s.indexOf(separator) == -1) {
			return ArrayLst.build(s);
		}

		String sp = separator;

		if (Js.inJava) {
			sp = Pattern.quote(sp);
		}

		Array<String> array = UNative.asArray(s.split(sp), String.class);
		if (Js.inJava) {
			if (s.endsWith(separator) && StringEmpty.notIs(separator)) {
				array.push("");
			}
		}

		return new ArrayLst<String>(array);

	}

}