package src.commom.utils.string;

import gm.utils.number.UInteger;
import gm.utils.string.StringNormalizer;

public class StringToConstantName {

	private StringToConstantName() {}

	public static String exec(String s) {
		s = StringTrim.plus(s);
		if (StringEmpty.is(s)) {
			return s;
		}
		s = s.replace("%", " percent ");
		s = s.replace("/", " barra ");
		s = s.replace(">", " maior ");
		s = s.replace("<", " menor ");
		s = s.replace("=", " igual ");
		s = s.replace("\"", " aspasduplas ");
		s = s.replace("-", " ");
		s = StringTrim.plus(s);
		s = StringToCamelCaseSepare.exec(s);
		s = s.replace("-", "");
		s = s.replace(" ", "_");
		s = s.toUpperCase();
		s = StringNormalizer.get(s);
		StringBox box = new StringBox(s);
		StringConstants.NUMEROS.forEach(numero -> {
			box.replace("_" + numero, numero);
		});
		s = box.get();
		StringReplace.exec(s, "__", "_");
		if (UInteger.isInt(s.substring(0, 1))) {
			s = "_" + s;
		}
		return s;
	}

}
