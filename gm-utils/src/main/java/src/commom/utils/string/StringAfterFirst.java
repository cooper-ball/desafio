package src.commom.utils.string;

import src.commom.utils.integer.IntegerCompare;

public class StringAfterFirst {

	private StringAfterFirst() {}

	public static String get(String s, String substring) {
		int i = s.indexOf(substring);
		if (IntegerCompare.eq(i, -1)) {
			return null;
		}
		i += substring.length();
		return s.substring(i);
	}

	public static String obrig(String s, String substring) {
		return StringObrig.get(get(s, substring));
	}

}
