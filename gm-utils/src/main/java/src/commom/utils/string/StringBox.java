package src.commom.utils.string;

import gm.utils.anotacoes.Ignorar;
import src.commom.utils.object.ObjJs;
import src.commom.utils.object.UJson;

public class StringBox extends ObjJs {

	private String value;

	public StringBox(String s) {
		set(s);
	}

	//@IgnoreStart
	@Ignorar
	public StringBox() {
		set("");
	}
	//@IgnoreEnd

	public String get() {
		return this.value;
	}

	public String set(String s) {
		if (StringEmpty.is(s)) {
			this.value = "";
		} else {
			this.value = s;
		}
		return this.value;
	}

	public StringBox add(String s) {
		set(get()+s);
		return this;
	}

	public boolean isEmpty() {
		return StringEmpty.is(value);
	}

	public String removeLeft(int count) {
		String x = value.substring(0, count);
		value = value.substring(count);
		return x;
	}

	public StringBox replace(String a, String b) {
		set(StringReplace.exec(this.value, a, b));
		return this;
	}

	public void clear() {
		set("");
	}

	@Override
	public String toString() {
		return value;
	}

	public boolean eq(String s) {
		return StringCompare.eq(get(), s);
	}
	@Override
	protected String toJsonImpl() {
		return "{" + UJson.itemString("value", value) + "}";
	}
}
