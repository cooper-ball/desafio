package src.commom.utils.string;

public class StringLike {

	private StringLike() {}

	public static boolean is(String a, String b) {
		if (StringEmpty.is(b)) {
			return true;
		} else if (StringEmpty.is(a)) {
			return false;
		}
		a = StringRemoveAcentos.exec(a).toLowerCase();
		b = StringRemoveAcentos.exec(b).toLowerCase();
		return StringContains.is(a, b);
	}

}
