package src.commom.utils.string;

import js.Js;
import src.commom.utils.object.Equals;
import src.commom.utils.object.Null;

public class StringCompare {

	private StringCompare() {}

	public static boolean eqq(String a, String b) {

		if (StringEmpty.is(a)) {
			return StringEmpty.is(b);
		} else if (StringEmpty.is(b)) {
			return false;
		} else {
			return eq(a, b);
		}

	}

	public static boolean eq(String a, String b) {

		if (Null.is(a)) {
			return Null.is(b);
		} else if (Null.is(b)) {
			return false;
		}

		/* garante que a comparacao seja por conteudo e não por referencia */
		a += "";
		b += "";

		if (Equals.is(a, b)) {
			return true;
		} else if (StringEmpty.is(a)) {
			return StringEmpty.is(b);
		} else if (StringEmpty.is(b)) {
			return false;
		} else {
			if (Js.inJava) {
				return a.contentEquals(b);
			}
			return false;
		}

	}

	public static boolean eqIgnoreCase(String a, String b) {
		if (Null.is(a)) {
			return Null.is(b);
		} else if (Null.is(b)) {
			return false;
		} else {
			return eq(a.toLowerCase(), b.toLowerCase());
		}
	}

	public static int compare(String a, String b) {
		if (eq(a, b)) {
			return 0;
		} else if (Null.is(a)) {
			return -1;
		} else if (Null.is(b)) {
			return 1;
		} else {
			return a.compareTo(b);
		}
	}

}
