package src.commom.utils.string;

import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.object.Null;

public class StringEmpty {

	private StringEmpty() {}

	public static boolean is(String s) {
		return Null.is(s) || IntegerCompare.eq(s.trim().length(), 0);
	}

	public static boolean isPlus(String s) {

		if (is(s)) {
			return true;
		}

		s = StringTrim.plus(s).toLowerCase();

		if (StringCompare.eq(s, "null")) {
			return true;
		} else if (StringCompare.eq(s, "undefined")) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean notIs(String s) {
		return !is(s);
	}

}
