package src.commom.utils.longo;

import src.commom.utils.object.Equals;
import src.commom.utils.object.Null;

public class LongCompare {

	public static boolean eq(Long a, Long b) {
		if (Equals.is(a, b)) {
			return true;
		} else if (Null.is(a) || Null.is(b)) {
			return false;
		} else if (a - b == 0L) {
			return true;
		} else if (Equals.is(a+1, b+1)) {
			return true;
		} else {
			return false;
		}
	}

	public static int compare(Long a, Long b) {
		if (eq(a,b)) {
			return 0;
		} else if (Null.is(a)) {
			return -1;
		} else if (Null.is(b)) {
			return 1;
		} else if (a < b) {
			return -1;
		} else {
			return 1;
		}
	}

	public static boolean isZero(Long i) {
		return eq(i, 0L);
	}

}
