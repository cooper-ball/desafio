package src.commom.utils.integer;

import js.Js;
import src.commom.utils.object.Null;

public class IntegerParse {

	public static int toIntDef(Object o, int def) {
		if (IntegerIs.is(o)) {
			return toInt(o);
		}
		try {
			o = toInt(o);
			if (IntegerIs.is(o)) {
				return (int) o;
			} else {
				return def;
			}
		} catch (Exception e) {
			return def;
		}

	}

	public static Integer toInt(Object o) {
		if (Null.is(o)) {
			return null;
		} else {
			return Js.parseInt(o);
		}
	}

}
