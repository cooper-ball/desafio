package src.commom.utils.integer;

import js.outros.Number;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringExtraiNumeros;
import src.commom.utils.string.StringLength;
import src.commom.utils.string.StringParse;
import src.commom.utils.string.StringRight;
import src.commom.utils.string.StringTrim;

public class IntegerIs {

	public static boolean is(Object o) {

		if (Null.is(o)) {
			return false;
		}

		if (Number.isInteger(o)) {
			return true;
		}

		String s = StringParse.get(o);
		s = StringTrim.plus(s);

		if (StringEmpty.is(s)) {
			return false;
		}

		if (s.endsWith(".")) {
			s = StringRight.ignore1(s);
		}

		if (s.startsWith("-")) {
			s = s.substring(1);
		}

		String n = StringExtraiNumeros.exec(s);

		if (!StringCompare.eq(s, n)) {
			return false;
		}

		int len = StringLength.get(s);

		if (len > 10) {
			return false;
		} else if (len < 10) {
			return true;
		} else {
			return StringCompare.compare(s, "2147483647") < 1;
		}

	}

}
