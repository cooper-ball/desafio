package src.commom.utils.array;

import gm.utils.lambda.FTTT;
import js.array.Array;
import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.object.Equals;
import src.commom.utils.object.Null;

public class ArrayEquals {

	public static boolean size(ArrayLst<?> a, ArrayLst<?> b) {
		return IntegerCompare.eq(ArrayLength.get(a), ArrayLength.get(b));
	}

	public static boolean size0(Array<?> a, Array<?> b) {
		return IntegerCompare.eq(ArrayLength.get0(a), ArrayLength.get0(b));
	}

	public static boolean is0(ArrayLst<?> a, ArrayLst<?> b) {
		return is(a, b, (x,y) -> Equals.is(x, y));
	}

	public static boolean is(ArrayLst<?> a, ArrayLst<?> b, FTTT<Boolean, Object, Object> comparator) {
		if (!size(a, b)) {
			return false;
		} else if (ArrayEmpty.is(a)) {
			return ArrayEmpty.is(b);
		} else if (ArrayEmpty.is(b)) {
			return false;
		} else {
			for (int i = 0; i < a.size(); i++) {
				if (!comparator.call(a.get(i), b.get(i))) {
					return false;
				}
			}
			return true;
		}
	}

	public static <T> boolean isT(ArrayLst<T> a, ArrayLst<T> b, FTTT<Boolean, T, T> comparator) {
		if (!size(a, b)) {
			return false;
		} else if (ArrayEmpty.is(a)) {
			return ArrayEmpty.is(b);
		} else if (ArrayEmpty.is(b)) {
			return false;
		} else {
			for (int i = 0; i < a.size(); i++) {
				if (!comparator.call(a.get(i), b.get(i))) {
					return false;
				}
			}
			return true;
		}
	}

	public static <T> boolean isT0(Array<T> a, Array<T> b, FTTT<Boolean, T, T> comparator) {
		if (!size0(a, b)) {
			return false;
		} else if (ArrayEmpty.is0(a)) {
			return ArrayEmpty.is0(b);
		} else if (ArrayEmpty.is0(b)) {
			return false;
		} else {
			for (int i = 0; i < a.length(); i++) {
				if (!comparator.call(a.get(i), b.get(i))) {
					return false;
				}
			}
			return true;
		}
	}

	public static boolean equalsNotification(ArrayLst<?> a, ArrayLst<?> b) {

		if (Null.is(a)) {
			return Null.is(b);
		}
		if (Null.is(b)) {
			return false;
		}
		if (a.size() != b.size()) {
			return false;
		}

		return is(a, b, (x,y) -> {
			if (x instanceof ArrayLst) {
				ArrayLst<?> xx = (ArrayLst<?>) x;
				ArrayLst<?> yy = (ArrayLst<?>) y;
				return equalsNotification(xx, yy);
			} else {
				return a == b;
			}
		});

	}

}
