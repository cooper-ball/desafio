package src.commom.utils.array;

import java.util.function.Consumer;
import java.util.function.Predicate;

import gm.utils.anotacoes.Ignorar;
import gm.utils.lambda.FTT;
import gm.utils.lambda.FTTT;
import gm.utils.lambda.FVoidTT;
import js.UNative;
import js.array.Array;
import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.object.Null;

public class ArrayLst<T> {

	/*@IgnoreStart*/
	public ArrayLst() {
		this((Array<T>) null);
	}
	/*@IgnoreEnd*/

	private final Array<T> valueArray;

	public ArrayLst(Array<T> array) {
		if (Null.is(array)) {
			this.valueArray = new Array<>();
		} else if (!Array.isArray(array)) {
			throw new Error("Não é um array");
		} else {
			this.valueArray = array;
		}
	}

//	@IgnoreStart
	@Ignorar
	public ArrayLst<T> add(T o) {
		return add(o, null);
	}
//	@IgnoreEnd

	public ArrayLst<T> add(T o, Integer index) {
		if (Null.is(index)) {
			valueArray.push(o);
		} else {
			valueArray.splice(index, 0, o);
		}
		return this;
	}

	public void addAll(ArrayLst<T> list) {
		if (!ArrayEmpty.is(list)) {
			list.forEach(o -> this.push(o));
		}
	}

	public ArrayLst<T> filter(Predicate<T> predicate) {
		return new ArrayLst<>(valueArray.filter(predicate));
	}

	public <RESULT> RESULT reduce(FTTT<RESULT, RESULT, T> func, RESULT startValue) {
		return valueArray.reduce(func, startValue);
	}

	public boolean exists(Predicate<T> predicate) {
		return !filter(predicate).isEmpty();
	}

	public boolean some(Predicate<T> predicate) {
		return exists(predicate);
	}

	public boolean isEmpty() {
		return IntegerCompare.isZero(valueArray.length());
	}

	public ArrayLst<T> clear() {
		while (!isEmpty()) {
			removeLast();
		}
		return this;
	}

	public T removeLast() {
		return valueArray.pop();
	}

	public T removeFirst() {
		return valueArray.shift();
	}

	public void removeIf(Predicate<T> predicate) {
		ArrayLst<T> filter = filter(predicate);
		while (!filter.isEmpty()) {
			T o = filter.removeFirst();
			removeObject(o);
		}
	}

	public boolean removeObject(T o) {
		int index = indexOf(o);
		if (index == -1) {
			return false;
		} else {
			remove(index);
			return true;
		}
	}

	public T remove(int index) {
		T o = get(index);
		valueArray.splice(index, 1);
		return o;
	}

	public T get(int index) {
		return valueArray.get(index);
	}

	public int indexOf(T o) {
		return valueArray.indexOf(o);
	}

	public ArrayLst<T> concat(ArrayLst<T> other) {
		int va = valueArray.length();
		int vb = other.valueArray.length();
		ArrayLst<T> a = new ArrayLst<T>(valueArray.concat(other.valueArray));
		int vc = a.size();
		if (IntegerCompare.ne(va+vb, vc)) {
			throw new RuntimeException("O concat nao funcionou");
		}
		return a;
	}

	@SuppressWarnings("unchecked")
	public ArrayLst<T> concat2(T... os) {
		return concat(build(os));
	}

	public ArrayLst<T> forEach(Consumer<T> action) {
		valueArray.forEach(action);
		return this;
	}

	public ArrayLst<T> forEachI(FVoidTT<T, Integer> action) {
		valueArray.forEach(action);
		return this;
	}

	public ArrayLst<T> copy() {
		ArrayLst<T> list = new ArrayLst<>();
		forEach(o -> list.add(o));
		return list;
	}

	public boolean contains(T o) {
		return indexOf(o) > -1;
	}

	public String join(String separator) {
		return valueArray.join(separator);
	}

	public int size() {
		return valueArray.length();
	}

	public T uniqueObrig(Predicate<T> predicate) {
		T o = unique(predicate);
		if (Null.is(o)) {
			throw new Error("O filtro não retornou resultados");
		}
		return o;
	}

	public T unique(Predicate<T> predicate) {
		ArrayLst<T> itens = filter(predicate);
		if (itens.isEmpty()) {
			return null;
		} else if (itens.size() > 1) {
			throw new Error("O filtro retornou + de 1 resultado");
		} else {
			return itens.get(0);
		}
	}

	public ArrayLst<T> sort(FTTT<Integer, T, T> comparator) {
		valueArray.sort(comparator);
		return this;
	}

	public T pop() {
		return removeLast();
	}
	public T shift() {
		return removeFirst();
	}

	@Override
	public String toString() {
		/* um array nao possui o metodo toJSON em js */
		if (isEmpty()) {
			return "[]";
		} else {
			return "["+valueArray.join(", ")+"]";
		}
	}

	public T byId(Integer id, FTT<Integer, T> getId) {
		return unique(o -> IntegerCompare.eq(getId.call(o), id));
	}

	public boolean existsId(Integer id, FTT<Integer, T> getId) {
		return !Null.is(byId(id, getId));
	}

	public ArrayLst<T> push(T o) {
		return add(o);
	}

	public <TT> ArrayLst<TT> map(FTT<TT,T> func) {
		return new ArrayLst<>(valueArray.map(func));
	}

	public <TT> ArrayLst<TT> mapi(FTTT<TT,T, Integer> func) {
		return new ArrayLst<>(valueArray.map(func));
	}

	public boolean addIfNotContains(T o) {
		if (Null.is(o) || contains(o)) {
			return false;
		} else {
			add(o);
			return true;
		}
	}

	public Array<T> getArray() {
		return copy().valueArray;
	}

	public T getSafe(int index) {
		if (isEmpty()) {
			return null;
		} else if (size() < index +1) {
			return null;
		} else {
			return get(index);
		}
	}

	public T getLast() {
		return get(size()-1);
	}

	public <RESULT> ArrayLst<RESULT> distinct(FTT<RESULT, T> func) {
		ArrayLst<RESULT> list = new ArrayLst<>();
		valueArray.forEach(o -> list.addIfNotContains(func.call(o)));
		return list;
	}

	@SuppressWarnings("unchecked") @SafeVarargs
	public static <X> ArrayLst<X> build(X... array) {
		ArrayLst<X> lst = new ArrayLst<X>();
		Array<?> ar = UNative.asArray(array);
		ar.forEach(a -> lst.add((X) a));
		return lst;
	}

}
