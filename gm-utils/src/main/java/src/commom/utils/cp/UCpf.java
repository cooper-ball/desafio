package src.commom.utils.cp;

import src.commom.utils.date.BaseData;
import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.integer.IntegerFormat;
import src.commom.utils.integer.IntegerParse;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringExtraiNumeros;
import src.commom.utils.string.StringLength;
import src.commom.utils.string.StringReplace;
import src.commom.utils.string.StringRight;

public class UCpf {

	public static void main(String[] args) {
		String s = "515";
		System.out.println(isValid(s));
	}

	public static String format(String s) {
		if (StringEmpty.is(s)) {
			return null;
		}
		s = StringExtraiNumeros.exec(s);
		int len = StringLength.get(s);
		if (len > 11) {
			s = s.substring(0, 11);
		} else {
			while (len < 11) {
				s = "0" + s;
			}
		}
		s = s.substring(0, 3) + "." + s.substring(3, 6) + "." + s.substring(6, 9) + "-" + s.substring(9);
		return s;
	}

	public static String formatParcial(String cpf) {
		cpf = StringExtraiNumeros.exec(cpf);
		if (cpf.length() >= 11) {
			return format(cpf);
		} else if (cpf.length() < 4) {
			return cpf;
		}
		String s = cpf.substring(0, 3) + ".";
		cpf = cpf.substring(3);

		if (cpf.length() < 4) {
			return s + cpf;
		}
		s += cpf.substring(0, 3) + ".";
		cpf = cpf.substring(3);

		if (cpf.length() < 4) {
			return s + cpf;
		}
		s += cpf.substring(0, 3) + "-";
		cpf = cpf.substring(3);
		if (cpf.length() > 2) {
			cpf = cpf.substring(0, 2);
		}
		s += cpf;
		return s;

	}

    public static boolean isValid(String cpf) {

    	if (StringEmpty.is(cpf)) {
			return false;
		}

		cpf = StringExtraiNumeros.exec(cpf);

    	if (!StringLength.is(cpf, 11)) {
			return false;
		}

    	for (int i = 0; i < 10; i++) {
			String s = i + "";
			s = StringReplace.exec(cpf, s, "");
			if (StringEmpty.is(s)) {
				return false;
			}
		}

        String numDig = cpf.substring(0, 9);
        String digitoInformado = cpf.substring(9, 11);
        String digitoCalculado = calcDigVerif(numDig);

        if (!StringCompare.eq(digitoCalculado, digitoInformado)) {
        	return false;
        }

        return true;
    }

	private static String calcDigVerif(String num) {

		int soma = 0;
		int peso = 10;
		for (int i = 0; i < num.length(); i++) {
        	soma += IntegerParse.toInt(num.substring(i, i + 1)) * peso--;
		}

		int primDig = 0;
		int resto = soma % 11;

		if (IntegerCompare.isZero(resto) || IntegerCompare.eq(resto, 1)) {
			primDig = 0;
		} else {
			primDig = IntegerParse.toInt(11 - resto);
		}

		soma = 0;
		peso = 11;

		for (int i = 0; i < num.length(); i++) {
        	soma += IntegerParse.toInt(num.substring(i, i + 1)) * peso--;
		}

		soma += primDig * 2;
		resto = soma % 11;

		int segDig = 0;
		if (IntegerCompare.isZero(resto) || IntegerCompare.eq(resto, 1)) {
			segDig = 0;
		} else {
			segDig = IntegerParse.toInt(11 - resto);
		}
		return "" + primDig + segDig;
	}

    public static String mock(int i) {

    	String s = IntegerFormat.xxx(i);
    	s = s + s + s;
    	int digito = 0;
    	while (!isValid(s + IntegerFormat.xx(digito))) {
    		digito++;
		}
    	return format(s + IntegerFormat.xx(digito));

    }

	private static int countAleatorios = 0;

    public static String aleatorio() {

    	long value = BaseData.now().getTime() + (countAleatorios++);

    	String s = "" + value;
    	s = StringRight.get(s, 9);
    	s += calcDigVerif(s);
    	return format(s);

    }

}
