package src.commom.utils.doubles;

import src.commom.utils.array.ArrayLst;
import src.commom.utils.integer.IntegerIs;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringContains;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringExtraiCaracteres;
import src.commom.utils.string.StringExtraiNumeros;
import src.commom.utils.string.StringParse;
import src.commom.utils.string.StringSplit;

public class DoubleIs {

	public static final ArrayLst<String> NUMEROS = StringExtraiNumeros.NUMEROS.concat2(".");

	public static boolean is(Object o) {

		if (IntegerIs.is(o)) {
			return true;
		}

		if (Null.is(o)) {
			return false;
		}

		String s = StringParse.get(o);

		if (StringEmpty.is(s)) {
			return false;
		}

		String n = StringExtraiCaracteres.exec(s, NUMEROS);

		if (!StringCompare.eq(s, n)) {
			return false;
		}

		String ints;
		String decs;

		if (StringContains.is(s, ".")) {

			if (s.startsWith(".")) {
				s = "0" + s;
			}

			ArrayLst<String> list = StringSplit.exec(s, ".");

			if (list.size() != 2) {
				return false;
			}

			ints = list.get(0);
			decs = list.get(1);

		} else {
			ints = s;
			decs = "0";
		}

		return IntegerIs.is(ints) && IntegerIs.is(decs);

	}

}
