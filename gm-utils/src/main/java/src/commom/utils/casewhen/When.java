package src.commom.utils.casewhen;

import gm.utils.lambda.FT;

public class When<T> {

	FT<Boolean> e;
	FT<T> r;
	When(FT<Boolean> e, FT<T> r) {
		this.e = e;
		this.r = r;
	}

}
