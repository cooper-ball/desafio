package src.commom.utils.object;

import gm.utils.lambda.FT;
import gm.utils.lambda.FTT;

public class Safe {

	public static <OUT,IN> OUT get(IN o, FTT<OUT,IN> getter, FT<OUT> other) {
		if (Null.is(o)) {
			return other.call();
		} else {
			return getter.call(o);
		}
	}

	public static <OUT,IN> OUT safeNullPointer(IN o, FTT<OUT,IN> getter, FT<OUT> other) {
		if (Null.is(o)) {
			return other.call();
		} else {
			try {
				return getter.call(o);
			} catch (Exception e) {
				return other.call();
			}
		}
	}

}
