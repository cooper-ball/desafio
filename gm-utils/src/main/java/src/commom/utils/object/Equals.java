package src.commom.utils.object;

public class Equals {
	public static boolean is(Object a, Object b) {
		return a == b || (Null.is(a) && Null.is(b));
	}
}