package src.commom.utils.object;

import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringReplace;

public abstract class ObjJs {

//	//@IgnoreStart
//	//metodo deprecado para garantir q nao serah chamado de um js
//	@Override @Deprecated @Ignorar
//	public final String toString() {
//		return toJSON();
//	}
//	//@IgnoreEnd

	public final String toJSON() {

		String s = toJsonImpl();

		if (StringEmpty.is(s)) {
			return null;
		}

		s = StringReplace.exec(s, " ,", ",");
		s = StringReplace.exec(s, ",,", ",");
		s = StringReplace.exec(s, " }", "}");
		s = StringReplace.exec(s, ",}", "}");
		s = StringReplace.exec(s, " ]", "]");
		s = StringReplace.exec(s, ",]", "]");

		return s;

	}

	protected abstract String toJsonImpl();

}
