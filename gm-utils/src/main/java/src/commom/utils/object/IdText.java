package src.commom.utils.object;

import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.string.StringEmpty;

public class IdText extends ObjJs {

	public Integer id;
	public String text;
	public String icon;

	public static final IdText VAZIO = new IdText(0, "");

//	@IgnoreStart
	public IdText() {
		this(null, null);
	}
//	@IgnoreEnd


	public IdText(Integer id, String text) {
		this.id = id;
		this.text = text;
	}

	public Integer getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public boolean eqId(Integer value) {
		return IntegerCompare.eq(id, value);
	}

	public boolean eq(IdText o) {
		if (Null.is(o)) {
			return false;
		} else {
			return eqId(o.id);
		}
	}

	@Override
	protected String toJsonImpl() {

		String s = "";

		if (!Null.is(id)) {
			s += ", \"id\": " + id;
		}

		if (!Null.is(text)) {
			s += ", \"text\": " + text;
		}

		if (!Null.is(icon)) {
			s += ", \"icon\": " + icon;
		}

		if (StringEmpty.notIs(s)) {
			s = s.substring(2);
		}

		return "{"+s+"}";

	}

}
