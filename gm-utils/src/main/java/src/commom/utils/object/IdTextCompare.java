package src.commom.utils.object;

import src.commom.utils.integer.IntegerCompare;

public class IdTextCompare {

	public static boolean isNull(IdText o) {
		return Null.is(o) || Null.is(o.id);
	}

	public static boolean eq(IdText a, IdText b) {

		if (isNull(a)) {
			return isNull(b);
		} else if (isNull(b)) {
			return false;
		} else {
			return IntegerCompare.eq(a.id, b.id);
		}

	}

}
