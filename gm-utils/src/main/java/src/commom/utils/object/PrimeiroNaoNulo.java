package src.commom.utils.object;

import gm.utils.lambda.FT;

public class PrimeiroNaoNulo {

	@SafeVarargs
	public static <T> T get(FT<T>... calls) {
		for (FT<T> ft : calls) {
			T o = ft.call();
			if (!Null.is(o)) {
				return o;
			}
		}
		return null;
	}

}
