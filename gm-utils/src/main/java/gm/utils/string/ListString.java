package gm.utils.string;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import gm.utils.abstrato.Lista;
import gm.utils.abstrato.SimpleIdObject;
import gm.utils.classes.UClass;
import gm.utils.comum.Lst;
import gm.utils.comum.SO;
import gm.utils.comum.UAssert;
import gm.utils.comum.UConstantes;
import gm.utils.comum.UHtml;
import gm.utils.comum.ULog;
import gm.utils.exception.UException;
import gm.utils.files.UFile;
import gm.utils.lambda.FTT;
import gm.utils.lambda.FVoidTT;
import gm.utils.number.ListInteger;
import gm.utils.number.UInteger;
import gm.utils.reflection.Atributo;
import gm.utils.reflection.Atributos;
import gm.utils.reflection.ListAtributos;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import js.support.console;
import lombok.Getter;
import lombok.Setter;
import src.commom.utils.string.StringAfterFirst;
import src.commom.utils.string.StringBeforeFirst;
import src.commom.utils.string.StringBeforeLast;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringContains;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringParse;
import src.commom.utils.string.StringPrimeiraMaiuscula;
import src.commom.utils.string.StringRight;
import src.commom.utils.string.StringTrim;

@Getter @Setter
public class ListString extends Lista<String> {

	private CharSet charSet = CharSet.UTF8;
	private boolean printOnAdd = false;
	private boolean autoIdentacao = false;

	public ListString() {}

	private Margem margem = new Margem();

	public ListString(Object... os) {
		for (Object o : os) {
			this.add(StringParse.get(o));
		}
	}

	public ListString(String... list) {
		for (String s : list) {
			this.add(s);
		}
	}
	public ListString(List<String> list) {
		this.addAll(list);
	}

	private static final long serialVersionUID = 1;

	private String fileName;

	public static ListString newFromArray(String... strings) {
		ListString list = new ListString();
		for (String s : strings) {
			list.add(s);
		}
		return list;
	}
	public static ListString array(String... strings) {
		return ListString.newFromArray(strings);
	}
	public static ListString fromFile(File file) {
		return ListString.fromFile(file.toString());
	}
	public static ListString fromFile(String file) {
		ListString list = new ListString();
		list.load(file);
		return list;
	}
	public static ListString fromFileUTF8(String file) {
		ListString list = new ListString();
		list.loadUTF8(file);
		return list;
	}
	public static ListString fromFileISO88591(String file) {
		ListString list = new ListString();
		list.loadISO88591(file);
		return list;
	}
	public static ListString newListString(ListString... lists) {
		ListString list = new ListString();
		for (ListString l : lists) {
			list.add(l);
		}
		return list;
	}
	public static ListString newListString(Enumeration<String> list) {
		ListString result = new ListString();
		while (list.hasMoreElements()) {
			result.add(list.nextElement());
		}
		return result;
	}
	public ListString addArray(String... list) {
		for (String string : list) {
			this.add(string);
		}
		return this;
	}

	private List<FTT<String, String>> filters = new ArrayList<>();

	public void addFilter(FTT<String, String> filter) {
		filters.add(filter);
	}

	@Override
	public boolean add(String s) {

		if (s != null && StringContains.is(s, "{\"array\":")) {
			throw new RuntimeException();
		}

		if (s != null && StringContains.is(s, "\n") && !s.contentEquals("\n")) {
			ListString split = ListString.split(s, "\n");
			for (String ss : split) {
				this.add(ss);
			}
			return true;
		}

		for (FTT<String, String> f : filters) {
			s = f.call(s);
			if (s == null) {
				return false;
			}
		}

		if (this.autoIdentacao) {
			return this.addComAutoIdentacao(s);
		} else {
			return this.addSemAutoIdentacao(s);
		}

	}

	private boolean addComAutoIdentacao(String s) {

		s = StringTrim.plus(s);

		while (s.endsWith(";;")) {
			s = StringRight.ignore1(s);
		}

		if (s.startsWith("}") || s.startsWith(")")) {
			while (StringEmpty.is(this.getLast())) {
				this.removeLast();
			}
			String last = this.getLast();
			if (last.endsWith(",")) {
				this.removeLast();
				last = StringRight.ignore1(last).trim();
				this.addSemAutoIdentacao(last);
			}
			this.getMargem().dec();
		}
		boolean result = this.addSemAutoIdentacao(s);
		if (StringContains.is(s, "//")) {
			s = StringBeforeFirst.get(s, "//");
		}
		if (s.endsWith("{") || s.endsWith("(")) {
			this.getMargem().inc();
//		} else if (StringContains.is(s, "(") && !StringAfterLast.get(s, "(").contains(")")) {
//			this.getMargem().inc();
		}
		return result;
	}

	private boolean addSemAutoIdentacao(String s) {

		s = this.margem + s;
		if (!this.isAceitaRepetidos()) {
			if (this.contains(s)) {
				return false;
			}
		}
		if (this.printOnAdd) {
			console.log(s);
		}

		return super.add(s);

	}

	@Override
	public void add(int index, String element) {
		super.add(index, this.margem + element);
	}
	public boolean add(String... list) {
		boolean b = false;
		for (String string : list) {
			b = this.add(string) || b;
		}
		return b;
	}
	public ListString loadUTF8() {
		this.loadUTF8("c:\\temp\\x.txt");
		return this;
	}
	public ListString loadISO88591() {
		this.loadISO88591("c:\\temp\\x.txt");
		return this;
	}
	public ListString load() {
		this.load("c:\\temp\\x.txt");
		return this;
	}
	public ListString loadUTF8(String file) {
		this.load(file, CharSet.UTF8);
		return this;
	}
	public ListString loadISO88591(String file) {
		this.load(file, CharSet.ISO88591);
		return this;
	}
	public ListString loadISO88591(File file) {
		return this.loadISO88591(file.toString());
	}
	public ListString load(String file) {
		this.load(file, this.getCharSet());
		return this;
	}
	public ListString load(File file) {
		return this.load(file.toString());
	}
	public ListString load(File file, CharSet charSet) {
		return this.load(file.toString(), charSet);
	}

	public ListString load(String file, CharSet charSet, FTT<String, String> filterAdd) {
		if (filterAdd != null) {
			this.filters.add(filterAdd);
		}
		ListString list = this.load(file.toString(), charSet);
		this.filters.remove(filterAdd);
		return list;
	}

	public ListString load(String file, CharSet charSet) {

		this.fileName = file;

		try {
			if (file.startsWith("smb://")) {
				String domain = System.getProperty("smb-domain");
				String username = System.getProperty("smb-username");
				String password = System.getProperty("smb-password");
//@gm-utils     final NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("cooperforte", "gamarra", "@m03386551157");
				NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(domain, username, password);
				SmbFile smbFile = new SmbFile(file, auth);
				return this.load(smbFile, charSet);
			} else {
				return this.load(new FileInputStream(file), charSet);
			}
		} catch (Exception e) {
			throw UException.runtime(e);
		}
	}

	public ListString load(SmbFile file) {
		return this.load(file, CharSet.UTF8);
	}

	public ListString load(SmbFile file, CharSet charSet) {
		try {
			return this.load(file.getInputStream(), charSet);
		} catch (Exception e) {
			throw UException.runtime(e);
		}
	}

//	String detectCharSet(InputStream is){
//
//		try {
//			byte[] buf = new byte[4096];
//			UniversalDetector detector = new UniversalDetector(null);
//			int nread;
//			while ((nread = is.read(buf)) > 0 && !detector.isDone()) {
//			  detector.handleData(buf, 0, nread);
//			}
//			detector.dataEnd();
//			String encoding = detector.getDetectedCharset();
//			detector.reset();
//			return encoding;
//
//		} catch (Exception e) {
//			U.printStackTrace(e);
//			return null;
//		}
//	}

	public ListString load(InputStream is, CharSet charSet) {

		try {

			UAssert.notEmpty(is, "is == null");

			this.setCharSet(charSet);

			InputStreamReader in;

			if ( charSet == null ) {
				in = new InputStreamReader(is);
			} else {
				in = new InputStreamReader(is, charSet.getNome());
			}

			UAssert.notEmpty(in, "in == null");

			BufferedReader buffer = new BufferedReader(in);

			String linha = buffer.readLine();
			while (linha != null) {
				this.add(linha);
				linha = buffer.readLine();
			}
			buffer.close();
			return this;
		} catch (Exception e) {

			if (e instanceof java.io.UnsupportedEncodingException) {

				if (charSet == null) {
					this.load(is, CharSet.UTF8);
				} else if (charSet == CharSet.UTF8) {
					this.load(is, CharSet.ISO88591);
				}
				return this;

			}

			throw UException.runtime(e);

		}
	}
	public boolean has(){
		return !this.isEmpty();
	}
	public ListString print() {
		if (this.isEmpty()) {
			return this;
		}
		ULog.debug(this.toString("\n"));
		return this;
	}
	public ListString sort_considerando_case() {
		this.sort((a, b) -> {
			a = StringNormalizer.get(a);
			b = StringNormalizer.get(b);
			return a.compareTo(b);
		});
		return this;
	}
	public void inverteOrdem() {
		ListString list = this.copy();
		this.clear();
		for (String s : list) {
			this.add(0, s);
		}
	}
	public ListString sort() {
		this.sort((a, b) -> {
			a = StringNormalizer.get(a).toLowerCase();
			b = StringNormalizer.get(b).toLowerCase();
			return a.compareTo(b);
		});
		return this;
	}
	public ListString mergeBefore(String s) {
		ListString x = new ListString();
		for (String string : this) {
			x.add(s + string);
		}
		this.clear();
		this.addAll(x);
		return this;
	}
	public ListString mergeAfter(String s) {
		ListString x = new ListString();
		for (String string : this) {
			x.add(string + s);
		}
		this.clear();
		this.addAll(x);
		return this;
	}
	public ListString add() {
		this.add("");
		return this;
	}
	public ListString trataCaracteresEspeciais() {
		return this;
	}
	public ListString replace(int char1, int char2, String s) {
		Character c1 = Character.toChars(195)[0];
		Character c2 = Character.toChars(65533)[0];
		String a = "" + c1 + c2;
		this.replaceTexto(a, s);
		return this;
	}
	public ListString replace(String a, Object b) {
		this.replace(a, b.toString());
		return this;
	}
	public ListString replace(String a, String b) {
		if (a.equals(b)) {
			return this;
		}
		while (this.contains(a)) {
			int i = this.indexOf(a);
			this.remove(i);
			this.add(i, b);
		}
		return this;
	}
	public ListString replaceTexto(String a, String b) {
		ListString x = new ListString();
		for (String string : this) {
			if (StringEmpty.is(string)) {
				x.add();
				continue;
			}
			String s = string.replace(a, b);
			if (StringEmpty.is(s)) {
				x.add();
				continue;
			}
			x.add(s);
		}
		this.clear();
		this.addAll(x);
		return this;
	}

	public boolean eq(ListString list){
		if (super.equals(list)) {
			return true;
		}
		if (list.toString("").equals(this.toString(""))) {
			return true;
		}
		return false;
	}

	public boolean save(File diretorio, String fileName) {
		return this.save(diretorio.toString() + "/" + fileName);
	}
	public boolean save() {
		if (StringEmpty.is(this.fileName)) {
			throw UException.runtime("fileName == null");
		}
		return this.save(this.fileName);
	}
	public boolean save(String fileName) {
		return this.salvar(fileName);
	}
	public boolean salvar(String fileName) {

//		UConfig.checaStop();

		if (SO.windows()) {
			fileName = fileName.replace("\\", "/");
		}

		if (fileName.contains(" ")) {
			throw new RuntimeException("Nao coloque espacos em nomes de arquivos: " + fileName);
		}

		String pasta = StringBeforeLast.get(fileName, "/");
		if (pasta != null && !UFile.exists(pasta)) {
			console.log("Criando diretório: " + pasta);
			new File(pasta).mkdirs();
		}

		boolean exists = UFile.exists(fileName);

		if (exists) {
			ListString list = new ListString();
			list.load(fileName);
			//se for igual nao precisa salvar de novo
			if (list.eq(this)) {
				return false;
			}
		}

		if ( fileName.contains("null.war") ) {
			throw UException.runtime("Gravando no lugar errado");
		}

		if (exists) {
			console.log("Gravando: " + fileName + " (replace)");
		} else {
			console.log("Gravando: " + fileName + " (new)");
		}

		try {
			try (FileOutputStream fos = new FileOutputStream(fileName)) {
				if (getCharSet() == null) {
					try (OutputStreamWriter osw = new OutputStreamWriter(fos)) {
						save(osw);
					}
				} else {
					try (OutputStreamWriter osw = new OutputStreamWriter(fos, getCharSet().getNome())) {
						save(osw);
					}
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return true;
	}

	private void save(OutputStreamWriter osw) {
		try (BufferedWriter out = new BufferedWriter(osw)) {
			for (String s : this) {
				out.write(StringTrim.right(s) + "\n");
			}
		} catch (Exception e) {
			throw UException.runtime(e);
		}
	}

	public static ListString split(String s, String delimiter) {
		return ListString.byDelimiter(s, delimiter);
	}
	public static ListString byDelimiter(String s, String... delimiters) {

		ListString list = new ListString();
		if (StringEmpty.is(s)) {
			return list;
		}
		while (UString.contains(s, delimiters)) {
			for (String delimiter : delimiters) {
				if (delimiter != null && !"".equals(delimiter)) {
					while (StringContains.is(s, delimiter)) {
						String x = s.substring(0, s.indexOf(delimiter));
						s = s.substring(x.length() + delimiter.length(), s.length());
						list.add(x);
					}
				}
			}
		}
		if (!StringEmpty.is(s)) {
			list.add(s);
		}
		list.removeLastEmptys();
		return list;
	}
	public ListString add(String before, ListString list) {
		for (String s : list) {
			this.add(before + s);
		}
		return this;
	}
	public ListString add(ListString list) {
		this.addAll(list);
		return this;
	}
	public Integer getInt(int index) {
		return UInteger.toInt(this.get(index));
	}

	@Override
	public ListString filter(Predicate<String> predicate) {
		return new ListString(super.filter(predicate));
	}

	@Override
	public ListString removeAndGet(Predicate<String> predicate) {
		return new ListString(super.removeAndGet(predicate));
	}

	public ListString removeIfTrimStartsWith(String s) {
		Predicate<String> p = x -> {
			if (x == null) {
				return s == null;
			}
			return x.trim().startsWith(s.trim());
		};
		this.removeIf(p);
		return this;
	}
	public ListString removeIfStartsWith(String s) {
		Predicate<String> p = x -> {
			if (x == null) {
				return false;
			}
			return x.startsWith(s);
		};
		this.removeIf(p);
		return this;
	}

	public ListString removeIfNotStartsWith(ListString itens) {

		Predicate<String> p = x -> {
			if (x == null) {
				return true;
			}
			for (String s : itens) {
				if (x.startsWith(s)) {
					return false;
				}
			}
			return true;
		};
		this.removeIf(p);
		return this;

	}

	public ListString removeIfNotEndsWith(ListString itens) {

		Predicate<String> p = x -> {
			if (x == null) {
				return true;
			}
			for (String s : itens) {
				if (x.endsWith(s)) {
					return false;
				}
			}
			return true;
		};
		this.removeIf(p);
		return this;

	}

	public ListString removeIfNotStartsWith(String... list) {
		return this.removeIfNotStartsWith( ListString.array(list) );
	}
	public ListString removeIfNotEndsWith(String... list) {
		return this.removeIfNotEndsWith( ListString.array(list) );
	}
	public ListString removeIfTrimEquals(String s) {
		Predicate<String> p = x -> {
			if (x == null) {
				return s == null;
			}
			return x.trim().equals(s);
		};
		this.removeIf(p);
		return this;
	}
	public ListString removeFisrtEmptys() {
		while (!this.isEmpty() && StringEmpty.is(this.get(0))) {
			this.remove(0);
		}
		return this;
	}
	public ListString removeLastEmptys() {
		while (!this.isEmpty() && StringEmpty.is(this.getLast())) {
			this.removeLast();
		}
		return this;
	}
	public ListString removeIfEquals(String s) {
		Predicate<String> p = x -> {
			if (x == null) {
				return s == null;
			}
			return x.equals(s);
		};
		this.removeIf(p);
		return this;
	}
	@Override
	public boolean contains(Object o) {
		if (o instanceof String) {
			String s = (String) o;
			return this.contains(s);
		} else {
			return this.contains( StringParse.get(o) );
		}
	}

	public boolean containsAny(String... strings) {
		for (String s : strings) {
			if (this.contains(s)) {
				return true;
			}
		}
		return false;
	}

//	nao remover
	public boolean contains(String arg0) {
		return super.contains(arg0);
	}
	public ListString removeWhites() {
		while (this.contains("")) {
			this.remove("");
		}
		return this;
	}
	public ListString addLeft(String s) {
		ListString list = new ListString();
		for (String linha : this) {
			list.add(s + linha);
		}
		this.clear();
		this.addAll(list);
		return this;
	}
	public ListString addRight(String s) {
		ListString list = new ListString();
		for (String linha : this) {
			list.add(linha + s);
		}
		this.clear();
		this.addAll(list);
		return this;
	}
	public ListString removeLeft(int length) {
		ListString list = new ListString();
		for (String s : this) {
			if (s.length() <= length) {
				list.add();
			} else {
				list.add(s.substring(length));
			}
		}
		this.clear();
		this.addAll(list);
		return this;
	}
	public ListString removeRight(int length) {
		ListString list = new ListString();
		for (String s : this) {
			if (s.length() <= length) {
				list.add();
			} else {
				list.add(StringRight.ignore(s, length));
			}
		}
		this.clear();
		this.addAll(list);
		return this;
	}

	public ListString removeLast(int quantidade) {
		return (ListString) super.removeLast_(quantidade);
	}

	public ListString remove(int index, int quantidade) {
		return (ListString) super.remove_(index, quantidade);
	}
	public ListString add(List<?> list) {
		for (Object o : list) {
			this.add(o.toString());
		}
		return this;
	}
	public ListString trimPlus() {
		Lista<String> lista = this.copy();
		this.clear();
		for (String s : lista) {
			s = StringTrim.plus(s);
			if (!StringEmpty.is(s)) {
				this.add(s);
			}
		}
		this.removeWhites();
		return this;
	}
	public ListString trim() {
		Lista<String> lista = this.copy();
		this.clear();
		for (String s : lista) {
			this.add(s.trim());
		}
		return this;
	}
	public ListString rtrim() {
		Lista<String> lista = this.copy();
		this.clear();
		for (String s : lista) {
			this.add(StringTrim.right(s));
		}
		return this;
	}
	public static ListString loadResource(Class<?> classe) {
		return ListString.loadResourceByExtensao(classe, "txt");
	}
	public static ListString loadResourceByExtensao(Class<?> classe, String extensao) {
		return ListString.loadResource(classe, classe.getSimpleName() + "." + extensao);
	}
	public static ListString loadResource(Class<?> classe, String fileName) {
		return ListString.fromFile(ListString.s_resource(classe, fileName));
	}
	public static ListString loadResourceUTF8(Class<?> classe, String fileName) {
		return ListString.fromFileUTF8(ListString.s_resource(classe, fileName));
	}
	public static ListString loadResourceISO88591(Class<?> classe, String fileName) {
		return ListString.fromFileISO88591(ListString.s_resource(classe, fileName));
	}
	static String s_resource(Class<?> classe, String fileName) {
		if (fileName.startsWith("+")) {
			fileName = fileName.replace("+", classe.getSimpleName());
		}
		URL resource = classe.getResource(fileName);
		if (resource == null) {
			throw UException.runtime("resource == null : " + fileName);
		}
		String s = resource.toString();
		s = s.replace("file:/", "");
		s = s.replace("vfs:/", "");
		s = s.replace("/", "\\");
		return s;
	}
	public static ListString nova(String[] list) {
		ListString l = new ListString();
		l.addArray(list);
		return l;
	}
	public static ListString separaPalavras(String s) {

//		long start = Date.now();

		try {

			ListString simbolos = UConstantes.SIMBOLOS.copy();
			simbolos.remove("_");
			simbolos.remove("$");
			simbolos.remove("@");

			ListString list = new ListString();
			String x = "";
			while (!s.isEmpty()) {
				String caracter = s.substring(0, 1);
				s = s.substring(1);
				if (simbolos.contains(caracter)) {
					if (!"".equals(x)) {
						list.add(x);
					}
					list.add(caracter);
					x = "";
				} else {
					x += caracter;
				}
			}
			if (!"".equals(x)) {
				list.add(x);
			}
			return list;

		} finally {
//			Tempo.exec(start);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public ListString copy() {
		ListString list = super.copy();
		list.autoIdentacao = autoIdentacao;
		list.getMargem().set(getMargem().get());
		return list;
	}
	public ListString menos(ListString list) {
		ListString copy = this.copy();
		for (String string : list) {
			copy.remove(string);
		}
		return copy;
	}
	public static ListString loadClass(Class<?> classe) {
		String s = UClass.javaFileName(classe);
		if (s == null) {
			throw UException.runtime("ListString - JavaFile nao encontrado: " + classe.getSimpleName());
		}
		File file = new File(s);
		return new ListString().load(file);
	}
	public ListString juntarComASuperiorSe(FTT<Boolean, String> predicate, String separador) {
		return juntarComASuperiorSe(predicate, separador, "");
	}
	public ListString juntarComASuperiorSe(FTT<Boolean, String> predicate, String separador, String after) {
		ListString list = new ListString();
		for (String linha : this) {
			if (!list.isEmpty() && predicate.call(linha)) {
				linha = list.removeLast() + separador + linha.trim() + after;
			}
			list.add(linha);
		}
		this.clear();
		this.add(list);
		return this;
	}
	public ListString juntarComASuperiorSeEquals(String s, String separador, String after) {
		return juntarComASuperiorSe(linha -> StringCompare.eq(s, linha), separador, after);
	}
	public ListString juntarComASuperiorSeEquals(String s, String separador) {
		return juntarComASuperiorSe(linha -> StringCompare.eq(s, linha), separador);
	}
	public ListString juntarComASuperiorSeEquals(String s) {
		return juntarComASuperiorSe(linha -> StringCompare.eq(s, linha), "");
	}
	public ListString juntarComASuperiorSeTrimStartarCom(String prefix) {
		return this.juntarComASuperiorSeTrimStartarCom(prefix, "");
	}
	public ListString juntarComASuperiorSeTrimStartarCom(String prefix, String separador) {
		return juntarComASuperiorSe(s -> s.trim().startsWith(prefix), separador);
	}

	public ListString juntarComAProximaSeTrimTerminarCom(String prefix) {
		return this.juntarComAProximaSeTrimTerminarCom(prefix, "");
	}
	public ListString juntarComAProximaSe(FTT<Boolean, String> predicate, String separador, String after) {
		ListString list = new ListString();
		boolean juntar = false;
		for (String linha : this) {
			if (juntar) {
				linha = StringTrim.right(list.removeLast()) + separador + linha.trim() + after;
				juntar = false;
			} else if (predicate.call(linha)) {
				juntar = true;
			}
			list.add(linha);
		}
		this.clear();
		this.add(list);
		return this;
	}
	public ListString juntarComAProximaSe(FTT<Boolean, String> predicate, String separador) {
		return juntarComAProximaSe(predicate, separador, "");
	}
	public ListString juntarComAProximaSeEquals(String s, String separador) {
		return juntarComAProximaSe(linha -> StringCompare.eq(s, linha), separador);
	}
	public ListString juntarComAProximaSeEquals(String s) {
		return juntarComAProximaSe(linha -> StringCompare.eq(s, linha), "");
	}
	public ListString juntarComAProximaSeTrimTerminarCom(String prefix, String separador) {
		return juntarComAProximaSe(s -> s.trim().endsWith(prefix), separador);
	}
	public ListString add(Throwable e) {
		this.add(e.getClass().getName());
		StackTraceElement[] stack = e.getStackTrace();
		for (StackTraceElement o : stack) {
			this.add(o.toString());
		}
		return this;
	}
	public ListString loadResource(String fileName) {
		return this.loadResource(fileName, this.getCharSet());
	}
	public ListString loadResource(String fileName, CharSet charSet) {

		ClassLoader classLoader = this.getClass().getClassLoader();
		InputStream is = classLoader.getResourceAsStream(fileName);

		if (is == null) {
			throw UException.runtime("is is null: " + fileName);
		}

		this.load(is, charSet);
//		File file = new File(classLoader.getResource(fileName).getFile());
//		load(file, charSet);

//		ServletContext x = ServletActionContext.getServletContext();
//		String realPath = x.getRealPath("");
//		String realFileName = realPath + "/" + fileName;
//		load(realFileName);
		return this;
	}
	public ListString toStringSqlDeclaration() {
		ListString list = new ListString();
		list.add("static final String sql = \"\"");

		for (String s : this) {

			if (StringEmpty.is(s)) {
				list.add();
				continue;
			}
			s = StringTrim.right(s);
			if (StringContains.is(s, "--")) {
				s = s.replace("--", "/*");
				s += "*/";
			}

			s = s.replace("\t", "  ");

			s = "\t+ \" " + s;
			s += "\"";

			list.add(s);
		}

		list.add(";");
		return list;
	}
//	public static ListString clipboard(){
//		ListString list = new ListString();
//		list.add(UString.clipboard().split("\n"));
//		return list;
//	}
//	public void toClipboard(){
//		UString.clipboard(this.toString("\n"));
//	}
	public void save(Class<?> classe){
		this.save(UClass.javaFileName(classe));
	}
	public static ListString loadClassText(Class<?> classe){
  		return new ListString().load( UClass.javaFileName(classe) );
	}
	public ListString removeIfStartsWithAndEndsWith(String a, String b) {

		Predicate<String> p = x -> x.startsWith(a) && x.endsWith(b);
		this.removeIf(p);
		return this;

	}
	public ListString removeIfNotContains(String s) {

		Predicate<String> p = x -> {
			if (x == null) {
				return s == null;
			}
			return !x.contains(s);
		};
		this.removeIf(p);
		return this;

	}
	public ListString removeIfContains(String s) {
		String s2 = UHtml.replaceSpecialChars(s);
		Predicate<String> p = x -> {
			if (x == null) {
				return s == null;
			}
			return x.contains(s) || x.contains(s2);
		};
		return this.remove(p);
	}

	private ListString remove(Predicate<String> predicate) {
		ListString list = new ListString();
		for (String s : this) {
			if ( predicate.test(s) ) {
				list.add(s);
			}
		}
		for (String s : list) {
			this.remove(s);
		}
		return list;
	}

	public void save(File file) {
		this.save(file.toString());
	}

	private String lastRemoved;

	@Override
	public String remove(int index) {
		this.lastRemoved = super.remove(index);
		return this.lastRemoved;
	}

	static void testeRemoveTextEntre(){

		ListString list = new ListString();
		list.add("teste a");
		list.add("teste b");
		list.add("teste c");
		list.add("<nome>na mesma linha abc");
		list.add("linha abc</nome>");
		list.add("teste x");
		list.add("teste y");
		list.add("teste z");

		ListString result = list.removeTextEntre("<nome>", "</nome>");
		list.print();
		ULog.debug("=======");
		result.print();

	}

	public ListString removeTextEntre(String inicio, String fim) {

		ListString result = new ListString();
		ListString l = new ListString();

		int inicios = 0;

		while ( this.has() ) {

			String s = this.remove(0);

			if (StringContains.is(s, inicio)) {
				String x = StringBeforeFirst.get(s, inicio);
				s = StringAfterFirst.get(s, inicio);
				if (!StringEmpty.is(x)) {
					l.add(x);
				}
				inicios++;
				if (StringEmpty.is(s)) {
					continue;
				}
			}

			if ( inicios == 0 ) {
				l.add(s);
				continue;
			}

			if (!StringContains.is(s, fim)) {
				result.add(s);
				continue;
			}

			String x = StringAfterFirst.get(s, fim);
			s = StringBeforeFirst.get(s, fim);
			if (!StringEmpty.is(x)) {
				l.add(x);
			}

			if (!StringEmpty.is(s) && inicios > 0) {
				result.add(s);
			}

			inicios--;

			if (inicios == 0) {
				l.addAll( this );
				this.clear();
				break;
			}

		}

		this.addAll(l);
		return result;

	}

	private static ListInteger invalids = new ListInteger(65533);

	public static ListString load_e_escolhe_qual_por_encoding(File file){
		return ListString.load_e_escolhe_qual_encoding(file.toString());
	}
	public static ListString load_e_escolhe_qual_encoding(String file){

		ListString a = new ListString();
		a.loadUTF8(file);

		ListString b = new ListString();
		b.loadISO88591(file);

		for (String s : a) {
			while (!s.isEmpty()) {
				int c = s.charAt(0);
				if (ListString.invalids.contains(c)) {
//					ULog.debug("iso");
					return b;
				}
//				ULog.debug(c);
				s = s.substring(1);
			}
		}

//		ULog.debug("utf");
		return a;

	}

	public ListString eachRemoveBefore(String string, boolean paramTo) {

		ListString list = new ListString();

		for (String s : this) {

			if (StringContains.is(s, string)) {
				s = StringAfterFirst.get(s, string);
				if (!paramTo) {
					s = string + s;
				}
			}

			list.add(s);

		}

		this.clear();
		this.addAll(list);
		return this;

	}
	public ListString eachRemoveAfter(String string, boolean paramTo) {

		ListString list = new ListString();

		for (String s : this) {

			if (StringContains.is(s, string)) {
				s = StringBeforeFirst.get(s, string);
				if (!paramTo) {
					s += string;
				}
			}

			list.add(s);

		}

		this.clear();
		this.addAll(list);
		return this;

	}
	public boolean size(int i) {
		return this.size() == i;
	}

	public void addObject(Object o) {
		this.add(o.toString());
	}

	public InputStream getFileStream() {

		return null;
	}
	public String toSQL() {
		this.replaceTexto("'", "''");
		return this.toString("");
	}
	public static <T> List<T> loadResource(Class<?> from, String file, String delimiter, Class<T> classe){
		ListString list = ListString.loadResource(from, file);
		return ListString.load(list, delimiter, classe);
	}
	public static <T> List<T> load(String file, String delimiter, Class<T> classe){
		ListString list = new ListString();
		list = list.load(file);
		return ListString.load(list, delimiter, classe);
	}
	public static <T> List<T> load(ListString list, String delimiter, Class<T> classe){

		List<T> result = new ArrayList<>();

		Atributos as = ListAtributos.get(classe);

		for( final String s : list ) {
			T o = UClass.newInstance(classe);
			result.add(o);

			ListString values = ListString.byDelimiter(s, delimiter);

			for (Atributo a : as) {
				a.set(o, values.remove(0));
			}

		}

		return result;

	}
	private static String getTempFile() {
		if (SO.windows()) {
			return "c:/temp/x.txt";
		} else {
			return "/tmp/x.txt";
		}
	}
	private static String getTempFile(int index) {
		if (SO.windows()) {
			return "c:/temp/x"+index+".txt";
		} else {
			return "/tmp/x"+index+".txt";
		}

	}
	public void loadTemp() {
		this.load( ListString.getTempFile() );
	}
	public void saveTemp() {
		this.save( ListString.getTempFile() );
	}
	public void saveTemp(int index) {
		this.save( ListString.getTempFile(index) );
	}
	public ListString load(Map<?,?> map){
		for (Object key : map.keySet()) {
			this.add( StringParse.get(key) + " = " + StringParse.get( map.get(key) ) );
		}
		return this;
	}
	public void quebraPor(String delimiter, boolean antes) {
		ListString list = this.copy();
		this.clear();
		for (String s : list) {
			while (StringContains.is(s, delimiter)) {
				String x = StringBeforeFirst.get(s, delimiter);
				if (antes) {
					x += delimiter;
					this.add(x);
				} else {
					this.add(x);
					this.add(delimiter);
				}
				s = StringAfterFirst.get(s, delimiter).trim();
			}
			if (!StringEmpty.is(s)) {
				this.add(s);
			}
		}

		if (!antes) {
			list = this.copy();
			this.clear();
			String s = "";
			while (!list.isEmpty()) {
				s += list.remove(0);
				if (s.equals(delimiter)) {
					continue;
				}
				this.add(s);
				s = "";
			}
		}
	}
	public void removeHtml() {
		ListString list = this.copy();
		this.clear();
		for( String s : list ) {
			s = UHtml.removeAtributos(s);
			s = UHtml.removeHtml(s);
			this.add(s);
		}
	}
	public void toHtml(){
		ListString list = this.copy();
		this.clear();
		for( String s : list ) {
			s = UHtml.replaceSpecialChars(s);
			this.add(s);
		}
	}
	public List<SimpleIdObject> putIds() {
		List<SimpleIdObject> list = new ArrayList<>();
		int i = 1;
		for (String s : this) {
			list.add( new SimpleIdObject(i++, s) );
		}
		return list;
	}
	public ListString primeirasMaiusculas() {
		ListString x = new ListString();
		for (String s : this) {
			if (s == null) {
				continue;
			}
			s = StringPrimeiraMaiuscula.exec(s);
			x.add(s);
		}
		this.clear();
		this.addAll(x);
		return this;
	}
	public String join(String separador) {
		return this.toString(separador);
	}

	public int indexOfWithTrim(String s) {
		if (s == null) {
			throw new NullPointerException("s == null");
		}
		return this.copy().trim().indexOf(s.trim());
	}
	public int indexOfWithTrim(ListString list) {

		for (int i = 0; i < this.size()-list.size()+1; i++) {
			boolean igual = true;
			for (int j = 0; j < list.size(); j++) {
				String a = StringTrim.plus(this.get(i+j));
				String b = StringTrim.plus(list.get(j));
				if (StringCompare.eq(a, b)) {
					continue;
				}
				if (b.endsWith("(*?)")) {
					b = b.substring(0, b.length()-4);
					if (a.startsWith(b)) {
						continue;
					}
				}
				igual = false;
				break;
			}
			if (igual) {
				return i;
			}
		}

		return -1;
	}
	public void add(int index, ListString list){
		for(String s : list){
			this.add(index, s);
			index++;
		}
	}
	public int replace(ListString velho, String novo) {
		ListString list = new ListString();
		list.add(novo);
		return this.replace(velho, list);
	}
	private boolean replacePrivate(ListString velho, FTT<ListString, ListString> func) {
		int index = this.indexOfWithTrim(velho);
		if (index == -1) {
			return false;
		}
		int margens = 0;
		String s = this.get(index);
		while (s.startsWith("\t")) {
			s = s.substring(1);
			this.margemInc();
			margens++;
		}
		this.remove(velho);
		int m = this.margem.get();
		this.margem.set(margens);
		ListString novo = func.call(velho);
		this.add(index, novo);
		this.margem.set(m);
		return true;
	}
	public int replace(ListString velho, ListString novo) {
		return replace(velho, v -> novo);
	}
	public int replace(ListString velho, FTT<ListString, ListString> func) {
		int result = 0;
		while (this.replacePrivate(velho, func)) {
			result++;
		}
		return result;
	}
	public int remove(String... itens) {
		return this.remove(ListString.array(itens));
	}
	public int remove(ListString list) {
		int index = this.indexOfWithTrim(list);
		if (index == -1) {
			return -1;
		}
		for (int i = 0; i < list.size(); i++) {
			this.remove(index);
		}
		return index;
	}
	public int removeDoubleWhites(){
		ListString novo = new ListString();
		novo.add();
		ListString velho = new ListString();
		velho.add();
		velho.add();
		return this.replace(velho, novo);
	}
	@Override
	public ListString addIfNotContains(String s) {
		if (s != null) {
			super.addIfNotContains(s);
		}
		return this;
	}
	public static ListString newListString(Set<String> keySet) {
		ListString list = new ListString();
		for (String string : keySet) {
			list.add(string);
		}
		return list;
	}

	public void removeEmptys() {
		while (this.remove((String)null)) {

		}
		this.removeIfTrimEquals("");
	}

	public boolean moveToFirst(String s, boolean addIfNotContains) {
		boolean remove = this.remove(s);
		if (remove || addIfNotContains) {
			this.add(0, s);
		}
		return remove;
	}
	public boolean moveToLast(String s, boolean addIfNotContains) {
		boolean remove = this.remove(s);
		if (remove || addIfNotContains) {
			this.add(s);
		}
		return remove;
	}
	public List<Map<String, Object>> asMap() {
		List<Map<String, Object>> maps = new ArrayList<>();
		int id = 1;
		for (String s : this) {
			HashMap<String, Object> map = new HashMap<>();
			map.put("id", id++);
			map.put("text", s);
			maps.add(map);
		}
		return maps;
	}

	public void removeRepetidos() {
		ListString copy = this.copy();
		this.clear();
		for (String s : copy) {
			this.addIfNotContains(s);
		}
	}

	public boolean containsIgnoreCase(String valor) {
		for (String s : this) {
			if ( s.equalsIgnoreCase(valor) ) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String[] toArray() {
		String[] list = new String[this.size()];
		for (int i = 0; i < this.size(); i++) {
			list[i] = this.get(i);
		}
		return list;
	}

	public void identarJava() {
		ListString list = this.copy().trim();
		this.clear();
		this.margem.set(0);
		for (String s : list) {
			if (s.startsWith("}")) {
				this.margem.dec();
			}
			this.add(s);
			if (s.endsWith("{")) {
				this.margem.inc();
			}
		}
	}

	public ListString union(ListString list) {
		return this.copy().add(list);
	}

	public static ListString convert(List<?> list) {
		ListString listString = new ListString();
		for (Object o : list) {
			String s = StringParse.get(o);
			if (s != null) {
				listString.add(s);
			}
		}
		return listString;
	}

	public static ListString byQuebra(String s) {
		s = s.replace("\r", "\n");
		return ListString.byDelimiter(s, "\n");
	}

	public static void main(String[] args) {
		ListString list = new ListString();
		list.load("/opt/desen/git/notec/associacao-digital-2019/src/app/misc/utils/StringBox.js");
		list.juntarFimComComecos("{", "}", "");
		list.print();
	}

	public void juntarFimComComecos(String fim, String comeco, String separador) {

		this.removeFisrtEmptys();
		this.removeLastEmptys();

		if (this.isEmpty()) {
			return;
		}

		ListString list2 = new ListString();

		boolean lastEndsWithFim = false;

		while (!this.isEmpty()) {

			String s = StringTrim.right(this.remove(0));

			if (StringEmpty.is(s)) {
				list2.add();
				continue;
			}

			if (!lastEndsWithFim) {
				list2.add(s);
				lastEndsWithFim = s.endsWith(fim);
				continue;
			}

			if (!s.trim().startsWith(comeco)) {
				list2.add(s);
				lastEndsWithFim = false;
				continue;
			}

			list2.removeLastEmptys();
			s = list2.removeLast() + separador + s.trim();
			list2.add(s);

			lastEndsWithFim = s.endsWith(fim);

		}

		this.add(list2);

	}

	public boolean first(String s, String... strings) {
		if (this.isEmpty()) {
			return false;
		} else {
			return UString.in(this.get(0), s, strings);
		}
	}

	public boolean endsWith(String s) {
		if (this.isEmpty()) {
			return false;
		} else {
			return this.getLast().equals(s);
		}
	}

	public boolean endsWith(String s, String... strings) {
		if (this.isEmpty()) {
			return false;
		} else {
			return UString.in(this.getLast(), s, strings);
		}
	}

	public static ListString loadFile(File file) {
		UFile.assertExists(file);
		ListString list = new ListString();
		list.load(file);
		return list;
	}

	public void replaceEach(FTT<String, String> func) {
		ListString list = this.copy();
		this.clear();
		for (String s : list) {
			s = func.call(s);
			this.add(s);
		}
	}

	public void forEach(FVoidTT<String, Integer> action) {
		for (int i = 0; i < this.size(); i++) {
			action.call(this.get(i), i);
		}
	}

	public int margemInc() {
		return this.getMargem().inc();
	}
	public int margemDec() {
		return this.getMargem().dec();
	}
	public ListString rm(String s) {
		this.remove(s);
		return this;
	}

	public String getFirst() {
		if (this.isEmpty()) {
			return null;
		} else {
			return this.get(0);
		}
	}

	public ListString mapString(FTT<String, String> func) {
		ListString list = new ListString();
		for (String s : this) {
			list.add(func.call(s));
		}
		return list;
	}

	public <T> Lst<T> map(FTT<T, String> func) {
		Lst<T> list = new Lst<>();
		for (String s : this) {
			list.add(func.call(s));
		}
		return list;
	}

	public static ListString readBytes(byte[] bytes) {
		try {
			return byQuebra(new String(bytes, "UTF-8"));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
