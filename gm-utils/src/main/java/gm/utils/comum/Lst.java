package gm.utils.comum;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import gm.utils.lambda.FTT;
import gm.utils.lambda.FTTT;
import gm.utils.string.ListString;
import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.longo.LongCompare;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import js.support.console;

public class Lst<T> implements List<T> {

	private final List<T> list;

	public Lst() {
		this.list = new ArrayList<>();
	}
	public Lst(List<T> list) {
		this.list = UList.distinct(list);
	}

	public Lst<T> filter(Predicate<T> predicate) {
		return new Lst<>(stream().filter(predicate).collect(Collectors.toList()));
	}

	@Override
	public int size() {
		return this.list.size();
	}

	@Override
	public boolean isEmpty() {
		return this.list.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return this.list.contains(o);
	}

	@Override
	public Iterator<T> iterator() {
		return this.list.iterator();
	}

	@Override
	public Object[] toArray() {
		return this.list.toArray();
	}

	@SuppressWarnings("hiding")
	@Override
	public <T> T[] toArray(T[] a) {
		return this.list.toArray(a);
	}

	@Override
	public boolean add(T e) {
		return this.list.add(e);
	}

	@Override
	public boolean remove(Object o) {
		return this.list.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return this.list.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		return this.list.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		return this.list.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return this.list.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return this.list.retainAll(c);
	}

	@Override
	public void clear() {
		this.list.clear();
	}

	@Override
	public T get(int index) {
		return this.list.get(index);
	}

	@Override
	public T set(int index, T element) {
		return this.list.set(index, element);
	}

	@Override
	public void add(int index, T element) {
		this.list.add(index, element);
	}

	@Override
	public T remove(int index) {
		return this.list.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return this.list.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return this.list.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		return this.list.listIterator();
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		return this.list.listIterator(index);
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		return this.list.subList(fromIndex, toIndex);
	}
	public <TT> Lst<TT> map(FTT<TT,T> func) {
		return UList.map(this, func);
	}

	public Lst<T> distincts() {
		return distinct(o -> o, (a,b) -> a == b);
	}

	public <TT> Lst<TT> distinct(FTT<TT,T> func, FTTT<Boolean, TT, TT> compare) {

		Lst<TT> map = map(func);

		Lst<TT> rs = new Lst<TT>();
		for (TT i : map) {
			if (!rs.exists(o -> compare.call(o, i))) {
				rs.add(i);
			}
		}

		return rs;

	}

	public Lst<Integer> distinctInts(FTT<Integer,T> func) {
		return distinct(func, (a,b) -> IntegerCompare.eq(a, b));
	}

	public Lst<String> distinctStrings(FTT<String,T> func) {
		return distinct(func, (a,b) -> StringCompare.eq(a, b));
	}

	public Lst<Long> distinctLongs(FTT<Long,T> func) {
		return distinct(func, (a,b) -> LongCompare.eq(a, b));
	}

	public <TT> Lst<TT> map(FTTT<TT,T,Integer> func) {
		return UList.map(this, func);
	}
	public T getLast() {
		return UList.getLast(list);
	}
	public T uniqueObrig(Predicate<T> predicate) {
		return UList.filterUniqueObrig(list, predicate);
	}
	public T unique(Predicate<T> predicate) {
		return UList.filterUnique(list, predicate);
	}
	public boolean exists(Predicate<T> predicate) {
		return filter(predicate).size() > 0;
	}
	public <RESULT> RESULT reduce(FTTT<RESULT, RESULT, T> func, RESULT startValue) {
		return UList.reduce(this, func, startValue);
	}
	@Override
	public String toString() {
		return list.toString();
	}

	public String toString(FTT<String,T> get, String separator) {

		if (isEmpty()) {
			return "";
		}

		String s = "";
		for (T o : list) {
			s += separator + get.call(o);
		}
		s = s.substring(separator.length());
		return s;
	}

	public T removeLast() {
		return list.remove(size()-1);
	}

	public Lst<T> copy() {
		return UList.copy(this);
	}

	public BigDecimal soma(FTT<BigDecimal, T> func) {
		BigDecimal rs = BigDecimal.ZERO;
		for (T t : list) {
			BigDecimal o = func.call(t);
			if (!Null.is(o)) {
				rs = rs.add(o);
			}
		}
		return rs;
	}

	public Integer somaInt(FTT<Integer, T> func) {
		int rs = 0;
		for (T t : list) {
			Integer o = func.call(t);
			if (!Null.is(o)) {
				rs += o;
			}
		}
		return rs;
	}

	public void print() {
		for (T t : list) {
			console.log(t);
		}
	}

	public ListString toListString(FTT<String, T> func) {
		Lst<String> map = map(func);
		return new ListString(map);
	}

	public Lst<T> sorted(Comparator<T> c) {
		Lst<T> list = copy();
		list.sort(c);
		return list;
	}

	public Lst<T> sortInt(FTT<Integer, T> func) {
		sort((a,b) -> IntegerCompare.compare(func.call(a), func.call(a)));
		return this;
	}

	public Lst<T> sortString(FTT<String, T> func) {
		sort((a,b) -> StringCompare.compare(func.call(a), func.call(a)));
		return this;
	}

}
