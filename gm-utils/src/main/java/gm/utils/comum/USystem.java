package gm.utils.comum;

import java.net.InetAddress;

import gm.utils.exception.UException;
import gm.utils.lambda.FVoidVoid;
import src.commom.utils.string.StringBeforeFirst;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringContains;

public class USystem {

	public static void sleepHoras(double horas) {
		USystem.sleepMinutos(horas * 60);
	}
	public static void sleepMinutos(double minutos) {
		USystem.sleepSegundos(minutos * 60);
	}
	public static void sleepSegundos(double segundos) {
		long d = (long) (segundos * 1000);
		USystem.sleepMiliSegundos(d);
	}
	public static void sleepMiliSegundos(long milisegundos) {
		try {
			Thread.sleep(milisegundos);
		} catch (InterruptedException e) {
			UException.printTrace(e);
		}
	}

	private static String hostName;

	public static boolean hostIs(String s) {
		return StringCompare.eq(USystem.hostName(), s);
	}

	public static String hostName() {
		if (USystem.hostName != null) {
			return USystem.hostName;
		}
		try {
			USystem.hostName = InetAddress.getLocalHost().getHostName();
			return USystem.hostName;
		} catch (Exception e) {
			String s = e.getMessage();

			if (StringContains.is(s, ":")) {
				String a = StringBeforeFirst.get(s, ":");
				if (StringContains.is(s, ":")) {
					String b = StringBeforeFirst.get(s, ":");
					if (a.equals(b)) {
						USystem.hostName = a;
						return USystem.hostName;
					}
				}
			}

			throw UException.runtime(e);
		}
	}

	public static int timeoutsEmExecucao = 0;

	public static void setTimeout(FVoidVoid f, int milisegundos) {
		USystem.setTimeout(f, milisegundos, true);
	}

	private static void setTimeout(Thread threadOrigem, FVoidVoid f, int milisegundos, boolean count) {

//		long currentTimeMillis = System.currentTimeMillis();

		if (count) {
			USystem.timeoutsEmExecucao++;
		}

		new Thread() {
			@Override
			public void run() {
				try {
					USystem.sleepMiliSegundos(milisegundos);
//					while (System.currentTimeMillis() - currentTimeMillis < milisegundos) {
//
//					}
					if (threadOrigem.isAlive()) {
						USystem.setTimeout(threadOrigem, f, 500, count);
					} else {
						f.call();
					}
				} finally {
					if (count) {
						USystem.timeoutsEmExecucao--;
					}
				}
			}
		}.start();

	}

	public static void setTimeout(FVoidVoid f, int milisegundos, boolean count) {
		USystem.setTimeout(Thread.currentThread(), f, milisegundos, count);
	}

	public static String userHome() {
		return System.getProperty("user.home");
	}

}
