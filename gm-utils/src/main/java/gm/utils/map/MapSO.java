package gm.utils.map;

import java.io.File;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gm.utils.anotacoes.IgnoreJson;
import gm.utils.classes.UClass;
import gm.utils.comum.Lst;
import gm.utils.comum.UBoolean;
import gm.utils.comum.UList;
import gm.utils.comum.UObject;
import gm.utils.comum.UType;
import gm.utils.date.Data;
import gm.utils.exception.UException;
import gm.utils.files.UFile;
import gm.utils.lambda.FTTT;
import gm.utils.number.ListInteger;
import gm.utils.number.Numeric15;
import gm.utils.number.Numeric2;
import gm.utils.number.Numeric3;
import gm.utils.number.Numeric4;
import gm.utils.number.Numeric5;
import gm.utils.number.Numeric8;
import gm.utils.number.UBigDecimal;
import gm.utils.number.UDouble;
import gm.utils.number.UInteger;
import gm.utils.number.ULong;
import gm.utils.reflection.Atributo;
import gm.utils.reflection.Atributos;
import gm.utils.reflection.ListAtributos;
import gm.utils.reflection.ListMetodos;
import gm.utils.reflection.Metodo;
import gm.utils.reflection.Metodos;
import gm.utils.reflection.Parametro;
import gm.utils.string.ListString;
import js.support.console;
import src.commom.utils.string.StringAfterFirst;
import src.commom.utils.string.StringBeforeFirst;
import src.commom.utils.string.StringContains;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringParse;
import src.commom.utils.string.StringRight;

public class MapSO extends LinkedHashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	private static int count = Integer.MIN_VALUE;
	private int uniqueId = count++;

	@IgnoreJson
	private MapSO pai;

	public MapSO() {}

	public MapSO(Map<String, Object> map) {
		this.add(map);
	}
	public static <T> T get(MapSO map, String... keys){
		for (int i = 0; i < keys.length-1; i++) {
			String key = keys[i];
			Map<String, Object> mp = map.get(key);
			map = MapSO.toMapSO(mp);
		}
		String key = keys[keys.length-1];
		return map.get(key);
	}
	public <T> T get(String... keys){
		return MapSO.get(this, keys);
	}
	public static MapSO toMapSO(Map<String, Object> map) {
		if (map instanceof MapSO) {
			return (MapSO) map;
		} else {
			return new MapSO(map);
		}
	}
	public ListInteger getInts(String... keys){
		Object o = this.get(keys);
		if (o == null) {
			return null;
		}
		if (o instanceof ListInteger) {
			return (ListInteger) o;
		}
		if (o instanceof Object[]) {
			Object[] os = this.get(keys);
			ListInteger list = new ListInteger();
			for (Object i : os) {
				list.add(UInteger.toInt(i));
			}
			return list;
		}
		throw UException.runtime("Não foi possível converver o objeto para um ListInteger: " + o.getClass().getSimpleName());
	}
	public Integer getInt(String key){
		return UInteger.toInt(this.get(key));
	}
	public Long getLong(String key){
		return ULong.toLong(this.get(key));
	}
	public long getLongObrig(String key) {
		return ULong.toLong(this.getObrig(key));
	}
	public Integer getInt(String key, Integer def){
		return UInteger.toInt(this.get(key), def);
	}
	public String getString(String key){
		return StringParse.get(this.get(key));
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String key){
		Object o = super.get(key);
		if (o == null) {
			Set<String> keySet = keySet();
			for (String s : keySet) {
				if (s.equalsIgnoreCase(key)) {
					o = super.get(s);
					break;
				}
			}
		}
		return (T) o;
	}
	public <T> T getObrig(String key){
		T o = this.get(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}
	@Override
	public Object put(String key, Object value) {
		if (key == null) {
			throw UException.runtime("key == null");
		}
		return super.put(key, value);
	}
	public MapSO add(String key, Object value){
		put(key, value);
		return this;
	}
	public MapSO add(Params params) {
		this.add(params.getMap());
		return this;
	}
	public MapSO add(Map<String, Object> map) {
		Set<String> ks = map.keySet();
		for (String key : ks) {
			Object value = map.get(key);
			put(key, value);
		}
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T> T as(Class<T> classe) {
		if (classe == String.class) {
			return (T) MapSoToJson.get(this);
		}
		T o = UClass.newInstance(classe);
		setInto(o, false);
		return o;
	}

	public void setInto(Object o, boolean clear) {
		Class<Object> classe = UClass.getClass(o);
		setInto(o, classe, clear);
	}
	public void setInto(Object o, Class<?> classe, boolean clear) {
		Metodos metodos = ListMetodos.get(classe);
		Atributos as = ListAtributos.get(classe);
		Atributo id = as.getId();
		if (id != null) {
			as.add(0, id);
		}
		Set<String> keys = keySet();
		for (String key : keys) {

			Object value = this.get(key);

			Atributo a = as.get(key);
			if (a != null) {
				if (value == null) {
					if (clear) {
						a.set(o, null);
					}
				} else if (a.isPrimitivo()){
					a.set(o, value);
				} else {
					if (value instanceof MapSO && !a.getType().equals(MapSO.class)) {
						MapSO map = (MapSO) value;
						try {
							value = map.as(a.getType());
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}
					a.set(o, value);
				}
			} else {
				Metodo metodo = metodos.get("set" + key);
				if (metodo != null) {
					Parametro p = metodo.getParametros().get(0);
					if (value == null) {
						if (clear) {
							metodo.invoke(o, value);
						}
					} else if (UType.isPrimitiva(p.getType())){
						metodo.invoke(o, value);
					} else {
						if (value instanceof MapSO && !p.getType().equals(MapSO.class)) {
							MapSO map = (MapSO) value;
							value = map.as(p.getType());
						}
						metodo.invoke(o, value);
					}
				}

			}
		}
	}
	public void print() {
		Set<String> keys = keySet();
		for (String key : keys) {
			Object value = this.get(key);
			console.log(key + ": " + value);
		}
	}
	public ListString asJson() {
		ListString list = this.asJson(new Lst<>());
		ListString lst = new ListString();

//		remover virgulas excedentes
		while (!list.isEmpty()) {
			String s = list.remove(0);

			if (s.endsWith(",")) {

				if (list.isEmpty()) {
					s = StringRight.ignore1(s);
				} else {
					String x = list.get(0).trim();
					if (x.startsWith("}") || x.startsWith("]")) {
						s = StringRight.ignore1(s);
					}
				}

			}

			lst.add(s);
		}

		return lst;

	}

	private ListString asJson(Lst<MapSO> jaProcessados) {
		ListString list = new ListString();
		if (jaProcessados.contains(this)) {
			if (jaProcessados.exists(o -> o.uniqueId == uniqueId)) {
				return list;
			}
		}
		jaProcessados.add(this);
		list.add("{");
		list.margemInc();
		Set<String> keys = keySet();
		for (String key : keys) {
			Object value = this.get(key);
			if (value == null) {
				list.add("\"" + key + "\": null,");
			} else if (UType.isPrimitiva(value)) {
				list.add("\"" + key + "\": \"" + value + "\",");
			} else if (value instanceof MapSO) {
				MapSO m = (MapSO) value;//jaProcessados.size()
				ListString lst = m.asJson(jaProcessados);
				if (lst.isEmpty()) {
					list.add("\"" + key + "\": {},");
				} else {
					list.add("\"" + key + "\": {");
					lst.remove(0);
					list.add(lst);
				}
			} else if (value instanceof List) {
				List<?> lst = (List<?>) value;
				if (lst.isEmpty()) {
					list.add("\"" + key + "\": [],");
				} else if (UType.isPrimitiva(lst.get(0))) {
					list.add("\"" + key + "\": [");
					list.margemInc();
					for (Object obj : lst) {
						list.add("\"" + obj + "\",");
					}
					list.margemDec();
					list.add("],");
				} else {
					list.add("\"" + key + "\": [");
					list.margemInc();
					for (Object obj : lst) {

						if (obj == null) {
							continue;
						}

						MapSO m;
						if (obj instanceof MapSO) {
							m = (MapSO) obj;
						} else {
							m = MapSoFromObject.get(obj);
						}

						if (m == null) {
							console.log(m);
						}

						ListString asJson = m.asJson(jaProcessados);
						list.add(asJson);
					}
					list.margemDec();
					list.add("],");
				}
			}
		}
		list.margemDec();
		list.add("},");
		return list;
	}

	public int getIntObrig(String key) {
		Object o = this.getObrig(key);
		if (o instanceof String) {
			String s = (String) o;
			o = s.replace(".", "");
		}
		return UInteger.toInt(o);
	}
	public String getStringObrig(String key) {
		return StringParse.get(this.getObrig(key));
	}
	public boolean isEmpty(String key) {
		Object o = this.get(key);
		return UObject.isEmpty(o);
	}
	public Data getData(String key) {
		Object o = this.get(key);
		return Data.to(o);
	}
	public Data getDataObrig(String key) {
		Object o = this.getObrig(key);
		return Data.to(o);
	}
	public Double getDouble(String key) {
		Object o = this.get(key);
		return UDouble.toDouble(o);
	}
	public double getDoubleSafe(String key) {
		Double o = getDouble(key);
		return o == null ? 0 : o;
	}
	public Calendar getCalendar(String key) {
		Data data = getData(key);
		return data == null ? null : data.getCalendar();
	}

	@SuppressWarnings("unchecked")
	public Numeric2 getNumeric2(String key) {

		Object o = this.get(key);

		if (o == null) {
			return null;
		}

		if (o instanceof Numeric2) {
			return (Numeric2) o;
		}

		if (o instanceof Map<?, ?>) {
			Map<String, Object> map = (Map<String, Object>) o;
			if ("Numeric2".equals(map.get("tipo"))) {
				String s = StringParse.get(map.get("id"));
				if (StringContains.is(s, ".")) {
					int inteiros = UInteger.toInt(StringBeforeFirst.get(s, "."));
					int centavos = UInteger.toInt(StringAfterFirst.get(s, "."));
					return new Numeric2(inteiros, centavos);
				} else {
					return new Numeric2(UInteger.toInt(s));
				}
			}
		}

		String s = getString(key);
		if (s == null) return null;
		return new Numeric2(s);
	}

	@SuppressWarnings("unchecked")
	public Numeric3 getNumeric3(String key) {

		Object o = this.get(key);

		if (o == null) {
			return null;
		}

		if (o instanceof Numeric3) {
			return (Numeric3) o;
		}

		if (o instanceof Map<?, ?>) {
			Map<String, Object> map = (Map<String, Object>) o;
			if ("Numeric3".equals(map.get("tipo"))) {
				String s = StringParse.get(map.get("id"));
				if (StringContains.is(s, ".")) {
					int inteiros = UInteger.toInt(StringBeforeFirst.get(s, "."));
					int centavos = UInteger.toInt(StringAfterFirst.get(s, "."));
					return new Numeric3(inteiros, centavos);
				} else {
					return new Numeric3(UInteger.toInt(s));
				}
			}
		}

		String s = getString(key);
		if (s == null) return null;
		return new Numeric3(s);
	}

	@SuppressWarnings("unchecked")
	public Numeric4 getNumeric4(String key) {

		Object o = this.get(key);

		if (o == null) {
			return null;
		}

		if (o instanceof Numeric4) {
			return (Numeric4) o;
		}

		if (o instanceof Map<?, ?>) {
			Map<String, Object> map = (Map<String, Object>) o;
			if ("Numeric4".equals(map.get("tipo"))) {
				String s = StringParse.get(map.get("id"));
				if (StringContains.is(s, ".")) {
					int inteiros = UInteger.toInt(StringBeforeFirst.get(s, "."));
					int centavos = UInteger.toInt(StringAfterFirst.get(s, "."));
					return new Numeric4(inteiros, centavos);
				} else {
					return new Numeric4(UInteger.toInt(s));
				}
			}
		}

		String s = getString(key);
		if (s == null) return null;
		return new Numeric4(s);
	}

	@SuppressWarnings("unchecked")
	public Numeric5 getNumeric5(String key) {

		Object o = this.get(key);

		if (o == null) {
			return null;
		}

		if (o instanceof Numeric5) {
			return (Numeric5) o;
		}

		if (o instanceof Map<?, ?>) {
			Map<String, Object> map = (Map<String, Object>) o;
			if ("Numeric5".equals(map.get("tipo"))) {
				String s = StringParse.get(map.get("id"));
				if (StringContains.is(s, ".")) {
					int inteiros = UInteger.toInt(StringBeforeFirst.get(s, "."));
					int centavos = UInteger.toInt(StringAfterFirst.get(s, "."));
					return new Numeric5(inteiros, centavos);
				} else {
					return new Numeric5(UInteger.toInt(s));
				}
			}
		}

		String s = getString(key);
		if (s == null) return null;
		return new Numeric5(s);
	}

	@SuppressWarnings("unchecked")
	public Numeric8 getNumeric8(String key) {

		Object o = this.get(key);

		if (o == null) {
			return null;
		}

		if (o instanceof Numeric8) {
			return (Numeric8) o;
		}

		if (o instanceof Map<?, ?>) {
			Map<String, Object> map = (Map<String, Object>) o;
			if ("Numeric8".equals(map.get("tipo"))) {
				String s = StringParse.get(map.get("id"));
				if (StringContains.is(s, ".")) {
					int inteiros = UInteger.toInt(StringBeforeFirst.get(s, "."));
					int centavos = UInteger.toInt(StringAfterFirst.get(s, "."));
					return new Numeric8(inteiros, centavos);
				} else {
					return new Numeric8(UInteger.toInt(s));
				}
			}
		}

		String s = getString(key);
		if (s == null) return null;
		return new Numeric8(s);
	}

	@SuppressWarnings("unchecked")
	public Numeric15 getNumeric15(String key) {

		Object o = this.get(key);

		if (o == null) {
			return null;
		}

		if (o instanceof Numeric15) {
			return (Numeric15) o;
		}

		if (o instanceof Map<?, ?>) {
			Map<String, Object> map = (Map<String, Object>) o;
			if ("Numeric15".equals(map.get("tipo"))) {
				String s = StringParse.get(map.get("id"));
				if (StringContains.is(s, ".")) {
					int inteiros = UInteger.toInt(StringBeforeFirst.get(s, "."));
					int centavos = UInteger.toInt(StringAfterFirst.get(s, "."));
					return new Numeric15(inteiros, centavos);
				} else {
					return new Numeric15(UInteger.toInt(s));
				}
			}
		}

		String s = getString(key);
		if (s == null) return null;
		return new Numeric15(s);
	}

	public Numeric2 getNumeric2Obrig(String key) {
		Numeric2 o = getNumeric2(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}

	public Numeric3 getNumeric3Obrig(String key) {
		Numeric3 o = getNumeric3(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}

	public Numeric4 getNumeric4Obrig(String key) {
		Numeric4 o = getNumeric4(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}

	public Numeric5 getNumeric5Obrig(String key) {
		Numeric5 o = getNumeric5(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}

	public Numeric8 getNumeric8Obrig(String key) {
		Numeric8 o = getNumeric8(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}

	public Numeric15 getNumeric15Obrig(String key) {
		Numeric15 o = getNumeric15(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}

	public BigDecimal getBigDecimal(String key) {
		Numeric2 o = getNumeric2(key);
		return o == null ? null : o.getValor();
	}
	public BigDecimal getBigDecimal(String key, int casas) {
		Object o = this.get(key);
		if (o == null) return null;
		return UBigDecimal.toBigDecimal(o, casas);
	}

	public int id() {
		return getIntObrig("id");
	}

	public Boolean getBoolean(String key) {
		return UBoolean.toBoolean( this.get(key) );
	}
	public boolean getBooleanObrig(String key) {
		Boolean o = getBoolean(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}

	public MapSO sub(String key) {
		MapSO o = new MapSO();
		o.pai = this;
		this.add(key, o);
		return o;
	}

	public MapSO out() {
		return pai;
	}
	public Lst<MapSO> getSubList(String key) {
		Lst<MapSO> list = new Lst<>();

		List<?> lst = this.get(key);
		if (!UList.isEmpty(lst)) {
			for (Object o : lst) {
				list.add(MapSoFromObject.get(o));
			}
		}
		return list;
	}
	public MapSO getSubObrig(String key) {
		MapSO o = getSub(key);
		if (o == null) {
			throw UException.runtime("key não encontrado: " + key);
		}
		return o;
	}
	public MapSO getSub(String key) {
		Object o = this.get(key);
		if (o instanceof MapSO) {
			return (MapSO) o;
		} else {
			return MapSoFromObject.get(o);
		}
	}

	public ListString struct() {
		ListString list = new ListString();
		list.add("new MapSO()");
		this.struct(list);
		return list;
	}

	private void struct(ListString list) {
		Set<String> keys = keySet();
		for (String key : keys) {
			Object value = this.get(key);
			if (value == null) {
				list.add(".add(\"" + key + "\", null)");
			} else if (UType.isPrimitiva(value)) {
				if (value instanceof String) {
					list.add(".add(\"" + key + "\", \""+value+"\")");
				} else {
					list.add(".add(\"" + key + "\", "+value+")");
				}
			} else if (value instanceof MapSO) {
				list.add(".sub(\"" + key + "\")");
				list.getMargem().inc();
				MapSO map = (MapSO) value;
				map.struct(list);
				list.getMargem().dec();
				list.add(".out()");
			} else if (value instanceof List) {

				List<?> lst = (List<?>) value;

				if (lst.isEmpty()) {
					list.add(".add(\"" + key + "\", [])");
				} else {

					list.add(".add(\"" + key + "\", Arrays.asList(");
					list.getMargem().inc();

					Object first = lst.get(0);

					if (first instanceof String) {
						for (Object o : lst) {
							list.add("\"" + o + "\",");
						}
					} else if (UType.isPrimitiva(first)) {
						for (Object o : lst) {
							list.add(o + ",");
						}
					} else if (first instanceof MapSO) {
						for (Object o : lst) {
							MapSO map = (MapSO) o;
							list.add(map.struct());
							list.add(",");
						}
					} else {
						throw new RuntimeException("???");
					}

					String s = list.removeLast();
					s = StringRight.ignore1(s);
					if (!StringEmpty.is(s)) {
						list.add(s);
					}
					list.getMargem().dec();
					list.add("))");

				}

			} else {
				throw new RuntimeException("???");
			}
		}
	}

	public void save(String file) {
		this.save(new File(file));
	}
	public void save(File file) {
		ListString list = new ListString();
		forEach((key, value) -> {
			list.add(key + "=" + StringParse.get(value));
		});
		list.save(file);
	}

	public boolean loadIfExists(File file) {
		if (UFile.exists(file)) {
			ListString list = new ListString().load(file);
			list.trimPlus();
			list.removeEmptys();
			list.removeIfStartsWith("#");
			for (String s : list) {
				this.add(StringBeforeFirst.get(s, "=").trim(), StringAfterFirst.get(s, "=").trim());
			}
			return true;
		} else {
			return false;
		}

	}
	public boolean loadIfExists(String fileName) {
		return this.loadIfExists(new File(fileName));
	}

	public ListString getKeys() {
		ListString list = new ListString();
		list.addAll(keySet());
		return list;
	}

	public MapSO setObrig(String key, Object value) {
		if (StringEmpty.is(key)) {
			throw new RuntimeException("key is empty");
		}
		if (UObject.isEmpty(value)) {
			throw new RuntimeException("value is empty");
		}
		return this.add(key, value);
	}

	public boolean isTrue(String key) {
		return UBoolean.isTrue(getBoolean(key));
	}

	public <T> Lst<T> map(FTTT<T,String,Object> func) {
		Lst<T> result = new Lst<>();
		ListString keys = getKeys();
		for (String s : keys) {
			result.add(func.call(s, get(s)));
		}
		return result;
	}

}
