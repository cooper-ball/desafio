package gm.utils.javaCreate;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.List;

import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.comum.UType;
import gm.utils.reflection.Parametro;
import gm.utils.string.ListString;
import lombok.Getter;
import lombok.Setter;
import src.commom.utils.array.ArrayLst;
import src.commom.utils.string.StringAfterFirst;
import src.commom.utils.string.StringAfterLast;
import src.commom.utils.string.StringBeforeLast;
import src.commom.utils.string.StringBox;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringTrim;
import js.support.console;
import src.commom.utils.string.StringContains;

@Getter
public class JcTipo {

	private JcTipo extends_;
	private boolean isGenerics;
	private Boolean primitivo;
	private String name;
	private Lst<JcTipo> generics;

	private static final List<JcTipo> GENERICS_NULL = null;

	@Setter private boolean fullyQualifiedName;

	private JcTipo() {}

	public static JcTipo INTERROGACAO() {
		return GENERICS("?");
	}

	public static JcTipo GENERICS(String name) {
		JcTipo o = new JcTipo();
		o.name = name;
		o.isGenerics = true;
		return o;
	}

	/*--------------------------*/

	public JcTipo(String s, List<JcTipo> generics) {

		s = StringTrim.plus(s);
		if (StringContains.is(s, " ")) {
			throw new RuntimeException("nome nao pode conter espacos");
		}
		if (s.startsWith("java.lang.") && !s.contentEquals("java.lang.Class")) {
			s = StringAfterLast.get(s, ".");
			this.primitivo = true;
		} else if (!isPrimitivo(s)) {
			if (!StringContains.is(s, ".")) {
				throw new RuntimeException("O nome deve ser completo: " + s);
			}
			if (StringContains.is(s, "<")) {
				throw new RuntimeException("use addGenerics " + s);
			}
		}
		this.name = s;

		if (generics != null) {
			for (JcTipo o : generics) {
				addGenerics(o);
			}
		}

	}

	private static List<JcTipo> toGenerics(JcTipo... generics) {
		return UList.asList(generics);
	}
	private static List<JcTipo> toGenerics(Class<?>... generics) {
		return UList.map(generics, o -> new JcTipo(o));
	}
	private static List<JcTipo> toGenerics(String... generics) {
		return UList.map(generics, o -> new JcTipo(o));
	}
	private static List<JcTipo> toGenerics(JcClasse... generics) {
		return UList.map(generics, o -> o.getTipo());
	}

	/* String */
	public JcTipo(String nome) {
		this(nome, GENERICS_NULL);
	}
	public JcTipo(String nome, JcTipo... generics) {
		this(nome, toGenerics(generics));
	}
	public JcTipo(String nome, Class<?>... generics) {
		this(nome, toGenerics(generics));
	}
	public JcTipo(String nome, String... generics) {
		this(nome, toGenerics(generics));
	}
	public JcTipo(String nome, JcClasse... generics) {
		this(nome, toGenerics(generics));
	}

	/* JcTipo */
	public JcTipo(JcTipo o) {
		this(o.getName(), GENERICS_NULL);
	}
	public JcTipo(JcTipo o, JcTipo... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(JcTipo o, Class<?>... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(JcTipo o, String... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(JcTipo o, JcClasse... generics) {
		this(o.getName(), toGenerics(generics));
	}

	/* Class<?> */
	public JcTipo(Class<?> o) {
		this(o.getName(), GENERICS_NULL);
	}
	public JcTipo(Class<?> o, JcTipo... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(Class<?> o, Class<?>... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(Class<?> o, String... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(Class<?> o, JcClasse... generics) {
		this(o.getName(), toGenerics(generics));
	}

	/* JcClasse */
	public JcTipo(JcClasse o) {
		this(o.getName(), GENERICS_NULL);
	}
	public JcTipo(JcClasse o, JcTipo... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(JcClasse o, Class<?>... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(JcClasse o, String... generics) {
		this(o.getName(), toGenerics(generics));
	}
	public JcTipo(JcClasse o, JcClasse... generics) {
		this(o.getName(), toGenerics(generics));
	}

	private static String getType(File file) {
		String s = file.toString();
		if (!s.endsWith(".java")) {
			throw new RuntimeException();
		}
		s = StringBeforeLast.get(s, ".");
		s = StringAfterFirst.get(s, "/src/main/java/");
		s = s.replace("/", ".");
		return s;
	}

	/* File */
	public JcTipo(File o) {
		this(getType(o), GENERICS_NULL);
	}
	public JcTipo(File o, JcTipo... generics) {
		this(getType(o), toGenerics(generics));
	}
	public JcTipo(File o, Class<?>... generics) {
		this(getType(o), toGenerics(generics));
	}
	public JcTipo(File o, String... generics) {
		this(getType(o), toGenerics(generics));
	}
	public JcTipo(File o, JcClasse... generics) {
		this(getType(o), toGenerics(generics));
	}

	/*--------------------------*/

	public boolean isPrimitivo() {
		if (this.primitivo == null) {
			this.primitivo = this.calculaPrimitivo();
		}
		return this.primitivo;
	}

	private boolean calculaPrimitivo() {
		return isPrimitivo(name);
	}

	private boolean isPrimitivoMesmo() {
		return isPrimitivoMesmo(name);
	}

	private static boolean isPrimitivo(String s) {

		if (s.equals("?")) {
			return true;
		}

		if (isPrimitivoMesmo(s)) {
			return true;
		}

		if (isDate(s)) {
			return false;
		}

		if ("java.math.BigDecimal".contentEquals(s)) {
			return false;
		}

		if ("java.math.BigInteger".contentEquals(s)) {
			return false;
		}

		if (UType.PRIMITIVAS_JAVA.contains(s)) {

			if (StringContains.is(s, "Calen")) {
				isDate(s);
			}

			return true;

		}

		return false;

	}

	private static boolean isPrimitivoMesmo(String s) {
		return UType.PRIMITIVAS_JAVA_REAL.contains(s);
	}

	/*
	 * 001 - garantir que não seja mudada uma superClass por acidente
	 * */

	public JcTipo extends_(JcTipo tipo, String... generics) {
		if (isPrimitivo()) {
			throw new RuntimeException();
		}
		if (extends_ != null) {
			/* 001 */
			throw new RuntimeException();
		}
		this.extends_ = tipo;
		for (String s : generics) {
			addGenerics(s);
		}
		return this;
	}

	/* 001 */
	public void clearExtends() {
		this.extends_ = null;
	}

	public JcTipo extends_(Class<?> classe) {
		return extends_(new JcTipo(classe));
	}
	public JcTipo addGenerics(Class<?> classe) {
		return this.addGenerics(new JcTipo(classe));
	}
	public JcTipo addGenerics(JcClasse classe) {
		return this.addGenerics(classe.getTipo());
	}
	public JcTipo addGenerics(String nome) {
		return this.addGenerics(new JcTipo(nome));
	}
	public JcTipo addGenerics(JcTipo tipo) {
		if (isPrimitivo()) {
			throw new RuntimeException();
		}
		if (tipo.isPrimitivoMesmo() && !tipo.getName().contentEquals("?")) {
			throw new RuntimeException(tipo.getName());
		}
		if (generics == null) {
			generics = new Lst<>();
		}

		if (tipo == this) {
			throw new RuntimeException();
		}

		if (tipo.containsGenerics(this)) {
			throw new RuntimeException();
		}

		generics.add(tipo);
		return this;
	}

	//evitar recursividade
	private boolean containsGenerics(JcTipo o) {
		if (this.generics == null) {
			return false;
		}
		if (generics.contains(o)) {
			return true;
		}
		for (JcTipo gen : generics) {
			if (gen.containsGenerics(o)) {
				return true;
			}
		}
		return false;
	}

	public String syntaxDiamond() {
		return toString(true);
	}

	@Override
	public String toString() {
		return toString(false);
	}

	private String toString(boolean diamond) {
		if (isPrimitivo()) {
			return name;
		}

		String s;
		if (fullyQualifiedName || isGenerics) {
			s = name;
		} else {
			s = StringAfterLast.get(name, ".");
		}

		if (generics != null) {
			if (diamond) {
				s += "<>";
			} else {
				String x = "";
				for (JcTipo jcTipo : this.generics) {
					x += ", " + jcTipo;
				}
				x = x.substring(2);
				s += "<" + x + ">";
			}
		}

		if (extends_ != null) {
			s += " extends " + extends_;
		}

		return s;
	}

	public void clearGenerics() {
		generics = null;
	}

	private void addTipos(JcTipos list, ListString pilha) {
		if (isPrimitivo()) {
			return;
		}
		list.add(this);
		if (generics != null) {
			for (JcTipo o : generics) {
				if (!pilha.contains(o.getName())) {
					pilha.add(o.getName());
					o.addTipos(list, pilha);
				}
			}
		}
	}

	public void addTipos(JcTipos list) {
		addTipos(list, new ListString());
	}

	public JcTipos getTipos() {
		JcTipos tipos = new JcTipos();
		addTipos(tipos);
		if (extends_ != null) {
			tipos.add(extends_);
			tipos.add(extends_.getTipos());
		}
		return tipos;
	}

	public String getPkg() {
		return StringBeforeLast.get(getName(), ".");
	}

	public String getSimpleName() {
		if (fullyQualifiedName) {
			return getName();
		} else {
			return StringAfterLast.get("." + getName(), ".");
		}
	}

	public boolean eq(String nome) {
		if (nome.startsWith("java.lang.")) {
			nome = StringAfterLast.get(nome, ".");
			return StringCompare.eq(getSimpleName(), nome);
		} else {
			return StringCompare.eq(getName(), nome) || StringCompare.eq(getSimpleName(), nome);
		}
	}

	public boolean eq(Class<?> o) {
		return eq(o.getName());
	}

	public boolean eq(JcTipo o) {
		return eq(o.getName());
	}

	public boolean eq(JcClasse o) {
		return eq(o.getName());
	}

	private static ArrayLst<String> TIPOS_LIST = ArrayLst.build("Array");

	public boolean isList() {
		return TIPOS_LIST.contains(getName());
	}

	public boolean isDate() {
		return isDate(name);
	}

	public static boolean isDate(String nome) {
		return UType.CLASSES_DATA.contains(nome);
	}

	public boolean isBigDecimal() {
		return name.contentEquals(BigDecimal.class.getName()) || name.contentEquals("BigDecimal") || name.contentEquals("BigDecimal.class");
	}

	public boolean isDouble() {
		return name.contentEquals(Double.class.getName()) || name.contentEquals("Double") || name.contentEquals("Double.class");
	}

	public static JcTipo descobre(String str) {

		str = StringTrim.plus(str);

		if (str.startsWith("class ")) {
			str = str.substring(6);
		}

		if (UType.PRIMITIVAS_JAVA_REAL.contains(str)) {
			return new JcTipo(str);
		}

		if (!str.contains("<")) {
			if (str.contains(".")) {
				return new JcTipo(str);
			} else {
				return GENERICS(str);
			}
		}

		ListString list = ListString.separaPalavras(str);
		list.trimPlus();
		list.juntarComAProximaSe(s -> s.contentEquals("."), "");
		list.juntarComASuperiorSe(s -> s.startsWith("."), "");
		list.removeIfEquals(",");

		Lst<Object> os = list.map(s -> {
			if (StringContains.is(s, ".")) {
				return new JcTipo(s);
			} else if (s.contentEquals("<") || s.contentEquals(">")) {
				return new StringBox(s);
			} else {
				return GENERICS(s);
			}
		});

		Lst<Object> ends = os.filter(s -> is(s,">"));

		while (os.size() > 1) {
			int i = os.indexOf(ends.remove(0));
			os.remove(i);
			i--;
			Lst<JcTipo> tipos = new Lst<>();
			while (!is(os.get(i),"<")) {
				tipos.add(0, (JcTipo) os.remove(i));
				i--;
			}
			os.remove(i);
			i--;
			JcTipo tipo = (JcTipo) os.get(i);
			for (JcTipo tp : tipos) {
				tipo.addGenerics(tp);
			}
		}

		return (JcTipo) os.get(0);

	}

	private static boolean is(Object s, String str) {
		return s instanceof StringBox && ((StringBox) s).eq(str);
	}

	public static JcTipo build(Field field) {
		return JcTipo.descobre(field.getAnnotatedType().getType().getTypeName());
	}

	public static JcTipo build(Parametro param) {
		return JcTipo.descobre(param.getParameter().getParameterizedType().toString());
	}

	public static void main(String[] args) {
		console.log(descobre("java.util.Map<java.lang.String, java.util.Map<java.lang.Integer, T>>"));
	}

	public static JcTipo build(Type type) {
		return JcTipo.descobre(type.toString());
	}

}
