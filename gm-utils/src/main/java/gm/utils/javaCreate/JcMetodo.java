package gm.utils.javaCreate;

import java.lang.annotation.Annotation;

import gm.utils.classes.UClass;
import gm.utils.comum.Lst;
import gm.utils.exception.UException;
import gm.utils.lambda.FTT;
import gm.utils.reflection.Atributo;
import gm.utils.reflection.Atributos;
import gm.utils.reflection.ListMetodos;
import gm.utils.reflection.Metodos;
import gm.utils.string.CorretorOrtografico;
import gm.utils.string.ListString;
import js.support.console;
import lombok.Getter;
import lombok.Setter;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringContains;
import src.commom.utils.string.StringPrimeiraMinuscula;
import src.commom.utils.string.StringRight;

@Getter @Setter
public class JcMetodo {

	private JcClasse jc;
	private JcAnotacoes anotacoes = new JcAnotacoes();
	private String afterComment;
	private String nome;
	private JcTipo genericsReverso;
	private JcTipo tipoRetorno;
	private String acesso = "private";

	private boolean hasThrow;
	private boolean override;
	private boolean abstrato;
	private boolean estatico;

	private boolean synchronized_;
	private boolean final_;
	private boolean construtor;
	private boolean pularLinhaAntes = true;
	private boolean naoQuebrarLinhas;
	private int ordem = 0;
	private FTT<String, String> tratarToString;

	private JcTipos tipos = new JcTipos();

	private Lst<JcParametro> parametros = new Lst<>();
	private ListString body = new ListString();
	private ListString bodyEnd = new ListString();

//	30032764 skynet vitoria
//	ligacao fixo ilimitada
//	72316011

	public JcMetodo(String nome) {
		this.nome = nome;
		this.body.setAutoIdentacao(true);
	}
	public JcMetodo add() {
		if (this.abstrato) {
			throw UException.runtime("Método abstrato");
		}
		this.body.add();
		return this;
	}
	public JcMetodo returnNull() {
		return this.return_("null");
	}
	public JcMetodo while_(String s) {
		return this.add("while ("+s+") {");
	}
	public JcMetodo if_(String s) {
		return this.add("if ("+s+") {");
	}
	public JcMetodo ifIsNull(String s) {
		tipos.add(Null.class);
		return this.add("if (Null.is("+s+")) {");
	}
	public JcMetodo ifIsNotNull(String s) {
		tipos.add(Null.class);
		return this.add("if (!Null.is("+s+")) {");
	}
	public JcMetodo throwBusiness(String message) {
		return throw_(message, true);
	}
	public JcMetodo throw_(String s, boolean colocarAspas) {
		if (colocarAspas) {
			s = "\"" + CorretorOrtografico.exec(s) + "\"";
		}
		hasThrow = true;
		return this.add("throw new [exceptionClass]("+s+");");
	}
	public JcMetodo ifReturn(String condicao, String resultado) {
		return this.if_(condicao, "return " + resultado + ";");
	}
	public JcMetodo ifReturnVoid(String condicao) {
		return this.if_(condicao, "return;");
	}
	public JcMetodo if_(String condicao, String... add) {
		this.if_(condicao);
		for (String string : add) {
			this.add(string);
		}
		return this.close();
	}
	public JcMetodo ifElse(String s, String ifTrue, String ifFalse) {
		return this.if_(s).add(ifTrue + ";").else_().add(ifFalse + ";").close();
	}
	public JcMetodo else_() {
		return this.add("} else {");
	}
	public JcMetodo elseif_(String s) {
		return this.add("} else if ("+s+") {");
	}
	public JcMetodo close() {
		return this.add("}");
	}
	public JcMetodo add(String s) {

		if (StringContains.is(s, "setDisabled(!this.status.eq(AGUARDANDO_APROVACAO_DO_GERENTE_GERAL_E_DIRETORES)")) {
			console.log(s);
		}

		if (this.abstrato) {
			throw UException.runtime("Método abstrato");
		}
		this.body.add(s);
		return this;
	}
	public JcMetodo add(Object... os) {

		String s = "";

		for (Object o : os) {

			if (o == null) {
				s += "null";
			} else if (o instanceof JcTipo) {
				tipos.add((JcTipo) o);
				s += o;
			} else if (o.getClass().getSimpleName().contentEquals("Class")) {
				Class<?> classe = (Class<?>) o;
				tipos.add(classe);
				s += classe.getSimpleName();
			} else {
				s += o;
			}
		}

		return add(s);
	}
	public JcMetodo addEnd(String s) {
		if (this.abstrato) {
			throw UException.runtime("Método abstrato");
		}
		this.bodyEnd.add(s);
		return this;
	}

	public String getAssinatura() {

		String s;

		if (jc.isInterface()) {

			if (isEmpty()) {
				s = "";
			} else {
				s = "default ";
			}

			if (this.abstrato) {
				throw new RuntimeException();
			}

			if (this.construtor) {
				throw new RuntimeException();
			}

			if (this.estatico) {
				throw new RuntimeException();
			}

			if (this.synchronized_) {
				throw new RuntimeException();
			}

			if (this.final_) {
				throw new RuntimeException();
			}


		} else {
			s = this.acesso + " ";
		}

		if (this.abstrato) {
			s += "abstract ";
		}

		if (!this.construtor) {
			if (this.estatico) {
				s += "static ";
			}
			if (this.synchronized_) {
				s += "synchronized ";
			}
			if (this.final_) {
				s += "final ";
			}

			if (genericsReverso != null) {
				s += "<"+genericsReverso+"> ";
			}

			if (this.tipoRetorno == null) {
				s += "void";
			} else {
				s += this.tipoRetorno;
			}
			s += " ";
		}

		s += this.nome + "(";
		if (!this.parametros.isEmpty()) {
			for (JcParametro parametro : this.parametros) {
				s += parametro + ", ";
			}
			s = StringRight.ignore(s, 2);
		}

		if (this.abstrato) {
			s += ");";
		} else if (isEmpty()) {
			if (jc.isInterface()) {
				s += ");";
			} else {
				s += ") {}";
			}
		} else {
			s += ") {";
		}

		return s;

	}

	public ListString get() {

		ListString list = new ListString();

		if (this.pularLinhaAntes) {
			list.add();
		}

		if (this.override) {
			this.anotacoes.add(Override.class);
		}

		String ans = this.anotacoes.toString();
		if (!ans.isEmpty()) {
			list.add(this.anotacoes.toString());
		}

		list.add(this.getAssinatura());

		if (this.abstrato || isEmpty()) {
			return list;
		}

		list.margemInc();

		if (bodyEnd.isEmpty()) {
			list.add(this.body);
		} else {
			list.add(body.copy().add(bodyEnd));
		}

		list.margemDec();
		list.add("}");

		if (this.afterComment != null) {
			list.add("\\" + this.afterComment);
		}

		if (this.naoQuebrarLinhas) {
			list.trim();
			String s = list.toString(" ");
			s = s.replace("( ","(");
			s = s.replace(" )",")");
			s = s.replace("{ ","{");
			s = s.replace(" }","}");
			list.clear();
			list.add(s.trim());
		}

		return list;

	}

	private boolean isEmpty() {
		return body.isEmpty() && bodyEnd.isEmpty();
	}

	@Override
	public String toString() {
		String s = get().toString("\n");
		if (tratarToString != null) {
			s = tratarToString.call(s);
		}
		return s;
	}

	public JcMetodo param(JcParametro param) {
		return paramFim(param);
	}
	public JcMetodo param(String nome, Class<?> tipo, JcClasse generics) {
		return paramFim(nome, new JcTipo(tipo), generics.getTipo());
	}
	public JcMetodo param(String nome, Class<?> tipo, Class<?> generics) {
		return paramFim(nome, new JcTipo(tipo), new JcTipo(generics));
	}
	public JcMetodo param(String nome, String tipo, String generics) {
		return paramFim(nome, new JcTipo(tipo), new JcTipo(generics));
	}
	public JcMetodo param(String nome, Class<?> tipo, String generics) {
		return paramFim(nome, new JcTipo(tipo), new JcTipo(generics));
	}
	public JcMetodo param(String nome, Class<?> tipo, JcTipo generics) {
		return paramFim(nome, new JcTipo(tipo), generics);
	}
	public JcMetodo param(String nome, JcTipo tipo) {
		return paramFim(nome, tipo);
	}
	public JcMetodo paramId() {
		return paramFim("id", new JcTipo(int.class));
	}
	public JcMetodo param(String nome, JcClasse tipo) {
		return paramFim(nome, tipo.getTipo());
	}
	public JcMetodo sparams(String... nomes) {
		for (String nome : nomes) {
			paramFim(nome, new JcTipo(String.class));
		}
		return this;
	}
	public JcMetodo param(String nome, Class<?> tipo) {
		return paramFim(nome, new JcTipo(tipo));
	}
	public JcMetodo param(String nome, String tipo) {
		return paramFim(nome, new JcTipo(tipo));
	}
	public JcMetodo param(Class<?> classe) {
		return paramFim(StringPrimeiraMinuscula.exec(classe.getSimpleName()), new JcTipo(classe));
	}

	public JcMetodo paramArray(String nome, String tipo) {
		return paramArrayFim(nome, new JcTipo(tipo));
	}
	public JcMetodo paramArray(String nome, Class<?> tipo) {
		return paramArrayFim(nome, new JcTipo(tipo));
	}
	public JcMetodo paramArray(String nome, JcTipo tipo) {
		return paramArrayFim(nome, tipo);
	}

	/*finais*/

	private JcMetodo paramFim(String nome, JcTipo tipo, JcTipo... generics) {
		return paramFim(new JcParametro(nome, tipo, generics));
	}

	private JcMetodo paramArrayFim(String nome, JcTipo tipo) {
		return paramFim(new JcParametro(nome, tipo).array());
	}

	private JcMetodo paramFim(JcParametro param) {
		this.parametros.add(param);
		return this;
	}

	/*finais*/

	public JcMetodo setTipoRetorno(String tipo) {
		return this.setTipoRetorno(new JcTipo(tipo));
	}
	public JcMetodo setTipoRetorno(JcTipo tipo) {
		this.tipoRetorno = tipo;
		return this;
	}
	public JcMetodo setTipoRetorno(Class<?> tipo) {
		this.setTipoRetorno(tipo.getName());
		return this;
	}
	public JcMetodo clearGenericsRetorno() {
		this.tipoRetorno.clearGenerics();
		return this;
	}
	public JcMetodo addGenericsRetorno(JcClasse tipo) {
		this.tipoRetorno.addGenerics(tipo.getName());
		return this;
	}
	public JcMetodo addGenericsRetorno(JcTipo tipo) {
		this.tipoRetorno.addGenerics(tipo);
		return this;
	}
	public JcMetodo addGenericsRetorno(String tipo) {
		if (tipo == null || tipo.contentEquals(".null")) {
			return this;
		}
		this.addGenericsRetorno(new JcTipo(tipo));
		return this;
	}
	public JcMetodo setGenericsRetornoNivel2(String tipo) {
		if (tipo == null || tipo.contentEquals(".null")) {
			return this;
		}
		this.tipoRetorno.getGenerics().get(0).addGenerics(tipo);
		return this;
	}
	public JcMetodo setGenericsRetorno(JcTipo tipo) {
		return addGenericsRetorno(tipo);
	}
	public JcMetodo setGenericsRetorno(Class<?> tipo) {
		addGenericsRetorno(tipo.getName());
		return this;
	}
	public JcMetodo setGenericsRetornoNivel2(Class<?> tipo) {
		this.setGenericsRetornoNivel2(tipo.getName());
		return this;
	}
	public JcMetodo margemInc() {
		this.body.margemInc();
		return this;
	}
	public JcMetodo margemDec() {
		this.body.margemDec();
		return this;
	}
	public String removeLast() {
		return this.body.removeLast();
	}
	public JcMetodo final_() {
		this.setFinal_(true);
		return this;
	}
	public JcMetodo public_() {
		this.setAcesso("public");
		return this;
	}
	public JcMetodo protected_() {
		this.setAcesso("protected");
		return this;
	}
	public JcMetodo synchronized_() {
		this.setSynchronized_(true);
		return this;
	}
	public JcMetodo abstract_() {
		if (!this.body.isEmpty()) {
			throw UException.runtime("Método contém body");
		}
		if (!this.acesso.equals("public")) {
			this.protected_();
		}
		this.setAbstrato(true);
		return this;
	}
	public JcMetodo override() {
		if (!this.acesso.equals("public")) {
			this.protected_();
		}
		this.setOverride(true);

		if (jc != null && jc.getExtends_() != null && tipoRetorno == null) {
			JcTipo extends_ = jc.getExtends_();
			Class<?> classe = UClass.getClass(extends_.getName());
			if (classe != null) {
				Metodos metodos = ListMetodos.get(classe);
				metodos = metodos.filter(o -> !o.returnVoid());
				metodos = metodos.find(getNome());
				if (metodos.size() == 1) {
					Class<?> retorno = metodos.get(0).retorno();
					if (retorno != null) {
						setTipoRetorno(retorno);
					}
				}
			}
		}

		return this;
	}
	public JcMetodo static_() {
		this.setEstatico(true);
		return this;
	}
	public JcMetodo typeSemValidacao(String tipo) {
		this.tipoRetorno = new JcTipo(tipo);
		return this;
	}
	public JcMetodo type(JcClasse tipo) {
		return this.type(tipo.getTipo());
	}
	public JcMetodo type(JcTipo tipo) {
		this.setTipoRetorno(tipo);
		return this;
	}
	public JcMetodo type(String tipo) {
		return this.type(new JcTipo(tipo));
	}
	public JcMetodo type(Class<?> tipo) {
		this.setTipoRetorno(tipo);
		return this;
	}
	public JcMetodo type(Class<?> tipo, Class<?> generics) {
		this.setTipoRetorno(tipo);
		this.setGenericsRetorno(generics);
		return this;
	}
	public JcMetodo type(Class<?> tipo, String generics) {
		this.setTipoRetorno(tipo);
		this.addGenericsRetorno(generics);
		return this;
	}
	public JcMetodo type(Class<?> tipo, JcClasse generics) {
		this.setTipoRetorno(tipo);
		this.addGenericsRetorno(generics);
		return this;
	}

	public JcMetodo type(JcTipo tipo, JcTipo generics) {
		this.setTipoRetorno(tipo);
		this.setGenericsRetorno(generics);
		return this;
	}

	public JcMetodo type(Class<?> tipo, JcTipo generics) {
		this.setTipoRetorno(tipo);
		this.setGenericsRetorno(generics);
		return this;
	}

	public JcTipos getTipos() {

		JcTipos list = new JcTipos();

		if (this.tipoRetorno != null) {
			this.tipoRetorno.addTipos(list);
		}

		this.parametros.forEach(o -> o.getTipo().addTipos(list));
		this.parametros.forEach(o -> o.getAnotacoes().getList().forEach(i -> list.add(i.getTipos())));

		if (!this.anotacoes.getList().isEmpty()) {
			this.anotacoes.getList().forEach(o -> {
				if (o.getTipo().getName().contains(".")) {
					list.add(o.getTipo());
				}
			});
		}

		for (JcTipo tipo : tipos.getList()) {
			list.add(tipo);
		}

		return list;
	}
	public final JcMetodo addAnotacao(JcAnotacao anotacao) {
		this.anotacoes.add(anotacao);
		return this;
	}
	public final JcMetodo addAnotacao(String tipo) {
		this.anotacoes.add(tipo);
		return this;
	}
	public final JcMetodo addAnotacao(JcTipo tipo) {
		this.anotacoes.add(tipo);
		return this;
	}
	public final JcMetodo addAnotacao(Class<? extends Annotation> classe) {
		this.anotacoes.add(classe);
		return this;
	}
	public final JcMetodo addAnotacao(Class<? extends Annotation> classe, String parametro) {
		this.anotacoes.add(classe, parametro);
		return this;
	}
//	(Class<? extends Annotation> classe, String parametros)

	public JcMetodo return_(String s) {
		return this.add("return " + s + ";");
	}

	public JcMetodo return_(int i) {
		if (getTipoRetorno() == null) {
			type(int.class);
		}
		return this.add("return " + i + ";");
	}

	public JcMetodo return_(boolean b) {
		if (getTipoRetorno() == null) {
			type(boolean.class);
		}
		return this.add("return " + b + ";");
	}

	public JcMetodo return_() {
		return this.add("return;");
	}
	public String getPenultimo() {
		return this.body.getPenultimo();
	}
	public String getLast() {
		return this.body.getLast();
	}
	public JcMetodo suppressWarningsUnchecked() {
		this.addAnotacao(SuppressWarnings.class, "\"unchecked\"");
		return this;
	}
	public JcMetodo ifStringEquals(String a, String b) {
		tipos.add(StringCompare.class);
		if_("StringCompare.eq("+a+", "+b+")");
		return this;
	}

	public JcMetodo transactional() {
		addAnotacao("javax.transaction.Transactional");
		return this;
	}

	public void call(JcMetodo m) {
		call(m, m.parametros.toListString(o -> o.getNome()));
	}

	public void call(JcMetodo m, String... params) {
		call(m, ListString.array(params));
	}

	public void call(JcMetodo m, ListString params) {

		if (params.size() != m.parametros.size()) {
			throw new RuntimeException("qtd de parametros invalido");
		}

		add(m.getNome() + "(" + params.toString(", ") + ");");

	}

	public JcMetodo paramClass() {
		return paramClass(JcTipo.INTERROGACAO());
	}

	public JcMetodo paramClass(JcTipo generics) {
		param("classe", Class.class, generics);
		return this;
	}

	public JcMetodo forAs() {
		return forAs("as");
	}

	public JcMetodo forAs(String as) {
		tipos.add(Atributos.class);
		add("for(", Atributo.class, " a : " + as + ") {");
		return this;
	}

	public JcTipo setGenericsReverso(String nome) {
		this.genericsReverso = JcTipo.GENERICS(nome);
		return this.genericsReverso;
	}

}
