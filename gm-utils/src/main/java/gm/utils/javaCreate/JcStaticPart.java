package gm.utils.javaCreate;

class JcStaticPart extends JcMetodo {

	public JcStaticPart() {
		super("");
	}

	@Override
	public String getAssinatura() {
		return "static {";
	}

}
