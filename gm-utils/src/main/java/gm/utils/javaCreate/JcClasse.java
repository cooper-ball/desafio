package gm.utils.javaCreate;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gm.utils.classes.UClass;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.config.UConfig;
import gm.utils.files.UFile;
import gm.utils.lambda.FT;
import gm.utils.lambda.FVoidT;
import gm.utils.number.UInteger;
import gm.utils.reflection.Atributo;
import gm.utils.reflection.Atributos;
import gm.utils.reflection.ListAtributos;
import gm.utils.string.ListString;
import lombok.Getter;
import lombok.Setter;
import src.commom.utils.array.ArrayLst;
import src.commom.utils.integer.IntegerCompare;
import src.commom.utils.string.StringAfterLast;
import src.commom.utils.string.StringBeforeFirst;
import src.commom.utils.string.StringBeforeLast;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringContains;
import src.commom.utils.string.StringPrimeiraMaiuscula;
import src.commom.utils.string.StringPrimeiraMinuscula;

@Getter
public class JcClasse {

	public String getSimpleName() {
		return tipo.getSimpleName();
	}

	private final JcAnotacoes anotations = new JcAnotacoes();
	private final JcTipos interfaces = new JcTipos();
	private final ListString statesSets = new ListString();
	private final JcAtributos atributos = new JcAtributos(this);
	private final JcTipos imports = new JcTipos();
	private final List<JcMetodo> metodos = new ArrayList<>();
	private final List<JcClasse> inners = new ArrayList<>();
	private String singletonCommonsClass;
	private final String path;
	private Class<?> exceptionClass = RuntimeException.class;

	private final ListString adds = new ListString();
	private boolean sortMetodos = false;
	private boolean abstract_ = false;
	private Map<String, String> replacesTexto = new HashMap<>();
	private final JcTipo tipo;
	private JcTipo extends_;

	private boolean isInterface = false;
	private boolean formal = false;
	@Setter private boolean anyThrow = false;

	@Setter private boolean pularLinhaAntesDeCadaMetodo = true;
	private boolean js;

	private static Lst<JcClasse> instancias = new Lst<>();
	private static void addInstancia(JcClasse o) {
		if (getInstancia(o.getName()) != null) {
			throw new RuntimeException("Instancia já criada: " + o.getName());
		}
		instancias.add(o);
	}

	public static JcClasse getInstancia(String nome) {
		return instancias.unique(o -> StringCompare.eq(o.getName(), nome));
	}
	public static JcClasse getInstancia(JcTipo tipo) {
		return getInstancia(tipo.getName());
	}

	protected JcClasse(JcTipo tipo, String path) {
		this.tipo = tipo;
		this.path = path;
		addInstancia(this);
	}

	public static JcClasse build(JcTipo tipo, String path) {
		return new JcClasse(tipo, path);
	}

	public static JcClasse build(Class<?> classe) {
		JcTipo tipo = new JcTipo(classe);
		String path = StringBeforeFirst.get(UClass.javaFileName(classe), "/src/main/java/") + "/src/main/java/";
		return build(tipo, path);
	}

	public static JcClasse buildReal(Class<?> classe) {
//		TODO completar
		JcClasse o = build(classe);
		Atributos as = ListAtributos.get(o);
		for (Atributo a : as) {
			o.atributo(a);
		}
		return o;
	}

	public static JcClasse getOrBuild(String path, JcTipo tipo) {
		JcClasse o = getInstancia(tipo);
		if (o == null) {
			return build(tipo, path);
		} else {
			return o;
		}
	}

	public static JcClasse getOrBuild(String path, String pkg, String nome) {
		return getOrBuild(path, new JcTipo(pkg + "." + nome));
	}

	public static JcClasse build(String path, String pacote, String nome) {
		JcTipo tipo = new JcTipo(pacote + "." + nome);
		return build(tipo, path);
	}

	public static JcClasse build(String pacote, String nome) {
		String path = UConfig.get().getPathRaizProjetoAtual() + "java/";
		return build(path, pacote, nome);
	}

	public JcClasse extends_(JcTipo classe, String... generics) {
		extends_ = classe;
		for (String s : generics) {
			extends_.addGenerics(s);
		}
		return this;
	}

	public JcClasse extends_(JcTipo classe, JcTipo generics) {
		extends_ = classe;
		extends_.addGenerics(generics);
		return this;
	}

//	classe.getTipoJava()

	public JcClasse extends_(Class<?> classe) {
		return extends_(new JcTipo(classe), new ListString().toArray());
	}

	public JcClasse extends_(JcClasse classe, String... generics) {
		return extends_(classe.getTipo(), generics);
	}

	public JcClasse extends_(String classe) {
		return extends_(new JcTipo(classe), new ListString().toArray());
	}

	public JcClasse addImports(ListString tipos) {
		for (String s : tipos) {
			addImport(s);
		}
		return this;
	}

	public JcClasse addImports(JcTipos tipos) {
		tipos.forEach(o -> addImport(o));
		return this;
	}

	public JcClasse addImport(JcTipo tipo) {
		imports.add(tipo);
		JcTipos tipos = tipo.getTipos();
		for (JcTipo o : tipos) {
			if (canAddImport(o.getName())) {
				imports.add(o);
			}
		}
		return this;
	}

	public JcClasse addImport(JcClasse classe) {
		return addImport(classe.getTipo());
	}

	private boolean canAddImport(String s) {
		if (s.startsWith("java.lang.") || s.startsWith("*.")) {
			return false;
		} else {
			if (StringContains.is(s, "<")) {
				s = StringBeforeFirst.get(s, "<");
			}
			if (s.startsWith("$GENERICS$.")) {
				return false;
			}
			return true;
		}
	}

	public JcClasse addImport(String s) {
		if (canAddImport(s)) {
			if (StringContains.is(s, "<")) {
				s = StringBeforeFirst.get(s, "<");
			}
			if (!s.startsWith("$GENERICS$.")) {
				imports.add(s);
			}
		}
		return this;
	}

	public JcClasse addImport(Class<?> tipo) {
		return this.addImport(tipo.getName());
	}

	public JcClasse addAtributo(JcAtributo atributo) {
		atributos.add(atributo);
		return this;
	}

	public JcClasse addMetodo(JcMetodo metodo) {
		metodos.add(metodo);
		metodo.setJc(this);
		return this;
	}

	public String getFileName() {
		String p = path.endsWith("/") ? path : path + "/";
		return p + getPkg().replace(".", "/") + "/" + getSimpleName() + ".java";
	}

	public boolean exists() {
		return UFile.exists(getFileName());
	}

	public boolean hasAnnotation(Class<?> tipo) {
		return anotations.has(tipo);
	}
	public JcClasse addAnnotation(Class<? extends Annotation> tipo) {
		anotations.add(tipo);
		return this;
	}
	public JcClasse addAnnotation(Class<?> tipo, String value) {
		return this.addAnnotation(tipo.getName(), value);
	}
	public JcClasse addAnnotation(String tipo) {
		anotations.add(tipo);
		return this;
	}
	public JcClasse addAnnotation(String tipo, Object value) {
		JcAnotacao jcAnotacao = new JcAnotacao(tipo, value);
		return this.addAnnotation(jcAnotacao);
	}
	public JcClasse addAnnotation(JcAnotacao anotacao) {
		anotations.add(anotacao);
		return this;
	}
	public JcClasse addAnnotations(JcAnotacoes anotacoes) {
		anotations.add(anotacoes);
		return this;
	}
	public JcClasse addImplements(JcTipo tipo) {
		this.addImport(tipo);
		interfaces.add(tipo);
		return this;
	}
	public JcClasse addImplements(Class<?> tipo) {
		return this.addImplements(new JcTipo(tipo));
	}
	public JcClasse addImplements(String tipo) {
		return this.addImplements(new JcTipo(tipo));
	}
	public void save() {
		get().save(getFileName());
	}
	public void save(FVoidT<ListString> tratar) {
		ListString list = get();
		tratar.call(list);
		list.save(getFileName());
	}

	public ListString get() {

		for (JcAtributo o : atributos) {
			if (o.isGetter()) {

				JcMetodo m = metodo("get" + StringPrimeiraMaiuscula.exec(o.getNome()))
				.type(o.getTipo())
				.public_();

				if (o.isStatico()) {
					m.static_().return_(getSimpleName() +"."+o.getNome());
				} else {
					m.return_("this."+o.getNome());
				}

			}
			if (o.isSetter()) {
				metodo("set" + StringPrimeiraMaiuscula.exec(o.getNome()))
				.param("value", o.getTipo())
				.add("this."+o.getNome()+" = value;")
				.public_()
				;
			}
			if (o.isGetterLombok()) {
				this.addImport(Getter.class);
			}
			if (o.isSetterLombok()) {
				this.addImport(Setter.class);
			}
			if (o.isBuilder()) {
				metodo(o.getNome()).type(this).public_().param(o.getNome(), o.getTipo()).add("this."+o.getNome()+" = "+o.getNome()+";").return_("this;");
				if (o.getBuilderNome() != null) {
					metodo(o.getBuilderNome()).type(this).public_().param(o.getNome(), o.getTipo()).return_("this."+o.getNome()+"("+o.getNome()+");");
				}
			}
		}

		ListString list = new ListString();

		list.add("package "+getPkg()+";");
		list.add();

		anotations.getList().forEach(o -> addImports(o.getTipos()));
		metodos.forEach(o -> addImports(o.getTipos()));

		metodos.forEach(o -> {
			if (o.isHasThrow()) {
				addImport(exceptionClass);
				anyThrow = true;
			}
		});

		atributos.forEach(o -> {
			addImports(o.getTipos());
			o.getAnotacoes().getList().forEach(i -> {
				addImports(i.getTipos());
			});
		});

		if (singletonCommonsClass != null) {
			this.addImport(FT.class);
		}

		ListString innerStrings = new ListString();

		for (JcClasse inner : inners) {

			ListString lst = inner.get();

			while (!lst.get(0).startsWith("public class ")) {
				lst.remove(0);
			}

			String s = lst.remove(0);
			s = s.replace("public class", "private static class");
			innerStrings.add();
			innerStrings.add(s);
			innerStrings.add(lst);

			for (JcTipo tipo : inner.imports.getList()) {
				addImport(tipo);
			}
		}

		addImports(getTipo().getTipos());

		if (extends_ != null) {
			addImport(extends_);
			addImports(extends_.getTipos());
		}

		imports.sort();

		for (JcTipo tipo : imports.getList()) {
			if (tipo.isGenerics()) {
				continue;
			}
			if (tipo.isFullyQualifiedName()) {
				continue;
			}
			String s = tipo.getName();
			if (!StringBeforeLast.get(s, ".").equals(getPkg()) && !s.startsWith(".")) {

				if (s.startsWith("java.lang.")) {
					if (ListString.byDelimiter(s, ".").size() == 3) {
						//as classe s do pacote java.lang nao precisam ser importadas
						continue;
					}
				}

				list.add("import "+s+";");
			}
		}

		list.add();

		if (anotations.has()) {
			list.add(anotations.toString());
		}

		boolean abstrato = abstract_ || UList.exists(metodos, m -> m.isAbstrato());

		String s;
		if (abstrato) {
			s = "public abstract class ";
		} else if (isInterface) {
			s = "public interface ";
		} else {
			s = "public class ";
		}

		if (getSimpleName().contentEquals("ArquivoSelect")) {
			getSimpleName();
		}


		s += this.tipo;
		if (extends_ != null) {
			s += " extends " + extends_;
		}

		if (!interfaces.isEmpty()) {
			String x = "";
			for (JcTipo interf : interfaces) {
				x += ", " + StringAfterLast.get("." + interf, ".");
			}
			s += " implements" + x.substring(1);
		}

		list.add(s + " {");

		list.add(innerStrings);

		list.getMargem().inc();
		if (!atributos.isEmpty()) {

			list.add();
//			nao deve ser ordenado
//			atributos.sort((a,b) -> a.toString().compareTo(b.toString()));

			if (formal) {

				atributos.filter(o -> o.isStatico()).forEach(o -> {
					list.add(o.toListString());
					list.add();
				});

				String autowired = "org.springframework.beans.factory.annotation.Autowired";
				ListString lst = new ListString();

				String prefixo = "@Autowired " + (abstract_ ? "protected " : "");

				atributos.filter(o -> o.hasAnnotation(autowired)).forEach(o -> {
					lst.add(prefixo + o.getTipo() + " " + o.getNome() + ";");
				});

				lst.sort();
				lst.sort((a,b) -> IntegerCompare.compare(a.length(), b.length()));

				list.addAll(lst);

				atributos.filter(o -> !o.isStatico() && !o.hasAnnotation(autowired)).forEach(o -> {
					list.add(o.toListString());
					list.add();
				});

			} else {
				atributos.filter(o -> o.isStatico()).forEach(o -> list.add(o.toString()));
				atributos.filter(o -> !o.isStatico()).forEach(o -> list.add(o.toString()));
			}
		}

		if (!pularLinhaAntesDeCadaMetodo) {
			metodos.forEach(o -> o.setPularLinhaAntes(false));
		}

		if (sortMetodos) {
			metodos.sort((a, b) -> {
				int x = UInteger.compare(a.getOrdem(), b.getOrdem());
				if (x == 0) {
					x = a.getAssinatura().compareTo(b.getAssinatura());
				}
				return x;
			});
		} else {
			metodos.sort((a, b) -> UInteger.compare(a.getOrdem(), b.getOrdem()));
		}

		metodos.forEach(o -> list.add(o.get()));

		if (!statesSets.isEmpty()) {
			list.add();
			list.add(statesSets);
		}

		if (!adds.isEmpty()) {
			list.add();
			list.add(adds);
		}

		if (singletonCommonsClass != null) {
			list.add();
			list.add("private static "+getSimpleName()+" instance;");
			if (abstrato) {
				list.add("public static FT<"+getSimpleName()+"> newInstance;");
			} else {
				list.add("public static FT<"+getSimpleName()+"> newInstance = () -> new "+getSimpleName()+"();");
			}
			list.add("public static "+getSimpleName()+" getInstance() {");
			list.add("	if ("+StringAfterLast.get(singletonCommonsClass, ".")+".isEmpty(instance)) {");
			list.add("		instance = newInstance.call();");
			list.add("	}");
			list.add("	return instance;");
			list.add("}");
		}

		list.getMargem().dec();
		list.add();

		list.add("}");

		list.removeDoubleWhites();
		list.getMargem().set(0);
		list.rtrim();
		list.replace(ListString.array("\n", "}"), "}");
		list.juntarFimComComecos("{", "}", "");
		list.juntarFimComComecos("}", "else", " ");
		list.juntarFimComComecos("}", "catch", " ");
		list.rtrim();
		if (anyThrow) {
			list.replaceTexto("[exceptionClass]", exceptionClass.getSimpleName());
		}
		list.replaceTexto(" + \"\")", ")");
		list.replaceTexto("[TAB]", "\t");

		replacesTexto.forEach((k,v) -> list.replaceTexto(k, v));

		return list;

	}

	public JcMetodo construtor() {
		JcMetodo o = metodo(getSimpleName());
		o.setConstrutor(true);
		return o;
	}
	public JcMetodo staticPart() {
		JcMetodo o = new JcStaticPart();
		addMetodo(o);
		return o;
	}
	public JcMetodo metodo(String nome) {
		JcMetodo o = new JcMetodo(nome);
		addMetodo(o);
		return o;
	}
	public JcAtributo constant(String nome, Class<?> tipo, Class<?> generic, String inicializacao) {
		return this.constant(nome, tipo, inicializacao).generics(generic);
	}
	public JcAtributo constantString(String nome, String value) {
		return this.constant(nome, String.class, "\""+value+"\"");
	}
	public static String toInicializacao(ListString list) {
		list.add(0, "");
		list.addLeft("\t\t");
		list.add("\t");
		return list.toString("\n");
	}
	public JcAtributo constant(String nome, Class<?> tipo, ListString inicializacao) {
		return this.constant(nome, tipo, JcClasse.toInicializacao(inicializacao));
	}
	public JcAtributo constant(String nome, Class<?> tipo, String inicializacao) {
		return this.constant(nome, new JcTipo(tipo), inicializacao);
	}
	public JcAtributo constant(String nome, Class<?> tipo, Class<?> generics, ListString inicializacao) {
		return this.constant(nome, new JcTipo(tipo, generics), JcClasse.toInicializacao(inicializacao));
	}
	public JcAtributo constant(String nome, JcTipo tipo, ListString inicializacao) {
		return this.constant(nome, tipo, JcClasse.toInicializacao(inicializacao));
	}
	public JcAtributo constant(String nome, JcTipo tipo, String inicializacao) {
		return atributo(nome, tipo).constant(inicializacao);
	}
	public JcAtributo atributo(Atributo a) {
		return atributo(a.nome(), a.getType());
	}
	public JcAtributo atributo(String nome, String tipo) {
		JcAtributo o = new JcAtributo(nome, tipo);
		addAtributo(o);
		return o;
	}
	public JcAtributo atributo(String nome, JcClasse tipo) {
		JcAtributo o = new JcAtributo(nome, tipo);
		addAtributo(o);
		return o;
	}
	public JcAtributo atributo(String nome, JcTipo tipo) {
		JcAtributo o = new JcAtributo(nome, tipo);
		addAtributo(o);
		return o;
	}
	public JcAtributo atributo(String nome, Class<?> tipo, Class<?>... generics) {
		return atributo(nome, new JcTipo(tipo, generics));
	}
	public JcAtributo aString(String nome) {
		return atributo(nome, String.class);
	}
	public JcAtributo aInteger(String nome) {
		return atributo(nome, Integer.class);
	}
	public JcAtributo atributo(String nome, Class<?> tipo) {
		JcAtributo o = new JcAtributo(nome, tipo);
		addAtributo(o);
		return o;
	}
	public JcClasse add(String s) {
		adds.add(s);
		return this;
	}
	public JcClasse singletonCommonsClass(String classe) {
		this.addImport(classe);
		singletonCommonsClass = classe;
		return this;
	}
	public JcClasse singletonCommonsClass(Class<?> classe) {
		return this.singletonCommonsClass(classe.getName());
	}
	public void delete() {
		UFile.delete(getFileName());
	}
	public JcMetodo main() {
		return metodo("main").public_().static_().paramArray("args", String.class);
	}
	public JcClasse sortMetodos() {
		sortMetodos = true;
		return this;
	}
	public JcClasse lombokGetter() {
		return this.addAnnotation(Getter.class);
	}
	public JcClasse lombokSetter() {
		return this.addAnnotation(Setter.class);
	}
	public JcClasse setAbstract() {
		abstract_ = true;
		return this;
	}
	public void setExceptionClass(Class<?> classe) {
//		addImport(classe);
		exceptionClass = classe;
	}
	public JcAtributo getAtributo(String s) {
		return UList.filterUnique(atributos, o -> StringCompare.eq(o.getNome(), s));
	}
	public String getName() {
		return getPkg() + "." + getSimpleName();
	}
	public void addInner(JcClasse inner) {
		inners.add(inner);
	}
	public void remove(JcMetodo o) {
		metodos.remove(o);
	}
	@Deprecated
	public String getNome() {
		return getSimpleName();
	}

	public String getPkg() {
		return tipo.getPkg();
	}

	@Deprecated
	public String getPacote() {
		return getPkg();
	}

	public JcClasse addGenerics(JcTipo o) {
		this.tipo.addGenerics(o);
		return this;
	}

	@Override
	public String toString() {
		return getName() + " / " + getPath();
	}

	public JcClasse js() {
		this.js = true;
		return this;
	}

	public JcAtributo mapSS(String nome) {
		return map(nome, String.class, String.class);
	}

	public JcAtributo mapSO(String nome) {
		return map(nome, String.class, Object.class);
	}

	public JcAtributo map(String nome, Class<?> a, Class<?> b) {
		if (js) {
			return atributo(nome, js.map.Map.class).generics(a).generics(b);
		} else {
			return atributo(nome, Map.class).generics(a).generics(b);
		}
	}

	public JcAtributo array(String nome, Class<?> generics) {
		return atributo(nome, ArrayLst.class).generics(generics);
	}

	public JcAtributo constantArrayString(String nome, ListString itens) {
		String s = "ArrayLst.build(\"" + itens.toString("\", \"") + "\")";
		return constant(nome, ArrayLst.class, s).generics(String.class);
	}
	public JcAtributo constantArray(Class<?> classe, String nome, ListString itens) {
		return constantArray(new JcTipo(classe), nome, itens);
	}
	public JcAtributo constantArray(JcTipo tipo, String nome, ListString itens) {
		String s = "ArrayLst.build(\n\t" + itens.toString(",\n\t") + "\n)";
		JcAtributo o = constant(nome, ArrayLst.class, s);
		o.setGenerics(tipo);
		return o;
	}

	public JcClasse setFormal(boolean formal) {
		this.formal = formal;
		return this;
	}

	public void setIsInterface(boolean value) {
		this.isInterface = value;
	}

	public String autowired(Class<?> classe) {
		componentSpring();
		String s = classe.getSimpleName();
		JcAtributo a = atributo(StringPrimeiraMinuscula.exec(s), classe);
		a.addAnotacao("org.springframework.beans.factory.annotation.Autowired");
		a.default_();
		return s;
	}

	public JcClasse componentSpring() {
		addAnnotation("org.springframework.stereotype.Component");
		return this;
	}

	public JcMetodo initSpring() {
		componentSpring();
		return metodo("init").public_().addAnotacao("javax.annotation.PostConstruct");
	}

}
