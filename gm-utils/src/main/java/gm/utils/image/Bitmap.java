package gm.utils.image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import gm.utils.exception.UException;
import lombok.Getter;

@Getter
public class Bitmap {

	private BufferedImage image;
	private String caminho;

	public static void main(String[] args) {
		Bitmap bitmap = new Bitmap("/opt/desen/tmp/x.png");
		bitmap.removerUltimasLinhasEmBranco();
		bitmap.saveAs("/opt/desen/tmp/x1.png");
//		System.out.println(new RGB(255, 255, 255));

	}

	public Bitmap(BufferedImage image) {
		this.image = image;
	}
	public Bitmap(int width, int height) {
		this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
//		this(new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR));
	}

	public Bitmap(String caminho) {
		this.caminho = caminho;
		try {
			this.image = ImageIO.read(new File(caminho));
		} catch (Exception e) {
			throw UException.runtime(e);
		}
	}
	public int get(int x, int y) {
		if (this.invalid(x, y)) return 0;
		return this.image.getRGB(x, y);
	}
	private boolean invalid(int x, int y) {
		return x < 0 || y < 0 || x >= this.getWidth() || y >= this.getHeight();
	}
	public void set(int x, int y, int rgb) {
		this.image.setRGB(x, y, rgb);
	}
	public void save() {
		this.saveAs(this.caminho);
	}
	public void saveAs(String caminho) {
//		this.saveAs(caminho, "jpg");
		this.saveAs(caminho, "png");
	}
	public void saveAs(String caminho, String extensao) {
		try {
			ImageIO.write(this.image, extensao, new File(caminho));
		} catch (Exception e) {
			throw UException.runtime(e);
		}
	}
	public void removerUltimasLinhasEmBranco() {
		int y = this.getHeight()-1;
		while (this.rowIsBranco(y)) y--;
		this.image = this.image.getSubimage(0, 0, this.getWidth(), y);
	}
	public boolean rowIsBranco(int y) {
		for (int x = 0; x < this.getWidth(); x++) {
			if (!this.isBranco(x, y)) {
				return false;
			}
		}
		return true;
	}
	private boolean isBranco(int x, int y) {
		return this.get(x, y) == Color.WHITE.getRGB();
	}
	public int getWidth() {
		return this.image.getWidth();
	}
	public int getHeight() {
		return this.image.getHeight();
	}
	public void setWidth(int width) {
		this.setSize(width, this.getHeight());
	}
	public void setHeight(int height) {
		this.setSize(this.getWidth(), height);
	}
	public void setSize(int width, int height) {
		java.awt.Image img = this.image.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
		this.image = new BufferedImage(width, height, this.image.getType());
		Graphics2D g2d = this.image.createGraphics();
		g2d.drawImage(img, 0, 0, null);
		g2d.dispose();
	}
}
