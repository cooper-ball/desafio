package gm.utils.image;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import gm.utils.comum.USystem;
import gm.utils.files.UFile;
import gm.utils.number.UInteger;
import gm.utils.number.UNumber;
import src.commom.utils.string.StringParse;

public class PrintScreen {

	static Robot robot;

	public static void main(String[] args) {
		try {
			robot = new Robot();
			print(0);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void exec() {
		robot.mouseMove(350, 380);
		int mask = InputEvent.BUTTON1_DOWN_MASK;
		sleep();

		for (int i = 0; i < 1; i+=5) {

			robot.mousePress(mask);
			sleep();
			robot.mouseRelease(mask);
			key(KeyEvent.VK_DELETE);
			String s = StringParse.get(i);
			while (!s.isEmpty()) {
				int x = UInteger.toInt(s.substring(0,1));
				s = s.substring(1);
				keyNumber(x);
			}
			key(KeyEvent.VK_ENTER);
			print(i);
		}

	}

	private static void print(int i) {
		String file = "/tmp/pie"+UNumber.format00(i, 3)+".jpg";

		UFile.delete(file);
		USystem.sleepSegundos(1);
		int w = 84;
		BufferedImage bi = robot.createScreenCapture(new Rectangle(500, 498, w, w));

		try {
			ImageIO.write(bi, "jpg", new File(file));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private static void sleep() {
		USystem.sleepMiliSegundos(75);
	}

	private static void keyNumber(int i) {
		key(KeyEvent.VK_0+i);
	}

	private static void key(int i) {
		sleep();
		robot.keyPress(i);
		sleep();
		robot.keyRelease(i);
		sleep();
	}

}
