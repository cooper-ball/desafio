/*
exemplo:

import javax.persistence.Convert;
@Convert(converter=JpaConverterSN.class)
private Boolean campo;
*/

package gm.utils.jpa.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import gm.utils.comum.UBoolean;

@Converter
public class JpaConverterSN implements AttributeConverter<Boolean, String> {
	
	@Override
	public String convertToDatabaseColumn(Boolean value) {
		
		if (value == null) {
			return null;
		} else if (UBoolean.isTrue(value)) {
			return "S";
		} else if (UBoolean.isFalse(value)) {
			return "N";
		} else {
			return null;
		}
		
	}

	@Override
	public Boolean convertToEntityAttribute(String value) {
		
		if (value == null) {
			return null;
		} else if (UBoolean.isTrue(value)) {
			return true;
		} else if (UBoolean.isFalse(value)) {
			return false;
		} else {
			return null;
		}
		
	}

}
