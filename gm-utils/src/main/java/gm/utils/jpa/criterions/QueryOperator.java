package gm.utils.jpa.criterions;

import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import gm.utils.comum.IWrapper;
import gm.utils.comum.UAssert;
import gm.utils.jpa.SqlNativeValue;
import gm.utils.string.ListString;
import lombok.Getter;
import lombok.Setter;
import src.commom.utils.string.StringAfterFirst;

@Getter @Setter
public abstract class QueryOperator {

	protected boolean not;
	private String campo;
	private Object value;

	public QueryOperator(String campo) {
		this(campo, null, true);
	}

	public ListString getCampos() {
		ListString list = new ListString();
		list.add(campo);
		return list;
	}

	public QueryOperator(String campo, Object value, boolean valorPodeSerNulo) {
		UAssert.notEmpty(campo, "O valor não pode ser nulo");
		this.campo = campo;
		this.value = value;
		if (!valorPodeSerNulo) {
			UAssert.notEmpty(getValue(), "O valor não pode ser nulo");
		}
	}

	public Object getRealValue() {
		return value;
	}

	public Object getValue() {

		if (value == null) {
			return null;
		} else if (value instanceof IWrapper) {
			IWrapper<?> wrapper = (IWrapper<?>) value;
			return wrapper.unwrapper();
		} else {
			return value;
		}

	}

	public final Criterion getCriterion() {
		Criterion c = criterion();
		if (not) {
			return Restrictions.not(c);
		} else {
			return c;
		}
	}

	public QueryOperator not() {
		setNot(true);
		return this;
	}

	protected abstract Criterion criterion();

	public final String getNativo() {
		String s = nativo();
		if (not) {
			s = "not ( " + s + " )";
		}
		return s;
	}

	protected abstract String nativo();
//	protected String nativo() {
//		throw UException.runtime("Não implementado: " + getClass().getSimpleName());
//	}

	@Override
	public String toString() {
		String s = getClass().getSimpleName();
		s = StringAfterFirst.get(s, "_");
		s = getCampo() + " " + s + " " + getValue();
		if (not) {
			s = "not (" + s + ")";
		}
		return s;
	}
	public final String getNativeValue() {
		return SqlNativeValue.get( getValue() );
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (super.equals(obj)) {
			return true;
		}
		QueryOperator o = (QueryOperator) obj;
		if (o.isNot() != isNot()) {
			return false;
		}
		if (o.getCampo() != getCampo()) {
			return false;
		}
		if (o.getValue() != getValue()) {
			return false;
		}
		return true;
	}
	@Override
	public int hashCode() {
		Object v = getValue();
		int prime = 31;
		int result = 1;
		result = prime * result + ((campo == null) ? 0 : campo.hashCode());
		result = prime * result + (not ? 1231 : 1237);
		result = prime * result + ((v == null) ? 0 : v.hashCode());
		return result;
	}

	private From<?, ?> root;

	public final <TT> Predicate getPredicate(CriterioQuery<?> cq) {

		String field = cq.trataCampo(getCampo());

		if (not) {
			return getPredicateFalse(cq, field);
		} else {
			return getPredicateTrue(cq, field);
		}
	}

//	protected static String getField(From<?, ?> path, String campo) {
//		if (campo.contains(".")) {
//			ListString campos = ListString.byDelimiter(campo, ".");
//			campo = campos.removeLast();
//			for (String s : campos) {
//				path = path.join(s, JoinType.LEFT);
//			}
//		}
//		return campo;
//	}

	protected abstract Predicate getPredicateTrue(CriterioQuery<?> cq, String campo);

	protected Predicate getPredicateFalse(CriterioQuery<?> cq, String campo) {
		return cq.getCb().not(getPredicateTrue(cq, campo));
	}

}
