package gm.utils.jpa;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Table;

import gm.utils.exception.UException;
import src.commom.utils.string.StringEmpty;

public class USchema {

	public static String SCHEMA_DEFAULT;

	private final ConexaoJdbc con;
	private final DriverJDBC driver;
	private static Map<Class<?>, String> map = new HashMap<>();

	public USchema(ConexaoJdbc con) {
		this.con = con;
		this.driver = con.getDriver();
	}

	public void create(String nome) {
		if (!this.exists(nome)) {
			this.con.exec("create schema " + nome);
//			con.criarViewsParaFuncionamento();
		}
	}

	public boolean exists(String nome) {
		return this.getId(nome) != null;
	}

	public void create(Table table) {
		this.create(USchema.get(table));
	}

	public static String get(Class<?> classe) {
		String s = map.get(classe);
		if (s != null) {
			return s;
		}

		Table table = classe.getAnnotation(Table.class);
		if (table == null) {
			throw UException.runtime("A classe "+ classe.getSimpleName() + " está sem a anotacao @Table");
		}
		s = get(table);
		map.put(classe, s);
		return s;
	}

	public static String get(Table table) {
		String s = table.schema();
		if (StringEmpty.is(s)) {
			if (SCHEMA_DEFAULT == null) {
				throw UException.runtime("table schema is empty");
			} else {
				return SCHEMA_DEFAULT;
			}
		}
		return s;
	}

	public Integer getId(String schema) {
		if (this.driver == DriverJDBC.PostgreSQL) {
			return this.con.selectInt("select 1 from pg_catalog.pg_namespace where lower(nspname) = '" + schema.toLowerCase() + "'");
		} else if (this.driver == DriverJDBC.MSSQLServer) {
			return this.con.selectInt("select schema_id from sys.schemas where lower(name) = '" + schema.toLowerCase() + "'");
		} else {
			throw UException.runtime("driver nao configurado: " + this.driver);
		}
	}

}
