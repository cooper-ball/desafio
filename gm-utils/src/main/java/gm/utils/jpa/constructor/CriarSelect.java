package gm.utils.jpa.constructor;
import java.util.Calendar;

import gm.utils.classes.UClass;
import gm.utils.javaCreate.JcAtributo;
import gm.utils.javaCreate.JcAtributos;
import gm.utils.javaCreate.JcClasse;
import gm.utils.javaCreate.JcMetodo;
import gm.utils.javaCreate.JcParametro;
import gm.utils.javaCreate.JcTipo;
import gm.utils.jpa.criterions.Criterio;
import gm.utils.jpa.select.SelectBase;
import gm.utils.jpa.select.SelectBoolean;
import gm.utils.jpa.select.SelectDate;
import gm.utils.jpa.select.SelectInteger;
import gm.utils.jpa.select.SelectLong;
import gm.utils.jpa.select.SelectNumeric1;
import gm.utils.jpa.select.SelectString;
import gm.utils.lambda.FTT;
import lombok.Setter;

@Setter
public class CriarSelect {

	public static JcClasse get(
		String pathJavaMain,
		JcClasse model,
		FTT<JcTipo, JcTipo> getSelect
	) {

		JcTipo ORIGEM = JcTipo.GENERICS("ORIGEM");
		String sn = model.getSimpleName();

		JcClasse jc = JcClasse.build(getSelect.call(model.getTipo()), pathJavaMain);
		jc.addGenerics(ORIGEM);

		{
			JcTipo o = new JcTipo(SelectBase.class);
			o.addGenerics(ORIGEM);
			o.addGenerics(model);
			o.addGenerics(new JcTipo(jc, ORIGEM));
			jc.extends_(o);
		}

		jc.addImport(Criterio.class);
		jc.addImport(SelectBase.class);
		jc.addImport(model);

		String n = sn;

		JcAtributos as = model.getAtributos().getPersistAndVirtuais();
//		Atributos as = UAtributos.getPersistAndVirtuais(classe);

		jc.construtor()
		.public_()
		.param("origem", ORIGEM)
		.param(new JcParametro("criterio", Criterio.class).generics("?"))
		.param("prefixo", String.class)
		.add("super(origem, criterio, prefixo, " + n + ".class);");

		if (as.contains("ordem") || as.contains("nome")) {
			JcMetodo m = jc.metodo("beforeSelect").override();
			if (as.contains("ordemHierarquia")) {
				m.add("ordemHierarquia().asc();");
			}
			if (as.contains("ordem")) {
				m.add("ordem().asc();");
			}
			if (as.contains("nome")) {
				m.add("nome().asc();");
			}
		}
		/*
		if (pojo) {
			jc.metodo("pojo").public_().type(jc.getPkg() + "."+n+"Pojo").add("return new " + n + "Pojo(list());");
		}

		if (update) {
			jc.metodo("update").public_().type(jc.getPkg() + "."+n+"Update").add("return new " + n + "Update(this);");
		}
		*/
		/*
		if (classeDia != null) {
			Class<?> fwTabela = UClass.getClass("gm.fw.model.Tabela");
			if (classe.equals(fwTabela)) {
				Class<?> fwEntidade = UClass.getClass("gm.fw.support.Entidade");
				jc.addImport(fwEntidade);
				Class<?> fwTabelaBoo = UClass.getClass("gm.fw.boos.TabelaBoo");
				jc.addImport(fwTabelaBoo);

				JcTipo tipo = new JcTipo(Class.class);
				tipo.addGenerics(JcTipo.INTERROGACAO().setSuperClass(new JcTipo(fwEntidade, JcTipo.INTERROGACAO())));

				jc.metodo("eq").public_().type(ORIGEM)
				.param("classe", tipo)
				.add("return eq(TabelaBoo.get(classe));");
			}
		}

		/**/

		for (JcAtributo a : as) {
			String s = a.getNome();
			String nomeValue = a.getNomeValue();

			JcMetodo m = jc.metodo(s).public_();

			if (a.is(Integer.class)) {
				m.type(SelectInteger.class).add("return new SelectInteger<>(this, \"" + nomeValue + "\");");
			} else if (a.is(Long.class)) {
				m.type(SelectLong.class).add("return new SelectLong<>(this, \"" + nomeValue + "\");");
			} else if (a.is(Boolean.class)) {
				m.type(SelectBoolean.class).add("return new SelectBoolean<>(this, \"" + nomeValue + "\");");
			} else if (a.isString()) {
				m.type(SelectString.class).add("return new SelectString<>(this, \"" + nomeValue + "\");");
			} else if (a.isFk()) {
				JcTipo select = getSelect.call(a.getTipo());
				m.type(select);
				m.add("return new "+select.getSimpleName()+"<>(this, getC(), getPrefixo() + \"." + s + "\" );");
			} else if (a.isDate()) {
				m.type(SelectDate.class);
				m.add("return new SelectDate<>(this, \"" + nomeValue + "\");");
			} else if (a.isNumeric()) {
				int f = a.digitsFraction();
				String cs = SelectNumeric1.class.getName().replace("SelectNumeric1", "SelectNumeric" + f);
				Class<?> type = UClass.getClassObrig(cs);
				m.type(type);
				m.add("\t\treturn new SelectNumeric"+f+"<>(this, \""+nomeValue+"\");");
			} else {
				throw new RuntimeException("??? " + a);
			}

			JcTipo genericsRetorno = new JcTipo(jc.getPkg() + "." + n + "Select");
			genericsRetorno.addGenerics("?");
			m.addGenericsRetorno(genericsRetorno);

		}

		JcAtributos datas = as.getWhereType(Calendar.class);
		if (datas.contains("inicio") && datas.contains("fim")) {
			JcMetodo m = jc.metodo("entreInicioEFim").public_();
			m.type(jc.getPkg() + "."+n+"Select");
			m.addGenericsRetorno(ORIGEM);
			m.param("data", Calendar.class);
			m.add("inicio().menorOuIgual(data);");
			m.add("fim().maiorOuIgual(data);");
			m.add("return this;");
		}

		/*

		if (classeDia != null) {
			JcAtributos dias = as.getWhereType(classeDia);
			if (dias.contains("inicio") && dias.contains("fim")) {
				JcMetodo m = jc.metodo("entreInicioEFim").public_();
				m.addGenericsRetorno(ORIGEM);
				m.type(n+"Select");
				m.param("data", Data.class);
				m.add("inicio().menorOuIgual(data);");
				m.add("fim().maiorOuIgual(data);");
				m.add("return this;");
			}
		}

		/**/

		/*
		JcAtributo pai = as.get("pai");
		if (pai != null && pai.typeIs(classe)) {
			JcAtributo hierarquia = as.get("hierarquia");
			if (hierarquia != null && hierarquia.typeIs(String.class)) {
				jc.addImport(List.class);
				JcMetodo m = jc.metodo("hierarquia_of").public_();

				JcTipo tipo = new JcTipo(pacoteOndeAsClassesSeraoCriadas + "." + n + "Select");
				tipo.addGenerics(JcTipo.INTERROGACAO());
				m.type(tipo);

				m.param("list", List.class, classe);
				m.add("while (!list.isEmpty()) {");
				m.add(classe.getSimpleName()+" o = list.remove(0);");
				m.add("hierarquia().like(o.getHierarquia()+\";\");");
				m.add("if (!list.isEmpty()) or();");
				m.add("}");
				m.add("return this;");
			}
		}
		/**/

		JcTipo tipo = new JcTipo(jc.getPkg() + "."+n+"Select");
		tipo.addGenerics("?");

		jc.metodo("asc").public_().type(tipo).add("return id().asc();");

		/*

		if (classeDia != null && classe.equals(classeDia)) {

			jc.addImport(pacoteOndeAsClassesSeraoCriadas + ".FwSelect");
			jc.addImport(Calendar.class);
			jc.addImport(Data.class);

			jc.metodo("min").public_().type(classeDia)
			.add("Integer id = id().min();")
			.add("if (id == null) return null;")
			.add("return byId(id);");

			jc.metodo("max").public_().type(classeDia)
			.add("Integer id = id().max();")
			.add("if (id == null) return null;")
			.add("return byId(id);");

			jc.metodo("maior").public_().type(ORIGEM).param("dia", classeDia)
			.add("id().maior(dia.getId());")
			.add("return getOrigem();");

			jc.metodo("maiorOuIgual").public_().type(ORIGEM).param("dia", classeDia)
			.add("id().maiorOuIgual(dia.getId());")
			.add("return getOrigem();");

			jc.metodo("menorOuIgual").public_().type(ORIGEM).param("dia", classeDia)
			.add("id().menorOuIgual(dia.getId());")
			.add("return getOrigem();");

			jc.metodo("maiorOuIgual").public_().type(ORIGEM).param("dia", Calendar.class)
			.add("data().maiorOuIgual(dia);")
			.add("return getOrigem();");

			jc.metodo("menorOuIgual").public_().type(ORIGEM).param("dia", Calendar.class)
			.add("data().menorOuIgual(dia);")
			.add("return getOrigem();");

			jc.metodo("maiorOuIgual").public_().type(ORIGEM).param("dia", Data.class)
			.add("data().maiorOuIgual(dia);")
			.add("return getOrigem();");

			jc.metodo("menorOuIgual").public_().type(ORIGEM).param("dia", Data.class)
			.add("data().menorOuIgual(dia);")
			.add("return getOrigem();");

			jc.metodo("menor").public_().type(ORIGEM).param("dia", classeDia)
			.add("id().menor(dia.getId());")
			.add("return getOrigem();");

		}
		/**/

		return jc;

//		jc.save();

	}

}
