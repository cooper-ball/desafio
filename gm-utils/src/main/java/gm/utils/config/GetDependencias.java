package gm.utils.config;

import gm.utils.string.ListString;
import src.commom.utils.string.StringAfterFirst;
import src.commom.utils.string.StringBeforeFirst;
import src.commom.utils.string.StringBeforeLast;

public class GetDependencias {

	private ListString poms = new ListString();
	private ListString groups = ListString.array("react","reacts","gm");
	private ListString projetos = new ListString();

	public static ListString get() {
		return new GetDependencias().projetos;
	}

	private GetDependencias() {
		String pom = System.getProperty("user.dir").trim() + "/pom.xml";
		exec(pom);
		poms.remove("/opt/desen/gm/cs2019/gm.ast2/pom.xml");
		projetos = poms.mapString(s -> StringBeforeLast.get(s, "/pom.xml"));
	}

	private void exec(String pom) {

		if (poms.contains(pom)) {
			return;
		}

		poms.add(pom);

		ListString list = new ListString().load(pom);

		list.trimPlus();

		while (!list.get(0).equalsIgnoreCase("<dependencies>")) {
			list.remove(0);
		}

		while (!list.getLast().equalsIgnoreCase("</dependencies>")) {
			list.removeLast();
		}

		list.remove(0);
		list.removeLast();

		list.removeIf(s -> !s.startsWith("<groupId>") && !s.startsWith("<artifactId>"));

		ListString itens = new ListString();

		while (!list.isEmpty()) {

			String groupId = get(list.remove(0));
			String artifactId = get(list.remove(0));

			if (groups.contains(groupId)) {
				if (!projetos.contains(artifactId)) {
					itens.add(artifactId);
				}
//			} else {
//				System.out.println(groupId);
			}

		}

		for (String s : itens) {
			projetos.add(s);
			ListString pomClean = new ListString().load("/opt/desen/gm/cs2019/cleanInstall/" + s + "/pom.xml");
			pomClean.trimPlus();
			s = pomClean.filter(o -> o.startsWith("<module>")).get(0);
			s = get(s);
			s = StringAfterFirst.obrig(s, "../../");
			s = "/opt/desen/gm/cs2019/" + s + "/pom.xml";
			exec(s);
		}

	}

	private static String get(String s) {
		s = StringAfterFirst.get(s, ">");
		s = StringBeforeFirst.get(s, "<");
		return s;
	}


}
