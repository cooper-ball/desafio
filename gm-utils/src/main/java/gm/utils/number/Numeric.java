package gm.utils.number;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.nevec.rjm.BigDecimalMath;

import gm.utils.classes.UClass;
import gm.utils.comum.UGenerics;
import gm.utils.comum.ULog;
import lombok.Getter;
import src.commom.utils.string.StringAfterFirst;
import src.commom.utils.string.StringBeforeFirst;
import src.commom.utils.string.StringContains;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;
import src.commom.utils.string.StringRight;
@Getter
public class Numeric<T extends Numeric<T>> {

	BigDecimal valor;
	private int casas;

	private Class<T> classe;

	public Class<T> getClasse() {
		if (this.classe == null) {
			this.classe = UGenerics.getGenericClass(this);
		}
		return this.classe;
	}

	public Numeric(int casas) {
		this.casas = casas;
		this.valor = BigDecimal.ZERO;
	}
	public Numeric(String s, int casas) {
		this( UBigDecimal.toBigDecimal(s, casas), casas );
	}
	public Numeric(int inteiros, int centavos, int casas) {
		this(inteiros + "." + UNumber.format00(centavos, casas), casas);
	}
	public Numeric(BigDecimal valor, int casas) {
		this(casas);
		this.setValor(valor);
	}
	public Numeric(Double value, int casas) {
		this( UBigDecimal.toBigDecimal(value, casas), casas );
	}
	public Numeric(Integer value, int casas) {
		this( UBigDecimal.toBigDecimal(value, casas), casas );
	}
	public T inc(Numeric<?> x){
		return this.inc(x.getValor());
	}
	public T dec(Numeric<?> x){
		return this.dec(x.getValor());
	}
	public void inc(Integer x){
		this.add(UBigDecimal.toBigDecimal(x));
	}
	public void inc(Double x){
		this.add(UBigDecimal.toBigDecimal(x));
	}
	public void add(Integer value){
		if (value == null) {
			return;
		}
		BigDecimal money = UBigDecimal.toBigDecimal(value, this.casas);
		this.add(money);
	}
	public void add(Double value){
		if (value == null) {
			return;
		}
		BigDecimal money = UBigDecimal.toBigDecimal(value, this.casas);
		this.add(money);
	}
	public void add(BigDecimal value) {
		if (value != null){
			this.valor = this.valor.add(value);
		}
	}
	public void add(Numeric<?> o) {
		this.add(o.valor);
	}
	public void menosIgual(Numeric<?> x) {
		this.menosIgual( x.getValor() );
	}
	public void menosIgual(BigDecimal valor){
		this.setValor( this.menos(valor) );
	}
	@Override
	public String toString() {

		if (this.isZeroOrEmpty()) {
			return "0," + UNumber.format00(0, this.casas);
		}

		String s = this.valor.toString().toLowerCase();

		if (StringContains.is(s, "e")) {
			s = this.toDouble().toString();
		}

		String ints = StringBeforeFirst.get(s, ".");
		s = "," + StringAfterFirst.get(s, ".");

		if (StringEmpty.is(ints)) {
			ints = "0";
		} else while (ints.length() > 3) {
			s = "." + StringRight.get(ints, 3) + s;
			ints = StringRight.ignore(ints, 3);
		}
		s = ints + s;
		return s;
	}

	public String toStringPonto() {
		String s = this.toString();
		String ints = StringBeforeFirst.get(s, ",");
		String decimais = StringAfterFirst.get(s, ",");
		while (StringLength.get(decimais) < this.casas) {
			decimais += "0";
		}

		decimais = StringLength.max(decimais, this.casas);

		return ints.replace(".", "") + "." + decimais;
	}

	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;
		result = prime * result + this.casas;
		result = prime * result + ((this.classe == null) ? 0 : this.classe.hashCode());
		result = prime * result + ((this.valor == null) ? 0 : this.valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o) {
		BigDecimal money = UBigDecimal.toBigDecimal(o, this.casas);
		BigDecimal result = this.valor.subtract(money);
		if (result.equals(BigDecimal.ZERO)) {
			return true;
		}
		String s = result.toString();
		s = s.replace("0", "");
		s = s.replace(".", "");
		return s.isEmpty();
	}
	public T dividido(GetNumeric valor) {
		return this.dividido(valor.valor());
	}
	public T dividido(Double valor) {
		return this.dividido( UBigDecimal.toBigDecimal(valor, this.casas) );
	}
	public T dividido(Numeric<?> valor) {
		return this.dividido(valor.valor);
	}
	public T dividido(int peso) {
		if (this.isZero()) {
			return this.newT(0.);
		}
		return this.dividido( UBigDecimal.toBigDecimal(peso, this.casas) );
	}

	public T dividido(BigDecimal divisor) {
		BigDecimal resultado = this.valor.divide(divisor, UNumber.ROUNDING_MODE);
		return this.newT(resultado);
	}
	public void setValor(Double valor){
		this.setValor( UBigDecimal.toBigDecimal(valor, this.casas) );
	}
	public void setValor(BigDecimal valor){
		if (valor == null) {
			valor = BigDecimal.ZERO;
		} else {
			valor = valor.setScale(this.casas, UNumber.ROUNDING_MODE);
		}
		this.valor = valor;
	}
	public void setValor(Integer valor){
		this.setValor(UBigDecimal.toMoney(valor));
	}
	public void setValor(Numeric<?> valor){
		this.setValor(valor.getValor());
	}
	public Double toDouble(){
		return this.valor.doubleValue();
	}
	public Integer toInt() {
		return this.valor.intValue();
	}
	public boolean menor(Numeric<?> x) {
		return this.menor(x.getValor());
	}
	public boolean maior(Numeric<?> x) {
		return this.maior(x.getValor());
	}
	public boolean menor(GetNumeric x) {
		return this.menor(x.valor());
	}
	public boolean maior(GetNumeric x) {
		return this.maior(x.valor());
	}
	public boolean menor(Integer x) {
		return this.menor(UBigDecimal.toBigDecimal(x, this.casas));
	}
	public boolean menor(BigDecimal x) {
		return this.valor.compareTo(x) < 0;
	}
	public boolean maior(Double x) {
		return this.maior( UBigDecimal.toBigDecimal(x, this.casas) );
	}
	public boolean maior(Long x) {
		return this.maior( UBigDecimal.toBigDecimal(x, this.casas) );
	}
	public boolean maior(Integer x) {
		return this.maior( UBigDecimal.toBigDecimal(x, this.casas) );
	}
	public boolean menorOuIgual(GetNumeric x) {
		return this.menorOuIgual(x.valor());
	}
	public boolean menorOuIgual(BigDecimal x) {
		return this.eq(x)||this.menor(x);
	}
	public boolean menorOuIgual(Integer x) {
		return this.eq(x)||this.menor(x);
	}
	public boolean entre(Integer x, Integer y) {
		return this.maiorOuIgual( UInteger.menor(x, y) ) && this.menorOuIgual( UInteger.maior(x, y) );
	}
	public boolean maior(BigDecimal x) {
		return this.valor.compareTo(x) > 0;
	}
	public boolean maiorOuIgual(Numeric<?> x) {
		return this.maiorOuIgual(x.getValor());
	}
	public boolean maiorOuIgual(GetNumeric x) {
		return this.maiorOuIgual(x.valor().getValor());
	}
	public boolean menorOuIgual(Numeric<?> x) {
		return this.menorOuIgual(x.getValor());
	}
	public boolean maiorOuIgual(Double x) {
		return this.maiorOuIgual( UBigDecimal.toBigDecimal(x, this.casas) );
	}
	public boolean maiorOuIgual(Integer x) {
		return this.maiorOuIgual( UBigDecimal.toBigDecimal(x, this.casas) );
	}
	public boolean maiorOuIgual(BigDecimal x) {
		return this.maior(x) || this.eq(x);
	}
	public boolean eq(Integer x) {
		return this.eq(this.newT(x));
	}
	public boolean eq(Double x) {
		return this.eq(this.newT(x));
	}
	public boolean eq(Numeric<?> x) {
		return this.eq(x.getValor());
	}
	public boolean ne(Numeric<?> x) {
		return !this.eq(x);
	}
	public boolean ne(GetNumeric x) {
		return this.ne(x.valor());
	}
	public boolean ne(Integer x) {
		return !this.eq(x);
	}
	public boolean ne(BigDecimal x) {
		return !this.eq(x);
	}
	public boolean eq(BigDecimal x) {
		if (x == null) {
			return this.isZero();
		}
		return this.valor.compareTo(x) == 0;
	}
	public void print() {
		ULog.debug(this + " / " + this.getValor());
	}
	public T vezes(Numeric<?> x) {
		return this.vezes(x.valor);
	}
	public T vezes(BigDecimal x) {
		x = this.valor.multiply(x);
		return this.newT(x);
	}
	public T vezes(Double x) {
		BigDecimal b = UBigDecimal.toBigDecimal(x, this.casas);
		return this.vezes(b);
	}
	public T vezes(Integer x) {
		BigDecimal b = UBigDecimal.toBigDecimal(x, this.casas);
		return this.vezes(b);
	}
	public void divididoIgual(Integer i) {
		this.valor = this.dividido(i).valor;
	}
	public void vezesIgual(Integer i) {
		this.valor = this.vezes(i).valor;
	}
	public void vezesIgual(Numeric<?> x) {
		this.valor = this.vezes(x).valor;
	}
	public boolean menor(Double valor) {
		return this.menor( this.newT(valor) );
	}
	public T newT(Integer x){
		BigDecimal b = UBigDecimal.toBigDecimal(x, this.casas);
		return this.newT(b);
	}
	public T newT(Double v){
		BigDecimal b = UBigDecimal.toBigDecimal(v, this.casas);
		return this.newT(b);
	}
	public T newT(BigDecimal v){
		BigDecimal b = UBigDecimal.toBigDecimal(v, this.casas);
		T t = UClass.newInstance(this.getClasse());
		t.setValor(b);
		return t;
	}
	@SuppressWarnings("unchecked")
	public T menos(T... values) {
		BigDecimal o = this.valor;
		for (T v : values) {
			o = o.subtract(v.valor);
		}
		return this.newT(o);
	}

	private List<T> toList(GetNumeric... values) {
		List<T> list = new ArrayList<>();
		for (GetNumeric v : values) {
			Numeric<?> x = v.valor();
			if (x == null) {
				continue;
			}
			BigDecimal y = x.getValor();
			if (y == null) {
				continue;
			}
			T t = this.newT(y);
			list.add(t);
		}
		return list;
	}

	public T menos(GetNumeric... values) {
		return this.menos( this.toList(values) );
	}
	public T mais(GetNumeric... values) {
		return this.mais( this.toList(values) );
	}

	public T menos(BigDecimal... values) {
		List<T> valores = new ArrayList<>();
		for (BigDecimal o : values) {
			if (o != null) {
				valores.add(this.newT(o));
			}
		}
		return this.menos(valores);
	}

	public T mais(Double d) {
		return this.mais( UBigDecimal.toBigDecimal(d, this.casas) );
	}

	public T mais(BigDecimal... values) {
		List<T> valores = new ArrayList<>();
		for (BigDecimal o : values) {
			if (o != null) {
				valores.add(this.newT(o));
			}
		}
		return this.mais(valores);
	}

	public T menos(List<T> valores) {
		if (valores.isEmpty()) {
			return this.newT(this.getValor());
		}
		BigDecimal o = this.valor;
		for (T v : valores) {
			if (v == null || v.valor == null || v.isZero()) {
				continue;
			}
			o = o.subtract(v.valor);
		}
		return this.newT(o);
	}

	public T mais(List<T> valores) {
		if (valores.isEmpty()) {
			return this.newT(this.getValor());
		}
		BigDecimal o = this.valor;
		for (T v : valores) {
			if (v == null || v.valor == null || v.isZero()) {
				continue;
			}
			o = o.add(v.valor);
		}
		return this.newT(o);
	}

	@SuppressWarnings("unchecked")
	public T mais(T... values) {
		BigDecimal o = this.valor;
		for (T v : values) {
			if (v == null || v.valor == null) {
				continue;
			}
			o = o.add(v.valor);
		}
		return this.newT(o);
	}
	public T menos(Integer x) {
		return this.newT( this.getValor().subtract( UBigDecimal.toBigDecimal(x) ) );
	}
	public T mais(Integer x) {
		return this.newT( this.getValor().add( UBigDecimal.toBigDecimal(x) ) );
	}
	public T dec(Double d) {
		return this.dec(this.newT(d));
	}
	public T dec(Integer x) {
		return this.dec(this.newT(x));
	}
	@SuppressWarnings("unchecked")
	public T dec(BigDecimal o) {
		if (o != null){
			this.valor = this.valor.subtract(o);
		}
		return (T) this;
	}
	@SuppressWarnings("unchecked")
	public T inc(BigDecimal o) {
		if (o != null){
			this.valor = this.valor.add(o);
		}
		return (T) this;
	}

	private boolean isZeroOrEmpty() {
		return this.valor == null || BigDecimal.ZERO.equals(this.valor);
	}

	public boolean isZero() {
		if (isZeroOrEmpty()) {
			return true;
		}
		if ( toString().replace("0", "").replace(",", "").isEmpty() ) {
			return true;
		}
		return false;
	}

	public T comoPercentualDe(Object valor) {
		return this.comoPercentualDe( UBigDecimal.toMoney(valor) );
	}
	public T comoPercentualDe(BigDecimal valor) {
		Numeric5 n = new Numeric5(valor);
		n = n.dividido(100).vezes(this);
		return this.newT( n.getValor() );
	}
	public T centavos(){
		return this.menos(this.inteiros());
	}

	public Integer inteiros(){
		String s = this.getValor().toString();
		if (StringContains.is(s, ".")) {
			s = StringBeforeFirst.get(s, ".");
		}
		return UInteger.toInt(s);
	}

	public T percentual(int x) {
		return this.vezes(x / 100.);
	}

	@SuppressWarnings("unchecked")
	public T menosPercentual(int percentual) {
		T x = this.percentual(percentual);
		return this.menos(x);
	}
	public void menosIgualPercentual(int percentual) {
		T x = this.menosPercentual(percentual);
		this.setValor(x.getValor());
	}
	public T pow(Numeric<?> valor) {
		return pow(valor.getValor());
	}
	public T pow(BigDecimal valor) {
		return newT(BigDecimalMath.pow(getValor(), valor));
	}
	public static Numeric<?> toNumeric(BigDecimal o, int precision) {
		if (precision == 1) return new Numeric1(o);
		if (precision == 2) return new Numeric2(o);
		if (precision == 3) return new Numeric3(o);
		if (precision == 4) return new Numeric4(o);
		if (precision == 5) return new Numeric5(o);
		if (precision == 8) return new Numeric8(o);
		if (precision == 15) return new Numeric15(o);
		throw new RuntimeException("precision nao tratada: " + precision);
	}
	public static Numeric<?> toNumeric(int o, int precision) {
		if (precision == 1) return new Numeric1(o);
		if (precision == 2) return new Numeric2(o);
		if (precision == 3) return new Numeric3(o);
		if (precision == 4) return new Numeric4(o);
		if (precision == 5) return new Numeric5(o);
		if (precision == 8) return new Numeric8(o);
		if (precision == 15) return new Numeric15(o);
		throw new RuntimeException("precision nao tratada: " + precision);
	}

}
