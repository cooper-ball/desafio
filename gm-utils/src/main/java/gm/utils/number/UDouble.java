package gm.utils.number;

import java.math.BigDecimal;
import java.math.BigInteger;

import gm.utils.exception.UException;
import gm.utils.string.UString;
import src.commom.utils.string.StringAfterFirst;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringRepete;
import js.support.console;
import src.commom.utils.string.StringContains;

public class UDouble {

	public static void main(String[] args) {
		console.log(toDouble("0,01"));
	}

	public static Double toDouble(Object o) {
		if (o == null) {
			return null;
		}
		if (o instanceof Double) {
			return (Double) o;
		}
		if (o instanceof BigInteger) {
			BigInteger b = (BigInteger) o;
			return b.doubleValue();
		}
		if (o instanceof BigDecimal) {
			BigDecimal b = (BigDecimal) o;
			return b.doubleValue();
		}
//		if (o instanceof Numeric2) {
//			Numeric2 b = (Numeric2) o;
//			return b.toDouble();
//		}
		if (o instanceof Integer) {
			Integer b = (Integer) o;
			return b.doubleValue();
		}
		if (o instanceof Long) {
			Long b = (Long) o;
			return b.doubleValue();
		}

		if (o instanceof String) {
			String s = (String) o;
			return UDouble.toDouble(s);
		}
//		if (o instanceof MapSO) {
//			MapSO map = (MapSO) o;
//			if (map.containsKey("valor") && map.containsKey("casas")) {
//
//			}
//		}
		throw UException.runtime("Não sei tratar: " + o.getClass());
	}

	public static Double toDouble(String s) {
		if (StringEmpty.is(s)) {
			return null;
		}
		s = s.trim();
		if (s.endsWith(",00.0")) {
			s = s.replace(",00.0", "");
		}
		if (s.endsWith(",00")) {
			s = s.replace(",00", "");
		}
		if (StringEmpty.is(s)) {
			return null;
		}
		if (StringContains.is(s, ".")) {
			while (s.endsWith("0")) {
				s = s.substring(0, s.length() - 1);
			}
			if (s.endsWith(".")) {
				s += "0";
			}
		} else if ( UInteger.isInt(s) ) {
			return UInteger.toInt(s, 0).doubleValue();
		} else if (StringContains.is(s, ",") && UString.ocorrencias(s, ",") == 1) {
			s = s.replace(",", ".");
			return toDouble(s);
		} else {
			s += ".0";
		}
		Double d = Double.parseDouble(s);
		String string = d.toString();
		if (!string.equals(s)) {
			throw UException.runtime("deu dif!!! : " + d + " ; " + s);
		}
		return d;
	}

	public static String format(Double d, int casas) {

		if (d == 0) {
			return "";
		}

		String s = d.toString();
		String afterFirst = StringAfterFirst.get(s, ".");
		s = s.replace(".", ",");

		if (afterFirst.length() == casas) {
			return s;
		}

		if (afterFirst.length() < casas) {
			s += StringRepete.exec("0", casas - afterFirst.length());
			return s;
		}

		while (s.length() - s.indexOf(",") < casas + 1) {
			s += "0";
		}

		return s;

	}

	public static boolean isEmptyOrZero(Double value) {
		return value == null || value == 0;
	}

}
