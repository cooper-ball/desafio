package gm.utils.feign;

import java.util.Map;

import javax.ws.rs.core.MediaType;

import feign.HeaderMap;
import feign.Headers;
import feign.RequestLine;
import feign.Response;
import src.commom.keycloak.KeyCloakResult;

public interface FeignSso extends FeignBasic {

    @RequestLine("POST /protocol/openid-connect/token")
    @Headers("Content-Type: " + MediaType.APPLICATION_FORM_URLENCODED)
    KeyCloakResult getAccessToken(FeignUserParams userParams);

    @RequestLine("GET /protocol/openid-connect/userinfo")
    Response userInfo(@HeaderMap Map<String, Object> headers);
}

