package gm.utils.feign;

import java.util.Map;

import feign.Response;
import src.commom.keycloak.KeyCloakResult;

public class FeignAuthentication {

	public static String URL_SSO;
//	private static String URL_SSO_DEFAULT = "https://sso.cloud.cooperforte.coop/auth/realms/pgn";
	private static String URL_SSO_DEFAULT = "https://sso.cloud.dev.cooperforte.coop/auth/realms/cooperforte";
//	https://sso.cloud.cooperforte.coop/auth

	private static FeignSso getFeign() {
		String url = URL_SSO == null || URL_SSO.contains("ssodesen") ? URL_SSO_DEFAULT : URL_SSO;
		return FeignBasic.createDefaultFeignBuilder(FeignSso.class, url);
	}

    public static KeyCloakResult authenticate(String username, String password, String clientId) {
    	FeignUserParams o = new FeignUserParams();
    	o.setGrant_type("password");
    	o.setClient_id(clientId);
    	o.setUsername(username);
    	o.setPassword(password);
        return getFeign().getAccessToken(o);
    }

    public static KeyCloakResult b3(String user, String pass) {
        return authenticate(user, pass, "b3");
    }

    public static KeyCloakResult autoAtendimento() {
        return authenticate(
            "sistema-mobile",
            "123456",
            "autoatendimento-backend"
        );
    }

    public static Response getUserInfo(String token) {
        FeignSso feign = getFeign();
		Map<String,Object> headers = feign.getDefaultHeaders(token);
        Response response = feign.userInfo(headers);
        return response;
    }

}
