package gm.utils.feign;

import java.util.HashMap;
import java.util.Map;

import feign.Feign;
import feign.form.FormEncoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

public interface FeignBasic {

    default Map<String, Object> getDefaultHeaders(String token) {
        Map<String, Object> defaultHeaders = new HashMap<>();
        defaultHeaders.put("Authorization", "Bearer " + token);
        defaultHeaders.put("Canal_Atendimento", "PGN");

        return defaultHeaders;
    }

    static <T> T createDefaultFeignBuilder(Class<T> endpointClass, String baseUrl){

         return Feign.builder()
                .encoder(new FormEncoder(new JacksonEncoder()))
                .decoder(new JacksonDecoder())
//                .logger(new Logger.ErrorLogger())
//                .logLevel(Logger.Level.FULL)
                .target(endpointClass, baseUrl);

    }
}
