package gm.utils.feign;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FeignUserParams {
    private String grant_type;
    private String client_id;
    private String username;
    private String password;
}