package gm.utils.rest;

import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import gm.utils.exception.UException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.map.MapSoFromJson;
import gm.utils.map.MapSoFromObject;
import gm.utils.map.MapSoToJson;
import lombok.Getter;
import src.commom.utils.comum.Box;
import src.commom.utils.string.StringBox;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringParse;
import js.support.console;

@Getter
public class URest {

    private MapSO data;
    private int code;
    private String message;
    private String cookies;
	private Map<String, List<String>> headers;

    public static void main(String[] args) {

//    	String host = "http://localhost:8080";
    	String host = "http://appdesen";

//		get("http://localhost:8080/mobile/service/gerenciamento/controle/disponibilidade", null).print();
//		get("http://localhost:8080/mobile/service/convenios/conveniosEducacionais", null).print();
//		post("http://localhost:8080/mobile/service/logon96", null).print();
		URest o = URest.post(host+"/mobile/service/logon2019", (Map<String, String>) null, new MapSO().add("username", "=96").add("password","123456"));
		console.log(o.cookies);
//		private Integer inscricao;
//		private Boolean sugeridos;
//		private String token;
		MapSO map = new MapSO().add("inscricao", 96).add("sugeridos", true).add("token", null);
		Map<String, String> headers = new HashMap<>();
		headers.put("cookie", o.cookies);
		URest.post(host+"/mobile/service/listarProdutosComCredito", headers, map).print();

	}

    private static Map<String, String> getMapStringString(GetMapStringString o) {
    	return o == null ? null : o.getMapStringString();
    }

	public static URest post(String url, GetMapStringString headers, MapSO dados) {
    	return URest.post(url, URest.getMapStringString(headers), dados);
    }
	public static URest get(String url, GetMapStringString headers, MapSO dados) {
    	return URest.get(url, URest.getMapStringString(headers), dados);
    }
	public static URest post(String url, Map<String, String> headers, String dados) {
		return post(url, headers, dados, null);
	}
	public static URest post(String url, Map<String, String> headers, String dados, FTT<String, String> tratarReader) {
		return new URest(url, headers, dados, true, tratarReader);
	}
	public static URest post(String url, Map<String, String> headers, Object dados) {
    	MapSO map = MapSoFromObject.get(dados);
		return post(url, headers, map, null);
    }
	public static URest post(String url, Map<String, String> headers, MapSO dados) {
    	return post(url, headers, dados, null);
    }
	public static URest post(String url, Map<String, String> headers, MapSO dados, FTT<String, String> tratarReader) {
		return new URest(url, headers, dados, true, tratarReader);
	}
    public static URest get(String url, Map<String, String> headers, MapSO dados) {
    	return get(url, headers, dados, null);
    }
    public static URest get(String url, Map<String, String> headers, MapSO dados, FTT<String, String> tratarReader) {
    	return new URest(url, headers, dados, false, tratarReader);
    }
    public static URest get(String url, Map<String, String> headers, String dados) {
    	return get(url, headers, dados, null);
    }
    public static URest get(String url, Map<String, String> headers, String dados, FTT<String, String> tratarReader) {
    	return new URest(url, headers, dados, false, tratarReader);
    }

    private static String convertDados(Map<String, String> headers, MapSO dados, boolean post) {
    	if (dados == null || dados.isEmpty()) {
			return null;
		}

		boolean form = headers != null && headers.get("Content-Type") != null && "application/x-www-form-urlencoded".contentEquals(headers.get("Content-Type"));

    	if (post && !form) {

    		try {
    			return new String(MapSoToJson.get(dados).getBytes("UTF-8"), "UTF-8");
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

		} else {

			StringBox box = new StringBox();

			dados.forEach((key, value) -> {
				box.add("&" + key + "=" + value);
			});

			String s = box.get().substring(1);

			if (post) {
				byte[] postData = s.getBytes( StandardCharsets.UTF_8 );
				int postDataLength = postData.length;
				headers.put("charset", "utf-8");
				headers.put("Content-Length", Integer.toString(postDataLength));
				return s;
			} else {
				return "?" + s;
			}

		}

    }

    private URest(String url, Map<String, String> headers, MapSO dados, boolean post, FTT<String, String> tratarReader) {
    	this(url, headers, URest.convertDados(headers, dados, post), post, tratarReader);
    }
	private URest(String url, Map<String, String> headers, String dados, boolean post, FTT<String, String> tratarReader) {

		CookieManager cookieManager = new CookieManager();
		CookieHandler.setDefault(cookieManager);

		if (!post && dados != null) {
			url += "?" + dados;
		}

		Box<HttpURLConnection> box = new Box<>();

		try {

			MapSO finalHeaders = new MapSO();
			finalHeaders.put("Content-Type", "application/json; charset=UTF-8");
			finalHeaders.put("Accept", "*/*");

			if (headers != null) {
				headers.forEach((key, value) -> finalHeaders.put(key, value));
			}

			URL obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			box.set(con);
			con.setDoOutput(true);
			con.setRequestMethod(post ? "POST" : "GET");

			finalHeaders.forEach((k,v) ->
				con.setRequestProperty(k, StringParse.get(v))
			);

			if (post && dados != null) {
				OutputStreamWriter out = new OutputStreamWriter(box.get().getOutputStream());
//				MapSO map = MapSoFromObject.get(dados);
//				String json = map.asJson().toString("");
				out.write(dados);
				out.close();
			}

			String reader;

			try {
				reader = IOUtils.toString(con.getInputStream(), "UTF-8");
			} catch (Exception e) {
				System.err.println(e.getMessage());
				try {
					reader = IOUtils.toString(con.getErrorStream(), "UTF-8");
				} catch (Exception e2) {
					reader = e.getMessage() + " / " + e2.getMessage();

					try {
						System.out.println(con.getContent());
					} catch (Exception e3) {
						// TODO: handle exception
					}

				}
			}

			if (tratarReader != null) {
				reader = tratarReader.call(reader);
			}

			if (StringEmpty.is(reader)) {
				this.data = null;
			} else {

				if (!reader.contains("{")) {
					reader = "{result:"+reader+"}";
				}

				this.data = MapSoFromJson.get(reader);

			}

			this.code = con.getResponseCode();
			this.message = con.getResponseMessage();

			this.headers = con.getHeaderFields();

			List<HttpCookie> cookies2 = cookieManager.getCookieStore().getCookies();

			if (cookies2.isEmpty()) {
				this.cookies = null;
			} else {
				this.cookies = "";
				cookies2.forEach(o -> this.cookies += ";"+o);
				this.cookies = this.cookies.substring(1);
			}

			if (this.code < 200 || this.code > 299) {
				throw new ResponseException(this.message, this.code, this.data);
			}

		} catch (ResponseException e) {
			throw e;
		} catch (Exception e) {
			throw UException.runtime(e);
		} finally {
			if (box.isNotNull()) {
				box.get().disconnect();
			}
		}
	}

	public void print() {

		console.log("---------------------");
		console.log("statusCode: " + this.code);
		console.log("message: " + this.message);
		console.log("cookies: " + this.cookies);

		if (getData() == null) {
			console.log("data: null");
		} else {
			this.getData().print();
		}

	}

	public String getAuthorization() {

		List<String> list = getHeaders().get("Authorization");

		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}

	}

	public void printHeaders() {

		getHeaders().forEach((key, value) -> {
			System.out.println(key);
			for (String s : value) {
				System.out.println("\t" + s);
			}
		});

	}

}
