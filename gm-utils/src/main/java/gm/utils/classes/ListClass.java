package gm.utils.classes;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import gm.utils.comum.ULog;
import gm.utils.exception.UException;
import gm.utils.string.ListString;
import src.commom.utils.string.StringBeforeLast;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringContains;

public class ListClass extends ArrayList<Class<?>>{

	private boolean locked = false;

	public ListClass(){
		super();
	}

	public ListClass(Class<?>... classes){
		this();
		for (Class<?> classe : classes) {
			this.add(classe);
		}
	}

	private static final long serialVersionUID = 1;

	@Override
	public Class<?> remove(int index) {
		if (locked) {
			throw new RuntimeException("???");
		}
		return super.remove(index);
	}

	@Override
	public boolean remove(Object o) {
		if (locked) {
			throw new RuntimeException("???");
		}
		return super.remove(o);
	}

	@Override
	public boolean add(Class<?> e) {
		if (locked) {
			throw new RuntimeException("???");
		}
		return super.add(e);
	}

	public ListClass getByAnnotation(Class<? extends Annotation> a) {
		return getByAnnotation(a, false);
	}

	public ListClass getByAnnotation(Class<? extends Annotation> a, boolean findSuper) {

		ListClass list = new ListClass();

		if (findSuper) {
			for (Class<?> c : this) {
				if (isAnnotationPresent(c,a)) {
					list.add(c);
				}
			}
		} else {
			for (Class<?> c : this) {
				if (c.isAnnotationPresent(a)) {
					list.add(c);
				}
			}
		}

		return list;

	}

	private boolean isAnnotationPresent(Class<?> c, Class<? extends Annotation> a) {
		if (c.isAnnotationPresent(a)) {
			return true;
		} else {
			c = c.getSuperclass();
			if (c == null) {
				return false;
			} else {
				return isAnnotationPresent(c, a);
			}
		}
	}

	public void print() {
		for (Class<?> c : this) {
			ULog.debug(c.getName());
		}
		ULog.debug("Total: " + size() + " classes");
	}

	public ListClass whereExtends(Class<?> classe) {
		ListClass list = new ListClass();
		for (Class<?> c : this) {
			if (UClass.instanceOf(c, classe)) {
				list.add(c);
			}
		}
		return list;
	}
	public ListClass whereNotExtends(Class<?> classe) {
		ListClass list = new ListClass();
		for (Class<?> c : this) {
			if (!UClass.instanceOf(c, classe)) {
				list.add(c);
			}
		}
		return list;
	}

	public ListClass copy() {
		ListClass list = new ListClass();
		list.addAll(this);
		return list;
	}

	public void sortAsImport() {
		Comparator<Class<?>> importComparator = (o1, o2) -> {
			String a = StringBeforeLast.get(o1.getName(),".");
			String b = StringBeforeLast.get(o2.getName(),".");

			if (a.equals(b)) {
				return o1.getSimpleName().compareTo(o2.getSimpleName());
			}

			if (a.startsWith("java.")) {
				if (b.startsWith("java.")) {
					return a.compareTo(b);
				} else {
					return -1;
				}
			}

			if (b.startsWith("java.")) {
				return 1;
			}

			return a.compareTo(b);
		};
		this.sort(importComparator);
		this.sort(importComparator);
		this.sort(importComparator);
	}

	public boolean removeIfPackage(String s) {
		String x = s + ".";
		Predicate<Class<?>> filter = t -> t.getName().startsWith(x);
		return removeIf(filter);
	}
	public boolean removeIfNotPackage(String s) {
		String x = s + ".";
		Predicate<Class<?>> filter = t -> !t.getName().startsWith(x);
		return removeIf(filter);
	}

	public boolean removeIfSimpleNameStartsWith(String s) {
		Predicate<Class<?>> filter = t -> t.getSimpleName().startsWith(s);
		return removeIf(filter);
	}

	public void sort() {
		ClassSortComparator comparator = new ClassSortComparator();
		this.sort(comparator);
	}
	public void addIfNotContains(Class<?> classe) {
		if (!this.contains(classe)) {
			this.add(classe);
		}
	}
	public void addIfNotContains(List<?> list) {
		for (Object classe : list) {
			if (!this.contains(classe)) {
				this.add((Class<?>) classe);
			}
		}
	}
	public Class<?> get(GetClassName getter) {
		return this.get(getter.getClassName());
	}
	public <T> Class<T> getObrig(GetClassName getter) {
		return this.getObrig(getter.getClassName());
	}
	public <T> Class<T> getObrig(String name) {
		Class<T> classe = this.get(name);
		if (classe == null) {
			throw UException.runtime("Classe não encontrada: " + name);
		}
		return classe;
	}
	@SuppressWarnings("unchecked")
	public <T> Class<T> get(String name) {
		if (StringEmpty.is(name)) {
			throw UException.runtime("O nome da classe está em branco");
		}
		for (Class<?> classe : this) {
			if (classe.getSimpleName().equalsIgnoreCase(name) || classe.getName().equalsIgnoreCase(name)) {
				return (Class<T>) classe;
			}
		}
		return null;
	}

	public Class<?> removeLast() {
		return this.remove(size() - 1);
	}

	public ListClass filter(Predicate<Class<?>> predicate) {
		List<Class<?>> collect = stream().filter(predicate).collect(Collectors.toList());
		ListClass list = new ListClass();
		list.addAll(collect);
		return list;
	}

	public ListClass join(ListClass list) {
		ListClass copy = copy();
		for (Class<?> classe : list) {
			copy.addIfNotContains(classe);
		}
		return copy;
	}

	public ListString getNames() {
		ListString list = new ListString();
		for (Class<?> classe : this) {
			list.addIfNotContains(classe.getSimpleName());
		}
		return list;
	}

	public ListString getNamesFull() {
		ListString list = new ListString();
		for (Class<?> classe : this) {
			list.addIfNotContains(classe.getName());
		}
		return list;
	}

	public static ListClass safe(ListClass list) {
		return list == null ? new ListClass() : list;
	}

	public boolean contains(String s) {
		if (StringContains.is(s, ".")) {
			return getNamesFull().contains(s);
		} else {
			return getNames().contains(s);
		}
	}

	public void lock() {
		locked = true;
	}

}
