package gm.utils.files;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;

import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.FileUtils;

import gm.utils.classes.UClass;
import gm.utils.comum.Lst;
import gm.utils.comum.SO;
import gm.utils.comum.UConstantes;
import gm.utils.comum.UList;
import gm.utils.date.Data;
import gm.utils.exception.UException;
import gm.utils.string.ListString;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
import js.support.console;
import src.commom.utils.object.Obrig;
import src.commom.utils.string.StringBeforeFirst;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringContains;

public class UFile {

	public static void assertExists(String s) {
		UFile.assertExists(new File(s));
	}

	public static void assertExists(File file) {
		if (!UFile.exists(file)) {
			throw UException.runtime("Arquivo n" + UConstantes.a_til + "o existe: " + file);
		}
	}

	public static boolean exists(String s) {
		return UFile.exists(new File(s));
	}

	public static boolean exists(File file) {
		return file.exists();
	}

	public static Data data(Class<?> classe) {
		return UFile.data(UClass.javaFileName(classe, true));
	}

	public static Data data(String s) {
		UFile.assertExists(s);
		return new Data(new File(s).lastModified());
	}

//	public static StreamingOutput streamOutput(String fileName) {
//		File file = new File(fileName);
//		return streamOutput(file);
//	}
//
//	public static StreamingOutput streamOutput(File file) {
//
//		try {
//
//			StreamingOutput streamOutput = new StreamingOutput() {
//
//				@Override
//				public void write(OutputStream output) throws IOException, WebApplicationException {
//					BufferedOutputStream bus = new BufferedOutputStream(output);
//
//					try {
//		                FileInputStream fizip = new FileInputStream(file);
//		                byte[] buffer2 = IOUtils.toByteArray(fizip);
//		                bus.write(buffer2);
//		            } catch (Exception e) {
//		            	UException.printTrace(e);
//		            }
//				}
//			};
//
//			return streamOutput;
//
//		} catch (Exception e) {
//			throw UException.runtime(e);
//		}
//
//	}

	public static boolean delete(String fileName) {

		if (SO.windows()) {
			if (fileName.startsWith("/")) {
				fileName = fileName.substring(1);
			}
		}

		if (!UFile.exists(fileName)) {
			return false;
		}
		while (UFile.exists(fileName)) {
			try {
				File file = new File(fileName);
				if (file.isDirectory()) {
					UFile.delete(file.listFiles());
				}
				Path path = Paths.get(fileName);
				Files.deleteIfExists(path);
			} catch (Exception e) {
				throw UException.runtime(e);
			}
		}

		console.log("delete " + fileName);
		return true;

	}

	public static void delete(List<File> files) {
		for (File file : files) {
			try {
				UFile.delete(file);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void delete(File file) {
		UFile.delete(file.getAbsolutePath());
		file.delete();
		console.log(file + " (delete)");
	}

	public static void save(File file, String fileName) throws IOException {
		UFile.save(new FileInputStream(file), fileName);
	}

	public static void save(InputStream is, String fileName) throws IOException {

		String path = StringBeforeFirst.get(fileName, "/");

//		file.exists();
		final File directory = new File(path);
		if (!directory.exists()) {
			directory.mkdirs();
		}
		File destinationFile = new File(path, fileName);
//		destinationFile.getAbsolutePath();
		FileOutputStream fos = new FileOutputStream(destinationFile, false);
		UFile.copyInputStreamToOutputStream(is, fos);
	}

	private static void copyInputStreamToOutputStream(InputStream src, OutputStream dest) throws IOException {
		if (!(dest instanceof BufferedOutputStream))
			dest = new BufferedOutputStream(dest);

		if (!(src instanceof BufferedInputStream))
			src = new BufferedInputStream(src);

		int countBytesRead = -1;
		byte[] bufferCopy = new byte[2048];
		while ((countBytesRead = src.read(bufferCopy)) != -1)
			dest.write(bufferCopy, 0, countBytesRead);

		dest.flush();
		dest.close();
	}

	public static void copy(File origem, String destino) {
		copy(origem, new File(destino));
	}

	public static void copy(File origem, SmbFile destino) {

		try (FileInputStream fis = new FileInputStream(origem); SmbFileOutputStream smbfos = new SmbFileOutputStream(destino);) {
		    final byte[] b  = new byte[16*1024];
		    int read = 0;
		    while ((read=fis.read(b, 0, b.length)) > 0) {
		        smbfos.write(b, 0, read);
		    }
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public static SmbFile toSmb(File file) {

		String domain = Obrig.check(System.getProperty("smb-domain"));
		String username = Obrig.check(System.getProperty("smb-username"));
		String password = Obrig.check(System.getProperty("smb-password"));
		//@gm-utils NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("cooperforte", "gamarra", "@m03386551157");
		NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(domain, username, password);

		try {
			return new SmbFile(file.toString(), auth);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private static boolean isSmb(File file) {
		return file.toString().startsWith("smb:/");
	}

	public static void copy(File origem, File destino) {

		if (isSmb(destino)) {
			copy(origem, toSmb(destino));
			return;
		}

		try {

			if (origem.isDirectory()) {

				if (!destino.exists()) {
					destino.mkdir();
				}

				String[] filhos = origem.list();

				if (filhos == null) {
					System.out.println(origem);
				} else {
					for (String filho : filhos) {
						UFile.copy(new File(origem, filho), new File(destino, filho));
					}
				}


			} else {

				boolean exists = destino.exists();

				if (exists) {
					if (origem.lastModified() == destino.lastModified() && origem.getTotalSpace() == destino.getTotalSpace()) {
						return;
					}
				} else {
					new ListString().save(destino);
				}

				try (InputStream in = new FileInputStream(origem); OutputStream out = new FileOutputStream(destino)) {

					byte[] buffer = new byte[1024];
					int len;
					while ((len = in.read(buffer)) > 0) {
						out.write(buffer, 0, len);
					}
				}

				destino.setLastModified(origem.lastModified());

				if (exists) {
					console.log("Gravando: " + destino + " (replace)");
				}

			}

		} catch (Exception e) {
			throw UException.runtime(e);
		}

	}

	public static void deleteFilesOfPath(String path) {
		UFile.deleteFilesOfPath(new File(path));
	}

	public static void deleteFilesOfPath(File path) {
		UFile.delete(path.listFiles());
	}

	private static void delete(File... files) {
		if (files == null || files.length == 0) {
			return;
		}
		List<File> list = UList.asList(files);
		UFile.delete(list);
	}

	public static Lst<File> getFilesAndDirectories(File file) {
		if (!file.exists()) {
			throw UException.runtime("!exists:" + file);
		}
		if (!file.isDirectory()) {
			throw UException.runtime("!isDirectory:" + file);
		}
		return UList.asList(file.listFiles());
	}

	private static Lst<File> getFilesOrDirectory(File file, boolean directory) {
		Lst<File> list = UFile.getFilesAndDirectories(file);
		Lst<File> result = new Lst<>();
		for (File file2 : list) {
			if (file2.isDirectory() == directory) {
				result.add(file2);
			}
		}
		UFile.sort(result);
		return result;
	}

	private static void sort(List<File> list) {
		list.sort((a, b) -> {
			if (a.isDirectory() == b.isDirectory()) {
				return StringCompare.compare(a.getName(), b.getName());
			} else if (a.isDirectory()) {
				return 1;
			} else {
				return -1;
			}
		});
	}

	public static Lst<File> getFiles(String path, String... extensoes) {
		File file = new File(path);
		return UFile.getFiles(file, extensoes);
	}

	public static Lst<File> getFiles(File path, String... extensoes) {
		Lst<File> files = UFile.getFiles(path);
		Lst<File> list = new Lst<>();
		for (String extensao : extensoes) {
			for (File file : files) {
				if (file.getName().endsWith("." + extensao)) {
					list.add(file);
				}
			}
		}
		return list;
	}

	public static Lst<File> getFiles(String path) {
		return UFile.getFiles(new File(path));
	}

	public static Lst<File> getFiles(File file) {
		return UFile.getFilesOrDirectory(file, false);
	}

	public static Lst<File> getDirectories(String fileName) {
		return UFile.getDirectories(new File(fileName));
	}

	public static Lst<File> getDirectories(File file) {
		return UFile.getFilesOrDirectory(file, true);
	}

	public static void criaDiretorio(String s) {
		new File(s).mkdirs();
	}

	public static void copy(String de, String para) {

		if (StringCompare.eq(de, para)) {
			throw new RuntimeException("de == para");
		}

		copy(new File(de), new File(para));
	}

	private static void getAllFiles(File path, List<File> list) {
		list.addAll(UFile.getFiles(path));
		Lst<File> directories = UFile.getDirectories(path);
		for (File file : directories) {
			String s = file.toString();
			if (StringContains.is(s, "/target"))
				continue;
			if (StringContains.is(s, "/node_modules"))
				continue;
			UFile.getAllFiles(file, list);
		}
	}

	public static Lst<File> getAllFiles(File path) {
		Lst<File> list = new Lst<>();
		UFile.getAllFiles(path, list);
		return list;
	}

	private static void getAllDirectories(File path, List<File> list) {
		list.addAll(UFile.getDirectories(path));
		List<File> directories = UFile.getDirectories(path);
		for (File file : directories) {
			String s = file.toString();
			if (StringContains.is(s, "/target"))
				continue;
			if (StringContains.is(s, "/node_modules"))
				continue;
			UFile.getAllDirectories(file, list);
		}
	}

	public static Lst<File> getAllDirectories(File path) {
		Lst<File> list = new Lst<>();
		UFile.getAllDirectories(path, list);
		return list;
	}

	public static Lst<File> getAllDirectories(String path) {
		return UFile.getAllDirectories(new File(path));
	}

	public static Lst<File> getAllFiles(String path) {
		return UFile.getAllFiles(new File(path));
	}

	public static Lst<File> getJavas() {
		return getJavas(System.getProperty("user.dir") + "/src");
	}

	public static Lst<File> getJavas(String path) {
		return UList.filter(UFile.getAllFiles(path), o -> o.toString().endsWith(".java"));
	}

	public static Lst<File> getPoms(String path) {
		return UFile.getAllFiles(path).filter(o -> o.toString().endsWith("pom.xml"));
	}

	public static Lst<File> findFilesByName(String path, String name) {
		Lst<File> files = UFile.getAllFiles(path);
		Lst<File> list = new Lst<>();
		for (File file : files) {
			if (file.getName().equals(name)) {
				list.add(file);
			}
		}
		return list;
	}

	public static byte[] getBytes(File file) {
		try {
			return FileUtils.readFileToByteArray(file);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static StreamingOutput streamOutput(String fileName) {
		File file = new File(fileName);
		return UFile.streamOutput(file);
	}

	public static StreamingOutput streamOutput(File file) {
		try {

			StreamingOutput streamOutput = output -> {
				BufferedOutputStream bus = new BufferedOutputStream(output);
				try {
					final FileInputStream fizip = new FileInputStream(file);
					final byte[] buffer2 = org.apache.poi.util.IOUtils.toByteArray(fizip);
					bus.write(buffer2);
				} catch (Exception e) {
					throw UException.runtime(e);
				}
			};

			return streamOutput;

		} catch (Exception e) {
			throw UException.runtime(e);
		}
	}

	public static long size(Path path) {
		try {
			return Files.size(path);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void deleteEmptyDirs(String path) {
		List<File> dirs = UFile.getAllDirectories(path);
		for (File dir : dirs) {
			if (dir.listFiles().length == 0) {
				UFile.delete(dir);
			}
		}
	}

	public static void deleteIfContains(String path, String text) {
		deleteIf(path, file -> file.toString().contains(text));
	}

	public static void deleteIfEndsWith(String path, String text) {
		deleteIf(path, file -> file.toString().endsWith(text));
	}

	public static void deleteIf(String path, Predicate<File> predicate) {
		UFile.getAllFiles(path).filter(predicate).forEach(file -> {
			delete(file);
		});
	}

}
