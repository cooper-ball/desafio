package gm.utils.reflection.atributo;

import java.util.List;

import org.junit.Test;

import gm.utils.comum.UAssert;
import gm.utils.reflection.Atributo;
import gm.utils.reflection.ListAtributos;

public class GetTypeOfListTest {

	List<Integer> list;

	@Test
	public void exec() {
		Atributo a = ListAtributos.get(GetTypeOfListTest.class).get(0);
		UAssert.eq(a.getTypeOfList(), Integer.class, "");
	}

	public static void main(String[] args) {
		new GetTypeOfListTest().exec();
	}


}
