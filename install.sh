docker-compose down --remove-orphans

mvn -f gm-utils/pom.xml clean install -DskipTests

mvn -f cooper-ball-back/pom.xml clean install -DskipTests
docker build -f cooper-ball-back/Dockerfile -t sistema/cooper-ball-back  cooper-ball-back/.

npm install --prefix cooper-ball-web/
npm run build --prefix cooper-ball-web/
docker build -f cooper-ball-web/Dockerfile -t sistema/cooper-ball-web  cooper-ball-web/.

docker-compose  up -d --remove-orphans
