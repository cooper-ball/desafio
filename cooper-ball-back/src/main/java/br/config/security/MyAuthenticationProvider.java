package br.config.security;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import br.cooper.infra.services.LoginService;
import src.commom.utils.string.StringEmpty;

@Component
public class MyAuthenticationProvider implements AuthenticationProvider {

	@Autowired LoginService loginService;

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        String username = auth.getName();
        
        if (StringEmpty.is(username)) {
			return null;
		}
        
        String password = auth.getCredentials().toString();
        Integer sessao = loginService.signin(username, password);
        return new UsernamePasswordAuthenticationToken(sessao.toString(), "", Collections.emptyList());

    }	

	@Override
	public boolean supports(Class<?> auth) {
		return auth.equals(UsernamePasswordAuthenticationToken.class);
	}

}