package br.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.cooper.infra.models.Usuario;
import br.cooper.infra.services.UsuarioService;
 
public class UserDetailsServiceImpl implements UserDetailsService {
 
    @Autowired
    private UsuarioService usuarioService;
     
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
    	
    	Usuario o = usuarioService.findByLogin(login);
    	
    	if (o == null) {
    		throw new UsernameNotFoundException("Usuario invalido");
		} else {
			return new MyUserDetails(o);
		}
         
    }
 
}