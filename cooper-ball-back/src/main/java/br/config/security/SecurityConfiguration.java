package br.config.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import br.config.security.jwt.JWTAuthenticationFilter;
import br.config.security.jwt.JWTLoginFilter;
import gm.utils.map.MapSO;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	JWTAuthenticationFilter jwtAuthenticationFilter;
	@Autowired
	MyAuthenticationProvider myAuthenticationProvider;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(myAuthenticationProvider);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.cors().and().csrf().disable()

				// filtra requisições de login
				.addFilterBefore(new JWTLoginFilter("/login/signin", authenticationManager()), UsernamePasswordAuthenticationFilter.class)

				// filtra outras requisições para verificar a presença do JWT no header
				.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)

				.authorizeRequests()

				/*
				 * configurando urls publicas
				 */
				.antMatchers("/login/signin", "/login/signout").permitAll()

				/*
				 * Qualquer outra solicitação precisa que o usuário seja autenticado
				 */
				.anyRequest().authenticated()

				.and().exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())

				/*
				 * Permitindo a autenticação básica, ou seja, enviando um cabeçalho HTTP Basic
				 * Auth para autenticação.
				 */
				.and().httpBasic()

//			Evitar o popup
//			https://stackoverflow.com/questions/31424196/disable-browser-authentication-dialog-in-spring-security/31429370
				.authenticationEntryPoint(new NoPopupBasicAuthenticationEntryPoint())

				/*
				 * Permitindo requisicoes da mesma origem para nao bloquear h2-console
				 */
				.and().headers().frameOptions().sameOrigin()

		;

	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOriginPatterns(Collections.singletonList("*")); // <-- you may change "*"
		configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
		configuration.setAllowCredentials(true);
		configuration.setAllowedHeaders(
				Arrays.asList("Accept", "Origin", "Content-Type", "Depth", "User-Agent", "If-Modified-Since,",
						"Cache-Control", "Authorization", "X-Req", "X-File-Size", "X-Requested-With", "X-File-Name"));

		configuration.setExposedHeaders(Arrays.asList("Authorization"));

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilterRegistrationBean() {
		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(
				new CorsFilter(corsConfigurationSource()));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}

	@Bean
	public AuthenticationEntryPoint authenticationEntryPoint() {
		
		return new AuthenticationEntryPoint() {

			@Override
			public void commence(HttpServletRequest req, HttpServletResponse res, AuthenticationException exception) throws IOException, ServletException {
				res.setContentType("application/json;charset=UTF-8");
				res.setStatus(403);
				String json = new MapSO().add("code", 403).add("message", "Usuario ou senha invalida!").asJson().trimPlus().toString("");
				res.getWriter().write(json);
			}
			
		};
		
	}

}