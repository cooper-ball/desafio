package br.config.kafka;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

@Configuration
public class KafcaConfig {

	public static final String TORCEDOR_CADASTRADO = "TorcedorCadastrado";
	public static final String TORCEDOR_DESLIGADO = "TorcedorDesligado";

	@Value("${spring.kafka.producer.servers}")
	private String servers;

	@Bean
	public Map<String, Object> producerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		return props;
	}

	@Bean
	public ProducerFactory<String, TorcedorKafka> producerFactory() {
		return new DefaultKafkaProducerFactory<>(producerConfigs());
	}

	@Bean
	public KafkaTemplate<String, TorcedorKafka> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}

//	@Bean
//	public NewTopic createTorcedorCadastrado() {
//		return TopicBuilder.name(TORCEDOR_CADASTRADO).build();
//	}
//
//	@Bean
//	public NewTopic createTorcedorDesligado() {
//		return TopicBuilder.name(TORCEDOR_DESLIGADO).build();
//	}

	// Consumer

//	@Bean
//	public Map<String, Object> consumerConfigs() {
//		Map<String, Object> props = new HashMap<>();
//		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
//		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
//		props.put(ConsumerConfig.GROUP_ID_CONFIG, "json");
//		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
//		return props;
//	}
//
//	@Bean
//	public ConsumerFactory<String, TorcedorKafka> consumerFactory() {
//		return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
//				new JsonDeserializer<>(TorcedorKafka.class));
//	}
//
//	@Bean
//	public ConcurrentKafkaListenerContainerFactory<String, TorcedorKafka> kafkaListenerContainerFactory() {
//		ConcurrentKafkaListenerContainerFactory<String, TorcedorKafka> factory = new ConcurrentKafkaListenerContainerFactory<>();
//		factory.setConsumerFactory(consumerFactory());
//		return factory;
//	}

}