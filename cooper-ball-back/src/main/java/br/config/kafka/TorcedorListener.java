//package br.config.kafka;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.messaging.MessageHeaders;
//import org.springframework.messaging.handler.annotation.Headers;
//import org.springframework.messaging.handler.annotation.Payload;
//import org.springframework.stereotype.Service;
//
//@Service
//public class TorcedorListener {
//
//    private static final Logger LOG = LoggerFactory.getLogger(TorcedorListener.class);
//
//    @KafkaListener(id = "a", topics = KafcaConfig.TORCEDOR_CADASTRADO)
//    public void receive(@Payload TorcedorKafka data,
//                        @Headers MessageHeaders headers) {
//        LOG.info("received data='{}'", data);
//
//        headers.keySet().forEach(key -> {
//            LOG.info("{}: {}", key, headers.get(key));
//        });
//    }
//
//}