package br.config.kafka;

import br.cooper.ball.models.Torcedor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor @Setter @Getter @ToString
public class TorcedorKafka {

	private String cpf;
	private String nome;
	
	public TorcedorKafka(Torcedor o) {
		this.cpf = o.getCpf();
		this.nome = o.getNome();
	}

}
