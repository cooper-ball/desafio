package br.cooper.ball.abstracoes;

import br.cooper.ball.models.Telefone;
import br.cooper.ball.models.Torcedor;
import br.cooper.ball.selects.TorcedorSelect;
import br.cooper.ball.services.TelefoneService;
import br.cooper.infra.outros.CargaCampos;
import br.cooper.infra.services.AuditoriaCampoService;
import br.cooper.infra.outros.AuditoriaEntidadeBox;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import src.commom.utils.cp.UCpf;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;
import src.commom.utils.string.StringParse;

public abstract class TorcedorServiceAbstract extends ServiceModelo<Torcedor> {

	@Autowired protected TelefoneService telefoneService;
	@Autowired protected AuditoriaCampoService auditoriaCampoService;

	@Override
	public Class<Torcedor> getClasse() {
		return Torcedor.class;
	}

	@Override
	public MapSO toMap(Torcedor o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getCep() != null) {
			map.put("cep", o.getCep());
		}
		if (o.getComplemento() != null) {
			map.put("complemento", o.getComplemento());
		}
		if (o.getCpf() != null) {
			map.put("cpf", UCpf.format(o.getCpf()));
		}
		if (o.getEmail() != null) {
			map.put("email", StringParse.get(o.getEmail()));
		}
		if (o.getNome() != null) {
			map.put("nome", StringParse.get(o.getNome()));
		}
		if (o.getNumero() != null) {
			map.put("numero", o.getNumero());
		}
		if (listas) {
			map.put("telefones", telefoneService.toMapList(getTelefones(o)));
		}
		map.put("excluido", o.getExcluido());
		return map;
	}

	private Lst<Telefone> getTelefones(Torcedor o) {
		return telefoneService.select().torcedor().eq(o).list();
	}

	@Override
	protected Torcedor fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Torcedor o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setCep(mp.getString("cep"));
		o.setComplemento(mp.getString("complemento"));
		o.setCpf(mp.getString("cpf"));
		o.setEmail(mp.getString("email"));
		o.setNome(mp.getString("nome"));
		o.setNumero(mp.getInt("numero"));
		return o;
	}

	@Override
	public Torcedor newO() {
		Torcedor o = new Torcedor();
		o.setExcluido(false);
		return o;
	}

	@Override
	protected final void validar(Torcedor o) {
		o.setCep(tratarString(o.getCep()));
		if (o.getCep() == null) {
			throw new MessageException("O campo Torcedor > Cep é obrigatório");
		}
		if (StringLength.get(o.getCep()) > 50) {
			throw new MessageException("O campo Torcedor > Cep aceita no máximo 50 caracteres");
		}
		o.setComplemento(tratarString(o.getComplemento()));
		if (StringLength.get(o.getComplemento()) > 200) {
			throw new MessageException("O campo Torcedor > Complemento aceita no máximo 200 caracteres");
		}
		o.setCpf(tratarCpf(o.getCpf(), false));
		if (o.getCpf() == null) {
			throw new MessageException("O campo Torcedor > Cpf é obrigatório");
		}
		if (StringLength.get(o.getCpf()) > 11) {
			throw new MessageException("O campo Torcedor > Cpf aceita no máximo 11 caracteres");
		}
		o.setEmail(tratarEmail(o.getEmail()));
		if (o.getEmail() == null) {
			throw new MessageException("O campo Torcedor > E-mail é obrigatório");
		}
		if (StringLength.get(o.getEmail()) > 50) {
			throw new MessageException("O campo Torcedor > E-mail aceita no máximo 50 caracteres");
		}
		o.setNome(tratarNomeProprio(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Torcedor > Nome é obrigatório");
		}
		if (StringLength.get(o.getNome()) > 50) {
			throw new MessageException("O campo Torcedor > Nome aceita no máximo 50 caracteres");
		}
		if (o.getNumero() == null) {
			throw new MessageException("O campo Torcedor > Número é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueCpf(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueCpf(Torcedor o) {
		TorcedorSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.cpf().eq(o.getCpf());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("torcedor_cpf"));
		}
	}

	protected void validar2(Torcedor o) {}

	protected void validar3(Torcedor o) {}

	@Override
	protected void saveListas(Torcedor o, MapSO map) {
		Lst<MapSO> list;
		int id = o.getId();
		list = map.getSubList("telefones");
		if (list != null) {
			for (MapSO item : list) {
				item.put("torcedor", id);
				if (item.isTrue("excluido")) {
					if (!item.isEmpty("id")) {
						telefoneService.delete(item.id());
					}
				} else {
					telefoneService.save(item);
				}
			}
		}
	}

	@Override
	public ResultadoConsulta consulta(MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Torcedor> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		TorcedorSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Torcedor> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public TorcedorSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		TorcedorSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.string(params, "cep", select.cep());
		FiltroConsulta.string(params, "complemento", select.complemento());
		FiltroConsulta.cpf(params, "cpf", select.cpf());
		FiltroConsulta.email(params, "email", select.email());
		FiltroConsulta.nomeProprio(params, "nome", select.nome());
		FiltroConsulta.integer(params, "numero", select.numero());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		return select;
	}

	@Override
	protected Torcedor buscaUnicoObrig(MapSO params) {
		TorcedorSelect<?> select = selectBase(null, null, params);
		Torcedor o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Torcedor com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return true;
	}

	@Override
	protected Torcedor setOld(Torcedor o) {
		Torcedor old = newO();
		old.setCep(o.getCep());
		old.setComplemento(o.getComplemento());
		old.setCpf(o.getCpf());
		old.setEmail(o.getEmail());
		old.setNome(o.getNome());
		old.setNumero(o.getNumero());
		o.setOld(old);
		return o;
	}

	@Override
	protected boolean houveMudancas(Torcedor o) {
		Torcedor old = o.getOld();
		if (!StringCompare.eq(o.getCep(), old.getCep())) {
			return true;
		}
		if (!StringCompare.eq(o.getComplemento(), old.getComplemento())) {
			return true;
		}
		if (!StringCompare.eq(o.getCpf(), old.getCpf())) {
			return true;
		}
		if (!StringCompare.eq(o.getEmail(), old.getEmail())) {
			return true;
		}
		if (!StringCompare.eq(o.getNome(), old.getNome())) {
			return true;
		}
		if (o.getNumero() != old.getNumero()) {
			return true;
		}
		return false;
	}

	@Override
	protected void registrarAuditoriaInsert(Torcedor o) {
		AuditoriaEntidadeBox.novoInsert(getIdEntidade(), o.getId());
	}

	@Override
	protected void registrarAuditoriaUpdate(Torcedor o) {
		int e = getIdEntidade();
		AuditoriaEntidadeBox a = AuditoriaEntidadeBox.novoUpdate(e, o.getId());
		Torcedor old = o.getOld();
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "cep"), old.getCep(), o.getCep());
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "complemento"), old.getComplemento(), o.getComplemento());
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "cpf"), old.getCpf(), o.getCpf());
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "email"), old.getEmail(), o.getEmail());
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "nome"), old.getNome(), o.getNome());
		auditoriaCampoService.registrar(a, CargaCampos.getId(e, "numero"), old.getNumero(), o.getNumero());
	}

	@Override
	protected void registrarAuditoriaDelete(Torcedor o) {
		AuditoriaEntidadeBox.novoDelete(getIdEntidade(), o.getId());
	}

	@Override
	protected void registrarAuditoriaUndelete(Torcedor o) {
		AuditoriaEntidadeBox.novoUndelete(getIdEntidade(), o.getId());
	}

	@Override
	protected void registrarAuditoriaBloqueio(Torcedor o) {
		AuditoriaEntidadeBox.novoBloqueio(getIdEntidade(), o.getId());
	}

	@Override
	protected void registrarAuditoriaDesbloqueio(Torcedor o) {
		AuditoriaEntidadeBox.novoDesbloqueio(getIdEntidade(), o.getId());
	}

	public TorcedorSelect<?> select(Boolean excluido) {
		TorcedorSelect<?> o = new TorcedorSelect<TorcedorSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public TorcedorSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(Torcedor o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(Torcedor o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Torcedor");
		list.add("cpf;nome;email;cep;complemento;numero");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("torcedor_cpf", "O campo Cpf não pode se repetir. Já existe um registro com este valor.");
	}
}
