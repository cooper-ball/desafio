package br.cooper.ball.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import br.config.kafka.KafcaConfig;
import br.config.kafka.TorcedorKafka;
import br.cooper.ball.abstracoes.TorcedorServiceAbstract;
import br.cooper.ball.models.Torcedor;

@Component
public class TorcedorService extends TorcedorServiceAbstract {

	@Autowired KafkaTemplate<String, TorcedorKafka> kafkaTemplate;

	@Override
	protected void afterInsert(Torcedor o) {
		sendKafka(o, KafcaConfig.TORCEDOR_CADASTRADO);
	}
	
	@Override
	protected void afterDelete(Torcedor o) {
		sendKafka(o, KafcaConfig.TORCEDOR_DESLIGADO);
	}
	
    private void sendKafka(Torcedor o, String topico){
    	
    	TorcedorKafka data = new TorcedorKafka(o);
    	
        Message<TorcedorKafka> message = MessageBuilder
                .withPayload(data)
                .setHeader(KafkaHeaders.TOPIC, topico)
                .build();
        
        kafkaTemplate.send(message);
    	
    }
	
}