package br.cooper.ball.selects;

import br.cooper.ball.models.Torcedor;
import gm.utils.jpa.criterions.Criterio;
import gm.utils.jpa.select.SelectBase;
import gm.utils.jpa.select.SelectBoolean;
import gm.utils.jpa.select.SelectInteger;
import gm.utils.jpa.select.SelectString;

public class TorcedorSelect<ORIGEM> extends SelectBase<ORIGEM, Torcedor, TorcedorSelect<ORIGEM>> {

	public TorcedorSelect(ORIGEM origem, Criterio<?> criterio, String prefixo) {
		super(origem, criterio, prefixo, Torcedor.class);
	}

	@Override
	protected void beforeSelect() {
		nome().asc();
	}

	public SelectInteger<TorcedorSelect<?>> id() {
		return new SelectInteger<>(this, "id");
	}

	public SelectString<TorcedorSelect<?>> cpf() {
		return new SelectString<>(this, "cpf");
	}

	public SelectString<TorcedorSelect<?>> nome() {
		return new SelectString<>(this, "nome");
	}

	public SelectString<TorcedorSelect<?>> email() {
		return new SelectString<>(this, "email");
	}

	public SelectString<TorcedorSelect<?>> cep() {
		return new SelectString<>(this, "cep");
	}

	public SelectString<TorcedorSelect<?>> complemento() {
		return new SelectString<>(this, "complemento");
	}

	public SelectInteger<TorcedorSelect<?>> numero() {
		return new SelectInteger<>(this, "numero");
	}

	public SelectBoolean<TorcedorSelect<?>> excluido() {
		return new SelectBoolean<>(this, "excluido");
	}

	public SelectString<TorcedorSelect<?>> busca() {
		return new SelectString<>(this, "busca");
	}

	public TorcedorSelect<?> asc() {
		return id().asc();
	}
}
