package br.cooper.ball.selects;

import br.cooper.ball.models.Telefone;
import gm.utils.jpa.criterions.Criterio;
import gm.utils.jpa.select.SelectBase;
import gm.utils.jpa.select.SelectBoolean;
import gm.utils.jpa.select.SelectInteger;
import gm.utils.jpa.select.SelectString;

public class TelefoneSelect<ORIGEM> extends SelectBase<ORIGEM, Telefone, TelefoneSelect<ORIGEM>> {

	public TelefoneSelect(ORIGEM origem, Criterio<?> criterio, String prefixo) {
		super(origem, criterio, prefixo, Telefone.class);
	}

	public SelectInteger<TelefoneSelect<?>> id() {
		return new SelectInteger<>(this, "id");
	}

	public TorcedorSelect<TelefoneSelect<?>> torcedor() {
		return new TorcedorSelect<>(this, getC(), getPrefixo() + ".torcedor" );
	}

	public SelectString<TelefoneSelect<?>> numero() {
		return new SelectString<>(this, "numero");
	}

	public SelectInteger<TelefoneSelect<?>> tipo() {
		return new SelectInteger<>(this, "tipo");
	}

	public SelectBoolean<TelefoneSelect<?>> principal() {
		return new SelectBoolean<>(this, "principal");
	}

	public SelectBoolean<TelefoneSelect<?>> excluido() {
		return new SelectBoolean<>(this, "excluido");
	}

	public SelectString<TelefoneSelect<?>> busca() {
		return new SelectString<>(this, "busca");
	}

	public TelefoneSelect<?> asc() {
		return id().asc();
	}
}
