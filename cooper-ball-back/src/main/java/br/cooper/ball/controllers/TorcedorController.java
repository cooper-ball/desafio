package br.cooper.ball.controllers;

import br.cooper.ball.services.TorcedorService;
import br.cooper.infra.outros.ControllerModelo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping("/Torcedor/")
public class TorcedorController extends ControllerModelo {

	@Autowired TorcedorService torcedorService;

	@Override
	protected TorcedorService getService() {
		return torcedorService;
	}
}
