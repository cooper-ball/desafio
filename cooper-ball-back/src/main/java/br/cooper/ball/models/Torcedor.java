package br.cooper.ball.models;

import br.cooper.infra.outros.EntityModelo;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity @Table(name = "torcedor")
public class Torcedor extends EntityModelo {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 11, name = "cpf", nullable = false)
	private String cpf;

	@Column(length = 50, name = "nome", nullable = false)
	private String nome;

	@Column(length = 60, name = "email", nullable = false)
	private String email;

	@Column(length = 50, name = "cep", nullable = false)
	private String cep;

	@Column(length = 200, name = "complemento", nullable = true)
	private String complemento;

	@Column(name = "numero", nullable = false)
	private Integer numero;

	@Column(name = "excluido", nullable = false)
	private Boolean excluido;

	@Column(length = 500, name = "busca", nullable = false)
	private String busca;

	@Override
	public Boolean getRegistroBloqueado() {
		return false;
	}

	@Override
	public void setRegistroBloqueado(Boolean value) {}

	@Override
	public Torcedor getOld() {
		return (Torcedor) super.getOld();
	}
}
