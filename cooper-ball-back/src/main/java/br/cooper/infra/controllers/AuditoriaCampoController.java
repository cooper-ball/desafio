package br.cooper.infra.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.cooper.infra.outros.ControllerModelo;
import br.cooper.infra.services.AuditoriaCampoService;

@RestController @RequestMapping("/AuditoriaCampo/")
public class AuditoriaCampoController extends ControllerModelo {

	@Autowired AuditoriaCampoService auditoriaCampoService;

	@Override
	protected AuditoriaCampoService getService() {
		return auditoriaCampoService;
	}
}
