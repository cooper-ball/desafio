package br.cooper.infra.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.cooper.infra.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity @Table(name = "campo")
public class Campo extends EntityModelo {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "entidade", nullable = false)
	private Entidade entidade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo", nullable = false)
	private Entidade tipo;

	@Column(length = 100, name = "nome", nullable = false)
	private String nome;

	@Column(length = 50, name = "nomenobanco", nullable = false)
	private String nomeNoBanco;

	@Column(name = "excluido", nullable = false)
	private Boolean excluido;

	@Column(name = "registrobloqueado", nullable = false)
	private Boolean registroBloqueado;

	@Column(length = 500, name = "busca", nullable = false)
	private String busca;

	@Override
	public Campo getOld() {
		return (Campo) super.getOld();
	}
}
