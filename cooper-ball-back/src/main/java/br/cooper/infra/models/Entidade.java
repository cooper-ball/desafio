package br.cooper.infra.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.cooper.infra.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity @Table(name = "entidade")
public class Entidade extends EntityModelo {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 100, name = "nome", nullable = false)
	private String nome;

	@Column(length = 100, name = "nomeclasse", nullable = false)
	private String nomeClasse;

	@Column(name = "primitivo", nullable = false)
	private Boolean primitivo;

	@Column(name = "excluido", nullable = false)
	private Boolean excluido;

	@Column(name = "registrobloqueado", nullable = false)
	private Boolean registroBloqueado;

	@Column(length = 500, name = "busca", nullable = false)
	private String busca;

	@Override
	public Entidade getOld() {
		return (Entidade) super.getOld();
	}
}
