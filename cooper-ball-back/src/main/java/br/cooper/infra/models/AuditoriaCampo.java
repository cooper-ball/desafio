package br.cooper.infra.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.cooper.infra.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity @Table(name = "auditoriacampo")
public class AuditoriaCampo extends EntityModelo {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "auditoriaentidade", nullable = false)
	private AuditoriaEntidade auditoriaEntidade;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "campo", nullable = false)
	private Campo campo;

	@Column(length = 8000, name = "de", nullable = true)
	private String de;

	@Column(length = 8000, name = "para", nullable = true)
	private String para;

	@Column(name = "excluido", nullable = false)
	private Boolean excluido;

	@Column(name = "registrobloqueado", nullable = false)
	private Boolean registroBloqueado;

	@Column(length = 500, name = "busca", nullable = false)
	private String busca;

	@Override
	public AuditoriaCampo getOld() {
		return (AuditoriaCampo) super.getOld();
	}
}
