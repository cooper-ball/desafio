package br.cooper.infra.models;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.cooper.infra.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity @Table(name = "login")
public class Login extends EntityModelo {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario", nullable = false)
	private Usuario usuario;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data", nullable = false)
	private Calendar data;

	@Column(length = 50, name = "token", nullable = false)
	private String token;

	@Override
	public Boolean getExcluido() {
		return false;
	}

	@Override
	public void setExcluido(Boolean value) {}

	@Override
	public Boolean getRegistroBloqueado() {
		return false;
	}

	@Override
	public void setRegistroBloqueado(Boolean value) {}

	@Override
	public Login getOld() {
		return (Login) super.getOld();
	}
}
