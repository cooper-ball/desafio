package br.cooper.infra.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.cooper.infra.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity @Table(name = "consultaoperador")
public class ConsultaOperador extends EntityModelo {

	@Id
	@Column(name = "id", nullable = false)
	private Integer id;

	@Column(length = 20, name = "nome", nullable = false)
	private String nome;

	@Column(name = "excluido", nullable = false)
	private Boolean excluido;

	@Column(name = "registrobloqueado", nullable = false)
	private Boolean registroBloqueado;

	@Column(length = 500, name = "busca", nullable = false)
	private String busca;

	@Override
	public ConsultaOperador getOld() {
		return (ConsultaOperador) super.getOld();
	}
}
