package br.cooper.infra.services;

import org.springframework.stereotype.Component;

import br.cooper.infra.abstracoes.AuditoriaEntidadeServiceAbstract;

@Component
public class AuditoriaEntidadeService extends AuditoriaEntidadeServiceAbstract {}
