package br.cooper.infra.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cooper.infra.models.Campo;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.CampoSelect;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;

@Component
public class CampoService extends ServiceModelo<Campo> {

	@Autowired EntidadeService entidadeService;

	@Override
	public Class<Campo> getClasse() {
		return Campo.class;
	}

	@Override
	public MapSO toMap(Campo o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected Campo fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Campo o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setEntidade(find(entidadeService, mp, "entidade"));
		o.setNome(mp.getString("nome"));
		o.setNomeNoBanco(mp.getString("nomeNoBanco"));
		o.setTipo(find(entidadeService, mp, "tipo"));
		return o;
	}

	@Override
	public Campo newO() {
		Campo o = new Campo();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(Campo o) {
		if (o.getEntidade() == null) {
			throw new MessageException("O campo Campo > Entidade é obrigatório");
		}
		o.setNome(tratarString(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Campo > Nome é obrigatório");
		}
		if (StringLength.get(o.getNome()) > 100) {
			throw new MessageException("O campo Campo > Nome aceita no máximo 100 caracteres");
		}
		o.setNomeNoBanco(tratarString(o.getNomeNoBanco()));
		if (o.getNomeNoBanco() == null) {
			throw new MessageException("O campo Campo > Nome No Banco é obrigatório");
		}
		if (StringLength.get(o.getNomeNoBanco()) > 50) {
			throw new MessageException("O campo Campo > Nome No Banco aceita no máximo 50 caracteres");
		}
		if (o.getTipo() == null) {
			throw new MessageException("O campo Campo > Tipo é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueNomeNoBancoEntidade(o);
		}
	}

	public void validarUniqueNomeNoBancoEntidade(Campo o) {
		CampoSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.nomeNoBanco().eq(o.getNomeNoBanco());
		select.entidade().eq(o.getEntidade());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("campo_nomenobanco_entidade"));
		}
	}

	@Transactional
	public Campo bloqueiaRegistro(Campo o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Campo> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		CampoSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Campo> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public CampoSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		CampoSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.fk(params, "entidade", select.entidade());
		FiltroConsulta.string(params, "nome", select.nome());
		FiltroConsulta.string(params, "nomeNoBanco", select.nomeNoBanco());
		FiltroConsulta.fk(params, "tipo", select.tipo());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected Campo buscaUnicoObrig(MapSO params) {
		CampoSelect<?> select = selectBase(null, null, params);
		Campo o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Campo com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected Campo setOld(Campo o) {
		Campo old = newO();
		old.setEntidade(o.getEntidade());
		old.setNome(o.getNome());
		old.setNomeNoBanco(o.getNomeNoBanco());
		old.setTipo(o.getTipo());
		o.setOld(old);
		return o;
	}

	public CampoSelect<?> select(Boolean excluido) {
		CampoSelect<?> o = new CampoSelect<CampoSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public CampoSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(Campo o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(Campo o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Campo");
		list.add("entidade;tipo;nome;nomeNoBanco");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("campo_nomenobanco_entidade", "A combinação de campos Nome No Banco + Entidade não pode se repetir. Já existe um registro com esta combinação.");
	}
}
