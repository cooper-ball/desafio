package br.cooper.infra.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import br.cooper.infra.abstracoes.EntidadeServiceAbstract;
import br.cooper.infra.models.Entidade;

@Component
public class EntidadeService extends EntidadeServiceAbstract {

	private static Map<String, Integer> ids = new HashMap<>();

	public int getId(Class<?> classe) {
		String nomeClasse = classe.getSimpleName();
		Integer id = EntidadeService.ids.get(nomeClasse);
		if (id == null) {
			Entidade o = select(null).nomeClasse().eq(nomeClasse).unique();
			if (o == null) {
				throw new RuntimeException("Nao encontrado: " + nomeClasse);
			}
			id = o.getId();
			EntidadeService.ids.put(nomeClasse, id);
		}
		return id;
	}
	
	
	public Entidade get(Class<?> classe) {
		return find(getId(classe));
	}

}
