package br.cooper.infra.services;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import br.cooper.infra.models.TipoAuditoriaEntidade;
import br.cooper.infra.outros.ServiceModelo;
import gm.utils.comum.Lst;
import gm.utils.exception.MessageException;
import gm.utils.map.MapSO;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringCompare;
import src.commom.utils.string.StringEmpty;

@Component
public class TipoAuditoriaEntidadeService extends ServiceModelo<TipoAuditoriaEntidade> {

	public static final int INCLUSAO = 1;

	public static final int ALTERACAO = 2;

	public static final int EXCLUSAO = 4;

	public static final int EXECUCAO = 3;

	public static final int RECUPERACAO = 5;

	public static final int BLOQUEIO = 6;

	public static final int DESBLOQUEIO = 7;

	public static final Map<Integer, TipoAuditoriaEntidade> MAP = new HashMap<>();

	@Override
	public Class<TipoAuditoriaEntidade> getClasse() {
		return TipoAuditoriaEntidade.class;
	}

	@Override
	public MapSO toMap(TipoAuditoriaEntidade o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected TipoAuditoriaEntidade fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		if (id == null || id < 1) {
			return null;
		} else {
			return find(id);
		}
	}

	@Override
	protected final void validar(TipoAuditoriaEntidade o) {
		throw new MessageException("Nao deveria cair aqui pois o model estah anotado com @SemTabela");
	}

	@Transactional
	public TipoAuditoriaEntidade bloqueiaRegistro(TipoAuditoriaEntidade o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected TipoAuditoriaEntidade buscaUnicoObrig(MapSO params) {
		String nome = params.getString("nome");
		Lst<TipoAuditoriaEntidade> list = new Lst<TipoAuditoriaEntidade>();
		list.addAll(MAP.values());
		TipoAuditoriaEntidade o = list.unique(item -> {
			if (!StringEmpty.is(nome) && !StringCompare.eq(item.getNome(), nome)) {
				return false;
			}
			return true;
		});
		if (o == null) {
			throw new MessageException("Não foi encontrado um TipoAuditoriaEntidade com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	static {
		MAP.put(INCLUSAO, new TipoAuditoriaEntidade(INCLUSAO, "Inclusão"));
		MAP.put(ALTERACAO, new TipoAuditoriaEntidade(ALTERACAO, "Alteração"));
		MAP.put(EXCLUSAO, new TipoAuditoriaEntidade(EXCLUSAO, "Exclusão"));
		MAP.put(EXECUCAO, new TipoAuditoriaEntidade(EXECUCAO, "Execução"));
		MAP.put(RECUPERACAO, new TipoAuditoriaEntidade(RECUPERACAO, "Recuperação"));
		MAP.put(BLOQUEIO, new TipoAuditoriaEntidade(BLOQUEIO, "Bloqueio"));
		MAP.put(DESBLOQUEIO, new TipoAuditoriaEntidade(DESBLOQUEIO, "Desbloqueio"));
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	public String getText(TipoAuditoriaEntidade o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public TipoAuditoriaEntidade findNotObrig(int id) {
		return MAP.get(id);
	}
}
