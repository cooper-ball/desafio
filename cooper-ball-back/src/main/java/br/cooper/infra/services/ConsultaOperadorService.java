package br.cooper.infra.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import br.cooper.infra.models.ConsultaOperador;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.ConsultaOperadorSelect;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;

@Component
public class ConsultaOperadorService extends ServiceModelo<ConsultaOperador> {

	public static final int IGUAL = 1;

	public static final int COMECA_COM = 2;

	public static final int CONTEM = 3;

	public static final int TERMINA_COM = 4;

	public static final int MAIOR_OU_IGUAL = 5;

	public static final int MENOR_OU_IGUAL = 6;

	public static final int ENTRE = 7;

	public static final int EM = 8;

	public static final int HOJE = 10;

	public static final int HOJE_MENOS = 11;

	public static final int HOJE_MAIS = 12;

	public static final int VAZIOS = 13;

	public static final int MAIS_OPCOES = 15;

	public static final int DESMEMBRAR = 16;

	public static final int SIM = 17;

	public static final int NAO = 18;

	public static final int DIFERENTE = 1001;

	public static final int NAO_COMECA_COM = 1002;

	public static final int NAO_CONTEM = 1003;

	public static final int NAO_TERMINA_COM = 1004;

	public static final int MENOR_QUE = 1005;

	public static final int MAIOR_QUE = 1006;

	public static final int NAO_ENTRE = 1007;

	public static final int NAO_EM = 1008;

	public static final int NAO_VAZIOS = 1013;

	public static final int TODOS = 9999;

	@Override
	public Class<ConsultaOperador> getClasse() {
		return ConsultaOperador.class;
	}

	@Override
	public MapSO toMap(ConsultaOperador o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getNome() != null) {
			map.put("nome", o.getNome());
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected ConsultaOperador fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		ConsultaOperador o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setNome(mp.getString("nome"));
		return o;
	}

	@Override
	public ConsultaOperador newO() {
		ConsultaOperador o = new ConsultaOperador();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(ConsultaOperador o) {
		throw new MessageException("Nao deveria cair aqui pois o model estah anotado com @Status");
	}

	@Transactional
	public ConsultaOperador bloqueiaRegistro(ConsultaOperador o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	public ResultadoConsulta consulta(MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, ConsultaOperador> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		ConsultaOperadorSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<ConsultaOperador> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public ConsultaOperadorSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		ConsultaOperadorSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.string(params, "nome", select.nome());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected ConsultaOperador buscaUnicoObrig(MapSO params) {
		ConsultaOperadorSelect<?> select = selectBase(null, null, params);
		ConsultaOperador o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um ConsultaOperador com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected ConsultaOperador setOld(ConsultaOperador o) {
		ConsultaOperador old = newO();
		old.setNome(o.getNome());
		o.setOld(old);
		return o;
	}

	public ConsultaOperadorSelect<?> select(Boolean excluido) {
		ConsultaOperadorSelect<?> o = new ConsultaOperadorSelect<ConsultaOperadorSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		o.id().asc();
		return o;
	}

	public ConsultaOperadorSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(ConsultaOperador o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(ConsultaOperador o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("ConsultaOperador");
		list.add("nome");
		return list;
	}
}
