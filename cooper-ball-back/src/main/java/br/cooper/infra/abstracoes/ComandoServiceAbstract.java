package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import br.cooper.infra.models.Comando;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.ComandoSelect;
import br.cooper.infra.services.EntidadeService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;

public abstract class ComandoServiceAbstract extends ServiceModelo<Comando> {

	@Autowired protected EntidadeService entidadeService;

	@Override
	public Class<Comando> getClasse() {
		return Comando.class;
	}

	@Override
	public MapSO toMap(Comando o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected Comando fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Comando o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setEntidade(find(entidadeService, mp, "entidade"));
		o.setNome(mp.getString("nome"));
		return o;
	}

	@Override
	public Comando newO() {
		Comando o = new Comando();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(Comando o) {
		if (o.getEntidade() == null) {
			throw new MessageException("O campo Comando > Entidade é obrigatório");
		}
		o.setNome(tratarString(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Comando > Nome é obrigatório");
		}
		if (StringLength.get(o.getNome()) > 50) {
			throw new MessageException("O campo Comando > Nome aceita no máximo 50 caracteres");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueNomeEntidade(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueNomeEntidade(Comando o) {
		ComandoSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.nome().eq(o.getNome());
		select.entidade().eq(o.getEntidade());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("comando_nome_entidade"));
		}
	}

	protected void validar2(Comando o) {}

	protected void validar3(Comando o) {}

	@Transactional
	public Comando bloqueiaRegistro(Comando o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Comando> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		ComandoSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Comando> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public ComandoSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		ComandoSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.fk(params, "entidade", select.entidade());
		FiltroConsulta.string(params, "nome", select.nome());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected Comando buscaUnicoObrig(MapSO params) {
		ComandoSelect<?> select = selectBase(null, null, params);
		Comando o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Comando com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected Comando setOld(Comando o) {
		Comando old = newO();
		old.setEntidade(o.getEntidade());
		old.setNome(o.getNome());
		o.setOld(old);
		return o;
	}

	public ComandoSelect<?> select(Boolean excluido) {
		ComandoSelect<?> o = new ComandoSelect<ComandoSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public ComandoSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(Comando o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(Comando o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Comando");
		list.add("entidade;nome");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("comando_nome_entidade", "A combinação de campos Nome + Entidade não pode se repetir. Já existe um registro com esta combinação.");
	}
}
