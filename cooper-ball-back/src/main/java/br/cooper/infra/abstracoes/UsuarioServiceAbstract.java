package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import br.cooper.infra.models.Usuario;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.UsuarioSelect;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;
import src.commom.utils.string.StringParse;

public abstract class UsuarioServiceAbstract extends ServiceModelo<Usuario> {

	@Override
	public Class<Usuario> getClasse() {
		return Usuario.class;
	}

	@Override
	public MapSO toMap(Usuario o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getLogin() != null) {
			map.put("login", StringParse.get(o.getLogin()));
		}
		if (o.getNome() != null) {
			map.put("nome", o.getNome());
		}
		if (o.getSenha() != null) {
			map.put("senha", o.getSenha());
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected Usuario fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		Usuario o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setLogin(mp.getString("login"));
		o.setNome(mp.getString("nome"));
		o.setSenha(mp.getString("senha"));
		return o;
	}

	@Override
	public Usuario newO() {
		Usuario o = new Usuario();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(Usuario o) {
		o.setLogin(tratarEmail(o.getLogin()));
		if (o.getLogin() == null) {
			throw new MessageException("O campo Usuário > Login é obrigatório");
		}
		if (StringLength.get(o.getLogin()) > 50) {
			throw new MessageException("O campo Usuário > Login aceita no máximo 50 caracteres");
		}
		o.setNome(tratarString(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Usuário > Nome é obrigatório");
		}
		if (StringLength.get(o.getNome()) > 60) {
			throw new MessageException("O campo Usuário > Nome aceita no máximo 60 caracteres");
		}
		o.setSenha(tratarString(o.getSenha()));
		if (o.getSenha() == null) {
			throw new MessageException("O campo Usuário > Senha é obrigatório");
		}
		if (StringLength.get(o.getSenha()) > 32) {
			throw new MessageException("O campo Usuário > Senha aceita no máximo 32 caracteres");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueLogin(o);
			validarUniqueNome(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueLogin(Usuario o) {
		UsuarioSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.login().eq(o.getLogin());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("usuario_login"));
		}
	}

	public void validarUniqueNome(Usuario o) {
		UsuarioSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.nome().eq(o.getNome());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("usuario_nome"));
		}
	}

	protected void validar2(Usuario o) {}

	protected void validar3(Usuario o) {}

	@Transactional
	public Usuario bloqueiaRegistro(Usuario o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	public ResultadoConsulta consulta(MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, Usuario> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		UsuarioSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<Usuario> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public UsuarioSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		UsuarioSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.email(params, "login", select.login());
		FiltroConsulta.string(params, "nome", select.nome());
		FiltroConsulta.senha(params, "senha", select.senha());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected Usuario buscaUnicoObrig(MapSO params) {
		UsuarioSelect<?> select = selectBase(null, null, params);
		Usuario o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um Usuario com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected Usuario setOld(Usuario o) {
		Usuario old = newO();
		old.setLogin(o.getLogin());
		old.setNome(o.getNome());
		old.setSenha(o.getSenha());
		o.setOld(old);
		return o;
	}

	public UsuarioSelect<?> select(Boolean excluido) {
		UsuarioSelect<?> o = new UsuarioSelect<UsuarioSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public UsuarioSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(Usuario o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(Usuario o) {
		if (Null.is(o)) {
			return null;
		}
		return o.getNome();
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Usuario");
		list.add("nome;login;senha");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("usuario_login", "O campo Login não pode se repetir. Já existe um registro com este valor.");
		CONSTRAINTS_MESSAGES.put("usuario_nome", "O campo Nome não pode se repetir. Já existe um registro com este valor.");
	}
}
