package br.cooper.infra.abstracoes;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import br.cooper.infra.models.AuditoriaCampo;
import br.cooper.infra.outros.FiltroConsulta;
import br.cooper.infra.outros.ResultadoConsulta;
import br.cooper.infra.outros.ServiceModelo;
import br.cooper.infra.selects.AuditoriaCampoSelect;
import br.cooper.infra.services.AuditoriaEntidadeService;
import br.cooper.infra.services.CampoService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import src.commom.utils.object.Null;
import src.commom.utils.string.StringEmpty;
import src.commom.utils.string.StringLength;

public abstract class AuditoriaCampoServiceAbstract extends ServiceModelo<AuditoriaCampo> {

	@Autowired protected CampoService campoService;
	@Autowired protected AuditoriaEntidadeService auditoriaEntidadeService;

	@Override
	public Class<AuditoriaCampo> getClasse() {
		return AuditoriaCampo.class;
	}

	@Override
	public MapSO toMap(AuditoriaCampo o, boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}

	@Override
	protected AuditoriaCampo fromMap(MapSO map) {
		MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		AuditoriaCampo o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setAuditoriaEntidade(find(auditoriaEntidadeService, mp, "auditoriaEntidade"));
		o.setCampo(find(campoService, mp, "campo"));
		o.setDe(mp.getString("de"));
		o.setPara(mp.getString("para"));
		return o;
	}

	@Override
	public AuditoriaCampo newO() {
		AuditoriaCampo o = new AuditoriaCampo();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}

	@Override
	protected final void validar(AuditoriaCampo o) {
		if (o.getAuditoriaEntidade() == null) {
			throw new MessageException("O campo Auditoria Campo > Auditoria Entidade é obrigatório");
		}
		if (o.getCampo() == null) {
			throw new MessageException("O campo Auditoria Campo > Campo é obrigatório");
		}
		o.setDe(tratarString(o.getDe()));
		if (StringLength.get(o.getDe()) > 8000) {
			throw new MessageException("O campo Auditoria Campo > De aceita no máximo 8000 caracteres");
		}
		o.setPara(tratarString(o.getPara()));
		if (StringLength.get(o.getPara()) > 8000) {
			throw new MessageException("O campo Auditoria Campo > Para aceita no máximo 8000 caracteres");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueCampoAuditoriaEntidade(o);
		}
		validar2(o);
		validar3(o);
	}

	public void validarUniqueCampoAuditoriaEntidade(AuditoriaCampo o) {
		AuditoriaCampoSelect<?> select = select();
		if (o.getId() != null) {
			select.ne(o);
		}
		select.campo().eq(o.getCampo());
		select.auditoriaEntidade().eq(o.getAuditoriaEntidade());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("auditoriacampo_campo_auditoriaentidade"));
		}
	}

	protected void validar2(AuditoriaCampo o) {}

	protected void validar3(AuditoriaCampo o) {}

	@Transactional
	public AuditoriaCampo bloqueiaRegistro(AuditoriaCampo o) {
		o.setRegistroBloqueado(true);
		o = merge(o);
		return o;
	}

	@Override
	protected ResultadoConsulta consultaBase(Integer pagina, List<Integer> ignorar, String busca, MapSO params, FTT<MapSO, AuditoriaCampo> func) {
		ResultadoConsulta result = new ResultadoConsulta();
		Integer registrosPorPagina = params.getInt("registrosPorPagina");
		if (registrosPorPagina == null) {
			registrosPorPagina = 30;
		}
		AuditoriaCampoSelect<?> select = selectBase(ignorar, busca, params);
		if (pagina == null) {
			result.pagina = 1;
			result.registros = select.count();
			result.paginas = result.registros / registrosPorPagina;
			if (result.registros > result.paginas * registrosPorPagina) {
				result.paginas++;
			}
		} else {
			select.page(pagina);
			result.pagina = pagina;
		}
		select.limit(registrosPorPagina);
		Lst<AuditoriaCampo> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}

	public AuditoriaCampoSelect<?> selectBase(List<Integer> ignorar, String busca, MapSO params) {
		AuditoriaCampoSelect<?> select = select(null);
		if (!StringEmpty.is(busca)) {
			select.busca().like(busca);
		}
		if (!UList.isEmpty(ignorar)) {
			select.id().notIn(ignorar);
		}
		FiltroConsulta.fk(params, "auditoriaEntidade", select.auditoriaEntidade());
		FiltroConsulta.fk(params, "campo", select.campo());
		FiltroConsulta.string(params, "de", select.de());
		FiltroConsulta.string(params, "para", select.para());
		FiltroConsulta.bool(params, "excluido", select.excluido());
		FiltroConsulta.bool(params, "registroBloqueado", select.registroBloqueado());
		return select;
	}

	@Override
	protected AuditoriaCampo buscaUnicoObrig(MapSO params) {
		AuditoriaCampoSelect<?> select = selectBase(null, null, params);
		AuditoriaCampo o = select.unique();
		if (o == null) {
			throw new MessageException("Não foi encontrado um AuditoriaCampo com os seguintes critérios:" + params);
		} else {
			return o;
		}
	}

	@Override
	public boolean auditar() {
		return false;
	}

	@Override
	protected AuditoriaCampo setOld(AuditoriaCampo o) {
		AuditoriaCampo old = newO();
		old.setAuditoriaEntidade(o.getAuditoriaEntidade());
		old.setCampo(o.getCampo());
		old.setDe(o.getDe());
		old.setPara(o.getPara());
		o.setOld(old);
		return o;
	}

	public AuditoriaCampoSelect<?> select(Boolean excluido) {
		AuditoriaCampoSelect<?> o = new AuditoriaCampoSelect<AuditoriaCampoSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}

	public AuditoriaCampoSelect<?> select() {
		return select(false);
	}

	@Override
	protected void setBusca(AuditoriaCampo o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}

	@Override
	public String getText(AuditoriaCampo o) {
		if (Null.is(o)) {
			return null;
		}
		return auditoriaEntidadeService.getText(o.getAuditoriaEntidade());
	}

	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("AuditoriaCampo");
		list.add("auditoriaEntidade;campo;de;para");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("auditoriacampo_campo_auditoriaentidade", "A combinação de campos Campo + Auditoria Entidade não pode se repetir. Já existe um registro com esta combinação.");
	}
}
