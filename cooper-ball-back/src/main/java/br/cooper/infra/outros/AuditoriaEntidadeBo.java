package br.cooper.infra.outros;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cooper.infra.models.AuditoriaEntidade;
import br.cooper.infra.models.AuditoriaTransacao;
import br.cooper.infra.models.Entidade;
import br.cooper.infra.models.Login;
import br.cooper.infra.selects.AuditoriaEntidadeSelect;
import br.cooper.infra.services.AuditoriaCampoService;
import br.cooper.infra.services.AuditoriaEntidadeService;
import br.cooper.infra.services.AuditoriaTransacaoService;
import br.cooper.infra.services.CampoService;
import br.cooper.infra.services.EntidadeService;
import br.cooper.infra.services.TipoAuditoriaEntidadeService;
import gm.utils.comum.Lst;
import gm.utils.map.MapSO;

@Component
public class AuditoriaEntidadeBo {

	@Autowired CampoService campoService;
	@Autowired EntidadeService entidadeService;
	@Autowired AuditoriaCampoService auditoriaCampoService;
	@Autowired AuditoriaEntidadeService auditoriaEntidadeService;
	@Autowired AuditoriaTransacaoService auditoriaTransacaoService;
	@Autowired TipoAuditoriaEntidadeService tipoLogEntidadeService;

	@Transactional
	public void registrar() {

		List<AuditoriaEntidadeBox> auditorias = ThreadScope.getAuditorias();

		if (auditorias.isEmpty()) {
			return;
		}

		auditorias.removeIf(o -> o.getTipo() == TipoAuditoriaEntidadeService.ALTERACAO && o.getCampos().isEmpty());

		if (auditorias.isEmpty()) {
			return;
		}

		AuditoriaTransacao transacao = auditoriaTransacaoService.criarTransacao();

		MapSO contadorOperacoes = new MapSO();

		for (AuditoriaEntidadeBox box : auditorias) {
			AuditoriaEntidade o = auditoriaEntidadeService.newO();
			o.setTransacao(transacao);
			o.setEntidade(entidadeService.find(box.getEntidade()));
			o.setRegistro(box.getRegistro());
			o.setTipo(box.getTipo());
			String key = box.getEntidade() + "-" + box.getTipo() + "-" + box.getRegistro();
			Integer numeroDaOperacao = contadorOperacoes.get(key);
			if (numeroDaOperacao == null) {
				numeroDaOperacao = 1;
			} else {
				numeroDaOperacao++;
			}
			contadorOperacoes.put(key, numeroDaOperacao);
			o.setNumeroDaOperacao(numeroDaOperacao);
			o = auditoriaEntidadeService.insertSemAuditoria(o);
			auditoriaCampoService.registrar(o, box.getCampos());
		}

		ThreadScope.clear();

	}

	public Lst<AuditoriaEntidade> list(Entidade entidade, int registro) {
		return auditoriaEntidadeService.select().entidade().eq(entidade).registro().eq(registro).list();
	}

	@Transactional
	public int getLoginInsert(int entidade, int registro) {
		AuditoriaEntidadeSelect<?> select = auditoriaEntidadeService.select();
		select.entidade().id().eq(entidade);
		select.registro().eq(registro);
		select.tipo().eq(TipoAuditoriaEntidadeService.INCLUSAO);
		AuditoriaEntidade auditoriaEntidade = select.unique();
		Login login = auditoriaEntidade.getTransacao().getLogin();
		return login.getId();
	}

}
