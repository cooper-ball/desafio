package br.cooper.infra.outros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import br.cooper.infra.models.AuditoriaEntidade;
import br.cooper.infra.models.Entidade;
import br.cooper.infra.models.TipoAuditoriaEntidade;
import br.cooper.infra.services.AuditoriaTransacaoService;
import br.cooper.infra.services.ComandoService;
import br.cooper.infra.services.EntidadeService;
import br.cooper.infra.services.LoginService;
import br.cooper.infra.services.TipoAuditoriaEntidadeService;
import gm.utils.comum.Lst;
import gm.utils.comum.UList;
import gm.utils.date.Data;
import gm.utils.map.MapSO;

public abstract class ControllerModelo {

	@Autowired protected ComandoService comandoService;
	@Autowired protected EntidadeService entidadeService;
	@Autowired protected LoginService loginService;
	@Autowired protected AuditoriaTransacaoService auditoriaTransacaoService;
	@Autowired protected AuditoriaEntidadeBo auditoriaEntidadeBo;
	@Autowired protected TipoAuditoriaEntidadeService tipoAuditoriaEntidadeService;
	protected abstract ServiceModelo<? extends EntityModelo> getService();

	@ResponseBody
	@PostMapping("/save")
	public ResponseEntity<Object> save(@RequestBody MapSO map) {
		return UController.ok(() -> getService().saveAndMap(map));
	}

	@ResponseBody
	@PostMapping("/edit")
	public ResponseEntity<Object> edit(@RequestBody MapSO map) {
		return UController.ok(() -> getService().toMap(map.id(), true));
	}

	public int getIdEntidade() {
		return getService().getIdEntidade();
	}

	public Entidade getEntidade() {
		return entidadeService.find(getIdEntidade());
	}

	@ResponseBody
	@PostMapping("/consulta")
	public ResponseEntity<Object> consulta(@RequestBody MapSO map) {
		return UController.ok(() -> getService().consulta(map));
	}

	@ResponseBody
	@PostMapping("/consulta-select")
	public ResponseEntity<Object> consultaSelect(@RequestBody MapSO map) {
		return UController.ok(() -> {
			map.put("busca", map.getString("text"));
			return getService().consultaSelect(map);
		});
	}

	@ResponseBody
	@PostMapping("/get-auditoria")
	public ResponseEntity<Object> logs(@RequestBody MapSO map) {
		return UController.ok(() -> {
			Lst<AuditoriaEntidade> list = auditoriaEntidadeBo.list(getEntidade(), map.id());
			List<MapSO> lst = UList.map(list, o -> {
				MapSO mp = new MapSO();
				TipoAuditoriaEntidade tipo = tipoAuditoriaEntidadeService.find(o.getTipo());
				mp.put("id", o.getId());
				mp.put("idTipo", tipo.getId());
				mp.put("tipo", tipo.getNome());
				mp.put("usuario", o.getTransacao().getLogin().getUsuario().getNome());
				mp.put("data", new Data(o.getTransacao().getData()).format_dd_mm_yyyy_hh_mm_ss());
				mp.put("tempo", o.getTransacao().getTempo());
				return mp;
			});
			return lst;
		});
	}
	
	@ResponseBody
	@PostMapping("/delete")
	public ResponseEntity<Object> delete(@RequestBody MapSO map) {
		return UController.ok(() -> {
			getService().delete(map.id());
			return new MapSO().add("message", "Registro Excluído com Sucesso!");
		});
	}

}
