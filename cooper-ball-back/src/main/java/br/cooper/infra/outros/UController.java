package br.cooper.infra.outros;

import org.springframework.http.ResponseEntity;

import gm.utils.exception.MessageException;
import gm.utils.exception.UException;
import gm.utils.lambda.FT;
import gm.utils.map.MapSO;

public abstract class UController {

	public static final ResponseEntity<Object> SUCESSO = message(200, "sucesso");
	
	@SuppressWarnings("unchecked")
	public static ResponseEntity<Object> ok(FT<Object> func) {
		try {
			long time = System.currentTimeMillis();
			Object o = func.call();
			if (ThreadScope.get() != null && ThreadScope.getDataHora() > 0 && ThreadScope.getDataHora() < time) {
				ThreadScope.setDataHora(time);
			}
			ThreadScope.finalizarComSucesso();
			if (o instanceof ResponseEntity) {
				return (ResponseEntity<Object>) o;
			} else {
				return ResponseEntity.ok(o);
			}
		} catch (Exception e) {

			if (e.getCause() instanceof MessageException) {
				e = (Exception) e.getCause();
			}

			ThreadScope.dispose();
			if (e instanceof MessageException) {
				return message(401, e.getMessage());
			} else {
				UException.trata(e).printStackTrace();
				return erro500("Ocorreu um erro no Servidor! Contacte a área de desenvolvimento.");
			}
		}
	}

	public static ResponseEntity<Object> erro500(String message) {
		return message(500, message);
	}

	public static ResponseEntity<Object> message(int code, String message) {
		return ResponseEntity.status(code).body(new MapSO().add("message", message));
	}

}
