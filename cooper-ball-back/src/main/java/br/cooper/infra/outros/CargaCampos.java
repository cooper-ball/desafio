package br.cooper.infra.outros;

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cooper.infra.models.Campo;
import br.cooper.infra.models.Entidade;
import br.cooper.infra.services.CampoService;
import br.cooper.infra.services.EntidadeService;
import gm.utils.classes.UClass;
import gm.utils.reflection.Atributo;
import gm.utils.reflection.Atributos;
import gm.utils.reflection.ListAtributos;

@Component
public class CargaCampos {
	
	private static final Map<Integer, Map<String, Integer>> IDS = new HashedMap<>();

	@Autowired CampoService campoService;
	@Autowired EntidadeService entidadeService;

	public void exec(Entidade e, Class<?> classe) {
		
		Map<String, Integer> map = new HashedMap<>();
		
		IDS.put(e.getId(), map);
		
		Atributos as = ListAtributos.persist(classe);
		
		for (Atributo a : as) {
			
			Campo o = campoService.select().entidade().eq(e).nomeNoBanco().eq(a.nome()).unique();
			
			if (o == null) {
				o = campoService.newO();
				o.setNome(a.getTitulo());
				o.setNomeNoBanco(a.nome());
				o.setEntidade(e);
				o.setTipo(entidadeService.get(UClass.getClass(a.getType())));
				o = campoService.insertSemAuditoria(o);
			}
			
			map.put(a.nome(), o.getId());
			
		}
		
	}
	
	public static int getId(int entidade, String campo) {
		return IDS.get(entidade).get(campo);
	}
	
}