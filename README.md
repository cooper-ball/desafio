
## Instruções
Executar os comandos a partir do diretorio raiz

### dar permissoes para instalacao
```shell
chmod +x install.sh
```

### executar instalacao para compilar os projetos e rodar o Dockerfile
```shell
./install.sh
```

### Subir o ambiente
'--remove-orphans': garante que vai remover todos os containers

```shell
docker-compose up -d --remove-orphans
```

caso não suba
```shell
 docker-compose up --force-recreate
```

### Acesse a aplicação
Abra o browser e acesse o endereço http://localhost/

### Baixar o **ambiente**
```shell
docker-compose down --remove-orphans
```

### Atencao
O front estah preparado para rodar na porta 80

Se houver algum servico rodando nesta porta deverah ser stopado ou devera configurar o compose para outra porta

Se houver algum postgres instalado na maquina este deve ser derrubado antes de iniciar este processo
```shell
systemctl stop postgresql
```

### Autenticacao
user: user@gmail.com

pass: 123456